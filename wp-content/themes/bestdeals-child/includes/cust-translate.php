<?php

function start_modify_html() {
ob_start();
}

function end_modify_html()
{
    $html = ob_get_clean();
    $html = str_replace('<input type="submit" class="sc_button_size_medium" name="property_search" value="Search">', '<input type="submit" class="sc_button_size_medium" name="property_search" value="Chercher">', $html);
    $html = str_replace('AxiomThemes', 'Powered by imust.be - immorichel', $html);
    $html = str_replace('<a href="http://bestdeals.axiomthemes.com/privacy-policy/">Privacy Policy</a>', '<a href="/privacy-policy/">Privacy Policy</a>', $html);


    $html = str_replace('<aside id="bestdeals_widget_flickr-2" class="widget_number_5 widget widget_flickr"><div class="widget_number_5 widget_bg"><h5 class="widget_title_">Flickr</h5>		<div class="flickr_images">
            <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=random&amp;flickr_display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155%40N08"></script>
        </div>

    </div></aside>', ' ', $html);
    $html = str_replace('<h5 class="widget_title_">Recent posts</h5>', '<h5 class="widget_title_">Articles récents</h5>', $html);
    $html = str_replace('<h5 class="widget_title_">Categories</h5>', '<h5 class="widget_title_">Catégories</h5>', $html);
    $html = str_replace('<h5 class="widget_title_">Calendar</h5>', '<h5 class="widget_title_">Calendrier</h5>', $html);
    $html = str_replace('<h5 class="widget_title_">Popular</h5>', '<h5 class="widget_title_">Populaire</h5>', $html);

    $html = str_replace('<span>Views</span>', '<span>Vues</span>', $html);
    $html = str_replace('<span>comments</span>', '<span>commentaires</span>', $html);
    $html = str_replace('<span>likes</span>', '', $html);

    $html = str_replace(
        '<a href="" target="_blank">Politique de confidentialité</a>',
        '<a href="/privacy-policy/" target="_blank" style="color: white;">Politique de confidentialité</a>'
        , $html);


    $html = str_replace('<a href="#" id="co_toggle" class="icon-params-light"></a>', '', $html);

    $html = str_replace(
        '<div class="form_left">
    <form action',
        '<div class="">
        <form action', $html);

    $html = str_replace(
        '>Similar Properties<',
        '>Biens Similaires<',
        $html);

    echo $html;
}


add_action( 'wp_head', 'start_modify_html' );
add_action( 'wp_footer', 'end_modify_html' );
?>