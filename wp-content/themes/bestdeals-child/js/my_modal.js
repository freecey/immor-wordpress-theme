

// ######### MODAL ###############
// Get the modal
var ebModal = document.getElementById('mySizeChartModal');

// Get the button that opens the modal
var ebBtn = document.getElementById("mySizeChart");

var modalContent = document.getElementById("modalContent");

// Get the <span> element that closes the modal
var ebSpan = document.getElementsByClassName("the_close")[0];
var btnClose = document.getElementsByClassName("btn_modal_close")[0];


// When the user clicks the button, open the modal
// ebBtn.onclick = function() {
//     ebModal.style.display = "block";
// }
function OpenModal() {
    ebModal.style.display = "block";
    var expiryDate = new Date();
    expiryDate.setMonth(expiryDate.getMonth() + 1);
    var expires = "expires="+ expiryDate.toUTCString();
    document.cookie = "modalbien=1" + ";" + expires + ";path=/";
 }


// When the user clicks on <span> (x), close the modal
ebSpan.onclick = function() {
    ebModal.style.display = "none";
}

btnClose.onclick = function() {
    ebModal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == ebModal) {
        ebModal.style.display = "none";
    }
}

setTimeout(OpenModal, 15000)