<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_property_2_theme_setup' ) ) {
    add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_property_2_theme_setup', 1 );
    function bestdeals_template_property_2_theme_setup() {
        bestdeals_add_template(array(
            'layout' => 'property-2',
            'template' => 'property-2',
            'mode'   => 'property',
            'title'  => esc_html__('Property /Style 2/', 'bestdeals')
        ));
    }
}

// Template output
if ( !function_exists( 'bestdeals_template_property_2_output' ) ) {
    function bestdeals_template_property_2_output($post_options, $post_data) {
        $show_title = true;
        $parts = explode('_', $post_options['layout']);
        $style = $parts[0];

        $_property_status_list = get_post_meta( $post_data['post_id'], 'property_status_list', true );
        $_property_type_single = get_post_meta( $post_data['post_id'], 'property_type_single', true );

        $_property_price_sign = get_post_meta( $post_data['post_id'], 'property_price_sign', true );
        $property_price = get_post_meta( $post_data['post_id'], 'property_price', true );
        $_property_price_per = get_post_meta( $post_data['post_id'], 'property_price_per', true );

        $_property_area = (int) get_post_meta( $post_data['post_id'], 'property_area', true );

        $_property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'property_bedrooms', true );
        $_property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'property_bathrooms', true );
        $_property_garages = (int) get_post_meta( $post_data['post_id'], 'property_garages', true );

        $_property_price = '';
        if ( isset($property_price) ) {
            $propertyPriceArray = explode(".", $property_price);
            if (isset($propertyPriceArray[0])) {
                $_property_price = (int) $propertyPriceArray[0];
                if (isset($propertyPriceArray[1])) {
                    $_property_price = number_format($_property_price, 0, ',', ' ') . '.' . $propertyPriceArray[1];
                } else {
                    $_property_price = number_format($_property_price, 0, ',', ' ');
                }
            }
        }


        ?>


        <div class="sc_property_item sc_property_item_<?php echo esc_attr($post_options['number']); ?>">

            <div class="sc_property_item_price">
                <?php

                if ( $_property_price != '0' ) {
                    if (strlen($_property_price_sign) > 0 ) {
                        echo esc_html($_property_price_sign);
                    }
                    echo esc_html($_property_price);
                    if ( ($_property_status_list == 'rent') and (strlen($_property_price_per) > 0) ) {
                        echo '<span> / '.esc_html__('per', 'bestdeals').' '. esc_html($_property_price_per) .'</span>';
                    }
                }
                ?>
            </div>

            <?php
            if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) {
                ?><div class="sc_property_item_title">
                <a href="<?php echo esc_url($post_data['post_link']); ?>"><?php if (strlen(esc_html($post_data['post_title'])) > 28) {
                        $str = substr(esc_html($post_data['post_title']), 0, 28) . '...';
                        echo $str; }
                    else { echo esc_html($post_data['post_title']); } ?></a></div><?php
            } else {
                ?><div class="sc_property_item_title"><?php echo esc_html($post_data['post_title']); ?></div><?php
            }
            ?>

            <div class="sc_property_item_description">
                <?php
                if ($post_data['post_protected']) {
                    bestdeals_show_layout($post_data['post_excerpt']);
                } else {
                    if ($post_data['post_excerpt']) {
                        echo in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) ? $post_data['post_excerpt'] : '<p>'.trim(bestdeals_strshort($post_data['post_excerpt'], isset($post_options['descr']) ? $post_options['descr'] : bestdeals_get_custom_option('post_excerpt_maxlength_masonry'))).'</p>';
                    }
                }
                ?>
            </div>

            <div class="sc_property_item_area">
                <?php echo esc_html(number_format($_property_area, 0, ',', ' ')).'&nbsp;'.esc_html__('Sq Ft', 'bestdeals');?>
            </div>

            <div class="sc_property_item_meta">
                <?php
                if ( $_property_bedrooms !=0 ) echo '<span class="bedrooms">' . esc_html($_property_bedrooms) . '</span>';
                if ( $_property_bathrooms !=0 ) echo '<span class="bathrooms">' . esc_html($_property_bathrooms) . '</span>';
                if ( $_property_garages !=0 ) echo '<span class="garages">' . esc_html($_property_garages) . '</span>';
                ?>
            </div>
            <div class="cL"></div>
            <?php

            if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) { ?>
                <a href="<?php echo esc_url($post_data['post_link']); ?>" class="sc_property_item_btn"><?php esc_html_e('Details', 'bestdeals'); ?></a>
            <?php } ?>

        </div>

        <?php
    }
}
?>