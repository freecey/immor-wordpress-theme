<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_property_1_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_property_1_theme_setup', 1 );
	function bestdeals_template_property_1_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'property-1',
			'template' => 'property-1',
			'mode'   => 'property',
			'need_columns' => true,
			'title'  => esc_html__('Property /Style 1/', 'bestdeals'),
			'thumb_title'  => esc_html__('Property image (crop)', 'bestdeals'),
			'w'		 => 370,
			'h'		 => 180
		));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_property_1_output' ) ) {
	function bestdeals_template_property_1_output($post_options, $post_data) {
		$show_title = true;
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($parts[1]) ? (!empty($post_options['columns_count']) ? $post_options['columns_count'] : 1) : (int) $parts[1]));
		
		$_property_status_list = get_post_meta( $post_data['post_id'], 'property_status_list', true );
		$_property_type_single = get_post_meta( $post_data['post_id'], 'property_type_single', true );
		
		$_property_price_sign = get_post_meta( $post_data['post_id'], 'property_price_sign', true );
		$property_price = get_post_meta( $post_data['post_id'], 'property_price', true );
		$_property_price_per = get_post_meta( $post_data['post_id'], 'property_price_per', true );
		
		$_property_area = (int) get_post_meta( $post_data['post_id'], 'property_area', true );
		
		$_property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'property_bedrooms', true );
		$_property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'property_bathrooms', true );
		$_property_garages = (int) get_post_meta( $post_data['post_id'], 'property_garages', true );
		
		
		if ( $_property_status_list == 'sale' ) {
			$propertyStatusListText = esc_html__('Sale', 'bestdeals');
		} elseif ( $_property_status_list == 'rent' ) {
			$propertyStatusListText = esc_html__('Rent', 'bestdeals');
		}
		
		$_property_price = '';
		if ( isset($property_price) ) {
			$propertyPriceArray = explode(".", $property_price);
			if (isset($propertyPriceArray[0])) {
				$_property_price = (int) $propertyPriceArray[0];
				if (isset($propertyPriceArray[1])) {
					$_property_price = number_format($_property_price, 0, ',', ' ') . '.' . $propertyPriceArray[1];
				} else {
					$_property_price = number_format($_property_price, 0, ',', ' ');
				}
			}
		}

		if (bestdeals_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><div class="sc_property_item_wrap"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php bestdeals_show_layout($post_options['tag_id'] ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''); ?>
				class="sc_property_item sc_property_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '') . (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"
				<?php bestdeals_show_layout($post_options['tag_css']!='' ? ' style="'.esc_attr($post_options['tag_css']).'"' : '');
				bestdeals_show_layout(!bestdeals_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($post_options['tag_animation'])).'"' : ''); ?>>


                <?php
                $property_terms = get_the_terms( get_the_ID(), 'property_group' );
                $color_class = '';
                foreach ($property_terms as $tax) {
                    if ($tax->term_id != 76) {
                        if ($tax->term_id != 78) {
//                            if ($propertyStatusListText != '') {
//                                $propertyStatusListText .= ' - '. __($tax->name);
//                            } else {
                                $propertyStatusListText = __($tax->name);
                                $color_class = ' style="background-color: #d87c46;"';
//                            }
                        }
                    }
                }
                ?>


                <div class="sc_property_item_featured">
                    <?php require(bestdeals_get_file_dir('templates/_parts/post-featured.php')); ?>
                    <?php if (strlen($propertyStatusListText) > 0 ) {
                        echo '<span class="sc_property_item_status"' . $color_class . '>' . esc_html($propertyStatusListText) . '</span>';
                    } ?>
                </div>

				<div class="sc_property_item_content">
				
					<div class="sc_property_item_type">
						<?php 
						if (strlen($_property_status_list) > 0 ) {
							echo esc_html($_property_type_single).'&nbsp;';
						} else {
							echo '&nbsp;';
						}
						?>
					</div>
					
					<div class="sc_property_item_price">
						<?php
						
						if ( $_property_price != '0' ) {
							if (strlen($_property_price_sign) > 0 ) {
								echo esc_html($_property_price_sign);
							}
							echo esc_html($_property_price);
							if ( ($_property_status_list == 'rent') and (strlen($_property_price_per) > 0) ) {
								echo '<span>'.esc_html__('per', 'bestdeals').' '. esc_html($_property_price_per) .'</span>';
							}
						}
						?>
					</div>
					
					<?php
					if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) {
						?><h4 class="sc_property_item_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo esc_html($post_data['post_title']); ?></a></h4><?php
					} else {
						?><h4 class="sc_property_item_title"><?php echo esc_html($post_data['post_title']); ?></h4><?php
					}
					?>
						
					<div class="cL"></div>
					
					<div class="sc_property_item_area">
						<?php echo esc_html(number_format($_property_area, 0, ',', ' ')).'&nbsp;'.esc_html__('Sq Ft', 'bestdeals');?>
					</div>
					
					<div class="sc_property_item_meta">
						<?php
						if ( $_property_bedrooms !=0 ) echo '<span class="bedrooms">' . esc_html($_property_bedrooms) . '</span>';
						if ( $_property_bathrooms !=0 ) echo '<span class="bathrooms">' . esc_html($_property_bathrooms) . '</span>';
						if ( $_property_garages !=0 ) echo '<span class="garages">' . esc_html($_property_garages) . '</span>';
						?>
					</div>
					
					<div class="cL"></div>
					
				</div>
			</div>
		<?php
		if (bestdeals_param_is_on($post_options['slider'])) {
			?></div></div><?php
		} else if ($columns > 1) {
			?></div><?php
		}
	}
}
?>