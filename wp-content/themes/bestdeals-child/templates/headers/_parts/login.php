<div id="popup_login" class="popup_wrap popup_login bg_tint_light">
	<a href="#" class="popup_close"></a>
	<div class="form_wrap">
		<div class="form_left">
			<form action="<?php echo wp_login_url(); ?>" method="post" name="login_form" class="popup_form login_form">
				<input type="hidden" name="redirect_to" value="<?php echo esc_url(wp_lostpassword_url(get_permalink())); ?>">
				<div class="popup_form_field login_field iconed_field icon-user"><input type="text" id="log" name="log" value="" placeholder="<?php esc_html_e('Login or Email', 'bestdeals'); ?>"></div>
				<div class="popup_form_field password_field iconed_field icon-lock"><input type="password" id="password" name="pwd" value="" placeholder="<?php esc_html_e('Password', 'bestdeals'); ?>"></div>
				<div class="popup_form_field remember_field">
					<a href="<?php echo esc_url(wp_lostpassword_url()); ?>" class="forgot_password"><?php esc_html_e('Forgot password?', 'bestdeals'); ?></a>
					<input type="checkbox" value="forever" id="rememberme" name="rememberme">
					<label for="rememberme"><?php esc_html_e('Remember me', 'bestdeals'); ?></label>
				</div>
				<div class="popup_form_field submit_field"><input type="submit" class="submit_button sc_button sc_button_size_medium" value="<?php esc_html_e('Login', 'bestdeals'); ?>"></div>
			</form>
		</div>
<!--		<div class="form_right">-->
<!--			<div class="login_socials_title">--><?php //esc_html_e('You can login using your social profile', 'bestdeals'); ?><!--</div>-->
<!--			<div class="login_socials_list">-->
<!--				--><?php //if(function_exists('bestdeals_sc_socials')) echo bestdeals_sc_socials(array('size'=>"tiny", 'shape'=>"round", 'socials'=>"facebook=#|twitter=#|gplus=#")); ?>
<!--			</div>-->
<!--			<div class="login_socials_problem"><a href="#">--><?php //esc_html_e('Problem with login?', 'bestdeals'); ?><!--</a></div>-->
<!--			<div class="result message_block"></div>-->
<!--		</div>-->
		<div class="cL"></div>
	</div>	<!-- /.login_wrap -->
</div>		<!-- /.popup_login -->
