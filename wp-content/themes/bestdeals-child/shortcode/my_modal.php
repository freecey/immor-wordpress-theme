<?php

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

function get_modal_form()
{

    if(!isset($_COOKIE['modalbien'])) {
        $render_modal =
            '<div id="mySizeChartModal" class="the_modal">
      <div id="modalContent" class="the_modal-content">
        <span class="the_close">&times;</span>
        <p>' . do_shortcode('[contact-form-7 id="3118" title="Modal Bien"]') . '</p>
        <p>
            <button class="btn_modal_close">Fermer</button>
        </p>
      </div>
    
    </div>
    <script src="'. get_stylesheet_directory_uri() . '/js/my_modal.js"></script>
    ';
    } else {
        $render_modal = '';
    }
    return $render_modal;
}

add_shortcode( 'MY_SC_MODAL_FORM', 'get_modal_form' );

?>