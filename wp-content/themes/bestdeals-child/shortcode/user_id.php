<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

function get_cookies_userid()
{

    $cook_full = isset($_COOKIE) ? $_COOKIE : 'not set';

    $val_cook_id = array();
    foreach ($cook_full as $key => $value) {
        if (strpos($key, 'matomouid') === 0) {
            $val_cook_id[$key] = $value;
        }
    }

    $keyname = array_keys($val_cook_id);

    return $val_cook_id[$keyname[0]];
}

add_shortcode( 'MY_SC_USERID', 'get_cookies_userid' );
?>