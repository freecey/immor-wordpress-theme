<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

function my_sc_ced() {
	
$images = get_field( "carousel_slider" );
$img_size = 'property-slide';
$row_i = 0;
$tag_img = '';
$tag_li = '';
$carousel_render = '';
	
// if( $value ) {
//     var_dump($value[property-slide]);
// } else {
//     echo 'empty';
// }
	
if( $images ): 
	foreach( $images as $image ): 
		$row_i ++; 
		if ($row_i == 1) {
			$tag_img .=
				'<div class="c-carousel-item active">
					<img src="' . esc_url($image['sizes'][$img_size]) . '" alt="' . esc_attr($image['alt']) . '">
				</div>';
			$tag_li .= '
				<li class = "c-carousel-indicator-li active"></li>';
		} else {
			$tag_img .=
				'<div class="c-carousel-item">
					<img src="' . esc_url($image['sizes'][$img_size]) . '" alt="' . esc_attr($image['alt']) . '">
				</div>';
			$tag_li .= '
				<li class = "c-carousel-indicator-li" style="padding-left: none !important;"></li>';
		}
         endforeach; 

	$carousel_render = '
	<div class="c-carousel" data-interval = "5000">
	  '. $tag_img .'
      <div class="c-carousel-control-prev">
        <i class="fas fa-chevron-left"></i>
      </div>
      <div class="c-carousel-control-next">
        <i class="fas fa-chevron-right"></i>
      </div>
      <div class="c-carousel-indicators">
        <ul>
			'. $tag_li .'
        </ul>
      </div>
    </div>
	<script src="/wp-content/themes/bestdeals-child/js/my-custom.js"></script>
	<script src="https://kit.fontawesome.com/5eef641eb4.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	';
	endif; 
	return $carousel_render;
}

add_shortcode( 'MY_SC_SLIDER', 'my_sc_ced' );


?>