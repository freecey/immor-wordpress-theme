<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

function pro_social_share() {
$title = esc_html( get_the_title() );
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
$descriptions = esc_html(get_field( "descriptions" ));
$tw_desc = substr($descriptions, 0, 277) . '...';
$post_url = get_the_permalink();
return
'<div class="sc_socials sc_socials_size_tiny sc_socials_share sc_socials_dir_horizontal">
    <span class="share_caption">Partagez:</span>
    <div class="sc_socials_item">
        <a class="social_icons social_icons_a social_facebook inited" href="https://www.facebook.com/sharer/sharer.php?u='.esc_url($post_url).'&p[images][0]='.esc_url($featured_img_url).'&p[title]='.$title.'&p[summary]='.$descriptions.'" data-link="http://www.facebook.com/sharer.php?u=link&amp;t=title">
            <span class="social_icon icon-facebook social_icon_share">
                <span class="">
                </span>
            </span>
        </a>
    </div>
    <div class="sc_socials_item">
        <a class="social_icons social_icons_a social_twitter inited" href="https://twitter.com/intent/tweet?url='.esc_url($post_url).'&text='.$tw_desc.'" data-link="http://www.twitter.com/intent/tweet?url=link&amp;via=title&amp;text=descr">
            <span class="social_icon icon-twitter social_icon_share">
                <span class="">
                </span>
            </span>
        </a>
    </div>
    <div class="sc_socials_item">
        <a class="social_icons social_icons_a social_linkedin inited" href="https://www.linkedin.com/sharing/share-offsite/?url='.esc_url($post_url).'" data-link="https://www.linkedin.com/sharing/share-offsite/?url=link">
            <span class="social_icon icon-linkedin social_icon_share">
                <span class="">
                </span>
            </span>
        </a>
    </div>
</div>';
}

function pro_social_show()
{
    return
        '<div class="">
    <div class="sc_socials_item"><a href="https://www.facebook.com/immorichel" target="_blank"
            class="social_icons social_facebook"><span class="icon-facebook"></span></a><a href="https://www.facebook.com/immorichel" target="_blank"> - Facebook</a></div>
    <div class="sc_socials_item"><a href="https://twitter.com/infoimmorichel" target="_blank"
            class="social_icons social_twitter"><span class="icon-twitter"> - Twitter</span></a></div>
    <div class="sc_socials_item"><a href="https://instagram.com/jprichel" target="_blank"
            class="social_icons social_instagramm"><span class="icon-instagramm"> - Instagram</span></a></div>
    <div class="sc_socials_item"><a href="https://www.pinterest.com/immorichel" target="_blank"
            class="social_icons social_pinterest-circled"><span class="icon-pinterest-circled"> - Pinterest</span></a></div>
    <div class="sc_socials_item"><a href="https://www.linkedin.com/company/6624622" target="_blank"
            class="social_icons social_linkedin"><span class="icon-linkedin"> - Linkedin</span></a></div>
    </div>';
}


add_shortcode( 'MY_SC_SHARE', 'pro_social_share' );
add_shortcode( 'MY_SC_SHOWSOCIAL', 'pro_social_show' );
?>