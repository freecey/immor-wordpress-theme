<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

function pro_acf_address() {
    $full_address = get_field( "rue" ) . ' ' . get_field( "code_postal" ) . ' ' . get_field( "commune" );
    return $full_address;
}

function pro_acf_desc() {
    $descriptions = esc_html(get_field( "descriptions" ));
    return $descriptions;
}

function pro_acf_video() {
    $video_player = '';
    if (have_rows('videos')):
        $video_player .=
        '<h5 class="sc_title sc_title_regular"><span class="VIiyi" lang="fr"><span class="JLqJ4b ChMk0b" data-language-for-alternatives="fr" data-language-to-translate-into="en" data-phrase-index="0">Vidéo</span></span></h5>
        <div class="sc_line sc_line_style_solid" style="border-top-style:solid;"></div>';
        while (have_rows('videos')) : the_row();
            $video_player .= do_shortcode( '[vc_video link="'. get_sub_field('video_url') . '"]' );
            $video_player .= '<div class="vc_empty_space" style="height: 1.867em"><span class="vc_empty_space_inner"></span></div>';
        endwhile;
    endif;


    return $video_player;
}

function pro_acf_caract() {
//    $type_de_bien = esc_html(get_field( "type_de_bien" ));
    $salles_de_bain = esc_html(get_field( "salles_de_bain" ));
    $chambres = esc_html(get_field( "chambres" ));
    $cuisine = esc_html(get_field( "cuisine" ));
    $revenu_cadastral = esc_html(get_field( "revenu_cadastral" ));
//    $surface_habitable = esc_html(get_field( "surface_habitable" ));
    $largeur_de_la_facade = esc_html(get_field( "largeur_de_la_facade" ));
    $superficie_de_la_parcelle = esc_html(get_field( "superficie_de_la_parcelle" ));
    $orientation_du_jardinarriere_maison = esc_html(get_field( "orientation_du_jardinarriere_maison" ));
    $annee_de_construction = esc_html(get_field( "annee_de_construction" ));
    $nombre_de_facades = esc_html(get_field( "nombre_de_facades" ));
    $type_de_chauffage = esc_html(get_field( "type_de_chauffage" ));
    $places_de_parking = esc_html(get_field( "places_de_parking" ));
    $cpeb = esc_html(get_field( "cpeb" ));
    $consommation_theorique_totale_par_an = esc_html(get_field( "consommation_theorique_totale_par_an" ));
    $classification_energetique = esc_html(get_field( "classification_energetique" ));
    $numero_du_rapport_cpeb = esc_html(get_field( "numero_du_rapport_cpeb" ));
    $permis_d_urbanisme = esc_html(get_field( "permis_d_urbanisme" ));
    $permis_de_lotissement = esc_html(get_field( "permis_de_lotissement" ));
    $assignations_delivrees = esc_html(get_field( "assignations_delivrees" ));
    $droit_de_preemption = esc_html(get_field( "droit_de_preemption" ));
    $zone_inondable = esc_html(get_field( "zone_inondable" ));

    $_revenu_cadastral = '';
    if ( isset($revenu_cadastral) ) {
        $propertyPriceArray = explode(".", $revenu_cadastral);
        if (isset($propertyPriceArray[0])) {
            $_revenu_cadastral = (int) $propertyPriceArray[0];
            if (isset($propertyPriceArray[1])) {
                $_revenu_cadastral = number_format($_revenu_cadastral, 0, ',', ' ') . '.' . $propertyPriceArray[1];
            } else {
                $_revenu_cadastral = number_format($_revenu_cadastral, 0, ',', ' ');
            }
        }
    }

    $render_caract =
        '<table>
            <tbody>';

//    if( $type_de_bien ):
//        $render_caract .=
//            '<tr>
//            <td class="name">Type de bien</td>
//            <td class="value">' . $type_de_bien . '</td>
//            </tr>';
//    endif;

    if( $salles_de_bain ):
        $render_caract .=
            '<tr>
            <td class="name">Salles de bain</td>
            <td class="value">' . $salles_de_bain . '</td>
            </tr>';
    endif;

    if( $chambres ):
        $render_caract .=
            '<tr>
            <td class="name">Chambres</td>
            <td class="value">' . $chambres . '</td>
            </tr>';
    endif;

    if( $cuisine ):
        $render_caract .=
            '<tr>
            <td class="name">Cuisine</td>
            <td class="value">' . $cuisine . '</td>
            </tr>';
    endif;

//    if( $surface_habitable ):
//        $render_caract .=
//            '<tr>
//            <td class="name">Surface habitable</td>
//            <td class="value">' . $surface_habitable . 'm<sup>2</sup></td>
//            </tr>';
//    endif;

    if( $_revenu_cadastral ):
        $render_caract .=
            '<tr>
            <td class="name">Revenu cadastral</td>
            <td class="value">' . $_revenu_cadastral . ' €</td>
            </tr>';
    endif;

    if( $largeur_de_la_facade ):
        $render_caract .=
            '<tr>
            <td class="name">Largeur de la façade</td>
            <td class="value">' . $largeur_de_la_facade . 'm</td>
            </tr>';
    endif;

    if( $superficie_de_la_parcelle ):
        $render_caract .=
            '<tr>
            <td class="name">Superficie de la parcelle</td>
            <td class="value">' . $superficie_de_la_parcelle . 'm<sup>2</sup></td>
            </tr>';
    endif;

    if( $orientation_du_jardinarriere_maison ):
        $render_caract .=
            '<tr>
            <td class="name">Orientation du jardin/arrière maison</td>
            <td class="value">' . $orientation_du_jardinarriere_maison . '</td>
            </tr>';
    endif;

    if( $annee_de_construction ):
        $render_caract .=
            '<tr>
            <td class="name">Année de construction</td>
            <td class="value">' . $annee_de_construction . '</td>
            </tr>';
    endif;

    if( $nombre_de_facades ):
        $render_caract .=
            '<tr>
            <td class="name">Nombre de façades</td>
            <td class="value">' . $nombre_de_facades . '</td>
            </tr>';
    endif;

    if( $type_de_chauffage ):
        $render_caract .=
            '<tr>
            <td class="name">Type de chauffage</td>
            <td class="value">' . $type_de_chauffage . '</td>
            </tr>';
    endif;

    if( $places_de_parking ):
        $render_caract .=
            '<tr>
            <td class="name">places de parking</td>
            <td class="value">' . $places_de_parking . '</td>
            </tr>';
    endif;

    if( $cpeb ):
        $render_caract .=
            '<tr>
            <td class="name">CPEB</td>
            <td class="value">' . $cpeb . ' kwh/m<sup>2</sup></td>
            </tr>';
    endif;

    if( $consommation_theorique_totale_par_an ):
        $render_caract .=
            '<tr>
            <td class="name">Consommation théorique totale par an</td>
            <td class="value">' . $consommation_theorique_totale_par_an . ' kwh/an</td>
            </tr>';
    endif;

    if( $classification_energetique ):
        $render_caract .=
            '<tr>
            <td class="name">Classification énergétique</td>
            <td class="value">' . $classification_energetique . '</td>
            </tr>';
    endif;

    if( $numero_du_rapport_cpeb ):
        $render_caract .=
            '<tr>
            <td class="name">Numéro du rapport CPEB</td>
            <td class="value">' . $numero_du_rapport_cpeb . '</td>
            </tr>';
    endif;

    if( $permis_d_urbanisme ):
        $render_caract .=
            '<tr>
            <td class="name">Permis d’urbanisme</td>
            <td class="value"> ' . $permis_d_urbanisme . ' </td>
            </tr>';
    endif;

    if( $permis_de_lotissement ):
        $render_caract .=
            '<tr>
            <td class="name">Permis de lotissement</td>
            <td class="value">' . $permis_de_lotissement . '</td>
            </tr>';
    endif;

    if( $assignations_delivrees ):
        $render_caract .=
            '<tr>
            <td class="name">Assignations délivrées</td>
            <td class="value">' . $assignations_delivrees . '</td>
            </tr>';
    endif;

    if( $droit_de_preemption ):
        $render_caract .=
            '<tr>
            <td class="name">Droit de préemption</td>
            <td class="value">' . $droit_de_preemption . '</td>
            </tr>';
    endif;

    if( $zone_inondable ):
        $render_caract .=
            '<tr>
            <td class="name">Zone inondable</td>
            <td class="value">' . $zone_inondable . '</td>
            </tr>';
    endif;

    $render_caract .=
        '</tbody>
            </table>';

    return $render_caract;
}





function pro_acf_proximity() {
    $enable_prox = esc_html(get_field( "afficher_prox" ));
    $arret_de_bus = esc_html(get_field( "arret_de_bus" ));
    $ecoles = esc_html(get_field( "ecoles" ));
    $magasins = esc_html(get_field( "magasins" ));
    $sortie_d_autoroute = esc_html(get_field( "sortie_d_autoroute" ));

    $render_prox_full = '';

    $render_prox =
        '<table>
            <tbody>';

    if( $arret_de_bus ):
        $render_prox .=
            '<tr>
            <td class="name">Arrêt de bus</td>
            <td class="value">' . $arret_de_bus . '</td>
            </tr>';
    endif;

    if( $ecoles ):
        $render_prox .=
            '<tr>
            <td class="name">Écoles</td>
            <td class="value">' . $ecoles . '</td>
            </tr>';
    endif;

    if( $magasins ):
        $render_prox .=
            '<tr>
            <td class="name">Magasins</td>
            <td class="value">' . $magasins . '</td>
            </tr>';
    endif;

    if( $sortie_d_autoroute ):
        $render_prox .=
            '<tr>
            <td class="name">Sortie d’autoroute</td>
            <td class="value">' . $sortie_d_autoroute . '</td>
            </tr>';
    endif;

    $render_prox .=
        '</tbody>
            </table>';

    if( $enable_prox == 1 ):
        $render_prox_full =
            '<h5 class="sc_title sc_title_regular"><span class="VIiyi" lang="fr"><span class="JLqJ4b ChMk0b" data-language-for-alternatives="fr" data-language-to-translate-into="en" data-phrase-index="0">Transports et lieux publics à proximité</span></span></h5>
            <div class="sc_line sc_line_style_solid" style="border-top-style:solid;"></div>
            <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                    ' . $render_prox . '        
                </div>
            </div>
';
    endif;

    return $render_prox_full;

}


function pro_acf_files() {
    $render_files = '';
    if (have_rows('fichiers')):

        $render_files =
            '<div class="vc_empty_space" style="height: 1.867em"><span class="vc_empty_space_inner"></span></div>
            <h5 class="sc_title sc_title_regular"><span class="VIiyi" lang="fr"><span class="JLqJ4b ChMk0b" data-language-for-alternatives="fr" data-language-to-translate-into="en" data-phrase-index="0">Pièces jointes</span></span></h5>
            <div class="sc_line sc_line_style_solid" style="border-top-style:solid;"></div>
            <div class="wpb_text_column wpb_content_element ">
		    <div class="wpb_wrapper">
			<table>
            <tbody>';

        // Loop through rows.
        while (have_rows('fichiers')) : the_row();

            // Load sub field value.
            $nom_du_fichier = get_sub_field('nom_du_fichier');
            $url_file = get_sub_field('url_file');
            $render_files .=
            '<tr>
            <td class="name"><i class="fas fa-file"></i> ' . $nom_du_fichier . ' :</td>
            <td class="value"><a href="' .  $url_file["url"] . '"> ' . $url_file["filename"] . '</a></td>
            </tr>';
        endwhile;
        $render_files .=
            '</tbody>
            </table>
          </div>
	</div>
<div class="vc_empty_space" style="height: 1.867em"><span class="vc_empty_space_inner"></span></div>';

    endif;

    return $render_files;
}

function pro_acf_summary() {
    $post_id = get_the_ID();
    $property_price = get_post_meta( $post_id, 'property_price', true );
    $type_de_bien = esc_html(get_field( "type_de_bien" ));
    $chambres = esc_html(get_field( "chambres" ));
//    $surface_habitable = esc_html(get_field( "surface_habitable" ));
    $property_area = (int) get_post_meta( $post_id, 'property_area', true );
    $rue = get_field( "rue" );
    $code_postal = get_field( "code_postal" );
    $ville = get_field( "commune" );
    $_property_status_list = get_post_meta( $post_id, 'property_status_list', true );
    $_property_type_single = get_post_meta( $post_id, 'property_type_single', true );
    $_property_bedrooms = (int) get_post_meta( $post_id, 'property_bedrooms', true );
    $cuisine = esc_html(get_field( "cuisine" ));

    if ( $_property_status_list == 'sale' ) {
        $propertyStatusListText = esc_html__('Sale', 'bestdeals');
    } elseif ( $_property_status_list == 'rent' ) {
        $propertyStatusListText = esc_html__('Rent', 'bestdeals');
    }

    $_property_price = '';
    if ( isset($property_price) ) {
        $propertyPriceArray = explode(".", $property_price);
        if (isset($propertyPriceArray[0])) {
            $_property_price = (int) $propertyPriceArray[0];
            if (isset($propertyPriceArray[1])) {
                $_property_price = number_format($_property_price, 0, ',', ' ') . '.' . $propertyPriceArray[1];
            } else {
                $_property_price = number_format($_property_price, 0, ',', ' ');
            }
        }
    }

    $_property_area = '';
    if ( isset($property_area) ) {
        $propertyPriceArray = explode(".", $property_area);
        if (isset($propertyPriceArray[0])) {
            $_property_area = (int) $propertyPriceArray[0];
            if (isset($propertyPriceArray[1])) {
                $_property_area = number_format($_property_area, 0, ',', ' ') . '.' . $propertyPriceArray[1];
            } else {
                $_property_area = number_format($_property_area, 0, ',', ' ');
            }
        }
    }

    $property_terms = get_the_terms( $post_id, 'property_group' );
    $color_class = '';
    foreach ($property_terms as $tax) {
        if ($tax->term_id != 76) {
            if ($tax->term_id != 78) {
                $propertyStatusListText = __($tax->name);
                $color_class = ' style="background-color: #d87c46; padding: 0.231em 1em 0.308em; color: #fff; border-radius: 2px;"';
            }
        }
    }

    $render_sum =
        '<table class="acf-resume">
            <tbody>';

    if( $property_price ):
        $render_sum .=
            '<tr>
            <td class="name">Prix</td>
            <td class="value"><span class="resume-price">' . $_property_price . ' €</span></td>
            </tr>';
    endif;

    if( $propertyStatusListText ):
        $render_sum .=
            '<tr>
            <td class="name">Cat.</td>
            <td class="value"><span class="resume-cat"' . $color_class . '>' . $propertyStatusListText . '</span></td>
            </tr>';
    endif;

    if( $_property_type_single ):
        $render_sum .=
            '<tr>
            <td class="name">Type</td>
            <td class="value">' . $_property_type_single . '</td>
            </tr>';
    endif;


    if( $property_area ):
        $render_sum .=
            '<tr>
            <td class="name">Surf. hab</td>
            <td class="value">' . $_property_area . ' m<sup>2</sup></td>
            </tr>';
    endif;

    if( $rue ):
        $render_sum .=
            '<tr>
            <td class="name">Rue</td>
            <td class="value">' . $rue . '</td>
            </tr>';
    endif;

    if( $code_postal ):
        $render_sum .=
            '<tr>
            <td class="name">CP</td>
            <td class="value">' . $code_postal . '</td>
            </tr>';
    endif;

    if( $ville ):
        $render_sum .=
            '<tr>
            <td class="name">Ville</td>
            <td class="value">' . $ville . '</td>
            </tr>';
    endif;

    if( $_property_bedrooms ):
        $render_sum .=
            '<tr>
            <td class="name">Chambres</td>
            <td class="value">' . $_property_bedrooms . '</td>
            </tr>';
    endif;

    if( $cuisine ):
        $render_sum .=
            '<tr>
            <td class="name">Cuisine</td>
            <td class="value">' . $cuisine . '</td>
            </tr>';
    endif;



    $render_sum .=
        '</tbody>
            </table>';

    return $render_sum;
}




add_shortcode( 'MY_ACF_ADDRESS', 'pro_acf_address' );
add_shortcode( 'MY_ACF_DESC', 'pro_acf_desc' );
add_shortcode( 'MY_ACF_CARACT', 'pro_acf_caract' );
add_shortcode( 'MY_ACF_PROXIMITY', 'pro_acf_proximity' );
add_shortcode( 'MY_ACF_FILES', 'pro_acf_files' );
add_shortcode( 'MY_ACF_RESUME', 'pro_acf_summary' );
add_shortcode( 'MY_ACF_VIDEO_URL', 'pro_acf_video' );



?>