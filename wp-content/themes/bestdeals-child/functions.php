<?php
/**
 * Child-Theme functions and definitions
 */


function wpdocs_theme_setup() {
	add_theme_support('property-slide');
    add_image_size( 'property-slide', 960, 540, true ); 
}

function my_theme_enqueue_styles() {
    wp_enqueue_style( 'bestdeals-theme-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// Translation & change part of text of theme not customizable
require_once( get_stylesheet_directory() . '/includes/cust-translate.php' );

// Add Shortcode
require_once( get_stylesheet_directory() . '/shortcode/my_slider.php' );
require_once( get_stylesheet_directory() . '/shortcode/acf_general.php' );
require_once( get_stylesheet_directory() . '/shortcode/social_share.php' );
require_once( get_stylesheet_directory() . '/shortcode/user_id.php' );
require_once( get_stylesheet_directory() . '/shortcode/my_modal.php' );

add_filter( 'author_link', 'my_author_link' );

function my_author_link() {
    return home_url( 'team/jean-pierre-richel' );
}




?>



