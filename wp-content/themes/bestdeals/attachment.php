<?php
/**
Template Name: Attachment page
 */
get_header(); 

while ( have_posts() ) { the_post();

	// Move bestdeals_set_post_views to the javascript - counter will work under cache system
	if (bestdeals_get_custom_option('use_ajax_views_counter')=='no') {
		bestdeals_set_post_views(get_the_ID());
	}

	bestdeals_show_post_layout(
		array(
			'layout' => 'attachment',
			'sidebar' => !bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_main'))
		)
	);

}

get_footer();
?>