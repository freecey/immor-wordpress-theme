<?php
/**
 * The Header for our theme.
 */

global $BESTDEALS_GLOBALS;

// Theme init - don't remove next row! Load custom options
bestdeals_core_init_theme();

$theme_skin = sanitize_file_name(bestdeals_get_custom_option('theme_skin'));
$body_scheme = bestdeals_get_custom_option('body_scheme');
if (empty($body_scheme)  || bestdeals_is_inherit_option($body_scheme)) $body_scheme = 'original';
$blog_style = bestdeals_get_custom_option(is_singular() && !bestdeals_get_global('blog_streampage') ? 'single_style' : 'blog_style');
$body_style  = bestdeals_get_custom_option('body_style');
$article_style = bestdeals_get_custom_option('article_style');
$top_panel_style = bestdeals_get_custom_option('top_panel_style');
$top_panel_position = bestdeals_get_custom_option('top_panel_position');
$top_panel_scheme = bestdeals_get_custom_option('top_panel_scheme');
$video_bg_show  = bestdeals_get_custom_option('show_video_bg')=='yes' && (bestdeals_get_custom_option('video_bg_youtube_code')!='' || bestdeals_get_custom_option('video_bg_url')!='');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo esc_attr('scheme_'.$body_scheme); ?>">
<head>
	<?php
	wp_head();
	?>
</head>

<body <?php 
	body_class('bestdeals_body body_style_' . esc_attr($body_style) 
		. ' body_' . (bestdeals_get_custom_option('body_filled')=='yes' ? 'filled' : 'transparent')
		. ' theme_skin_' . esc_attr($theme_skin)
		. ' article_style_' . esc_attr($article_style)
		. ' layout_' . esc_attr($blog_style)
		. ' template_' . esc_attr(bestdeals_get_template_name($blog_style))
		. (!bestdeals_param_is_off($top_panel_position) ? ' top_panel_show top_panel_' . esc_attr($top_panel_position) : 'top_panel_hide')
		. ' ' . esc_attr(bestdeals_get_sidebar_class())
		. ($video_bg_show ? ' video_bg_show' : '')
	);
	?>
>
	<?php bestdeals_show_layout(bestdeals_get_custom_option('gtm_code')); ?>

	<?php do_action( 'before' ); ?>

	<?php
	// Add TOC items 'Home' and "To top"
	if (bestdeals_get_custom_option('menu_toc_home')=='yes' && function_exists('bestdeals_sc_anchor'))
		echo bestdeals_sc_anchor(array(
			'id' => "toc_home",
			'title' => esc_html__('Home', 'bestdeals'),
			'description' => esc_html__('{{Return to Home}} - ||navigate to home page of the site', 'bestdeals'),
			'icon' => "icon-home",
			'separator' => "yes",
			'url' => home_url('/'))
			); 
	if (bestdeals_get_custom_option('menu_toc_top')=='yes' && function_exists('bestdeals_sc_anchor'))
		echo bestdeals_sc_anchor(array(
			'id' => "toc_top",
			'title' => esc_html__('To Top', 'bestdeals'),
			'description' => esc_html__('{{Back to top}} - ||scroll to top of the page', 'bestdeals'),
			'icon' => "icon-double-up",
			'separator' => "yes")
			); 
	?>

	<?php if ( !bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_outer')) ) { ?>
	<div class="outer_wrap">
	<?php } ?>

	<?php require_once( bestdeals_get_file_dir('sidebar_outer.php') ); ?>

	<?php
		$class = $style = '';
		if ($body_style=='boxed' || bestdeals_get_custom_option('bg_image_load')=='always') {
			if (($img = (int) bestdeals_get_custom_option('bg_image', 0)) > 0)
				$class = 'bg_image_'.($img);
			else if (($img = (int) bestdeals_get_custom_option('bg_pattern', 0)) > 0)
				$class = 'bg_pattern_'.($img);
			else if (($img = bestdeals_get_custom_option('bg_color', '')) != '')
				$style = 'background-color: '.($img).';';
			else if (bestdeals_get_custom_option('bg_custom')=='yes') {
				if (($img = bestdeals_get_custom_option('bg_image_custom')) != '')
					$style = 'background: url('.esc_url($img).') ' . str_replace('_', ' ', bestdeals_get_custom_option('bg_image_custom_position')) . ' no-repeat fixed;';
				else if (($img = bestdeals_get_custom_option('bg_pattern_custom')) != '')
					$style = 'background: url('.esc_url($img).') 0 0 repeat fixed;';
				else if (($img = bestdeals_get_custom_option('bg_image')) > 0)
					$class = 'bg_image_'.($img);
				else if (($img = bestdeals_get_custom_option('bg_pattern')) > 0)
					$class = 'bg_pattern_'.($img);
				if (($img = bestdeals_get_custom_option('bg_color')) != '')
					$style .= 'background-color: '.($img).';';
			}
		}
	?>

	<div class="body_wrap<?php bestdeals_show_layout($class ? ' '.esc_attr($class) : ''); ?>"<?php bestdeals_show_layout($style ? ' style="'.esc_attr($style).'"' : ''); ?>>

		<?php
		if ($video_bg_show) {
			$youtube = bestdeals_get_custom_option('video_bg_youtube_code');
			$video   = bestdeals_get_custom_option('video_bg_url');
			$overlay = bestdeals_get_custom_option('video_bg_overlay')=='yes';
			if (!empty($youtube)) {
				?>
				<div class="video_bg<?php bestdeals_show_layout($overlay ? ' video_bg_overlay' : ''); ?>" data-youtube-code="<?php echo esc_attr($youtube); ?>"></div>
				<?php
			} else if (!empty($video)) {
				$info = pathinfo($video);
				$ext = !empty($info['extension']) ? $info['extension'] : 'src';
				?>
				<div class="video_bg<?php echo esc_attr($overlay) ? ' video_bg_overlay' : ''; ?>"><video class="video_bg_tag" width="1280" height="720" data-width="1280" data-height="720" data-ratio="16:9" preload="metadata" autoplay loop src="<?php echo esc_url($video); ?>"><source src="<?php echo esc_url($video); ?>" type="video/<?php echo esc_attr($ext); ?>"></source></video></div>
				<?php
			}
		}
		?>

		<div class="page_wrap">

			<?php
			// Top panel 'Above' or 'Over'
			if (in_array($top_panel_position, array('above', 'over'))) {
				bestdeals_show_post_layout(array(
					'layout' => $top_panel_style,
					'position' => $top_panel_position,
					'scheme' => $top_panel_scheme
					), false);
			}
			// Slider
			require_once( bestdeals_get_file_dir('templates/headers/_parts/slider.php') );
			// Top panel 'Below'
			if ($top_panel_position == 'below') {
				bestdeals_show_post_layout(array(
					'layout' => $top_panel_style,
					'position' => $top_panel_position,
					'scheme' => $top_panel_scheme
					), false);
			}

			// Top of page section: page title and breadcrumbs
			$show_title = bestdeals_get_custom_option('show_page_title')=='yes';
			$show_breadcrumbs = bestdeals_get_custom_option('show_breadcrumbs')=='yes';
			if ($show_title || $show_breadcrumbs) {
				?>
				<div class="top_panel_title top_panel_style_<?php echo esc_attr(str_replace('header_', '', $top_panel_style)); ?> <?php bestdeals_show_layout(($show_title ? ' title_present' : '') . ($show_breadcrumbs ? ' breadcrumbs_present' : '')); ?> scheme_<?php echo esc_attr($top_panel_scheme); ?>">
					<div class="top_panel_title_inner top_panel_inner_style_<?php echo esc_attr(str_replace('header_', '', $top_panel_style)); ?> <?php bestdeals_show_layout(($show_title ? ' title_present_inner' : '') . ($show_breadcrumbs ? ' breadcrumbs_present_inner' : '')); ?>">
						<div class="content_wrap">
							<?php if ($show_title) { ?>
								<h1 class="page_title"><?php echo strip_tags(bestdeals_get_blog_title()); ?></h1>
							<?php } ?>
							<?php if ($show_breadcrumbs) { ?>
								<div class="breadcrumbs">
									<?php if (!is_404()) bestdeals_show_breadcrumbs(); ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php
			}
			?>

			<div class="page_content_wrap page_paddings_<?php echo esc_attr(bestdeals_get_custom_option('body_paddings')); ?>">

				<?php
				// Content and sidebar wrapper
				if ($body_style!='fullscreen') bestdeals_open_wrapper('<div class="content_wrap">');
				
				// Main content wrapper
				bestdeals_open_wrapper('<div class="content">');
				?>