<?php
/**
 * The template for displaying the footer.
 */

global $BESTDEALS_GLOBALS;

				bestdeals_close_wrapper();	// <!-- </.content> -->

				// Show main sidebar
				get_sidebar();

				if (bestdeals_get_custom_option('body_style')!='fullscreen') bestdeals_close_wrapper();	// <!-- </.content_wrap> -->
				?>
			
			</div>		<!-- </.page_content_wrap> -->
			
			<?php
			// Footer Custom shortcode box
			if (bestdeals_get_custom_option('show_cs_box_in_footer')=='yes') {
				$scbox = bestdeals_get_custom_option('cs_box_code');
				echo '<div class="custom_shortcode_box"><div class="content_wrap">';
				bestdeals_show_layout(do_shortcode($scbox));
				echo '</div></div>';
			}
			
			
			
			// Footer Testimonials stream
			if (bestdeals_get_custom_option('show_testimonials_in_footer')=='yes' && function_exists('bestdeals_sc_testimonials')) {
				$count = max(1, bestdeals_get_custom_option('testimonials_count'));
				$data = bestdeals_sc_testimonials(array('count'=>$count));
				if ($data) {
					?>
					<footer class="testimonials_wrap sc_section scheme_<?php echo esc_attr(bestdeals_get_custom_option('testimonials_scheme')); ?>">
						<div class="testimonials_wrap_inner sc_section_inner sc_section_overlay">
							<div class="content_wrap"><?php bestdeals_show_layout($data); ?></div>
						</div>
					</footer>
					<?php
				}
			}
			
			// Footer sidebar
			$footer_show  = bestdeals_get_custom_option('show_sidebar_footer');
			$sidebar_name = bestdeals_get_custom_option('sidebar_footer');
			if (!bestdeals_param_is_off($footer_show) && is_active_sidebar($sidebar_name)) { 
				$BESTDEALS_GLOBALS['current_sidebar'] = 'footer';
				
				?>
				<footer class="footer_wrap widget_area scheme_<?php echo esc_attr(bestdeals_get_custom_option('sidebar_footer_scheme')); ?>">
					<div class="footer_wrap_inner widget_area_inner">
						<div class="content_wrap">
							<div class="columns_wrap"><?php
							ob_start();
							do_action( 'before_sidebar' );
                                if ( is_active_sidebar( $sidebar_name ) ) {
                                    dynamic_sidebar( $sidebar_name );
                                }
							do_action( 'after_sidebar' );
							$out = ob_get_contents();
							ob_end_clean();
							bestdeals_show_layout(chop(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $out)));
							?></div>	<!-- /.columns_wrap -->
						</div>	<!-- /.content_wrap -->
					</div>	<!-- /.footer_wrap_inner -->
				</footer>	<!-- /.footer_wrap -->
			<?php
			}
			
			// ----------------- Newsletter -----------------------
			if ( bestdeals_get_custom_option('newsletter_show')=='yes' ) { ?>
				<div class="newsletter">
					<?php echo do_shortcode('[trx_emailer]'); ?>
				</div>
			<?php }
			
			
			

			// Google map
			if ( bestdeals_get_custom_option('show_googlemap')=='yes' && function_exists('bestdeals_sc_googlemap') ) {
				$map_address = bestdeals_get_custom_option('googlemap_address');
				$map_latlng  = bestdeals_get_custom_option('googlemap_latlng');
				$map_zoom    = bestdeals_get_custom_option('googlemap_zoom');
				$map_style   = bestdeals_get_custom_option('googlemap_style');
				$map_height  = bestdeals_get_custom_option('googlemap_height');
				if (!empty($map_address) || !empty($map_latlng)) {
					$args = array();
					if (!empty($map_style))		$args['style'] = esc_attr($map_style);
					if (!empty($map_zoom))		$args['zoom'] = esc_attr($map_zoom);
					if (!empty($map_height))	$args['height'] = esc_attr($map_height);
					echo bestdeals_sc_googlemap($args);
				}
			}

			// Footer contacts
			if (bestdeals_get_custom_option('show_contacts_in_footer')=='yes') { 
				$address_1 = bestdeals_get_theme_option('contact_address_1');
				$address_2 = bestdeals_get_theme_option('contact_address_2');
				$phone = bestdeals_get_theme_option('contact_phone');
				$fax = bestdeals_get_theme_option('contact_fax');
				if (!empty($address_1) || !empty($address_2) || !empty($phone) || !empty($fax)) {
					?>
					<footer class="contacts_wrap scheme_<?php echo esc_attr(bestdeals_get_custom_option('contacts_scheme')); ?>">
						<div class="contacts_wrap_inner">
							
							
							<div class="contacts_wrap_left">
								<?php require( bestdeals_get_file_dir('templates/_parts/logo.php') ); ?>
							</div>
							<div class="contacts_wrap_right">
								<div class="contacts_address">
									<address class="address">
										<?php if (!empty($address_2)) bestdeals_show_layout($address_2 . '<br>'); ?>
										<?php if (!empty($address_1)) bestdeals_show_layout($address_1 . '<br>'); ?>
										<?php if (!empty($phone)) echo esc_html__('Phone:', 'bestdeals') . ' ' . '<a href="tel:'.esc_attr($phone).'">' . ($phone) . '</a><br>'; ?>
										<?php if (!empty($fax)) echo esc_html__('Fax:', 'bestdeals') . ' ' . '<a href="fax:'.esc_attr($fax).'">' .($fax) . '</a>'; ?>
									</address>
								</div>
							</div>
							<div class="cL"></div>
						</div>	<!-- /.contacts_wrap_inner -->
					</footer><!-- /.contacts_wrap -->
					<?php
				}
			}

			// Copyright area
			$copyright_style = bestdeals_get_custom_option('show_copyright_in_footer');
			if (!bestdeals_param_is_off($copyright_style)) {
			?> 
				<div class="copyright_wrap copyright_style_<?php echo esc_attr($copyright_style); ?>  scheme_<?php echo esc_attr(bestdeals_get_custom_option('copyright_scheme')); ?>">
					<div class="copyright_wrap_inner">
						<div class="content_wrap">
							
							
							<?php
							if ($copyright_style == 'menu') {
								if (empty($BESTDEALS_GLOBALS['menu_footer']))	$BESTDEALS_GLOBALS['menu_footer'] = bestdeals_get_nav_menu('menu_footer');
								if (!empty($BESTDEALS_GLOBALS['menu_footer']))	bestdeals_show_layout($BESTDEALS_GLOBALS['menu_footer']);
							}
							?>
							
							
							<?php
							$contact_address_1=trim(bestdeals_get_custom_option('contact_address_1'));
							$contact_phone=trim(bestdeals_get_custom_option('contact_phone'));
							if ( !empty($contact_address_1) or !empty($contact_phone) ) {
								echo '<div class="contact_field">';
								
								if (!empty($contact_address_1)) { ?>
									<div class="contact_address">
										<span class="contact_icon icon-location"></span>
										<span class="contact_label contact_address_1"><?php bestdeals_show_layout($contact_address_1); ?></span>
									</div>
								<?php }
								
								if (!empty($contact_phone)) { ?>
									<div class="contact_phone">
										<span class="contact_icon icon-phone"></span>
										<a class="contact_label contact_phone_text" href="tel:<?php bestdeals_show_layout(esc_attr($contact_phone)); ?>"><?php bestdeals_show_layout($contact_phone); ?></a>
									</div>
								<?php }
								echo '<div class="cL"></div>';
								echo '</div>';
							}

							$footer_copyright = str_replace(array('{{Y}}', '{Y}'), date('Y'), bestdeals_get_theme_option('footer_copyright'));

							?>
							
							<div class="copyright_text"><?php bestdeals_show_layout($footer_copyright); ?></div>
							
							<div class="cL"></div>
							<?php
							if ($copyright_style == 'socials' && function_exists('bestdeals_sc_socials')) {
								echo bestdeals_sc_socials(array('size'=>"tiny"));
								echo '<div class="cL"></div>';
							}
							?>
							
						</div>
					</div>
				</div>
			<?php } ?>
			
		</div>	<!-- /.page_wrap -->

	</div>		<!-- /.body_wrap -->
	
	<?php if ( !bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_outer')) ) { ?>
	</div>	<!-- /.outer_wrap -->
	<?php } ?>

<?php
if (bestdeals_get_custom_option('show_theme_customizer')=='yes') {
	require_once( bestdeals_get_file_dir('core/core.customizer/front.customizer.php') );
}
?>

<a href="#" class="scroll_to_top icon-up" title="<?php esc_html_e('Scroll to top', 'bestdeals'); ?>"></a>

<div class="custom_html_section">
<?php bestdeals_show_layout(bestdeals_get_custom_option('custom_code')); ?>
</div>

<?php bestdeals_show_layout(bestdeals_get_custom_option('gtm_code2')); ?>

<?php wp_footer(); ?>

</body>
</html>