<?php
/**
 * Skin file for the theme.
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('bestdeals_action_skin_theme_setup')) {
	add_action( 'bestdeals_action_init_theme', 'bestdeals_action_skin_theme_setup', 1 );
	function bestdeals_action_skin_theme_setup() {

		// Add skin fonts in the used fonts list
		add_filter('bestdeals_filter_used_fonts',			'bestdeals_filter_skin_used_fonts');
		// Add skin fonts (from Google fonts) in the main fonts list (if not present).
		add_filter('bestdeals_filter_list_fonts',			'bestdeals_filter_skin_list_fonts');

		// Add skin stylesheets
		add_action('bestdeals_action_add_styles',			'bestdeals_action_skin_add_styles');
		// Add skin inline styles
		add_filter('bestdeals_filter_add_styles_inline',		'bestdeals_filter_skin_add_styles_inline');
		// Add skin responsive styles
		add_action('bestdeals_action_add_responsive',		'bestdeals_action_skin_add_responsive');
		// Add skin responsive inline styles
		add_filter('bestdeals_filter_add_responsive_inline',	'bestdeals_filter_skin_add_responsive_inline');

		// Add skin scripts
		add_action('bestdeals_action_add_scripts',			'bestdeals_action_skin_add_scripts');

		// Add skin less files into list for compilation
		add_filter('bestdeals_filter_compile_less',			'bestdeals_filter_skin_compile_less');

		// Add color schemes
		bestdeals_add_color_scheme('orange', array(

			'title'					=> esc_html__('Orange', 'bestdeals'),

			// Accent colors
			'accent1'				=> '#ff9638', //+
			'accent1_hover'			=> '#f07a10', //+
 			'accent2'				=> '#ffffff', //
 			'accent2_hover'			=> '#ffffff',
			
			// Headers, text and links colors
			'text'					=> '#8c8c8c', //+
			'text_light'			=> '#acb4b6',
			'text_dark'				=> '#343434', //+
			'inverse_text'			=> '#ffffff', //+
			'inverse_light'			=> '#ffffff',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff', //+
			'inverse_hover'			=> '#343434', //+
			
			// Whole block border and background
			'bd_color'				=> '#efefef', //+
			'bg_color'				=> '#f9f9f9', //+
			'bg_image'				=> '',
			'bg_image_position'		=> 'left top',
			'bg_image_repeat'		=> 'repeat',
			'bg_image_attachment'	=> 'scroll',
			'bg_image2'				=> '',
			'bg_image2_position'	=> 'left top',
			'bg_image2_repeat'		=> 'repeat',
			'bg_image2_attachment'	=> 'scroll',
		
			// Alternative blocks (submenu items, form's fields, etc.)
			'alter_text'			=> '#8c8c8c', //+
			'alter_light'			=> '#ffffff',
			'alter_dark'			=> '#343434', //+
			'alter_link'			=> '#ffffff',
			'alter_hover'			=> '#ffffff',
			'alter_bd_color'		=> '#e3e4e9', //+
			'alter_bd_hover'		=> '#ff9638', //+
			'alter_bg_color'		=> '#f9f9f9', //+
			'alter_bg_hover'		=> '#f9f9f9', //+
			'alter_bg_image'			=> '',
			'alter_bg_image_position'	=> 'left top',
			'alter_bg_image_repeat'		=> 'repeat',
			'alter_bg_image_attachment'	=> 'scroll',
			)
		);

		// Add color schemes
		bestdeals_add_color_scheme('blue', array(

			'title'					=> esc_html__('Blue', 'bestdeals'),

			// Accent colors
			'accent1'				=> '#46b6d8', //+
			'accent1_hover'			=> '#289ac4', //+
 			'accent2'				=> '#ffffff', //
 			'accent2_hover'			=> '#ffffff',
			
			// Headers, text and links colors
			'text'					=> '#8c8c8c', //+
			'text_light'			=> '#acb4b6',
			'text_dark'				=> '#343434', //+
			'inverse_text'			=> '#ffffff', //+
			'inverse_light'			=> '#ffffff',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff', //+
			'inverse_hover'			=> '#343434', //+
			
			// Whole block border and background
			'bd_color'				=> '#efefef', //+
			'bg_color'				=> '#f9f9f9', //+
			'bg_image'				=> '',
			'bg_image_position'		=> 'left top',
			'bg_image_repeat'		=> 'repeat',
			'bg_image_attachment'	=> 'scroll',
			'bg_image2'				=> '',
			'bg_image2_position'	=> 'left top',
			'bg_image2_repeat'		=> 'repeat',
			'bg_image2_attachment'	=> 'scroll',
		
			// Alternative blocks (submenu items, form's fields, etc.)
			'alter_text'			=> '#8c8c8c', //+
			'alter_light'			=> '#ffffff',
			'alter_dark'			=> '#343434', //+
			'alter_link'			=> '#ffffff',
			'alter_hover'			=> '#ffffff',
			'alter_bd_color'		=> '#e3e4e9', //+
			'alter_bd_hover'		=> '#ff9638', //+
			'alter_bg_color'		=> '#f9f9f9', //+
			'alter_bg_hover'		=> '#f9f9f9', //+
			'alter_bg_image'			=> '',
			'alter_bg_image_position'	=> 'left top',
			'alter_bg_image_repeat'		=> 'repeat',
			'alter_bg_image_attachment'	=> 'scroll',
			)
		);
		
		// Add color schemes
		bestdeals_add_color_scheme('green', array(

			'title'					=> esc_html__('Green', 'bestdeals'),

			// Accent colors
			'accent1'				=> '#70ae29', //+
			'accent1_hover'			=> '#97c936', //+
 			'accent2'				=> '#ffffff', //
 			'accent2_hover'			=> '#ffffff',
			
			// Headers, text and links colors
			'text'					=> '#8c8c8c', //+
			'text_light'			=> '#acb4b6',
			'text_dark'				=> '#343434', //+
			'inverse_text'			=> '#ffffff', //+
			'inverse_light'			=> '#ffffff',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff', //+
			'inverse_hover'			=> '#343434', //+
			
			// Whole block border and background
			'bd_color'				=> '#efefef', //+
			'bg_color'				=> '#f9f9f9', //+
			'bg_image'				=> '',
			'bg_image_position'		=> 'left top',
			'bg_image_repeat'		=> 'repeat',
			'bg_image_attachment'	=> 'scroll',
			'bg_image2'				=> '',
			'bg_image2_position'	=> 'left top',
			'bg_image2_repeat'		=> 'repeat',
			'bg_image2_attachment'	=> 'scroll',
		
			// Alternative blocks (submenu items, form's fields, etc.)
			'alter_text'			=> '#8c8c8c', //+
			'alter_light'			=> '#ffffff',
			'alter_dark'			=> '#343434', //+
			'alter_link'			=> '#ffffff',
			'alter_hover'			=> '#ffffff',
			'alter_bd_color'		=> '#e3e4e9', //+
			'alter_bd_hover'		=> '#ff9638', //+
			'alter_bg_color'		=> '#f9f9f9', //+
			'alter_bg_hover'		=> '#f9f9f9', //+
			'alter_bg_image'			=> '',
			'alter_bg_image_position'	=> 'left top',
			'alter_bg_image_repeat'		=> 'repeat',
			'alter_bg_image_attachment'	=> 'scroll',
			)
		);
		
		// Add color schemes
		bestdeals_add_color_scheme('bronze', array(

			'title'					=> esc_html__('Bronze', 'bestdeals'),

			// Accent colors
			'accent1'				=> '#e5cb1e', //+
			'accent1_hover'			=> '#deb90e', //+
 			'accent2'				=> '#ffffff', //
 			'accent2_hover'			=> '#ffffff',
			
			// Headers, text and links colors
			'text'					=> '#8c8c8c', //+
			'text_light'			=> '#acb4b6',
			'text_dark'				=> '#343434', //+
			'inverse_text'			=> '#ffffff', //+
			'inverse_light'			=> '#ffffff',
			'inverse_dark'			=> '#ffffff',
			'inverse_link'			=> '#ffffff', //+
			'inverse_hover'			=> '#343434', //+
			
			// Whole block border and background
			'bd_color'				=> '#efefef', //+
			'bg_color'				=> '#f9f9f9', //+
			'bg_image'				=> '',
			'bg_image_position'		=> 'left top',
			'bg_image_repeat'		=> 'repeat',
			'bg_image_attachment'	=> 'scroll',
			'bg_image2'				=> '',
			'bg_image2_position'	=> 'left top',
			'bg_image2_repeat'		=> 'repeat',
			'bg_image2_attachment'	=> 'scroll',
		
			// Alternative blocks (submenu items, form's fields, etc.)
			'alter_text'			=> '#8c8c8c', //+
			'alter_light'			=> '#ffffff',
			'alter_dark'			=> '#343434', //+
			'alter_link'			=> '#ffffff',
			'alter_hover'			=> '#ffffff',
			'alter_bd_color'		=> '#e3e4e9', //+
			'alter_bd_hover'		=> '#ff9638', //+
			'alter_bg_color'		=> '#f9f9f9', //+
			'alter_bg_hover'		=> '#f9f9f9', //+
			'alter_bg_image'			=> '',
			'alter_bg_image_position'	=> 'left top',
			'alter_bg_image_repeat'		=> 'repeat',
			'alter_bg_image_attachment'	=> 'scroll',
			)
		);

		// Add Custom fonts
		bestdeals_add_custom_font('h1', array(
			'title'			=> esc_html__('Heading 1', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '2.667em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> 'normal',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '0.700em'
			)
		);
		bestdeals_add_custom_font('h2', array(
			'title'			=> esc_html__('Heading 2', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '2.000em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> 'normal',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '0.933em'
			)
		);
		bestdeals_add_custom_font('h3', array(
			'title'			=> esc_html__('Heading 3', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '1.667em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> 'normal',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '1.120em'
			)
		);
		bestdeals_add_custom_font('h4', array(
			'title'			=> esc_html__('Heading 4', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '1.333em',
			'font-weight'	=> '400',
			'font-style'	=> 'i',
			'line-height'	=> 'normal',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '1.400em'
			)
		);
		bestdeals_add_custom_font('h5', array(
			'title'			=> esc_html__('Heading 5', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '1.067em',
			'font-weight'	=> '700',
			'font-style'	=> '',
			'line-height'	=> 'normal',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '1.750em'
			)
		);
		bestdeals_add_custom_font('h6', array(
			'title'			=> esc_html__('Heading 6', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '1.000em',
			'font-weight'	=> '400',
			'font-style'	=> 'i',
			'line-height'	=> '1.867em',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '0em'
			)
		);
		bestdeals_add_custom_font('p', array(
			'title'			=> esc_html__('Text', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '0.938em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.867em',
			'margin-top'	=> '',
			'margin-bottom'	=> '1.867em'
			)
		);
		bestdeals_add_custom_font('link', array(
			'title'			=> esc_html__('Links', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> ''
			)
		);
		bestdeals_add_custom_font('info', array(
			'title'			=> esc_html__('Post info', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> 'i',
			'line-height'	=> '1.3em',
			'margin-top'	=> '',
			'margin-bottom'	=> '1.5em'
			)
		);
		bestdeals_add_custom_font('menu', array(
			'title'			=> esc_html__('Main menu items', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '0.867em',
			'font-weight'	=> '700',
			'font-style'	=> '',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '0em'
			)
		);
		bestdeals_add_custom_font('submenu', array(
			'title'			=> esc_html__('Dropdown menu items', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '',
			'margin-bottom'	=> ''
			)
		);
		bestdeals_add_custom_font('logo', array(
			'title'			=> esc_html__('Logo', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '2.667em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '0.900em',
			'margin-top'	=> '0em',
			'margin-bottom'	=> '0em'
			)
		);
		bestdeals_add_custom_font('button', array(
			'title'			=> esc_html__('Buttons', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> 'PT Serif',
			'font-size' 	=> '0.867em',
			'font-weight'	=> '700',
			'font-style'	=> '',
			'line-height'	=> 'normal'
			)
		);
		bestdeals_add_custom_font('input', array(
			'title'			=> esc_html__('Input fields', 'bestdeals'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.3em'
			)
		);

	}
}





//------------------------------------------------------------------------------
// Skin's fonts
//------------------------------------------------------------------------------

// Add skin fonts in the used fonts list
if (!function_exists('bestdeals_filter_skin_used_fonts')) {
	//Handler of add_filter('bestdeals_filter_used_fonts', 'bestdeals_filter_skin_used_fonts');
	function bestdeals_filter_skin_used_fonts($theme_fonts) {
		return $theme_fonts;
	}
}

// Add skin fonts (from Google fonts) in the main fonts list (if not present).
// To use custom font-face you not need add it into list in this function
// How to install custom @font-face fonts into the theme?
// All @font-face fonts are located in "theme_name/css/font-face/" folder in the separate subfolders for the each font. Subfolder name is a font-family name!
// Place full set of the font files (for each font style and weight) and css-file named stylesheet.css in the each subfolder.
// Create your @font-face kit by using Fontsquirrel @font-face Generator (http://www.fontsquirrel.com/fontface/generator)
// and then extract the font kit (with folder in the kit) into the "theme_name/css/font-face" folder to install
if (!function_exists('bestdeals_filter_skin_list_fonts')) {
	//Handler of add_filter('bestdeals_filter_list_fonts', 'bestdeals_filter_skin_list_fonts');
	function bestdeals_filter_skin_list_fonts($list) {
		if (!isset($list['Lato']))	$list['Lato'] = array('family'=>'sans-serif');
		return $list;
	}
}



//------------------------------------------------------------------------------
// Skin's stylesheets
//------------------------------------------------------------------------------
// Add skin stylesheets
if (!function_exists('bestdeals_action_skin_add_styles')) {
	//Handler of add_action('bestdeals_action_add_styles', 'bestdeals_action_skin_add_styles');
	function bestdeals_action_skin_add_styles() {
		// Add stylesheet files
		wp_enqueue_style( 'bestdeals-skin-style', bestdeals_get_file_url('skin.css'), array(), null );
		if (file_exists(bestdeals_get_file_dir('skin.customizer.css')))
			wp_enqueue_style( 'bestdeals-skin-customizer-style', bestdeals_get_file_url('skin.customizer.css'), array(), null );
		if (file_exists(bestdeals_get_file_dir('skins.shortcodes.css')))
			wp_enqueue_style( 'bestdeals-skin-shortcodes-style', bestdeals_get_file_url('skins.shortcodes.css'), array(), null );
	}
}

// Add skin inline styles
if (!function_exists('bestdeals_filter_skin_add_styles_inline')) {
	//Handler of add_filter('bestdeals_filter_add_styles_inline', 'bestdeals_filter_skin_add_styles_inline');
	function bestdeals_filter_skin_add_styles_inline($custom_style) {
		// Todo: add skin specific styles in the $custom_style to override
		return $custom_style;	
	}
}

// Add skin responsive styles
if (!function_exists('bestdeals_action_skin_add_responsive')) {
	//Handler of add_action('bestdeals_action_add_responsive', 'bestdeals_action_skin_add_responsive');
	function bestdeals_action_skin_add_responsive() {
		$suffix = bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_outer')) ? '' : '-outer';
		if (file_exists(bestdeals_get_file_dir('skin.responsive'.($suffix).'.css'))) 
			wp_enqueue_style( 'bestdeals-skin-responsive-style', bestdeals_get_file_url('skin.responsive'.($suffix).'.css'), array(), null );
	}
}

// Add skin responsive inline styles
if (!function_exists('bestdeals_filter_skin_add_responsive_inline')) {
	//Handler of add_filter('bestdeals_filter_add_responsive_inline', 'bestdeals_filter_skin_add_responsive_inline');
	function bestdeals_filter_skin_add_responsive_inline($custom_style) {
		return $custom_style;	
	}
}

// Add skin.less into list files for compilation
if (!function_exists('bestdeals_filter_skin_compile_less')) {
	//Handler of add_filter('bestdeals_filter_compile_less', 'bestdeals_filter_skin_compile_less');
	function bestdeals_filter_skin_compile_less($files) {
		if (file_exists(bestdeals_get_file_dir('skin.less'))) {
		 	$files[] = bestdeals_get_file_dir('skin.less');
		}
		return $files;	
	}
}



//------------------------------------------------------------------------------
// Skin's scripts
//------------------------------------------------------------------------------

// Add skin scripts
if (!function_exists('bestdeals_action_skin_add_scripts')) {
	//Handler of add_action('bestdeals_action_add_scripts', 'bestdeals_action_skin_add_scripts');
	function bestdeals_action_skin_add_scripts() {
		if (file_exists(bestdeals_get_file_dir('skin.js')))
			wp_enqueue_script( 'bestdeals-skin-script', bestdeals_get_file_url('skin.js'), array(), null, true );
		if (bestdeals_get_theme_option('show_theme_customizer') == 'yes' && file_exists(bestdeals_get_file_dir('skin.customizer.js')))
			wp_enqueue_script( 'bestdeals-skin-customizer-script', bestdeals_get_file_url('skin.customizer.js'), array(), null, true );
		
	}
}

?>