<?php
/* Theme setup section
-------------------------------------------------------------------- */


// ONLY FOR PROGRAMMERS, NOT FOR CUSTOMER
// Framework settings
bestdeals_set_global('settings', array(
	
	'less_compiler'		=> 'lessc',								// no|lessc|less - Compiler for the .less
																// lessc - fast & low memory required, but .less-map, shadows & gradients not supprted
																// less  - slow, but support all features
	'less_nested'		=> false,								// Use nested selectors when compiling less - increase .css size, but allow using nested color schemes
	'less_prefix'		=> '',									// any string - Use prefix before each selector when compile less. For example: 'html '
	'less_separator'	=> '', //'/*---LESS_SEPARATOR---*/',	// string - separator inside .less file to split it when compiling to reduce memory usage
																// (compilation speed gets a bit slow)
	'less_map'			=> 'internal',							// no|internal|external - Generate map for .less files. 
																// Warning! You need more then 128Mb for PHP scripts on your server! Supported only if less_compiler=less (see above)
	
	'customizer_demo'	=> true,								// Show color customizer demo (if many color settings) or not (if only accent colors used)

	'allow_fullscreen'	=> false,								// Allow fullscreen and fullwide body styles

	'socials_type'		=> 'icons',								// images|icons - Use this kind of pictograms for all socials: share, social profiles, team members socials, etc.
	'slides_type'		=> 'images'								// images|bg - Use image as slide's content or as slide's background
	)
);



// Default Theme Options
if ( !function_exists( 'bestdeals_options_settings_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_options_settings_theme_setup', 2 );	// Priority 1 for add bestdeals_filter handlers
	function bestdeals_options_settings_theme_setup() {
		global $BESTDEALS_GLOBALS;

		// Settings 
		$socials_type = bestdeals_get_theme_setting('socials_type');
				
		// Prepare arrays 
		$BESTDEALS_GLOBALS['options_params'] = array(
			'list_fonts'		=> array('$bestdeals_get_list_fonts' => ''),
			'list_fonts_styles'	=> array('$bestdeals_get_list_fonts_styles' => ''),
			'list_socials' 		=> array('$bestdeals_get_list_socials' => ''),
			'list_icons' 		=> array('$bestdeals_get_list_icons' => ''),
			'list_posts_types' 	=> array('$bestdeals_get_list_posts_types' => ''),
			'list_categories' 	=> array('$bestdeals_get_list_categories' => ''),
			'list_menus'		=> array('$bestdeals_get_list_menus' => ''),
			'list_sidebars'		=> array('$bestdeals_get_list_sidebars' => ''),
			'list_positions' 	=> array('$bestdeals_get_list_sidebars_positions' => ''),
			'list_skins'		=> array('$bestdeals_get_list_skins' => ''),
			'list_color_schemes'=> array('$bestdeals_get_list_color_schemes' => ''),
			'list_bg_tints'		=> array('$bestdeals_get_list_bg_tints' => ''),
			'list_body_styles'	=> array('$bestdeals_get_list_body_styles' => ''),
			'list_header_styles'=> array('$bestdeals_get_list_templates_header' => ''),
			'list_blog_styles'	=> array('$bestdeals_get_list_templates_blog' => ''),
			'list_single_styles'=> array('$bestdeals_get_list_templates_single' => ''),
			'list_article_styles'=> array('$bestdeals_get_list_article_styles' => ''),
			'list_animations_in' => array('$bestdeals_get_list_animations_in' => ''),
			'list_animations_out'=> array('$bestdeals_get_list_animations_out' => ''),
			'list_filters'		=> array('$bestdeals_get_list_portfolio_filters' => ''),
			'list_hovers'		=> array('$bestdeals_get_list_hovers' => ''),
			'list_hovers_dir'	=> array('$bestdeals_get_list_hovers_directions' => ''),
			'list_sliders' 		=> array('$bestdeals_get_list_sliders' => ''),
			'list_revo_sliders'	=> array('$bestdeals_get_list_revo_sliders' => ''),
			'list_bg_image_positions' => array('$bestdeals_get_list_bg_image_positions' => ''),
			'list_popups' 		=> array('$bestdeals_get_list_popup_engines' => ''),
			'list_gmap_styles' 	=> array('$bestdeals_get_list_googlemap_styles' => ''),
			'list_yes_no' 		=> array('$bestdeals_get_list_yesno' => ''),
			'list_on_off' 		=> array('$bestdeals_get_list_onoff' => ''),
			'list_show_hide' 	=> array('$bestdeals_get_list_showhide' => ''),
			'list_sorting' 		=> array('$bestdeals_get_list_sortings' => ''),
			'list_ordering' 	=> array('$bestdeals_get_list_orderings' => ''),
			'list_locations' 	=> array('$bestdeals_get_list_dedicated_locations' => '')
			);


		// Theme options array
		$BESTDEALS_GLOBALS['options'] = array(

		
		//###############################
		//#### Customization         #### 
		//###############################
		'partition_customization' => array(
					'title' => esc_html__('Customization', 'bestdeals'),
					"start" => "partitions",
					"override" => "property_group,category,services_group,page,post",
					"icon" => "iconadmin-cog-alt",
					"type" => "partition"
					),
		
		
		// Customization -> Body Style
		//-------------------------------------------------
		
		'customization_body' => array(
					'title' => esc_html__('Body style', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"icon" => 'iconadmin-picture',
					"start" => "customization_tabs",
					"type" => "tab"
					),
		
		'info_body_1' => array(
					'title' => esc_html__('Body parameters', 'bestdeals'),
					"desc" => esc_attr__('Select body style, skin and color scheme for entire site. You can override this parameters on any page, post or category', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"
					),

		'body_style' => array(
					'title' => esc_html__('Body style', 'bestdeals'),
					"desc" => esc_attr__('Select body style:', 'bestdeals')
								. ' <br>' . wp_kses( __('<b>boxed</b> - if you want use background color and/or image', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags'])
								. ',<br>' . wp_kses( __('<b>wide</b> - page fill whole window with centered content', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags'])
								. (bestdeals_get_theme_setting('allow_fullscreen') 
									? ',<br>' . wp_kses( __('<b>fullwide</b> - page content stretched on the full width of the window (with few left and right paddings)', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags'])
									: '')
								. (bestdeals_get_theme_setting('allow_fullscreen') 
									? ',<br>' . wp_kses( __('<b>fullscreen</b> - page content fill whole window without any paddings', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']) 
									: ''),
					"override" => "property_group,category,services_group,post,page",
					"std" => "wide",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_body_styles'],
					"dir" => "horizontal",
					"type" => "radio"
					),
		
		'body_paddings' => array(
					'title' => esc_html__('Page paddings', 'bestdeals'),
					"desc" => esc_attr__('Add paddings above and below the page content', 'bestdeals'),
					"override" => "post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		'theme_skin' => array(
					'title' => esc_html__('Select theme skin', 'bestdeals'),
					"desc" => esc_attr__('Select skin for the theme decoration', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "bestdeals",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_skins'],
					"type" => "select"
					),

		"body_scheme" => array(
					'title' => esc_html__('Color scheme', 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the entire page', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),
		
		'body_filled' => array(
					'title' => esc_html__('Fill body', 'bestdeals'),
					"desc" => esc_attr__('Fill the page background with the solid color or leave it transparend to show background image (or video background)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		'info_body_2' => array(
					'title' => esc_html__('Background color and image', 'bestdeals'),
					"desc" => esc_attr__('Color and image for the site background', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"
					),

		'bg_custom' => array(
					'title' => esc_html__('Use custom background',  'bestdeals'),
					"desc" => esc_attr__("Use custom color and/or image as the site background", 'bestdeals'),
					"override" => "property_group,property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		
		'bg_color' => array(
					'title' => esc_html__('Background color',  'bestdeals'),
					"desc" => esc_attr__('Body background color',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "#ffffff",
					"type" => "color"
					),

		'bg_pattern' => array(
					'title' => esc_html__('Background predefined pattern',  'bestdeals'),
					"desc" => esc_attr__('Select theme background pattern (first case - without pattern)',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"options" => array(
						0 => bestdeals_get_file_url('images/spacer.png'),
						1 => bestdeals_get_file_url('images/bg/pattern_1.jpg'),
						2 => bestdeals_get_file_url('images/bg/pattern_2.jpg'),
						3 => bestdeals_get_file_url('images/bg/pattern_3.jpg'),
						4 => bestdeals_get_file_url('images/bg/pattern_4.jpg'),
						5 => bestdeals_get_file_url('images/bg/pattern_5.jpg')
					),
					"style" => "list",
					"type" => "images"
					),
		
		'bg_pattern_custom' => array(
					'title' => esc_html__('Background custom pattern',  'bestdeals'),
					"desc" => esc_attr__('Select or upload background custom pattern. If selected - use it instead the theme predefined pattern (selected in the field above)',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"type" => "media"
					),
		
		'bg_image' => array(
					'title' => esc_html__('Background predefined image',  'bestdeals'),
					"desc" => esc_attr__('Select theme background image (first case - without image)',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						0 => bestdeals_get_file_url('images/spacer.png'),
						1 => bestdeals_get_file_url('images/bg/image_1_thumb.jpg'),
						2 => bestdeals_get_file_url('images/bg/image_2_thumb.jpg'),
						3 => bestdeals_get_file_url('images/bg/image_3_thumb.jpg')
					),
					"style" => "list",
					"type" => "images"
					),
		
		'bg_image_custom' => array(
					'title' => esc_html__('Background custom image',  'bestdeals'),
					"desc" => esc_attr__('Select or upload background custom image. If selected - use it instead the theme predefined image (selected in the field above)',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"std" => "",
					"type" => "media"
					),
		
		'bg_image_custom_position' => array( 
					'title' => esc_html__('Background custom image position',  'bestdeals'),
					"desc" => esc_attr__('Select custom image position',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "left_top",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						'left_top' => "Left Top",
						'center_top' => "Center Top",
						'right_top' => "Right Top",
						'left_center' => "Left Center",
						'center_center' => "Center Center",
						'right_center' => "Right Center",
						'left_bottom' => "Left Bottom",
						'center_bottom' => "Center Bottom",
						'right_bottom' => "Right Bottom",
					),
					"type" => "select"
					),
		
		'bg_image_load' => array(
					'title' => esc_html__('Load background image', 'bestdeals'),
					"desc" => esc_attr__('Always load background images or only for boxed body style', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "boxed",
					"size" => "medium",
					"dependency" => array(
						'bg_custom' => array('yes')
					),
					"options" => array(
						'boxed' => esc_html__('Boxed', 'bestdeals'),
						'always' => esc_html__('Always', 'bestdeals')
					),
					"type" => "switch"
					),

		
		'info_body_3' => array(
					'title' => esc_html__('Video background', 'bestdeals'),
					"desc" => esc_attr__('Parameters of the video, used as site background', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"
					),

		'show_video_bg' => array(
					'title' => esc_html__('Show video background',  'bestdeals'),
					"desc" => esc_attr__("Show video as the site background", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		'video_bg_youtube_code' => array(
					'title' => esc_html__('Youtube code for video bg',  'bestdeals'),
					"desc" => esc_attr__("Youtube code of video", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"std" => "",
					"type" => "text"
					),

		'video_bg_url' => array(
					'title' => esc_html__('Local video for video bg',  'bestdeals'),
					"desc" => esc_attr__("URL to video-file (uploaded on your site)", 'bestdeals'),
					"readonly" =>false,
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"before" => array(	'title' => esc_html__('Choose video', 'bestdeals'),
										'action' => 'media_upload',
										'multiple' => false,
										'linked_field' => '',
										'type' => 'video',
										'captions' => array('choose' => esc_html__( 'Choose Video', 'bestdeals'),
															'update' => esc_html__( 'Select Video', 'bestdeals')
														)
								),
					"std" => "",
					"type" => "media"
					),

		'video_bg_overlay' => array(
					'title' => esc_html__('Use overlay for video bg', 'bestdeals'),
					"desc" => esc_attr__('Use overlay texture for the video background', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_video_bg' => array('yes')
					),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		
		
		
		
		
		// Customization -> Header
		//-------------------------------------------------
		
		'customization_header' => array(
					'title' => esc_html__("Header", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"icon" => 'iconadmin-window',
					"type" => "tab"),
		
		"info_header_1" => array(
					'title' => esc_html__('Top panel', 'bestdeals'),
					"desc" => esc_attr__('Top panel settings. It include user menu area (with contact info, cart button, language selector, login/logout menu and user menu) and main menu area (with logo and main menu).', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"),
		
		"top_panel_style" => array(
					'title' => esc_html__('Top panel style', 'bestdeals'),
					"desc" => esc_attr__('Select desired style of the page header', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "header_1",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_header_styles'],
					"style" => "list",
					"type" => "images"),

		"top_panel_image" => array(
					'title' => esc_html__('Top panel image', 'bestdeals'),
					"desc" => esc_attr__('Select default background image of the page header (if not single post or featured image for current post is not specified)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'top_panel_style' => array('header_7')
					),
					"std" => "",
					"type" => "media"),
		
		"top_panel_position" => array( 
					'title' => esc_html__('Top panel position', 'bestdeals'),
					"desc" => esc_attr__('Select position for the top panel with logo and main menu', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "above",
					"options" => array(
						'hide'  => esc_html__('Hide', 'bestdeals'),
						'above' => esc_html__('Above slider', 'bestdeals'),
						'below' => esc_html__('Below slider', 'bestdeals'),
						'over'  => esc_html__('Over slider', 'bestdeals')
					),
					"type" => "checklist"),

		"top_panel_scheme" => array(
					'title' => esc_html__('Top panel color scheme', 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the top panel', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),
		
		"show_page_title" => array(
					'title' => esc_html__('Show Page title', 'bestdeals'),
					"desc" => esc_attr__('Show post/page/category title', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_breadcrumbs" => array(
					'title' => esc_html__('Show Breadcrumbs', 'bestdeals'),
					"desc" => esc_attr__('Show path to current category (post, page)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"breadcrumbs_max_level" => array(
					'title' => esc_html__('Breadcrumbs max nesting', 'bestdeals'),
					"desc" => esc_attr__("Max number of the nested categories in the breadcrumbs (0 - unlimited)", 'bestdeals'),
					"dependency" => array(
						'show_breadcrumbs' => array('yes')
					),
					"std" => "0",
					"min" => 0,
					"max" => 100,
					"step" => 1,
					"type" => "spinner"),

		
		
		
		"info_header_2" => array( 
					'title' => esc_html__('Main menu style and position', 'bestdeals'),
					"desc" => esc_attr__('Select the Main menu style and position', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"),
		
		"menu_main" => array( 
					'title' => esc_html__('Select main menu',  'bestdeals'),
					"desc" => esc_attr__('Select main menu for the current page',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "default",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_menus'],
					"type" => "select"),
		
		"menu_attachment" => array( 
					'title' => esc_html__('Main menu attachment', 'bestdeals'),
					"desc" => esc_attr__('Attach main menu to top of window then page scroll down', 'bestdeals'),
					"std" => "fixed",
					"options" => array(
						"fixed"=>esc_html__("Fix menu position", 'bestdeals'), 
						"none"=>esc_html__("Don't fix menu position", 'bestdeals')
					),
					"dir" => "vertical",
					"type" => "radio"),

		"menu_slider" => array( 
					'title' => esc_html__('Main menu slider', 'bestdeals'),
					"desc" => esc_attr__('Use slider background for main menu items', 'bestdeals'),
					"std" => "yes",
					"type" => "switch",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no']),

		"menu_animation_in" => array( 
					'title' => esc_html__('Submenu show animation', 'bestdeals'),
					"desc" => esc_attr__('Select animation to show submenu ', 'bestdeals'),
					"std" => "bounceIn",
					"type" => "select",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_animations_in']),

		"menu_animation_out" => array( 
					'title' => esc_html__('Submenu hide animation', 'bestdeals'),
					"desc" => esc_attr__('Select animation to hide submenu ', 'bestdeals'),
					"std" => "fadeOutDown",
					"type" => "select",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_animations_out']),
		
		"menu_relayout" => array( 
					'title' => esc_html__('Main menu relayout', 'bestdeals'),
					"desc" => esc_attr__('Allow relayout main menu if window width less then this value', 'bestdeals'),
					"std" => 960,
					"min" => 320,
					"max" => 1024,
					"type" => "spinner"),
		
		"menu_responsive" => array( 
					'title' => esc_html__('Main menu responsive', 'bestdeals'),
					"desc" => esc_attr__('Allow responsive version for the main menu if window width less then this value', 'bestdeals'),
					"std" => 640,
					"min" => 320,
					"max" => 1024,
					"type" => "spinner"),
		
		"menu_width" => array( 
					'title' => esc_html__('Submenu width', 'bestdeals'),
					"desc" => esc_attr__('Width for dropdown menus in main menu', 'bestdeals'),
					"step" => 5,
					"std" => "",
					"min" => 180,
					"max" => 300,
					"mask" => "?999",
					"type" => "spinner"),
		
		
		
		"info_header_3" => array(
					'title' => esc_html__("User's menu area components", 'bestdeals'),
					"desc" => esc_attr__("Select parts for the user's menu area", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
		
		"show_top_panel_top" => array(
					'title' => esc_html__('Show user menu area', 'bestdeals'),
					"desc" => esc_attr__('Show user menu area on top of page', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"menu_user" => array(
					'title' => esc_html__('Select user menu',  'bestdeals'),
					"desc" => esc_attr__('Select user menu for the current page',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_top_panel_top' => array('yes')
					),
					"std" => "default",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_menus'],
					"type" => "select"),
		
		"show_languages" => array(
					'title' => esc_html__('Show language selector', 'bestdeals'),
					"desc" => esc_attr__('Show language selector in the user menu (if WPML plugin installed and current page/post has multilanguage version)', 'bestdeals'),
					"dependency" => array(
						'show_top_panel_top' => array('yes')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_login" => array( 
					'title' => esc_html__('Show Login/Logout buttons', 'bestdeals'),
					"desc" => esc_attr__('Show Login and Logout buttons in the user menu area', 'bestdeals'),
					"dependency" => array(
						'show_top_panel_top' => array('yes')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_bookmarks" => array(
					'title' => esc_html__('Show bookmarks', 'bestdeals'),
					"desc" => esc_attr__('Show bookmarks selector in the user menu', 'bestdeals'),
					"dependency" => array(
						'show_top_panel_top' => array('yes')
					),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"info_header_4" => array( 
					'title' => esc_html__("Table of Contents (TOC)", 'bestdeals'),
					"desc" => esc_attr__("Table of Contents for the current page. Automatically created if the page contains objects with id starting with 'toc_'", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
		
		"menu_toc" => array( 
					'title' => esc_html__('TOC position', 'bestdeals'),
					"desc" => esc_attr__('Show TOC for the current page', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "float",
					"options" => array(
						'hide'  => esc_html__('Hide', 'bestdeals'),
						'fixed' => esc_html__('Fixed', 'bestdeals'),
						'float' => esc_html__('Float', 'bestdeals')
					),
					"type" => "checklist"),
		
		"menu_toc_home" => array(
					'title' => esc_html__('Add "Home" into TOC', 'bestdeals'),
					"desc" => esc_attr__('Automatically add "Home" item into table of contents - return to home page of the site', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'menu_toc' => array('fixed','float')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"menu_toc_top" => array( 
					'title' => esc_html__('Add "To Top" into TOC', 'bestdeals'),
					"desc" => esc_attr__('Automatically add "To Top" item into table of contents - scroll to top of the page', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'menu_toc' => array('fixed','float')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		
		
		
		'info_header_5' => array(
					'title' => esc_html__('Main logo', 'bestdeals'),
					"desc" => esc_attr__("Select or upload logos for the site's header and select it position", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"
					),

		'logo' => array(
					'title' => esc_html__('Logo image', 'bestdeals'),
					"desc" => esc_attr__('Main logo image', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "",
					"type" => "media"
					),

		'logo_text' => array(
					'title' => esc_html__('Logo text', 'bestdeals'),
					"desc" => esc_attr__('Logo text - display it after logo image', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => wp_kses( __('<em>Best</em> Deals', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"type" => "text"
					),

		'logo_slogan' => array(
					'title' => esc_html__('Logo slogan', 'bestdeals'),
					"desc" => esc_attr__('Logo slogan - display it under logo image (instead the site tagline)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => 'Real Estate services',
					"type" => "text"
					),

		'logo_height' => array(
					'title' => esc_html__('Logo height', 'bestdeals'),
					"desc" => esc_attr__('Height for the logo in the header area', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"step" => 1,
					"std" => '',
					"min" => 10,
					"max" => 300,
					"mask" => "?999",
					"type" => "spinner"
					),

		'logo_offset' => array(
					'title' => esc_html__('Logo top offset', 'bestdeals'),
					"desc" => esc_attr__('Top offset for the logo in the header area', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"step" => 1,
					"std" => '',
					"min" => 0,
					"max" => 99,
					"mask" => "?99",
					"type" => "spinner"
					),
		
		
		
		
		
		
		
		// Customization -> Slider
		//-------------------------------------------------
		
		"customization_slider" => array( 
					'title' => esc_html__('Slider', 'bestdeals'),
					"icon" => "iconadmin-picture",
					"override" => "property_group,category,services_group,page",
					"type" => "tab"),
		
		"info_slider_1" => array(
					'title' => esc_html__('Main slider parameters', 'bestdeals'),
					"desc" => esc_attr__('Select parameters for main slider (you can override it in each category and page)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"type" => "info"),
					
		"show_slider" => array(
					'title' => esc_html__('Show Slider', 'bestdeals'),
					"desc" => esc_attr__('Do you want to show slider on each page (post)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_display" => array(
					'title' => esc_html__('Slider display', 'bestdeals'),
					"desc" => esc_attr__('How display slider: boxed (fixed width and height), fullwide (fixed height) or fullscreen', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => "fullwide",
					"options" => array(
						"boxed"=>__("Boxed", 'bestdeals'),
						"fullwide"=>__("Fullwide", 'bestdeals'),
						"fullscreen"=>__("Fullscreen", 'bestdeals')
					),
					"type" => "checklist"),
		
		"slider_height" => array(
					'title' => esc_html__("Height (in pixels)", 'bestdeals'),
					"desc" => esc_attr__("Slider height (in pixels) - only if slider display with fixed height.", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => '',
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"slider_engine" => array(
					'title' => esc_html__('Slider engine', 'bestdeals'),
					"desc" => esc_attr__('What engine use to show slider?', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes')
					),
					"std" => "revo",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sliders'],
					"type" => "radio"),
		
		"slider_alias" => array(
					'title' => esc_html__('Revolution Slider: Select slider',  'bestdeals'),
					"desc" => esc_attr__("Select slider to show (if engine=revo in the field above)", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('revo')
					),
					"std" => "",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_revo_sliders'],
					"type" => "select"),
		
		"slider_category" => array(
					'title' => esc_html__('Posts Slider: Category to show', 'bestdeals'),
					"desc" => esc_attr__('Select category to show in Flexslider (ignored for Revolution and Royal sliders)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "",
					"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals')), $BESTDEALS_GLOBALS['options_params']['list_categories']),
					"type" => "select",
					"multiple" => true,
					"style" => "list"),
		
		"slider_posts" => array(
					'title' => esc_html__('Posts Slider: Number posts or comma separated posts list',  'bestdeals'),
					"desc" => esc_attr__("How many recent posts display in slider or comma separated list of posts ID (in this case selected category ignored)", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "5",
					"type" => "text"),
		
		"slider_orderby" => array(
					'title' => esc_html__("Posts Slider: Posts order by",  'bestdeals'),
					"desc" => esc_attr__("Posts in slider ordered by date (default), comments, views, author rating, users rating, random or alphabetically", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "date",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sorting'],
					"type" => "select"),
		
		"slider_order" => array(
					'title' => esc_html__("Posts Slider: Posts order", 'bestdeals'),
					"desc" => esc_attr__('Select the desired ordering method for posts', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "desc",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
					
		"slider_interval" => array(
					'title' => esc_html__("Posts Slider: Slide change interval", 'bestdeals'),
					"desc" => esc_attr__("Interval (in ms) for slides change in slider", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => 7000,
					"min" => 100,
					"step" => 100,
					"type" => "spinner"),
		
		"slider_pagination" => array(
					'title' => esc_html__("Posts Slider: Pagination", 'bestdeals'),
					"desc" => esc_attr__("Choose pagination style for the slider", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "no",
					"options" => array(
						'no'   => esc_html__('None', 'bestdeals'),
						'yes'  => esc_html__('Dots', 'bestdeals'), 
						'over' => esc_html__('Titles', 'bestdeals')
					),
					"type" => "checklist"),
		
		"slider_infobox" => array(
					'title' => esc_html__("Posts Slider: Show infobox", 'bestdeals'),
					"desc" => esc_attr__("Do you want to show post's title, reviews rating and description on slides in slider", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "slide",
					"options" => array(
						'no'    => esc_html__('None',  'bestdeals'),
						'slide' => esc_html__('Slide', 'bestdeals'), 
						'fixed' => esc_html__('Fixed', 'bestdeals')
					),
					"type" => "checklist"),
					
		"slider_info_category" => array(
					'title' => esc_html__("Posts Slider: Show post's category", 'bestdeals'),
					"desc" => esc_attr__("Do you want to show post's category on slides in slider", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_info_reviews" => array(
					'title' => esc_html__("Posts Slider: Show post's reviews rating", 'bestdeals'),
					"desc" => esc_attr__("Do you want to show post's reviews rating on slides in slider", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"slider_info_descriptions" => array(
					'title' => esc_html__("Posts Slider: Show post's descriptions", 'bestdeals'),
					"desc" => esc_attr__("How many characters show in the post's description in slider. 0 - no descriptions", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_slider' => array('yes'),
						'slider_engine' => array('swiper')
					),
					"std" => 0,
					"min" => 0,
					"step" => 10,
					"type" => "spinner"),
		
		
		
		
		
		// Customization -> Sidebars
		//-------------------------------------------------
		
		"customization_sidebars" => array( 
					'title' => esc_html__('Sidebars', 'bestdeals'),
					"icon" => "iconadmin-indent-right",
					"override" => "property_group,category,services_group,post,page",
					"type" => "tab"),
		
		"info_sidebars_1" => array( 
					'title' => esc_html__('Custom sidebars', 'bestdeals'),
					"desc" => esc_attr__('In this section you can create unlimited sidebars. You can fill them with widgets in the menu Appearance - Widgets', 'bestdeals'),
					"type" => "info"),
		
		"custom_sidebars" => array(
					'title' => esc_html__('Custom sidebars',  'bestdeals'),
					"desc" => esc_attr__('Manage custom sidebars. You can use it with each category (page, post) independently',  'bestdeals'),
					"std" => "",
					"cloneable" => true,
					"type" => "text"),
		
		"info_sidebars_2" => array(
					'title' => esc_html__('Main sidebar', 'bestdeals'),
					"desc" => esc_attr__('Show / Hide and select main sidebar', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"),
		
		'show_sidebar_main' => array( 
					'title' => esc_html__('Show main sidebar',  'bestdeals'),
					"desc" => esc_attr__('Select position for the main sidebar or hide it',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "right",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_positions'],
					"dir" => "horizontal",
					"type" => "checklist"),

		"sidebar_main_scheme" => array(
					'title' => esc_html__("Color scheme", 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the main sidebar', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_main' => array('left', 'right')
					),
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),
		
		"sidebar_main" => array( 
					'title' => esc_html__('Select main sidebar',  'bestdeals'),
					"desc" => esc_attr__('Select main sidebar content',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_main' => array('left', 'right')
					),
					"std" => "sidebar_main",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sidebars'],
					"type" => "select"),
		
		"menu_side" => array(
					'title' => esc_html__('Select menu',  'bestdeals'),
					"desc" => esc_attr__('Select menu for the outer sidebar',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right'),
						'sidebar_outer_show_menu' => array('yes')
					),
					"std" => "default",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_menus'],
					"type" => "select"),
		
		"sidebar_outer_show_widgets" => array( 
					'title' => esc_html__('Show Widgets', 'bestdeals'),
					"desc" => esc_attr__('Show Widgets in the outer sidebar', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"sidebar_outer" => array( 
					'title' => esc_html__('Select outer sidebar',  'bestdeals'),
					"desc" => esc_attr__('Select outer sidebar content',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'sidebar_outer_show_widgets' => array('yes'),
						'show_sidebar_outer' => array('left', 'right')
					),
					"std" => "sidebar_outer",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sidebars'],
					"type" => "select"),
		
		
		
		
		// Customization -> Footer
		//-------------------------------------------------
		
		'customization_footer' => array(
					'title' => esc_html__("Footer", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"icon" => 'iconadmin-window',
					"type" => "tab"),
		
		
		"info_footer_1" => array(
					'title' => esc_html__("Footer components", 'bestdeals'),
					"desc" => esc_attr__("Select components of the footer, set style and put the content for the user's footer area", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
		
		"show_sidebar_footer" => array(
					'title' => esc_html__('Show footer sidebar', 'bestdeals'),
					"desc" => esc_attr__('Select style for the footer sidebar or hide it', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"sidebar_footer_scheme" => array(
					'title' => esc_html__("Color scheme", 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the footer', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),
		
		"sidebar_footer" => array( 
					'title' => esc_html__('Select footer sidebar',  'bestdeals'),
					"desc" => esc_attr__('Select footer sidebar for the blog page',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => "sidebar_footer",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sidebars'],
					"type" => "select"),
		
		"sidebar_footer_columns" => array( 
					'title' => esc_html__('Footer sidebar columns',  'bestdeals'),
					"desc" => esc_attr__('Select columns number for the footer sidebar',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_sidebar_footer' => array('yes')
					),
					"std" => 4,
					"min" => 1,
					"max" => 6,
					"type" => "spinner"),
		
		
		"info_footer_2" => array(
					'title' => esc_html__('Testimonials in Footer', 'bestdeals'),
					"desc" => esc_attr__('Select parameters for Testimonials in the Footer', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),

		"show_testimonials_in_footer" => array(
					'title' => esc_html__('Show Testimonials in footer', 'bestdeals'),
					"desc" => esc_attr__('Show Testimonials slider in footer. For correct operation of the slider (and shortcode testimonials) you must fill out Testimonials posts on the menu "Testimonials"', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"testimonials_scheme" => array(
					'title' => esc_html__("Color scheme", 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the testimonials area', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_testimonials_in_footer' => array('yes')
					),
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),

		"testimonials_count" => array( 
					'title' => esc_html__('Testimonials count', 'bestdeals'),
					"desc" => esc_attr__('Number testimonials to show', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_testimonials_in_footer' => array('yes')
					),
					"std" => 3,
					"step" => 1,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		
		"info_footer_3" => array(
					'title' => esc_html__('Newsletter parameters', 'bestdeals'),
					"desc" => esc_attr__('Select parameters for Newsletter (you can override it in each category and page)', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
			
		"newsletter_show" => array(
					'title' => esc_html__('Show Newsletter in footer', 'bestdeals'),
					"desc" => esc_attr__('Do you want to show Newsletter on each page (post)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
			

		"info_footer_4" => array(
					'title' => esc_html__('Google map parameters', 'bestdeals'),
					"desc" => esc_attr__('Select parameters for Google map (you can override it in each category and page)', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
					
		"show_googlemap" => array(
					'title' => esc_html__('Show Google Map', 'bestdeals'),
					"desc" => esc_attr__('Do you want to show Google map on each page (post)', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"googlemap_height" => array(
					'title' => esc_html__("Map height", 'bestdeals'),
					"desc" => esc_attr__("Map height (default - in pixels, allows any CSS units of measure)", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 353,
					"min" => 100,
					"step" => 10,
					"type" => "spinner"),
		
		"googlemap_address" => array(
					'title' => esc_html__('Address to show on map',  'bestdeals'),
					"desc" => esc_attr__("Enter address to show on map center", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "1, Central Road, Chicago",
					"type" => "text"),
		
		"googlemap_latlng" => array(
					'title' => esc_html__('Latitude and Longitude to show on map',  'bestdeals'),
					"desc" => esc_attr__("Enter coordinates (separated by comma) to show on map center (instead of address)", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_title" => array(
					'title' => esc_html__('Title to show on map',  'bestdeals'),
					"desc" => esc_attr__("Enter title to show on map center", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_description" => array(
					'title' => esc_html__('Description to show on map',  'bestdeals'),
					"desc" => esc_attr__("Enter description to show on map center", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => "",
					"type" => "text"),
		
		"googlemap_zoom" => array(
					'title' => esc_html__('Google map initial zoom',  'bestdeals'),
					"desc" => esc_attr__("Enter desired initial zoom for Google map", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 16,
					"min" => 1,
					"max" => 20,
					"step" => 1,
					"type" => "spinner"),
		
		"googlemap_style" => array(
					'title' => esc_html__('Google map style',  'bestdeals'),
					"desc" => esc_attr__("Select style to show Google map", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => 'style1',
					"options" => $BESTDEALS_GLOBALS['options_params']['list_gmap_styles'],
					"type" => "select"),
		
		"googlemap_marker" => array(
					'title' => esc_html__('Google map marker',  'bestdeals'),
					"desc" => esc_attr__("Select or upload png-image with Google map marker", 'bestdeals'),
					"dependency" => array(
						'show_googlemap' => array('yes')
					),
					"std" => '',
					"type" => "media"),
		
		
		
		"info_footer_5" => array(
					'title' => esc_html__("Contacts area", 'bestdeals'),
					"desc" => esc_attr__("Show/Hide contacts area in the footer", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
		
		"show_contacts_in_footer" => array(
					'title' => esc_html__('Show Contacts in footer', 'bestdeals'),
					"desc" => esc_attr__('Show contact information area in footer: site logo, contact info and large social icons', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"contacts_scheme" => array(
					'title' => esc_html__("Color scheme", 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the contacts area', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),

		'logo_footer' => array(
					'title' => esc_html__('Logo image for footer', 'bestdeals'),
					"desc" => esc_attr__('Logo image in the footer (in the contacts area)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"std" => "",
					"type" => "media"
					),
		
		'logo_footer_height' => array(
					'title' => esc_html__('Logo height', 'bestdeals'),
					"desc" => esc_attr__('Height for the logo in the footer area (in the contacts area)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_contacts_in_footer' => array('yes')
					),
					"step" => 1,
					"std" => 30,
					"min" => 10,
					"max" => 300,
					"mask" => "?999",
					"type" => "spinner"
					),
		
		
		
		"info_footer_6" => array(
					'title' => esc_html__("Copyright and footer menu", 'bestdeals'),
					"desc" => esc_attr__("Show/Hide copyright area in the footer", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),

		"show_copyright_in_footer" => array(
					'title' => esc_html__('Show Copyright area in footer', 'bestdeals'),
					"desc" => esc_attr__('Show area with copyright information, footer menu and small social icons in footer', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "socials",
					"options" => array(
						'none' => esc_html__('Hide', 'bestdeals'),
						'text' => esc_html__('Text', 'bestdeals'),
						'menu' => esc_html__('Text and menu', 'bestdeals'),
						'socials' => esc_html__('Text and Social icons', 'bestdeals')
					),
					"type" => "checklist"),

		"copyright_scheme" => array(
					'title' => esc_html__("Color scheme", 'bestdeals'),
					"desc" => esc_attr__('Select predefined color scheme for the copyright area', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_copyright_in_footer' => array('text', 'menu', 'socials')
					),
					"std" => "orange",
					"dir" => "horizontal",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_color_schemes'],
					"type" => "checklist"),
		
		"menu_footer" => array( 
					'title' => esc_html__('Select footer menu',  'bestdeals'),
					"desc" => esc_attr__('Select footer menu for the current page',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "default",
					"dependency" => array(
						'show_copyright_in_footer' => array('menu')
					),
					"options" => $BESTDEALS_GLOBALS['options_params']['list_menus'],
					"type" => "select"),

		"footer_copyright" => array(
					'title' => esc_html__('Footer copyright text',  'bestdeals'),
					"desc" => esc_attr__("Copyright text to show in footer area (bottom of site)", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'show_copyright_in_footer' => array('text', 'menu', 'socials')
					),
					"std" => wp_kses( __('<span class="textDark">AxiomThemes</span> &copy; {Y} All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>',  'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"rows" => "10",
					"type" => "editor"),
			
			
		"info_footer_7" => array(
					'title' => esc_html__('Custom shortcode box', 'bestdeals'),
					"desc" => '',
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),

		"show_cs_box_in_footer" => array(
					'title' => esc_html__('Show custom shortcode box in footer', 'bestdeals'),
					"desc" => '',
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
			
		'cs_box_code' => array(
					'title' => esc_html__('Your custom code',  'bestdeals'),
					"desc" => esc_attr__('Put here your code:',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_cs_box_in_footer' => array('yes')
					),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"),
			




		// Customization -> Other
		//-------------------------------------------------
		
		'customization_other' => array(
					'title' => esc_html__('Other', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"icon" => 'iconadmin-cog',
					"type" => "tab"
					),

		'info_other_1' => array(
					'title' => esc_html__('Theme customization other parameters', 'bestdeals'),
					"desc" => esc_attr__('Animation parameters and responsive layouts for the small screens', 'bestdeals'),
					"type" => "info"
					),

		'show_theme_customizer' => array(
					'title' => esc_html__('Show Theme customizer', 'bestdeals'),
					"desc" => esc_attr__('Do you want to show theme customizer in the right panel? Your website visitors will be able to customise it yourself.', 'bestdeals'),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		"customizer_demo" => array(
					'title' => esc_html__('Theme customizer panel demo time', 'bestdeals'),
					"desc" => esc_attr__('Timer for demo mode for the customizer panel (in milliseconds: 1000ms = 1s). If 0 - no demo.', 'bestdeals'),
					"dependency" => array(
						'show_theme_customizer' => array('yes')
					),
					"std" => "0",
					"min" => 0,
					"max" => 10000,
					"step" => 500,
					"type" => "spinner"),
		
		'css_animation' => array(
					'title' => esc_html__('Extended CSS animations', 'bestdeals'),
					"desc" => esc_attr__('Do you want use extended animations effects on your site?', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),

		'remember_visitors_settings' => array(
					'title' => esc_html__("Remember visitor's settings", 'bestdeals'),
					"desc" => esc_attr__('To remember the settings that were made by the visitor, when navigating to other pages or to limit their effect only within the current page', 'bestdeals'),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
					
		'responsive_layouts' => array(
					'title' => esc_html__('Responsive Layouts', 'bestdeals'),
					"desc" => esc_attr__('Do you want use responsive layouts on small screen or still use main layout?', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		


		'info_other_2' => array(
					'title' => esc_html__('Additional CSS and HTML/JS code', 'bestdeals'),
					"desc" => esc_attr__('Put here your custom CSS and JS code', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"
					),
					
		'custom_css_html' => array(
					'title' => esc_html__('Use custom CSS/HTML/JS', 'bestdeals'),
					"desc" => esc_attr__('Do you want use custom HTML/CSS/JS code in your site? For example: custom styles, Google Analitics code, etc.', 'bestdeals'),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"
					),
		
		"gtm_code" => array(
					'title' => esc_html__('Google tags manager or Google analitics code',  'bestdeals'),
					"desc" => esc_attr__('Put here Google Tags Manager (GTM) code from your account: Google analitics, remarketing, etc. This code will be placed after open body tag.',  'bestdeals'),
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"),
		
		"gtm_code2" => array(
					'title' => esc_html__('Google remarketing code',  'bestdeals'),
					"desc" => esc_attr__('Put here Google Remarketing code from your account. This code will be placed before close body tag.',  'bestdeals'),
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"divider" => false,
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"),
		
		'custom_code' => array(
					'title' => esc_html__('Your custom HTML/JS code',  'bestdeals'),
					"desc" => esc_attr__('Put here your invisible html/js code: Google analitics, counters, etc',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"
					),
		
		'custom_css' => array(
					'title' => esc_html__('Your custom CSS code',  'bestdeals'),
					"desc" => esc_attr__('Put here your css code to correct main theme styles',  'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'custom_css_html' => array('yes')
					),
					"divider" => false,
					"cols" => 80,
					"rows" => 20,
					"std" => "",
					"type" => "textarea"
					),
		
		
		
		
		
		
		
		
		
		//###############################
		//#### Blog and Single pages #### 
		//###############################
		"partition_blog" => array(
					'title' => esc_html__('Blog &amp; Single', 'bestdeals'),
					"icon" => "iconadmin-docs",
					"override" => "property_group,category,services_group,post,page",
					"type" => "partition"),
		
		
		
		// Blog -> Stream page
		//-------------------------------------------------
		
		'blog_tab_stream' => array(
					'title' => esc_html__('Stream page', 'bestdeals'),
					"start" => 'blog_tabs',
					"icon" => "iconadmin-docs",
					"override" => "property_group,category,services_group,post,page",
					"type" => "tab"),
		
		"info_blog_1" => array(
					'title' => esc_html__('Blog streampage parameters', 'bestdeals'),
					"desc" => esc_attr__('Select desired blog streampage parameters (you can override it in each category)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"type" => "info"),
		
		"blog_style" => array(
					'title' => esc_html__('Blog style', 'bestdeals'),
					"desc" => esc_attr__('Select desired blog style', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "excerpt",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_blog_styles'],
					"type" => "select"),
		
		"hover_style" => array(
					'title' => esc_html__('Hover style', 'bestdeals'),
					"desc" => esc_attr__('Select desired hover style (only for Blog style = Portfolio)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored')
					),
					"std" => "square effect_shift",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_hovers'],
					"type" => "select"),
		
		"hover_dir" => array(
					'title' => esc_html__('Hover dir', 'bestdeals'),
					"desc" => esc_attr__('Select hover direction (only for Blog style = Portfolio and Hover style = Circle or Square)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored'),
						'hover_style' => array('square','circle')
					),
					"std" => "left_to_right",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_hovers_dir'],
					"type" => "select"),
		
		"article_style" => array(
					'title' => esc_html__('Article style', 'bestdeals'),
					"desc" => esc_attr__('Select article display method: boxed or stretch', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "stretch",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_article_styles'],
					"size" => "medium",
					"type" => "switch"),
		
		"dedicated_location" => array(
					'title' => esc_html__('Dedicated location', 'bestdeals'),
					"desc" => esc_attr__('Select location for the dedicated content or featured image in the "excerpt" blog style', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"dependency" => array(
						'blog_style' => array('excerpt')
					),
					"std" => "default",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_locations'],
					"type" => "select"),
		
		"show_filters" => array(
					'title' => esc_html__('Show filters', 'bestdeals'),
					"desc" => esc_attr__('What taxonomy use for filter buttons', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_style' => array('portfolio','grid','square','colored')
					),
					"std" => "hide",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_filters'],
					"type" => "checklist"),
		
		"blog_sort" => array(
					'title' => esc_html__('Blog posts sorted by', 'bestdeals'),
					"desc" => esc_attr__('Select the desired sorting method for posts', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "date",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sorting'],
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_order" => array(
					'title' => esc_html__('Blog posts order', 'bestdeals'),
					"desc" => esc_attr__('Select the desired ordering method for posts', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "desc",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
		
		"posts_per_page" => array(
					'title' => esc_html__('Blog posts per page',  'bestdeals'),
					"desc" => esc_attr__('How many posts display on blog pages for selected style. If empty or 0 - inherit system wordpress settings',  'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "12",
					"mask" => "?99",
					"type" => "text"),
		
		"post_excerpt_maxlength" => array(
					'title' => esc_html__('Excerpt maxlength for streampage',  'bestdeals'),
					"desc" => esc_attr__('How many characters from post excerpt are display in blog streampage (only for Blog style = Excerpt). 0 - do not trim excerpt.',  'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_style' => array('excerpt', 'portfolio', 'grid', 'square', 'related')
					),
					"std" => "250",
					"mask" => "?9999",
					"type" => "text"),
		
		"post_excerpt_maxlength_masonry" => array(
					'title' => esc_html__('Excerpt maxlength for classic and masonry',  'bestdeals'),
					"desc" => esc_attr__('How many characters from post excerpt are display in blog streampage (only for Blog style = Classic or Masonry). 0 - do not trim excerpt.',  'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_style' => array('masonry', 'classic')
					),
					"std" => "150",
					"mask" => "?9999",
					"type" => "text"),
		
		
		
		
		// Blog -> Single page
		//-------------------------------------------------
		
		'blog_tab_single' => array(
					'title' => esc_html__('Single page', 'bestdeals'),
					"icon" => "iconadmin-doc",
					"override" => "property_group,category,services_group,post,page",
					"type" => "tab"),
		
		
		"info_single_1" => array(
					'title' => esc_html__('Single (detail) pages parameters', 'bestdeals'),
					"desc" => esc_attr__('Select desired parameters for single (detail) pages (you can override it in each category and single post (page))', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"type" => "info"),
		
		"single_style" => array(
					'title' => esc_html__('Single page style', 'bestdeals'),
					"desc" => esc_attr__('Select desired style for single page', 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"std" => "single-standard",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_single_styles'],
					"dir" => "horizontal",
					"type" => "radio"),

		"icon" => array(
					'title' => esc_html__('Select post icon', 'bestdeals'),
					"desc" => esc_attr__('Select icon for output before post/category name in some layouts', 'bestdeals'),
					"override" => "services_group,page,post",
					"std" => "",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_icons'],
					"style" => "select",
					"type" => "icons"
					),
		
		"show_featured_image" => array(
					'title' => esc_html__('Show featured image before post',  'bestdeals'),
					"desc" => esc_attr__("Show featured image (if selected) before post content on single pages", 'bestdeals'),
					"override" => "property_group,category,services_group,page,post",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_title" => array(
					'title' => esc_html__('Show post title', 'bestdeals'),
					"desc" => esc_attr__('Show area with post title on single pages', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_title_on_quotes" => array(
					'title' => esc_html__('Show post title on links, chat, quote, status', 'bestdeals'),
					"desc" => esc_attr__('Show area with post title on single and blog pages in specific post formats: links, chat, quote, status', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_info" => array(
					'title' => esc_html__('Show post info', 'bestdeals'),
					"desc" => esc_attr__('Show area with post info on single pages', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_text_before_readmore" => array(
					'title' => esc_html__('Show text before "Read more" tag', 'bestdeals'),
					"desc" => esc_attr__('Show text before "Read more" tag on single pages', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
					
		"show_post_author" => array(
					'title' => esc_html__('Show post author details',  'bestdeals'),
					"desc" => esc_attr__("Show post author information block on single post page", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_tags" => array(
					'title' => esc_html__('Show post tags',  'bestdeals'),
					"desc" => esc_attr__("Show tags block on single post page", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		"show_post_categories" => array(
					'title' => esc_html__('Show post categories',  'bestdeals'),
					"desc" => esc_attr__("Show categories block on single post page", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"show_post_related" => array(
					'title' => esc_html__('Show related posts',  'bestdeals'),
					"desc" => esc_attr__("Show related posts block on single post page", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"post_related_count" => array(
					'title' => esc_html__('Related posts number',  'bestdeals'),
					"desc" => esc_attr__("How many related posts showed on single post page", 'bestdeals'),
					"dependency" => array(
						'show_post_related' => array('yes')
					),
					"override" => "property_group,category,services_group,post,page",
					"std" => "2",
					"step" => 1,
					"min" => 2,
					"max" => 8,
					"type" => "spinner"),

		"post_related_columns" => array(
					'title' => esc_html__('Related posts columns',  'bestdeals'),
					"desc" => esc_attr__("How many columns used to show related posts on single post page. 1 - use scrolling to show all related posts", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'show_post_related' => array('yes')
					),
					"std" => "2",
					"step" => 1,
					"min" => 1,
					"max" => 4,
					"type" => "spinner"),
		
		"post_related_sort" => array(
					'title' => esc_html__('Related posts sorted by', 'bestdeals'),
					"desc" => esc_attr__('Select the desired sorting method for related posts', 'bestdeals'),
					"dependency" => array(
						'show_post_related' => array('yes')
					),
					"std" => "date",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_sorting'],
					"type" => "select"),
		
		"post_related_order" => array(
					'title' => esc_html__('Related posts order', 'bestdeals'),
					"desc" => esc_attr__('Select the desired ordering method for related posts', 'bestdeals'),
					"dependency" => array(
						'show_post_related' => array('yes')
					),
					"std" => "desc",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_ordering'],
					"size" => "big",
					"type" => "switch"),
		
		"show_post_comments" => array(
					'title' => esc_html__('Show comments',  'bestdeals'),
					"desc" => esc_attr__("Show comments block on single post page", 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		// Blog -> Other parameters
		//-------------------------------------------------
		
		'blog_tab_other' => array(
					'title' => esc_html__('Other parameters', 'bestdeals'),
					"icon" => "iconadmin-newspaper",
					"override" => "property_group,category,services_group,page",
					"type" => "tab"),
		
		"info_blog_other_1" => array(
					'title' => esc_html__('Other Blog parameters', 'bestdeals'),
					"desc" => esc_attr__('Select excluded categories, substitute parameters, etc.', 'bestdeals'),
					"type" => "info"),
		
		"exclude_cats" => array(
					'title' => esc_html__('Exclude categories', 'bestdeals'),
					"desc" => esc_attr__('Select categories, which posts are exclude from blog page', 'bestdeals'),
					"std" => "",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_categories'],
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"blog_pagination" => array(
					'title' => esc_html__('Blog pagination', 'bestdeals'),
					"desc" => esc_attr__('Select type of the pagination on blog streampages', 'bestdeals'),
					"std" => "pages",
					"override" => "property_group,category,services_group,page",
					"options" => array(
						'pages'    => esc_html__('Standard page numbers', 'bestdeals'),
						'viewmore' => esc_html__('"View more" button', 'bestdeals'),
						'infinite' => esc_html__('Infinite scroll', 'bestdeals')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_pagination_style" => array(
					'title' => esc_html__('Blog pagination style', 'bestdeals'),
					"desc" => esc_attr__('Select pagination style for standard page numbers', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'blog_pagination' => array('pages')
					),
					"std" => "pages",
					"options" => array(
						'pages'  => esc_html__('Page numbers list', 'bestdeals'),
						'slider' => esc_html__('Slider with page numbers', 'bestdeals')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"blog_counters" => array(
					'title' => esc_html__('Blog counters', 'bestdeals'),
					"desc" => esc_attr__('Select counters, displayed near the post title', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "comments",
					"options" => array(
						'views' => esc_html__('Views', 'bestdeals'),
						'likes' => esc_html__('Likes', 'bestdeals'),
						'rating' => esc_html__('Rating', 'bestdeals'),
						'comments' => esc_html__('Comments', 'bestdeals')
					),
					"dir" => "vertical",
					"multiple" => true,
					"type" => "checklist"),
		
		"close_category" => array(
					'title' => esc_html__("Post's category announce", 'bestdeals'),
					"desc" => esc_attr__('What category display in announce block (over posts thumb) - original or nearest parental', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "parental",
					"options" => array(
						'parental' => esc_html__('Nearest parental category', 'bestdeals'),
						'original' => esc_html__("Original post's category", 'bestdeals')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"show_date_after" => array(
					'title' => esc_html__('Show post date after', 'bestdeals'),
					"desc" => esc_attr__('Show post date after N days (before - show post age)', 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "30",
					"mask" => "?99",
					"type" => "text"),
		
		
		
		
		
		//###############################
		//#### Reviews               #### 
		//###############################
		"partition_reviews" => array(
					'title' => esc_html__('Reviews', 'bestdeals'),
					"icon" => "iconadmin-newspaper",
					"override" => "property_group,category,services_group,services_group",
					"type" => "partition"),
		
		"info_reviews_1" => array(
					'title' => esc_html__('Reviews criterias', 'bestdeals'),
					"desc" => esc_attr__('Set up list of reviews criterias. You can override it in any category.', 'bestdeals'),
					"override" => "property_group,category,services_group,services_group",
					"type" => "info"),
		
		"show_reviews" => array(
					'title' => esc_html__('Show reviews block',  'bestdeals'),
					"desc" => esc_attr__("Show reviews block on single post page and average reviews rating after post's title in stream pages", 'bestdeals'),
					"override" => "property_group,category,services_group,services_group",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"reviews_max_level" => array(
					'title' => esc_html__('Max reviews level',  'bestdeals'),
					"desc" => esc_attr__("Maximum level for reviews marks", 'bestdeals'),
					"std" => "100",
					"options" => array(
						'5'=>esc_html__('5 stars', 'bestdeals'), 
						'10'=>esc_html__('10 stars', 'bestdeals'), 
						'100'=>esc_html__('100%', 'bestdeals')
					),
					"type" => "radio",
					),
		
		"reviews_style" => array(
					'title' => esc_html__('Show rating as',  'bestdeals'),
					"desc" => esc_attr__("Show rating marks as text or as stars/progress bars.", 'bestdeals'),
					"std" => "stars",
					"options" => array(
						'text' => esc_attr__('As text (for example: 7.5 / 10)', 'bestdeals'), 
						'stars' => esc_html__('As stars or bars', 'bestdeals')
					),
					"dir" => "vertical",
					"type" => "radio"),
		
		"reviews_criterias_levels" => array(
					'title' => esc_html__('Reviews Criterias Levels', 'bestdeals'),
					"desc" => esc_attr__('Words to mark criterials levels. Just write the word and press "Enter". Also you can arrange words.', 'bestdeals'),
					"std" => esc_html__("bad,poor,normal,good,great", 'bestdeals'),
					"type" => "tags"),
		
		"reviews_first" => array(
					'title' => esc_html__('Show first reviews',  'bestdeals'),
					"desc" => esc_attr__("What reviews will be displayed first: by author or by visitors. Also this type of reviews will display under post's title.", 'bestdeals'),
					"std" => "author",
					"options" => array(
						'author' => esc_html__('By author', 'bestdeals'),
						'users' => esc_html__('By visitors', 'bestdeals')
						),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_second" => array(
					'title' => esc_html__('Hide second reviews',  'bestdeals'),
					"desc" => esc_attr__("Do you want hide second reviews tab in widgets and single posts?", 'bestdeals'),
					"std" => "show",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_show_hide'],
					"size" => "medium",
					"type" => "switch"),
		
		"reviews_can_vote" => array(
					'title' => esc_html__('What visitors can vote',  'bestdeals'),
					"desc" => esc_attr__("What visitors can vote: all or only registered", 'bestdeals'),
					"std" => "all",
					"options" => array(
						'all'=>esc_html__('All visitors', 'bestdeals'), 
						'registered'=>esc_html__('Only registered', 'bestdeals')
					),
					"dir" => "horizontal",
					"type" => "radio"),
		
		"reviews_criterias" => array(
					'title' => esc_html__('Reviews criterias',  'bestdeals'),
					"desc" => esc_attr__('Add default reviews criterias.',  'bestdeals'),
					"override" => "property_group,category,services_group,services_group",
					"std" => "",
					"cloneable" => true,
					"type" => "text"),

		// Don't remove this parameter - it used in admin for store marks
		"reviews_marks" => array(
					"std" => "",
					"type" => "hidden"),
		





		//###############################
		//#### Media                #### 
		//###############################
		"partition_media" => array(
					'title' => esc_html__('Media', 'bestdeals'),
					"icon" => "iconadmin-picture",
					"override" => "property_group,category,services_group,post,page",
					"type" => "partition"),
		
		"info_media_1" => array(
					'title' => esc_html__('Media settings', 'bestdeals'),
					"desc" => esc_attr__('Set up parameters to show images, galleries, audio and video posts', 'bestdeals'),
					"override" => "property_group,category,services_group,services_group",
					"type" => "info"),
					
		"retina_ready" => array(
					'title' => esc_html__('Image dimensions', 'bestdeals'),
					"desc" => esc_attr__('What dimensions use for uploaded image: Original or "Retina ready" (twice enlarged)', 'bestdeals'),
					"std" => "1",
					"size" => "medium",
					"options" => array(
						"1" => esc_html__("Original", 'bestdeals'), 
						"2" => esc_html__("Retina", 'bestdeals')
					),
					"type" => "switch"),
		
		"substitute_gallery" => array(
					'title' => esc_html__('Substitute standard Wordpress gallery', 'bestdeals'),
					"desc" => esc_attr__('Substitute standard Wordpress gallery with our slider on the single pages', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"gallery_instead_image" => array(
					'title' => esc_html__('Show gallery instead featured image', 'bestdeals'),
					"desc" => esc_attr__('Show slider with gallery instead featured image on blog streampage and in the related posts section for the gallery posts', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"gallery_max_slides" => array(
					'title' => esc_html__('Max images number in the slider', 'bestdeals'),
					"desc" => esc_attr__('Maximum images number from gallery into slider', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"dependency" => array(
						'gallery_instead_image' => array('yes')
					),
					"std" => "5",
					"min" => 2,
					"max" => 10,
					"type" => "spinner"),
		
		"popup_engine" => array(
					'title' => esc_html__('Popup engine to zoom images', 'bestdeals'),
					"desc" => esc_attr__('Select engine to show popup windows with images and galleries', 'bestdeals'),
					"std" => "magnific",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_popups'],
					"type" => "select"),
		
		"substitute_audio" => array(
					'title' => esc_html__('Substitute audio tags', 'bestdeals'),
					"desc" => esc_attr__('Substitute audio tag with source from soundcloud to embed player', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"substitute_video" => array(
					'title' => esc_html__('Substitute video tags', 'bestdeals'),
					"desc" => esc_attr__('Substitute video tags with embed players or leave video tags unchanged (if you use third party plugins for the video tags)', 'bestdeals'),
					"override" => "property_group,category,services_group,post,page",
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"use_mediaelement" => array(
					'title' => esc_html__('Use Media Element script for audio and video tags', 'bestdeals'),
					"desc" => esc_attr__('Do you want use the Media Element script for all audio and video tags on your site or leave standard HTML5 behaviour?', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		
		//###############################
		//#### Socials               #### 
		//###############################
		"partition_socials" => array(
					'title' => esc_html__('Socials', 'bestdeals'),
					"icon" => "iconadmin-users",
					"override" => "property_group,category,services_group,page",
					"type" => "partition"),
		
		"info_socials_1" => array(
					'title' => esc_html__('Social networks', 'bestdeals'),
					"desc" => esc_attr__("Social networks list for site footer and Social widget", 'bestdeals'),
					"type" => "info"),
		
		"social_icons" => array(
					'title' => esc_html__('Social networks',  'bestdeals'),
					"desc" => esc_attr__('Select icon and write URL to your profile in desired social networks.',  'bestdeals'),
					"std" => array(
						array('url'=>'#', 'icon'=>'icon-facebook'),
						array('url'=>'#', 'icon'=>'icon-twitter'),
						array('url'=>'#', 'icon'=>'icon-instagramm'),
						array('url'=>'#', 'icon'=>'icon-linkedin'),
						array('url'=>'#', 'icon'=>'icon-gplus')
					),
					"cloneable" => true,
					"size" => "small",
					"style" => $socials_type,
					"options" => $socials_type=='images' ? $BESTDEALS_GLOBALS['options_params']['list_socials'] : $BESTDEALS_GLOBALS['options_params']['list_icons'],
					"type" => "socials"),
		
		"info_socials_2" => array(
					'title' => esc_html__('Share buttons', 'bestdeals'),
					"desc" => wp_kses( __("Add button's code for each social share network.<br>
					In share url you can use next macro:<br>
					<b>{url}</b> - share post (page) URL,<br>
					<b>{title}</b> - post title,<br>
					<b>{image}</b> - post image,<br>
					<b>{descr}</b> - post description (if supported)<br>
					For example:<br>
					<b>Facebook</b> share string: <em>http://www.facebook.com/sharer.php?u={link}&amp;t={title}</em><br>
					<b>Delicious</b> share string: <em>http://delicious.com/save?url={link}&amp;title={title}&amp;note={descr}</em>", 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"override" => "property_group,category,services_group,page",
					"type" => "info"),
		
		"show_share" => array(
					'title' => esc_html__('Show social share buttons',  'bestdeals'),
					"desc" => esc_attr__("Show social share buttons block", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"std" => "horizontal",
					"options" => array(
						'hide'		=> esc_html__('Hide', 'bestdeals'),
						'vertical'	=> esc_html__('Vertical', 'bestdeals'),
						'horizontal'=> esc_html__('Horizontal', 'bestdeals')
					),
					"type" => "checklist"),

		"show_share_counters" => array(
					'title' => esc_html__('Show share counters',  'bestdeals'),
					"desc" => esc_attr__("Show share counters after social buttons", 'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"share_caption" => array(
					'title' => esc_html__('Share block caption',  'bestdeals'),
					"desc" => esc_attr__('Caption for the block with social share buttons',  'bestdeals'),
					"override" => "property_group,category,services_group,page",
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => esc_html__('Share:', 'bestdeals'),
					"type" => "text"),
		
		"share_buttons" => array(
					'title' => esc_html__('Share buttons',  'bestdeals'),
					"desc" => wp_kses( __('Select icon and write share URL for desired social networks.<br><b>Important!</b> If you leave text field empty - internal theme link will be used (if present).',  'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"dependency" => array(
						'show_share' => array('vertical', 'horizontal')
					),
					"std" => array(array('url'=>'', 'icon'=>'')),
					"cloneable" => true,
					"size" => "small",
					"style" => $socials_type,
					"options" => $socials_type=='images' ? $BESTDEALS_GLOBALS['options_params']['list_socials'] : $BESTDEALS_GLOBALS['options_params']['list_icons'],
					"type" => "socials"),
		
		
		"info_socials_3" => array(
					'title' => esc_html__('Twitter API keys', 'bestdeals'),
					"desc" => wp_kses( __("Put to this section Twitter API 1.1 keys.<br>
					You can take them after registration your application in <strong>https://apps.twitter.com/</strong>", 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"type" => "info"),
		
		"twitter_username" => array(
					'title' => esc_html__('Twitter username',  'bestdeals'),
					"desc" => esc_attr__('Your login (username) in Twitter',  'bestdeals'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_key" => array(
					'title' => esc_html__('Consumer Key',  'bestdeals'),
					"desc" => esc_attr__('Twitter API Consumer key',  'bestdeals'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_consumer_secret" => array(
					'title' => esc_html__('Consumer Secret',  'bestdeals'),
					"desc" => esc_attr__('Twitter API Consumer secret',  'bestdeals'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_key" => array(
					'title' => esc_html__('Token Key',  'bestdeals'),
					"desc" => esc_attr__('Twitter API Token key',  'bestdeals'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		"twitter_token_secret" => array(
					'title' => esc_html__('Token Secret',  'bestdeals'),
					"desc" => esc_attr__('Twitter API Token secret',  'bestdeals'),
					"divider" => false,
					"std" => "",
					"type" => "text"),
		
		
		
		
		
		//###############################
		//#### Contact info          #### 
		//###############################
		"partition_contacts" => array(
					'title' => esc_html__('Contact info', 'bestdeals'),
					"icon" => "iconadmin-mail",
					"type" => "partition"),
		
		"info_contact_1" => array(
					'title' => esc_html__('Contact information', 'bestdeals'),
					"desc" => esc_attr__('Company address, phones and e-mail', 'bestdeals'),
					"type" => "info"),
		
		"contact_email" => array(
					'title' => esc_html__('Contact form email', 'bestdeals'),
					"desc" => esc_attr__('E-mail for send contact form and user registration data', 'bestdeals'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-mail'),
					"type" => "text"),
		
		"contact_address_1" => array(
					'title' => esc_html__('Company address (part 1)', 'bestdeals'),
					"desc" => esc_attr__('Company country, post code and city', 'bestdeals'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_address_2" => array(
					'title' => esc_html__('Company address (part 2)', 'bestdeals'),
					"desc" => esc_attr__('Street and house number', 'bestdeals'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-home'),
					"type" => "text"),
		
		"contact_phone" => array(
					'title' => esc_html__('Phone', 'bestdeals'),
					"desc" => esc_attr__('Phone number', 'bestdeals'),
					"std" => "",
					"before" => array('icon'=>'iconadmin-phone'),
					"type" => "text"),
		
		"contact_fax" => array(
					'title' => esc_html__('Fax', 'bestdeals'),
					"desc" => esc_attr__('Fax number', 'bestdeals'),
					"std" => "888-765-4321 ",
					"before" => array('icon'=>'iconadmin-phone'),
					"type" => "text"),
		
		"info_contact_2" => array(
					'title' => esc_html__('Contact and Comments form', 'bestdeals'),
					"desc" => esc_attr__('Maximum length of the messages in the contact form shortcode and in the comments form', 'bestdeals'),
					"type" => "info"),
		
		"message_maxlength_contacts" => array(
					'title' => esc_html__('Contact form message', 'bestdeals'),
					"desc" => esc_attr__("Message's maxlength in the contact form shortcode", 'bestdeals'),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"message_maxlength_comments" => array(
					'title' => esc_html__('Comments form message', 'bestdeals'),
					"desc" => esc_attr__("Message's maxlength in the comments form", 'bestdeals'),
					"std" => "1000",
					"min" => 0,
					"max" => 10000,
					"step" => 100,
					"type" => "spinner"),
		
		"info_contact_3" => array(
					'title' => esc_html__('Default mail function', 'bestdeals'),
					"desc" => esc_attr__('What function you want to use for sending mail: the built-in Wordpress mail function or standard PHP mail function? Attention! Some plugins may not work with one of them and you always have the ability to switch to alternative.', 'bestdeals'),
					"type" => "info"),
		
		"mail_function" => array(
					'title' => esc_html__("Mail function", 'bestdeals'),
					"desc" => esc_attr__("What function you want to use for sending mail?", 'bestdeals'),
					"std" => "wp_mail",
					"size" => "medium",
					"options" => array(
						'wp_mail' => esc_html__('WP mail', 'bestdeals'),
						'mail' => esc_html__('PHP mail', 'bestdeals')
					),
					"type" => "switch"),
		
		
		
		
		
		
		
		//###############################
		//#### Search parameters     #### 
		//###############################
		"partition_search" => array(
					'title' => esc_html__('Search', 'bestdeals'),
					"icon" => "iconadmin-search",
					"type" => "partition"),
		
		"info_search_1" => array(
					'title' => esc_html__('Search parameters', 'bestdeals'),
					"desc" => esc_attr__('Enable/disable AJAX search and output settings for it', 'bestdeals'),
					"type" => "info"),
		
		"show_search" => array(
					'title' => esc_html__('Show search field', 'bestdeals'),
					"desc" => esc_attr__('Show search field in the top area and side menus', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"use_ajax_search" => array(
					'title' => esc_html__('Enable AJAX search', 'bestdeals'),
					"desc" => esc_attr__('Use incremental AJAX search for the search field in top of page', 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes')
					),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_min_length" => array(
					'title' => esc_html__('Min search string length',  'bestdeals'),
					"desc" => esc_attr__('The minimum length of the search string',  'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => 4,
					"min" => 3,
					"type" => "spinner"),
		
		"ajax_search_delay" => array(
					'title' => esc_html__('Delay before search (in ms)',  'bestdeals'),
					"desc" => esc_attr__('How much time (in milliseconds, 1000 ms = 1 second) must pass after the last character before the start search',  'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => 500,
					"min" => 300,
					"max" => 1000,
					"step" => 100,
					"type" => "spinner"),
		
		"ajax_search_types" => array(
					'title' => esc_html__('Search area', 'bestdeals'),
					"desc" => esc_attr__('Select post types, what will be include in search results. If not selected - use all types.', 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"std" => "",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_posts_types'],
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
		
		"ajax_search_posts_count" => array(
					'title' => esc_html__('Posts number in output',  'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => esc_attr__('Number of the posts to show in search results',  'bestdeals'),
					"std" => 3,
					"min" => 1,
					"max" => 10,
					"type" => "spinner"),
		
		"ajax_search_posts_image" => array(
					'title' => esc_html__("Show post's image", 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => esc_attr__("Show post's thumbnail in the search results", 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_date" => array(
					'title' => esc_html__("Show post's date", 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => esc_attr__("Show post's publish date in the search results", 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_author" => array(
					'title' => esc_html__("Show post's author", 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => esc_attr__("Show post's author in the search results", 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"ajax_search_posts_counters" => array(
					'title' => esc_html__("Show post's counters", 'bestdeals'),
					"dependency" => array(
						'show_search' => array('yes'),
						'use_ajax_search' => array('yes')
					),
					"desc" => esc_attr__("Show post's counters (views, comments, likes) in the search results", 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		
		
		
		
		//###############################
		//#### Service               #### 
		//###############################
		
		"partition_service" => array(
					'title' => esc_html__('Service', 'bestdeals'),
					"icon" => "iconadmin-wrench",
					"type" => "partition"),
		
		"info_service_1" => array(
					'title' => esc_html__('Theme functionality', 'bestdeals'),
					"desc" => esc_attr__('Basic theme functionality settings', 'bestdeals'),
					"type" => "info"),

		'privacy_text' => array(
					"title" => esc_html__('Text with Privacy Policy link', 'bestdeals'),
					"desc" => esc_html__("Specify text with Privacy Policy link for the checkbox 'I agree ...'", 'bestdeals'),
					"std" => esc_html__( 'I agree that my submitted data is being collected and stored.', 'bestdeals'),
					"type" => "text"),

		"use_ajax_views_counter" => array(
					'title' => esc_html__('Use AJAX post views counter', 'bestdeals'),
					"desc" => esc_attr__('Use javascript for post views count (if site work under the caching plugin) or increment views count in single page template', 'bestdeals'),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"allow_editor" => array(
					'title' => esc_html__('Frontend editor',  'bestdeals'),
					"desc" => esc_attr__("Allow authors to edit their posts in frontend area)", 'bestdeals'),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"admin_add_filters" => array(
					'title' => esc_html__('Additional filters in the admin panel', 'bestdeals'),
					"desc" => wp_kses( __('Show additional filters (on post formats, tags and categories) in admin panel page "Posts". <br>Attention! If you have more than 2.000-3.000 posts, enabling this option may cause slow load of the "Posts" page! If you encounter such slow down, simply open Appearance - Theme Options - Service and set "No" for this option.', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"show_overriden_taxonomies" => array(
					'title' => esc_html__('Show overriden options for taxonomies', 'bestdeals'),
					"desc" => esc_attr__('Show extra column in categories list, where changed (overriden) theme options are displayed.', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"show_overriden_posts" => array(
					'title' => esc_html__('Show overriden options for posts and pages', 'bestdeals'),
					"desc" => esc_attr__('Show extra column in posts and pages list, where changed (overriden) theme options are displayed.', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
		
		"admin_dummy_data" => array(
					'title' => esc_html__('Enable Dummy Data Installer', 'bestdeals'),
					"desc" => wp_kses( __('Show "Install Dummy Data" in the menu "Appearance". <b>Attention!</b> When you install dummy data all content of your site will be replaced!', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"admin_dummy_timeout" => array(
					'title' => esc_html__('Dummy Data Installer Timeout',  'bestdeals'),
					"desc" => esc_attr__('Web-servers set the time limit for the execution of php-scripts. By default, this is 30 sec. Therefore, the import process will be split into parts. Upon completion of each part - the import will resume automatically! The import process will try to increase this limit to the time, specified in this field.',  'bestdeals'),
					"std" => 1200,
					"min" => 30,
					"max" => 1800,
					"type" => "spinner"),
		
		"admin_emailer" => array(
					'title' => esc_html__('Enable Emailer in the admin panel', 'bestdeals'),
					"desc" => esc_attr__('Allow to use BestDEALS Emailer for mass-volume e-mail distribution and management of mailing lists in "Appearance - Emailer"', 'bestdeals'),
					"std" => "yes",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),

		"debug_mode" => array(
					'title' => esc_html__('Debug mode', 'bestdeals'),
					"desc" => wp_kses( __('In debug mode we are using unpacked scripts and styles, else - using minified scripts and styles (if present). <b>Attention!</b> If you have modified the source code in the js or css files, regardless of this option will be used latest (modified) version stylesheets and scripts. You can re-create minified versions of files using on-line services or utility <b>yuicompressor-x.y.z.jar</b>', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags']),
					"std" => "no",
					"options" => $BESTDEALS_GLOBALS['options_params']['list_yes_no'],
					"type" => "switch"),
			
		"info_service_3" => array(
			"title" => esc_html__('API Keys', 'bestdeals'),
			"desc" => wp_kses_data( __('API Keys for some Web services', 'bestdeals') ),
			"type" => "info"),
		'api_google' => array(
			"title" => esc_html__('Google API Key', 'bestdeals'),
			"desc" => wp_kses_data( __("Insert Google API Key for browsers into the field above to generate Google Maps", 'bestdeals') ),
			"std" => "",
			"type" => "text"),
			
			
			
		//###############################
		//#### Property              #### 
		//###############################
		
		"partition_property" => array(
			'title' => esc_html__('Property', 'bestdeals'),
			"icon" => "iconadmin-home",
			"type" => "partition"),
		
		"info_property_1" => array(
			'title' => esc_html__('Theme functionality', 'bestdeals'),
			"desc" => esc_attr__('Basic theme functionality settings', 'bestdeals'),
			"type" => "info"),
			
		"property_type_list" => array(
			'title' => esc_html__('Property type list', 'bestdeals'),
			"desc" => '',
			"std" => esc_html__("Office,Shop,Apartment,Apartment Building,Condominium,Single Family Home,Villa",'bestdeals'),
			"type" => "tags"),
			
		"property_location_list" => array(
			'title' => esc_html__('Property location list', 'bestdeals'),
			"desc" => '',
			"std" => esc_html__("Miami,Little Havana,Perrine,Doral",'bestdeals'),
			"type" => "tags"),
			
		"property_price_sign_list" => array(
			'title' => esc_html__('Currency Available', 'bestdeals'),
			"desc" => esc_attr__('Add your currecy and press Enter/Return Button', 'bestdeals'),
			"std" => esc_attr__("$,&euro;",'bestdeals'),
			"type" => "tags"),
			
		"property_price_per_list" => array(
			'title' => esc_html__('Rent Term', 'bestdeals'),
			"desc" => esc_attr__('Add available rent term here and press  Enter/Return Button', 'bestdeals'),
			"std" => esc_html__("year,month,week,day",'bestdeals'),
			"type" => "tags"),
			
		"property_options_list" => array(
			'title' => esc_html__('Property options list', 'bestdeals'),
			"desc" => '',
			"std" => esc_html__("Attended Lobby,Concierge,Fireplace,Gym,Outdoor Space,Parking,Pet Friendly,Pool,Views,Washer / Dryer",'bestdeals'),
			"type" => "tags"),
			
		);
	}
}


// Update all temporary vars (start with $bestdeals_) in the Theme Options with actual lists
if ( !function_exists( 'bestdeals_options_settings_theme_setup2' ) ) {
	add_action( 'bestdeals_action_after_init_theme', 'bestdeals_options_settings_theme_setup2', 1 );
	function bestdeals_options_settings_theme_setup2() {
		if (bestdeals_options_is_used()) {
			global $BESTDEALS_GLOBALS;
			// Replace arrays with actual parameters
			$lists = array();
			if (count($BESTDEALS_GLOBALS['options']) > 0) {
				foreach ($BESTDEALS_GLOBALS['options'] as $k=>$v) {
					if (isset($v['options']) && is_array($v['options']) && count($v['options']) > 0) {
						foreach ($v['options'] as $k1=>$v1) {
							if (bestdeals_substr($k1, 0, 11) == '$bestdeals_' || bestdeals_substr($v1, 0, 11) == '$bestdeals_') {
								$list_func = bestdeals_substr(bestdeals_substr($k1, 0, 11) == '$bestdeals_' ? $k1 : $v1, 1);
								unset($BESTDEALS_GLOBALS['options'][$k]['options'][$k1]);
								if (isset($lists[$list_func]))
									$BESTDEALS_GLOBALS['options'][$k]['options'] = bestdeals_array_merge($BESTDEALS_GLOBALS['options'][$k]['options'], $lists[$list_func]);
								else {
									if (function_exists($list_func)) {
										$BESTDEALS_GLOBALS['options'][$k]['options'] = $lists[$list_func] = bestdeals_array_merge($BESTDEALS_GLOBALS['options'][$k]['options'], $list_func == 'bestdeals_get_list_menus' ? $list_func(true) : $list_func());
								   	} else
								   		echo sprintf(__('Wrong function name %s in the theme options array', 'bestdeals'), $list_func);
								}
							}
						}
					}
				}
			}
		}
	}
}

?>