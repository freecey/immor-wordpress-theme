<?php
/**
 * Theme sprecific functions and definitions
 */


/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'bestdeals_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_theme_setup', 1 );
	function bestdeals_theme_setup() {

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );

		// Custom header setup
		add_theme_support( 'custom-header', array('header-text'=>false));

		// Custom backgrounds setup
		add_theme_support( 'custom-background');

		// Supported posts formats
		add_theme_support( 'post-formats', array('gallery', 'video', 'audio', 'link', 'quote', 'image', 'status', 'aside', 'chat') );

		// Autogenerate title tag
		add_theme_support('title-tag');

		// Add user menu
		add_theme_support('nav-menus');

		// WooCommerce Support
		add_theme_support( 'woocommerce' );

		// Add wide and full blocks support
		add_theme_support( 'align-wide' );



		// Register theme menus
		add_filter( 'bestdeals_filter_add_theme_menus',		'bestdeals_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'bestdeals_filter_add_theme_sidebars',	'bestdeals_add_theme_sidebars' );


	}
}


// Add/Remove theme nav menus
if ( !function_exists( 'bestdeals_add_theme_menus' ) ) {
	//Handler of add_filter( 'bestdeals_filter_add_theme_menus', 'bestdeals_add_theme_menus' );
	function bestdeals_add_theme_menus($menus) {
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'bestdeals_add_theme_sidebars' ) ) {
	//Handler of add_filter( 'bestdeals_filter_add_theme_sidebars',	'bestdeals_add_theme_sidebars' );
	function bestdeals_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'bestdeals' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'bestdeals' )
			);
			if (bestdeals_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'bestdeals' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}

// Register widgetized area
if ( !function_exists( 'bestdeals_register_theme_sidebars' ) ) {
	function bestdeals_register_theme_sidebars($sidebars=array()) {
		global $BESTDEALS_GLOBALS;
		if (!is_array($sidebars)) $sidebars = array();
		// Custom sidebars
		$custom = bestdeals_get_theme_option('custom_sidebars');
		if (is_array($custom) && count($custom) > 0) {
			foreach ($custom as $i => $sb) {
				if (trim(chop($sb))=='') continue;
				$sidebars['sidebar_custom_'.($i)]  = $sb;
			}
		}
		$sidebars = apply_filters( 'bestdeals_filter_add_theme_sidebars', $sidebars );
		$BESTDEALS_GLOBALS['registered_sidebars'] = $sidebars;
		if (is_array($sidebars) && count($sidebars) > 0) {
			foreach ($sidebars as $id=>$name) {
				register_sidebar( array(
					'name'          => $name,
					'id'            => $id,
					'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget_bg">',
					'after_widget'  => '</div></aside>',
					'before_title'  => '<h5 class="widget_title_">',
					'after_title'   => '</h5>',
				) );
			}
		}
	}
}

// Add page meta to the head
if (!function_exists('bestdeals_head_add_page_meta')) {
	add_action('wp_head', 'bestdeals_head_add_page_meta', 1);
	function bestdeals_head_add_page_meta() {
		$theme_skin = sanitize_file_name(bestdeals_get_custom_option('theme_skin'));
		?>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="format-detection" content="telephone=no">
		<?php
		if (bestdeals_get_theme_option('responsive_layouts') == 'yes') {
			?>
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<?php
		}
		?>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php
	}
}


// Callback for output single comment layout
if (!function_exists('bestdeals_output_single_comment')) {
	function bestdeals_output_single_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		switch ( $comment->comment_type ) {
			case 'pingback' :
				?>
				<li class="trackback"><?php esc_html_e( 'Trackback:', 'bestdeals' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'bestdeals' ), '<span class="edit-link">', '<span>' ); ?>
				<?php
				break;
			case 'trackback' :
				?>
				<li class="pingback"><?php esc_html_e( 'Pingback:', 'bestdeals' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( esc_html__( 'Edit', 'bestdeals' ), '<span class="edit-link">', '<span>' ); ?>
				<?php
				break;
			default :
				$author_id = $comment->user_id;
				$author_link = get_author_posts_url( $author_id );
				?>
				<li id="comment-<?php comment_ID(); ?>" <?php comment_class('comment_item'); ?>>
					<div class="comment_item_bg">
						<div class="comment_author_avatar"><?php echo get_avatar( $comment, 75*min(2, max(1, bestdeals_get_theme_option("retina_ready")))); ?></div>
						<div class="comment_content">
							<div class="comment_info">
								<span class="comment_author"><?php bestdeals_show_layout(($author_id ? '<a href="'.esc_url($author_link).'">' : '') . comment_author() . ($author_id ? '</a>' : '')); ?></span>
								<span class="comment_date"><span class="comment_date_label"><?php esc_html_e('Posted', 'bestdeals'); ?></span> <span class="comment_date_value"><?php echo get_comment_date(get_option('date_format')); ?></span></span>
								<span class="comment_time"><?php echo get_comment_date(get_option('time_format')); ?></span>
							</div>
							<div class="comment_text_wrap">
								<?php if ( $comment->comment_approved == 0 ) { ?>
								<div class="comment_not_approved"><?php esc_html_e( 'Your comment is awaiting moderation.', 'bestdeals' ); ?></div>
								<?php } ?>
								<div class="comment_text"><?php comment_text(); ?></div>
							</div>
							<?php if ($depth < $args['max_depth']) { ?>
								<div class="comment_reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></div>
							<?php } ?>
						</div>
					</div>
				<?php
				break;
		}
	}
}


// Return text for the "I agree ..." checkbox
if ( ! function_exists( 'bestdeals_trx_utils_privacy_text' ) ) {
	add_filter( 'trx_utils_filter_privacy_text', 'bestdeals_trx_utils_privacy_text' );
	function bestdeals_trx_utils_privacy_text( $text='' ) {
		return bestdeals_get_privacy_text();
	}
}

// Return text for the Privacy Policy checkbox
if ( ! function_exists('bestdeals_get_privacy_text' ) ) {
	function bestdeals_get_privacy_text() {
		$page = get_option( 'wp_page_for_privacy_policy' );
		$privacy_text = bestdeals_get_theme_option( 'privacy_text' );
		return apply_filters( 'bestdeals_filter_privacy_text', wp_kses_post(
				$privacy_text
				. ( ! empty( $page ) && ! empty( $privacy_text )
					// Translators: Add url to the Privacy Policy page
					? ' ' . sprintf( __( 'For further details on handling user data, see our %s', 'bestdeals' ),
						'<a href="' . esc_url( get_permalink( $page ) ) . '" target="_blank">'
						. __( 'Privacy Policy', 'bestdeals' )
						. '</a>' )
					: ''
				)
			)
		);
	}
}

// Add theme required plugins
if ( !function_exists( 'bestdeals_add_trx_utils' ) ) {
	add_filter( 'trx_utils_active', 'bestdeals_add_trx_utils' );
	function bestdeals_add_trx_utils($enable=true) {
		return true;
	}
}

/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files
if (!isset($_POST['action']) || $_POST['action']!="heartbeat") {
	require_once( get_template_directory().'/fw/loader.php' );
}


?>