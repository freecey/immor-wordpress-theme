<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_single_team_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_single_team_theme_setup', 1 );
	function bestdeals_template_single_team_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'single-team',
			'mode'   => 'team',
			'need_content' => true,
			'need_terms' => true,
			'title'  => esc_html__('Single Team member', 'bestdeals'),
			'thumb_title'  => esc_html__('Team image (crop)', 'bestdeals'),
			'w'		 => 470,
			'h'		 => 470
		));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_single_team_output' ) ) {
	function bestdeals_template_single_team_output($post_options, $post_data) {
		$post_data['post_views']++;
		$show_title = bestdeals_get_custom_option('show_post_title')=='yes';
		$title_tag = bestdeals_get_custom_option('show_page_title')=='yes' ? 'h3' : 'h1';

		bestdeals_open_wrapper('<article class="' 
				. join(' ', get_post_class('itemscope'
					. ' post_item post_item_single_team'
					. ' post_featured_' . esc_attr($post_options['post_class'])
					. ' post_format_' . esc_attr($post_data['post_format'])))
				. '"'
				. ' itemscope itemtype="http://schema.org/Article'
				. '">');
		
		if (!$post_data['post_protected'] && (
			!empty($post_options['dedicated']) ||
			(bestdeals_get_custom_option('show_featured_image')=='yes' && $post_data['post_thumb'])
		)) {
			?>
			<section class="post_featured">
			<?php
			if (!empty($post_options['dedicated'])) {
				bestdeals_show_layout($post_options['dedicated']);
			} else {
				bestdeals_enqueue_popup();
				?>
				<div class="post_thumb" data-image="<?php echo esc_url($post_data['post_attachment']); ?>" data-title="<?php echo esc_attr($post_data['post_title']); ?>">
					<a class="hover_icon hover_icon_view" href="<?php echo esc_url($post_data['post_attachment']); ?>" title="<?php echo esc_attr($post_data['post_title']); ?>"><?php bestdeals_show_layout($post_data['post_thumb']); ?></a>
				</div>
				<?php 
			}
			?>
			</section>
			<?php
		}
		
		
			?>
			<<?php echo esc_html($title_tag); ?> itemprop="name" class="post_title entry-title"><?php bestdeals_show_layout($post_data['post_title']); ?></<?php echo esc_html($title_tag); ?>>
			<?php 
		
		
		
		if ( isset($post_data["post_terms"]["team_group"]->terms_links) ) {
			$bdTermsLinks = $post_data["post_terms"]["team_group"]->terms_links;
			echo '<div class="post_team_group">';
			for ( $i=0; $i < count($bdTermsLinks); $i++ ) {
				bestdeals_show_layout($bdTermsLinks[$i]);
				if ( $i != (count($bdTermsLinks)-1) ) {
					echo esc_html(", ");
				}
			}
			echo '</div>';
		}
		
		// Print team description
		$post_meta = get_post_meta($post_data['post_id'], 'team_data', true);
		if( !empty( $post_meta['team_member_description_bd'] ) ) {
			echo '<div class="post_team_description">';
			bestdeals_show_layout($post_meta['team_member_description_bd']);
			echo '</div>';
		}

		// print socials icon
		$aaa ='';
		$soc_list = $post_meta['team_member_socials'];
		if (is_array($soc_list) && count($soc_list)>0) {
			$soc_str = '';
			foreach ($soc_list as $sn=>$sl) {
				if (!empty($sl))
					$soc_str .= (!empty($soc_str) ? '|' : '') . ($sn) . '=' . ($sl);
			}
			if (!empty($soc_str))
				$aaa = bestdeals_do_shortcode('[trx_socials size="tiny" shape="round" socials="'.esc_attr($soc_str).'"][/trx_socials]');
		}
		bestdeals_show_layout($aaa);
		
		
		
		bestdeals_open_wrapper('<section class="post_content'.(!$post_data['post_protected'] && $post_data['post_edit_enable'] ? ' '.esc_attr('post_content_editor_present') : '').'" itemprop="articleBody">');
		
		
		// Post content
		if ($post_data['post_protected']) {
			echo get_the_password_form(); 
		} else {
			echo '<div class="cL"></div>';
			bestdeals_show_layout(bestdeals_gap_wrapper(bestdeals_reviews_wrapper($post_data['post_content'])));
			require(bestdeals_get_file_dir('templates/_parts/single-pagination.php'));
			if ( bestdeals_get_custom_option('show_post_tags') == 'yes' && !empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_links)) {
				?>
				<div class="post_info post_info_bottom">
					<span class="post_info_item post_info_tags"><?php esc_html_e('Tags:', 'bestdeals'); ?> <?php echo join(', ', $post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_links); ?></span>
				</div>
				<?php 
			}
		} 
			
		if (!$post_data['post_protected'] && $post_data['post_edit_enable']) {
			require(bestdeals_get_file_dir('templates/_parts/editor-area.php'));
		}

		bestdeals_close_wrapper();
			
		if (!$post_data['post_protected']) {
			require(bestdeals_get_file_dir('templates/_parts/share.php'));
		}

		bestdeals_close_wrapper();

		echo '<div class="cL"></div>';
		
		if (!$post_data['post_protected']) {
			require(bestdeals_get_file_dir('templates/_parts/related-posts.php'));
			require(bestdeals_get_file_dir('templates/_parts/comments.php'));
		}

		require(bestdeals_get_file_dir('templates/_parts/views-counter.php'));
		
		
		
	}
}
?>