<?php
/*
 * The template for displaying "Page 404"
*/

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_404_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_404_theme_setup', 1 );
	function bestdeals_template_404_theme_setup() {
		bestdeals_add_template(array(
			'layout' => '404',
			'mode'   => 'internal',
			'title'  => 'Page 404',
			'theme_options' => array(
				'article_style' => 'stretch'
			),
			'w'		 => null,
			'h'		 => null
			));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_404_output' ) ) {
	function bestdeals_template_404_output() {
		global $BESTDEALS_GLOBALS;
		?>
		<article class="post_item post_item_404">
			<div class="post_content">
				<h1 class="page_title"><?php esc_html_e( '404', 'bestdeals' ); ?></h1>
				<h2 class="page_subtitle"><?php esc_html_e('The requested page cannot be found', 'bestdeals'); ?></h2>
				<p class="page_description"><?php echo wp_kses( sprintf( __('Can\'t find what you need? Take a moment and do a search below or start from <a href="%s">our homepage</a>.', 'bestdeals'), esc_url(home_url('/')) ), $BESTDEALS_GLOBALS['allowed_tags']); ?></p>
				<div class="page_search">
					<?php if ( function_exists('bestdeals_in_shortcode_blogger') && !bestdeals_in_shortcode_blogger() ) { ?>
						<div class="search_wrap search_style_regular search_state_fixed search_ajax inited">
							<div class="search_form_wrap">
								<form role="search" method="get" class="search_form" action="<?php echo esc_url( home_url('/') ); ?>">
									<input type="text" class="search_field" placeholder="<?php esc_html_e('To search type and hit enter', 'bestdeals')?>" value="" name="s">
									<button type="submit" class="search_submit icon-search" title="Start search"><span><?php esc_html_e('Search','bestdeals')?></span></button>
								</form>
							</div>
							<div class="search_results widget_area scheme_original"><a class="search_results_close icon-cancel"></a><div class="search_results_content"></div></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</article>
		<?php
	}
}
?>