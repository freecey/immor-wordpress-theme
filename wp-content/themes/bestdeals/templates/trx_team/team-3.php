<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_team_3_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_team_3_theme_setup', 1 );
	function bestdeals_template_team_3_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'team-3',
			'template' => 'team-3',
			'mode'   => 'team',
			'title'  => esc_html__('Team /Style 3/', 'bestdeals'),
			'thumb_title'  => esc_html__('Small team image (crop)', 'bestdeals'),
			'w' => 60,
			'h' => 60
		));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_team_3_output' ) ) {
	function bestdeals_template_team_3_output($post_options, $post_data) {
		$show_title = true;
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = 1;
		
		if (bestdeals_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php bestdeals_show_layout($post_options['tag_id'] ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''); ?>
				class="sc_team_item sc_team_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '') . (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"
				<?php bestdeals_show_layout($post_options['tag_css']!='' ? ' style="'.esc_attr($post_options['tag_css']).'"' : '');
					bestdeals_show_layout( (!bestdeals_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($post_options['tag_animation'])).'"' : '')); ?>>
				<div class="sc_team_item_avatar"><?php bestdeals_show_layout($post_options['photo']); ?>
					
				</div>
				<div class="sc_team_item_info">
					<h5 class="sc_team_item_title"><?php bestdeals_show_layout(($post_options['link'] ? '<a href="'.esc_url($post_options['link']).'">' : '') . ($post_data['post_title']) . ($post_options['link'] ? '</a>' : '')); ?></h5>
					<div class="sc_team_item_position"><?php bestdeals_show_layout($post_options['position']);?></div>
				</div>
				<div class="cL"></div>
				
				<?php
				// Print team description
				$post_meta = get_post_meta($post_data['post_id'], 'team_data', true);
				if( !empty( $post_meta['team_member_contact_info'] ) ) {
					echo '<div class="post_team_contact_info">';
					bestdeals_show_layout($post_meta['team_member_contact_info']);
					echo '</div>';
				}
				?>
				
			</div>
		<?php
		if (bestdeals_param_is_on($post_options['slider']) || $columns > 1) {
			?></div><?php
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
?>