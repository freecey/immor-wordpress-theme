<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_excerpt_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_excerpt_theme_setup', 1 );
	function bestdeals_template_excerpt_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'excerpt',
			'mode'   => 'blog',
			'title'  => esc_html__('Excerpt', 'bestdeals'),
			'thumb_title'  => esc_html__('Large image (crop)', 'bestdeals'),
			'w'		 => 870,
			'h'		 => 490
		));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_excerpt_output' ) ) {
	function bestdeals_template_excerpt_output($post_options, $post_data) {
		$show_title = true;
		$tag = bestdeals_in_shortcode_blogger(true) ? 'div' : 'article';
		?>
		<<?php bestdeals_show_layout($tag); ?> <?php post_class('post_item post_item_excerpt post_featured_' . esc_attr($post_options['post_class']) . ' post_format_'.esc_attr($post_data['post_format']) . ($post_options['number']%2==0 ? ' even' : ' odd') . ($post_options['number']==0 ? ' first' : '') . ($post_options['number']==$post_options['posts_on_page']? ' last' : '') . ($post_options['add_view_more'] ? ' viewmore' : '')); ?>>
			<?php
			if ($post_data['post_flags']['sticky']) {
				?><span class="sticky_label"></span><?php
			}

			if ($show_title && $post_options['location'] == 'center' && !empty($post_data['post_title'])) {
				?><h3 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php bestdeals_show_layout($post_data['post_title']); ?></a></h3><?php
				if (!$post_data['post_protected'] && $post_options['info']) {
					require(bestdeals_get_file_dir('templates/_parts/post-info.php'));
				}
			}
			
			if (!$post_data['post_protected'] && (!empty($post_options['dedicated']) || $post_data['post_thumb'] || $post_data['post_gallery'] || $post_data['post_video'] || $post_data['post_audio'])) {
				?>
				<div class="post_featured">
				<?php
				if (!empty($post_options['dedicated'])) {
					bestdeals_show_layout($post_options['dedicated']);
				} else if ($post_data['post_thumb'] || $post_data['post_gallery'] || $post_data['post_video'] || $post_data['post_audio']) {
					require(bestdeals_get_file_dir('templates/_parts/post-featured.php'));
				}
				?>
				</div>
			<?php
			}
			?>
	
			<div class="post_content clearfix">
				<?php
				if ($show_title && $post_options['location'] != 'center' && !empty($post_data['post_title'])) {
					?><h3 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php bestdeals_show_layout($post_data['post_title']); ?></a></h3><?php
					if (!$post_data['post_protected'] && $post_options['info']) {
						require(bestdeals_get_file_dir('templates/_parts/post-info.php'));
					}
				}
				?>
		
				<div class="post_descr">
				<?php
					if ($post_data['post_protected']) {
						bestdeals_show_layout($post_data['post_excerpt']);
					} else {
						if ($post_data['post_excerpt']) {
							echo in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status')) ? $post_data['post_excerpt'] : '<p>'.trim(bestdeals_strshort($post_data['post_excerpt'], isset($post_options['descr']) ? $post_options['descr'] : bestdeals_get_custom_option('post_excerpt_maxlength'))).'</p>';
						}
					}
					if (empty($post_options['readmore'])) $post_options['readmore'] = esc_html__('Read more', 'bestdeals');
					if (!bestdeals_param_is_off($post_options['readmore']) && !in_array($post_data['post_format'], array('quote', 'link', 'chat', 'aside', 'status'))) {
						echo '<a href="'. esc_url($post_data['post_link']). '"'
						. ' class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium">'.$post_options['readmore'].'</a>';
					}
				?>
				</div>

			</div>	<!-- /.post_content -->

		</<?php bestdeals_show_layout($tag); ?>>	<!-- /.post_item -->

	<?php
	}
}
?>