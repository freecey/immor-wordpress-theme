<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_header_1_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_header_1_theme_setup', 1 );
	function bestdeals_template_header_1_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'header_1',
			'mode'   => 'header',
			'title'  => esc_html__('Header 1', 'bestdeals'),
			'icon'   => bestdeals_get_file_url('templates/headers/images/1.jpg')
			));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_header_1_output' ) ) {
	function bestdeals_template_header_1_output($post_options, $post_data) {
		global $BESTDEALS_GLOBALS;

		// WP custom header
		$header_css = '';
		if ($post_options['position'] != 'over') {
			$header_image = get_header_image();
			$header_css = $header_image!='' 
				? ' style="background: url('.esc_url($header_image).') repeat center top"' 
				: '';
		}
		?>
		
		<div class="top_panel_fixed_wrap"></div>

		<header class="top_panel_wrap top_panel_style_1 scheme_<?php echo esc_attr($post_options['scheme']); ?>">
			<div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_<?php echo esc_attr(bestdeals_get_custom_option('top_panel_position')); ?>">
			
			<?php if (bestdeals_get_custom_option('show_top_panel_top')=='yes') { ?>
				<div class="top_panel_top">
					<div class="content_wrap clearfix">
						<?php
						$top_panel_top_components = array('login', 'currency', 'bookmarks');
						require_once( bestdeals_get_file_dir('templates/headers/_parts/top-panel-top.php') );
						?>
					</div>
				</div>
			<?php } ?>

			<div class="top_panel_middle" <?php bestdeals_show_layout($header_css); ?>>
				<div class="content_wrap">
					<div class="columns_wrap columns_fluid">
						<div class="contact_logo">
							<?php require_once( bestdeals_get_file_dir('templates/headers/_parts/logo.php') ); ?>
						</div>
							
						<div class="contact_field_all">
						<?php
						
						// Address
						$contact_address_1=trim(bestdeals_get_custom_option('contact_address_1'));
						$contact_address_2=trim(bestdeals_get_custom_option('contact_address_2'));
						if (!empty($contact_address_1) || !empty($contact_address_2)) {
							?><div class="contact_field contact_address">
								<span class="contact_icon icon-location"></span>
								<span class="contact_label contact_address_1"><?php bestdeals_show_layout($contact_address_1); ?></span>
								<!--<span class="contact_address_2"><?php bestdeals_show_layout($contact_address_2); ?></span>-->
								<div class="cL"></div>
							</div><?php
						}
						
						// Phone and email
						$contact_phone=trim(bestdeals_get_custom_option('contact_phone'));
						$contact_email=trim(bestdeals_get_custom_option('contact_email'));
						if (!empty($contact_phone) || !empty($contact_email)) {
							?><div class="contact_field contact_phone">
								<span class="contact_icon icon-phone"></span>
								<a class="contact_label contact_phone" href="tel:<?php bestdeals_show_layout(esc_attr($contact_phone)); ?>"><?php bestdeals_show_layout($contact_phone); ?></a>
								<div class="cL"></div>
							</div><?php
						}
						
						?>
						
						<div class="cL"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="top_panel_bottom">
				
				
				
				<div class="content_wrap clearfix">
					<a href="#" class="menu_main_responsive_button icon-down"><?php esc_html_e('Select menu item', 'bestdeals'); ?></a>
					<nav role="navigation" class="menu_main_nav_area">
						<?php
						if (empty($BESTDEALS_GLOBALS['menu_main'])) $BESTDEALS_GLOBALS['menu_main'] = bestdeals_get_nav_menu('menu_main');
						if (empty($BESTDEALS_GLOBALS['menu_main'])) $BESTDEALS_GLOBALS['menu_main'] = bestdeals_get_nav_menu();
						bestdeals_show_layout($BESTDEALS_GLOBALS['menu_main']);
						?>
					</nav>
					<?php if (bestdeals_get_custom_option('show_search')=='yes' && function_exists('bestdeals_sc_search')) echo bestdeals_sc_search(array('state'=>'closed')); ?>
				</div>
				
				
				
			</div>

			</div>
		</header>

		<?php
	}
}
?>