					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>"><?php
							
							bestdeals_show_layout($BESTDEALS_GLOBALS['logo_text']
								? '<div class="logo_text">'.($BESTDEALS_GLOBALS['logo_text']).'</div>' 
								: '');
							
							if ( !empty($BESTDEALS_GLOBALS['logo']) ) {
								echo '<img src="'.esc_url($BESTDEALS_GLOBALS['logo']).'" class="logo_main" alt="Image"><div class="cL"></div>';
							} else {
								$bd_i = get_template_directory_uri();
								switch (bestdeals_get_custom_option('body_scheme')) {
									case 'orange':
										echo '<img src="'.bestdeals_get_file_url('/images/logo/logo1.png').'" class="logo_main" alt="Image">';
										break;
								}
								echo '<div class="cL"></div>';
							}
							
							bestdeals_show_layout($BESTDEALS_GLOBALS['logo_slogan']
								? '<div class="logo_slogan">' . esc_html($BESTDEALS_GLOBALS['logo_slogan']) . '</div>' 
								: '');
						?></a>
					</div>
