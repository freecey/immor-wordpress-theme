<div id="popup_registration" class="popup_wrap popup_registration bg_tint_light">
	<a href="#" class="popup_close"></a>
	<div class="form_wrap">
		<form name="registration_form" method="post" class="popup_form registration_form">
			<input type="hidden" name="redirect_to" value="<?php echo esc_attr(home_url('/')); ?>"/>
			<div class="form_left">
				<div class="popup_form_field login_field iconed_field icon-user"><input type="text" id="registration_username" name="registration_username"  value="" placeholder="<?php esc_html_e('User name (login)', 'bestdeals'); ?>"></div>
				<div class="popup_form_field email_field iconed_field icon-mail"><input type="text" id="registration_email" name="registration_email" value="" placeholder="<?php esc_html_e('E-mail', 'bestdeals'); ?>"></div>
				<?php $privacy = bestdeals_get_privacy_text();
				if (!empty($privacy)) {?>
					<div class="popup_form_field agree_field">
						<div class="sc_emailer_checkbox trx_utils_form_field_agree">
						<input type="checkbox" value="1" id="i_agree_privacy_policy_registration" name="i_agree_privacy_policy"> <label for="i_agree_privacy_policy_registration"><?php
							bestdeals_show_layout($privacy); ?></label></div></div><?php
					}
					?>
			</div>
			<div class="form_right">
				<div class="popup_form_field password_field iconed_field icon-lock"><input type="password" id="registration_pwd"  name="registration_pwd"  value="" placeholder="<?php esc_html_e('Password', 'bestdeals'); ?>"></div>
				<div class="popup_form_field password_field iconed_field icon-lock"><input type="password" id="registration_pwd2" name="registration_pwd2" value="" placeholder="<?php esc_html_e('Confirm Password', 'bestdeals'); ?>"></div>
				<div class="popup_form_field description_field"><?php esc_html_e('Minimum 6 characters', 'bestdeals'); ?></div>
				<div class="popup_form_field submit_field"><input type="submit" class="submit_button sc_button sc_button_size_medium" value="<?php esc_html_e('Sign Up', 'bestdeals'); ?>"></div>
			</div>
		</form>
		<div class="result message_block"></div>
	</div>	<!-- /.registration_wrap -->
</div>		<!-- /.user-popUp -->
