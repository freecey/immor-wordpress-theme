<?php if (bestdeals_get_theme_option('use_ajax_views_counter')=='yes') {
// Post/page views count increment
$code = " jQuery(document).ready(function() { "
		. " jQuery.post(BESTDEALS_GLOBALS['ajax_url'], { "
	. " 	action: 'post_counter', "
	. " 	nonce: BESTDEALS_GLOBALS['ajax_nonce'], "
	. " 	post_id: " .( (int) $post_data['post_id'] ) . " , "
	. "		views:" .( (int)  $post_data['post_views'] ) . " , "
	. "		}); "
	. " }); ";

	bestdeals_set_global('js_code', bestdeals_get_global('js_code') . $code);
	wp_add_inline_script('bestdeals-core-init-script', $code);
}
