<?php
$show_all_counters = !isset($post_options['counters']);
$counters_tag = is_single() ? 'span' : 'a';
 
if ($show_all_counters || bestdeals_strpos($post_options['counters'], 'views')!==false) { ?>
	<span class="post_info_item post_info_counters"><<?php bestdeals_show_layout($counters_tag); ?> class="post_counters_item post_counters_views icon-eye" title="<?php echo sprintf( esc_attr__('Views - %s', 'bestdeals'), $post_data['post_views']); ?>" href="<?php echo esc_url($post_data['post_link']); ?>"><?php bestdeals_show_layout($post_data['post_views']); ?></<?php bestdeals_show_layout($counters_tag); ?>></span>
	<?php 
}

if ($show_all_counters || bestdeals_strpos($post_options['counters'], 'likes')!==false) {
	// Load core messages
	bestdeals_enqueue_messages();
	$likes = isset($_COOKIE['bestdeals_likes']) ? $_COOKIE['bestdeals_likes'] : '';
	$allow = bestdeals_strpos($likes, ','.($post_data['post_id']).',')===false;
	?>
	<span class="post_info_item post_info_counters">
	<a class="post_counters_item post_counters_likes icon-heart <?php bestdeals_show_layout($allow ? 'enabled' : 'disabled'); ?>" title="<?php echo esc_attr($allow ?  esc_attr__('Like', 'bestdeals') :  esc_attr__('Dislike', 'bestdeals')); ?>" href="#"
		data-postid="<?php echo esc_attr($post_data['post_id']); ?>"
		data-likes="<?php echo esc_attr($post_data['post_likes']); ?>"
		data-title-like="<?php esc_html_e('Like', 'bestdeals'); ?>"
		data-title-dislike="<?php esc_html_e('Dislike', 'bestdeals'); ?>"><span class="post_counters_number"><?php bestdeals_show_layout($post_data['post_likes']); ?></span></a></span>
	<?php
}

if ($show_all_counters || bestdeals_strpos($post_options['counters'], 'comments')!==false) { ?>
	<span class="post_info_item post_info_counters"><a class="post_counters_item post_counters_comments icon-comment" title="<?php echo sprintf( esc_attr__('Comments - %s', 'bestdeals'), $post_data['post_comments']); ?>" href="<?php echo esc_url($post_data['post_comments_link']); ?>"><span class="post_counters_number"><?php bestdeals_show_layout($post_data['post_comments']); ?></span></a></span>
	<?php 
}
 
$rating = $post_data['post_reviews_'.(bestdeals_get_theme_option('reviews_first')=='author' ? 'author' : 'users')];
if ($rating > 0 && ($show_all_counters || bestdeals_strpos($post_options['counters'], 'rating')!==false)) { 
	?>
	<span class="post_info_item post_info_counters"><<?php bestdeals_show_layout($counters_tag); ?> class="post_counters_item post_counters_rating icon-star" title="<?php echo sprintf( esc_attr__('Rating - %s', 'bestdeals'), $rating); ?>" href="<?php echo esc_url($post_data['post_link']); ?>"><span class="post_counters_number"><?php bestdeals_show_layout($rating); ?></span></<?php bestdeals_show_layout($counters_tag); ?>></span>
	<?php
}

if (is_single() && bestdeals_strpos($post_options['counters'], 'markup')!==false) {
	?>
	<meta itemprop="interactionCount" content="User<?php echo esc_attr(bestdeals_strpos($post_options['counters'],'comments')!==false ? 'Comments' : 'PageVisits'); ?>:<?php echo esc_attr(bestdeals_strpos($post_options['counters'], 'comments')!==false ? $post_data['post_comments'] : $post_data['post_views']); ?>" />
	<?php
}
?>