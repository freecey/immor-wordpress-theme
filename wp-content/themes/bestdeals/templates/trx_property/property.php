<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_property_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_property_theme_setup', 1 );
	function bestdeals_template_property_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'property_3',
			'template' => 'property',
			'mode'   => 'blog',
			'need_isotope' => true,
			'title'  => esc_html__('Property /3 columns/', 'bestdeals'),
			'thumb_title'  => esc_html__('Property image (crop)', 'bestdeals'),
			'w'		 => 370,
			'h'		 => 180
		));
		// Add template specific scripts
		add_action('bestdeals_action_blog_scripts', 'bestdeals_template_property_add_scripts');
	}
}

// Add template specific scripts
if (!function_exists('bestdeals_template_property_add_scripts')) {
	//Handler of add_action('bestdeals_action_blog_scripts', 'bestdeals_template_property_add_scripts');
	function bestdeals_template_property_add_scripts($style) {
		
			wp_enqueue_script( 'isotope', bestdeals_get_file_url('js/jquery.isotope.min.js'), array(), null, true );
		
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_property_output' ) ) {
	function bestdeals_template_property_output($post_options, $post_data) {
		$show_title = !in_array($post_data['post_format'], array('aside', 'chat', 'status', 'link', 'quote'));
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = max(1, min(12, empty($post_options['columns_count']) 
									? (empty($parts[1]) ? 1 : (int) $parts[1])
									: $post_options['columns_count']
									));
		$tag = bestdeals_in_shortcode_blogger(true) ? 'div' : 'article';
		?>
		<div class="isotope_item isotope_item_<?php echo esc_attr($style); ?> isotope_item_<?php echo esc_attr($post_options['layout']); ?> isotope_column_<?php echo esc_attr($columns); ?>
					<?php
					if ($post_options['filters'] != '') {
						if ($post_options['filters']=='categories' && !empty($post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy']]->terms_ids);
						else if ($post_options['filters']=='tags' && !empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids))
							echo ' flt_' . join(' flt_', $post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms_ids);
					}
					?>">
			<<?php bestdeals_show_layout($tag); ?> class="post_item post_item_<?php echo esc_attr($style); ?> post_item_<?php echo esc_attr($post_options['layout']); ?>
				 <?php echo ' post_format_'.esc_attr($post_data['post_format']) 
					. ($post_options['number']%2==0 ? ' even' : ' odd') 
					. ($post_options['number']==0 ? ' first' : '') 
					. ($post_options['number']==$post_options['posts_on_page'] ? ' last' : '');
				?>">
				
				
				<?php 
				$propertyStatusList = get_post_meta( $post_data['post_id'], 'property_status_list', true );
				
				if ( $propertyStatusList == 'sale' ) {
					$propertyStatusListText = esc_html__('Sale', 'bestdeals');
				} elseif ( $propertyStatusList == 'rent' ) {
					$propertyStatusListText = esc_html__('Rent', 'bestdeals');
				}


				if ($post_data['post_video'] || $post_data['post_audio'] || $post_data['post_thumb'] ||  $post_data['post_gallery']) { ?>
					<div class="post_featured">
						<?php
						require(bestdeals_get_file_dir('templates/_parts/post-featured.php'));
						if ( isset($propertyStatusList) and !empty($propertyStatusList) ) {
							echo '<span class="property_status_list">' . esc_html($propertyStatusListText) . '</span>';
						}
						?>
					</div>
				<?php }	?>
				
				<div class="post_content isotope_item_content">
					
					<?php
					$propertyTypeSingle = get_post_meta( $post_data['post_id'], 'property_type_single', true );
					if ( isset($propertyTypeSingle) and !empty($propertyTypeSingle) ) {
						echo '<div class="property_type_single">' . esc_html($propertyTypeSingle). '</div>';
					}
					
					$_property_price = '';
					$_property_price_sign = '';
					$_property_price_per = '';
					
					$propertyPriceSign = get_post_meta( $post_data['post_id'], 'property_price_sign', true );
					$propertyPrice = get_post_meta( $post_data['post_id'], 'property_price', true );
					$propertyPricePer = get_post_meta( $post_data['post_id'], 'property_price_per', true );
					
					if ( isset($propertyPrice) ) {
						$propertyPriceArray = explode(".", $propertyPrice);
						if (isset($propertyPriceArray[0])) {
							$_property_price = (int) $propertyPriceArray[0];
							if (isset($propertyPriceArray[1])) {
								$_property_price = number_format($_property_price, 0, ',', ' ') . '.' . $propertyPriceArray[1];
							} else {
								$_property_price = number_format($_property_price, 0, ',', ' ');
							}
						}
					}
					
					if ( isset($propertyPriceSign) ) {
						$_property_price_sign = $propertyPriceSign;
					}
					
					if ( isset($propertyPricePer) ) {
						$_property_price_per = $propertyPricePer;
					}
					
					if ( $_property_price != "0" ) {
						$_property_price = esc_html($_property_price_sign) . esc_html($_property_price);
						if ( (strlen($_property_price_per) > 0) and ( $propertyStatusList == 'rent' ) ) {
							$_property_price .= '<span>'.  esc_html__('per', 'bestdeals').' '.esc_html($_property_price_per).'</span>';
						}
						echo balanceTags('<div class="property_price">' . $_property_price . '</div>');
					}
					
					if ($show_title) {
							if (!isset($post_options['links']) || $post_options['links']) {
								?>
								<h4 class="post_title"><a href="<?php echo esc_url($post_data['post_link']); ?>"><?php echo esc_html($post_data['post_title']); ?></a></h4>
								<?php
							} else {
								?>
								<h4 class="post_title"><?php echo esc_html($post_data['post_title']); ?></h4>
								<?php
							}
						}
					?>
					
					<div class="cL"></div>
								
					<div class="post_descr">
						
						<?php
						$_property_area = get_post_meta( $post_data['post_id'], 'property_area', true );
						if ( isset($_property_area) ) {
							$_property_area = (int) $_property_area;
							$_property_area = number_format($_property_area, 0, ',', ' ');
						}

						if ( isset($_property_area) ) {
							echo '<div class="property_area">' . esc_html($_property_area) . ' '.esc_html__('Sq Ft', 'bestdeals').'</div>';
						}
						?>
							
						
						<div class="property_meta">
							
							<?php
							$_property_bedrooms = get_post_meta( $post_data['post_id'], 'property_bedrooms', true );
							if ( isset($_property_bedrooms) ) {
								echo '<span class="property-bedrooms">' . esc_html($_property_bedrooms) . '</span>';
							}
							$_property_bathrooms = get_post_meta( $post_data['post_id'], 'property_bathrooms', true );
							if ( isset($_property_bathrooms) ) {
								echo '<span class="property-bathrooms">' . esc_html($_property_bathrooms) . '</span>';
							}
							$_property_garages = get_post_meta( $post_data['post_id'], 'property_garages', true );
							if ( isset($_property_garages) ) {
								echo '<span class="property-garages">' . esc_html($_property_garages) . '</span>';
							}
							?>
							
						</div>
						<div class="cL"></div>
					</div>
					
				</div>				<!-- /.post_content -->
			</<?php bestdeals_show_layout($tag); ?>>	<!-- /.post_item -->
		</div>						<!-- /.isotope_item -->
		<?php
	}
}
?>