<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_template_property_3_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_template_property_3_theme_setup', 1 );
	function bestdeals_template_property_3_theme_setup() {
		bestdeals_add_template(array(
			'layout' => 'property-3',
			'template' => 'property-3',
			'mode'   => 'property',
			'title'  => esc_html__('Property /Style 3/', 'bestdeals'),
			'thumb_title'  => esc_html__('Small property image (crop)', 'bestdeals'),
			'w'		 => 60,
			'h'		 => 60
		));
	}
}

// Template output
if ( !function_exists( 'bestdeals_template_property_3_output' ) ) {
	function bestdeals_template_property_3_output($post_options, $post_data) {
		$show_title = true;
		$parts = explode('_', $post_options['layout']);
		$style = $parts[0];
		$columns = 1;
		
		$_property_status_list = get_post_meta( $post_data['post_id'], 'property_status_list', true );
		$_property_type_single = get_post_meta( $post_data['post_id'], 'property_type_single', true );
		
		$_property_price_sign = get_post_meta( $post_data['post_id'], 'property_price_sign', true );
		$property_price = get_post_meta( $post_data['post_id'], 'property_price', true );
		$_property_price_per = get_post_meta( $post_data['post_id'], 'property_price_per', true );
		
		$_property_area = (int) get_post_meta( $post_data['post_id'], 'property_area', true );
		
		$_property_bedrooms = (int) get_post_meta( $post_data['post_id'], 'property_bedrooms', true );
		$_property_bathrooms = (int) get_post_meta( $post_data['post_id'], 'property_bathrooms', true );
		$_property_garages = (int) get_post_meta( $post_data['post_id'], 'property_garages', true );
		
		$_property_price = '';
		if ( isset($property_price) ) {
			$propertyPriceArray = explode(".", $property_price);
			if (isset($propertyPriceArray[0])) {
				$_property_price = (int) $propertyPriceArray[0];
				if (isset($propertyPriceArray[1])) {
					$_property_price = number_format($_property_price, 0, ',', ' ') . '.' . $propertyPriceArray[1];
				} else {
					$_property_price = number_format($_property_price, 0, ',', ' ');
				}
			}
		}
		
		
		
		if (bestdeals_param_is_on($post_options['slider'])) {
			?><div class="swiper-slide" data-style="<?php echo esc_attr($post_options['tag_css_wh']); ?>" style="<?php echo esc_attr($post_options['tag_css_wh']); ?>"><div class="sc_property_item_wrap"><?php
		} else if ($columns > 1) {
			?><div class="column-1_<?php echo esc_attr($columns); ?> column_padding_bottom"><?php
		}
		?>
			<div<?php bestdeals_show_layout($post_options['tag_id'] ? ' id="'.esc_attr($post_options['tag_id']).'"' : ''); ?>
				class="sc_property_item sc_property_item_<?php echo esc_attr($post_options['number']) . ($post_options['number'] % 2 == 1 ? ' odd' : ' even') . ($post_options['number'] == 1 ? ' first' : '') . (!empty($post_options['tag_class']) ? ' '.esc_attr($post_options['tag_class']) : ''); ?>"
				<?php bestdeals_show_layout($post_options['tag_css']!='' ? ' style="'.esc_attr($post_options['tag_css']).'"' : '');
					bestdeals_show_layout((!bestdeals_param_is_off($post_options['tag_animation']) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($post_options['tag_animation'])).'"' : '')); ?>>
				
				<div class="sc_property_item_featured post_featured">
					<?php require(bestdeals_get_file_dir('templates/_parts/post-featured.php')); ?>
				</div>
				<div class="sc_property_item_box">
					<?php
					if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) {
						?><a class="sc_property_item_title" href="<?php echo esc_url($post_data['post_link']); ?>"><?php
					}
					if ($show_title) {
						?><?php echo esc_html($post_data['post_title']); ?><?php
					}
					if ((!isset($post_options['links']) || $post_options['links']) && !empty($post_data['post_link'])) {
						?></a><?php
					}
					?>
					
					<div class="sc_property_item_price">
						<?php
						if ( $_property_price != '0' ) {
							if (strlen($_property_price_sign) > 0 ) {
								echo esc_html($_property_price_sign);
							}
							echo esc_html($_property_price);
							if ( ($_property_status_list == 'rent') and (strlen($_property_price_per) > 0) ) {
								echo '<span> / '.esc_html__('per', 'bestdeals').' '. esc_html($_property_price_per) .'</span>';
							}
						}
						?>
					</div>
					
				</div>
				<div class="cL"></div>
			</div>
		<?php
		if (bestdeals_param_is_on($post_options['slider'])) {
			?></div></div><?php
		} else if ($columns > 1) {
			?></div><?php
		}
	}
}
?>