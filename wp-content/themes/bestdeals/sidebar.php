<?php
/**
 * The Sidebar containing the main widget areas.
 */

$sidebar_show   = bestdeals_get_custom_option('show_sidebar_main');
$sidebar_scheme = bestdeals_get_custom_option('sidebar_main_scheme');
$sidebar_name   = bestdeals_get_custom_option('sidebar_main');
$sidebar_main   = bestdeals_get_custom_option('show_sidebar_main');

if (!bestdeals_param_is_off($sidebar_show) && is_active_sidebar($sidebar_name) && !is_404()) {
	?>
	<div class="sidebar widget_area scheme_<?php echo esc_attr($sidebar_scheme) .' '. esc_attr($sidebar_main); ?>" role="complementary">
		<div class="sidebar_inner widget_area_inner">
			<?php
			ob_start();
			do_action( 'before_sidebar' );
			global $BESTDEALS_GLOBALS;
			if (!empty($BESTDEALS_GLOBALS['reviews_markup'])) 
				echo '<aside class="column-1_1 widget widget_reviews">' . ($BESTDEALS_GLOBALS['reviews_markup']) . '</aside>';
			$BESTDEALS_GLOBALS['current_sidebar'] = 'main';
            if ( is_active_sidebar( $sidebar_name ) ) {
                dynamic_sidebar( $sidebar_name );
            }
			do_action( 'after_sidebar' );
			$out = ob_get_contents();
			ob_end_clean();
			bestdeals_show_layout(chop(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $out)));
			?>
		</div>
	</div> <!-- /.sidebar -->
	<?php
}
?>