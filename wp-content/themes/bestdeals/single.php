<?php
/**
Template Name: Single post
 */
global $BESTDEALS_GLOBALS;
get_header(); 
$single_style = !empty($BESTDEALS_GLOBALS['single_style']) ? $BESTDEALS_GLOBALS['single_style'] : bestdeals_get_custom_option('single_style');
while ( have_posts() ) { the_post();

	// Move bestdeals_set_post_views to the javascript - counter will work under cache system
	if (bestdeals_get_custom_option('use_ajax_views_counter')=='no') {
		bestdeals_set_post_views(get_the_ID());
	}
	bestdeals_show_post_layout(
		array(
			'layout' => $single_style,
			'sidebar' => !bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_main')),
			'content' => bestdeals_get_template_property($single_style, 'need_content'),
			'terms_list' => bestdeals_get_template_property($single_style, 'need_terms')
		)
	);

}

get_footer();
?>