<?php
/**
 * BestDEALS Framework: strings manipulations
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Check multibyte functions
if ( ! defined( 'BESTDEALS_MULTIBYTE' ) ) define( 'BESTDEALS_MULTIBYTE', function_exists('mb_strpos') ? 'UTF-8' : false );

if (!function_exists('bestdeals_strlen')) {
	function bestdeals_strlen($text) {
		return BESTDEALS_MULTIBYTE ? mb_strlen($text) : strlen($text);
	}
}

if (!function_exists('bestdeals_strpos')) {
	function bestdeals_strpos($text, $char, $from=0) {
		return BESTDEALS_MULTIBYTE ? mb_strpos($text, $char, $from) : strpos($text, $char, $from);
	}
}

if (!function_exists('bestdeals_strrpos')) {
	function bestdeals_strrpos($text, $char, $from=0) {
		return BESTDEALS_MULTIBYTE ? mb_strrpos($text, $char, $from) : strrpos($text, $char, $from);
	}
}

if (!function_exists('bestdeals_substr')) {
	function bestdeals_substr($text, $from, $len=-999999) {
		if ($len==-999999) { 
			if ($from < 0)
				$len = -$from; 
			else
				$len = bestdeals_strlen($text)-$from;
		}
		return BESTDEALS_MULTIBYTE ? mb_substr($text, $from, $len) : substr($text, $from, $len);
	}
}

if (!function_exists('bestdeals_strtolower')) {
	function bestdeals_strtolower($text) {
		return BESTDEALS_MULTIBYTE ? mb_strtolower($text) : strtolower($text);
	}
}

if (!function_exists('bestdeals_strtoupper')) {
	function bestdeals_strtoupper($text) {
		return BESTDEALS_MULTIBYTE ? mb_strtoupper($text) : strtoupper($text);
	}
}

if (!function_exists('bestdeals_strtoproper')) {
	function bestdeals_strtoproper($text) { 
		$rez = ''; $last = ' ';
		for ($i=0; $i<bestdeals_strlen($text); $i++) {
			$ch = bestdeals_substr($text, $i, 1);
			$rez .= bestdeals_strpos(' .,:;?!()[]{}+=', $last)!==false ? bestdeals_strtoupper($ch) : bestdeals_strtolower($ch);
			$last = $ch;
		}
		return $rez;
	}
}

if (!function_exists('bestdeals_strrepeat')) {
	function bestdeals_strrepeat($str, $n) {
		$rez = '';
		for ($i=0; $i<$n; $i++)
			$rez .= $str;
		return $rez;
	}
}

if (!function_exists('bestdeals_strshort')) {
	function bestdeals_strshort($str, $maxlength, $add='...') {
		if ($maxlength < 0) 
			return '';
		if ($maxlength < 1 || $maxlength >= bestdeals_strlen($str)) 
			return strip_tags($str);
		$str = bestdeals_substr(strip_tags($str), 0, $maxlength - bestdeals_strlen($add));
		$ch = bestdeals_substr($str, $maxlength - bestdeals_strlen($add), 1);
		if ($ch != ' ') {
			for ($i = bestdeals_strlen($str) - 1; $i > 0; $i--)
				if (bestdeals_substr($str, $i, 1) == ' ') break;
			$str = trim(bestdeals_substr($str, 0, $i));
		}
		if (!empty($str) && bestdeals_strpos(',.:;-', bestdeals_substr($str, -1))!==false) $str = bestdeals_substr($str, 0, -1);
		return ($str) . ($add);
	}
}

// Clear string from spaces, line breaks and tags (only around text)
if (!function_exists('bestdeals_strclear')) {
	function bestdeals_strclear($text, $tags=array()) {
		if (empty($text)) return $text;
		if (!is_array($tags)) {
			if ($tags != '')
				$tags = explode($tags, ',');
			else
				$tags = array();
		}
		$text = trim(chop($text));
		if (is_array($tags) && count($tags) > 0) {
			foreach ($tags as $tag) {
				$open  = '<'.esc_attr($tag);
				$close = '</'.esc_attr($tag).'>';
				if (bestdeals_substr($text, 0, bestdeals_strlen($open))==$open) {
					$pos = bestdeals_strpos($text, '>');
					if ($pos!==false) $text = bestdeals_substr($text, $pos+1);
				}
				if (bestdeals_substr($text, -bestdeals_strlen($close))==$close) $text = bestdeals_substr($text, 0, bestdeals_strlen($text) - bestdeals_strlen($close));
				$text = trim(chop($text));
			}
		}
		return $text;
	}
}

// Return slug for the any title string
if (!function_exists('bestdeals_get_slug')) {
	function bestdeals_get_slug($title) {
		return bestdeals_strtolower(str_replace(array('\\','/','-',' ','.'), '_', $title));
	}
}

// Replace macros in the string
if (!function_exists('bestdeals_strmacros')) {
	function bestdeals_strmacros($str) {
		return str_replace(array("{{", "}}", "((", "))", "||"), array("<i>", "</i>", "<b>", "</b>", "<br>"), $str);
	}
}
?>