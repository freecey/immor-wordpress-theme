<?php
/* Royal Slider support functions
------------------------------------------------------------------------------- */

// Check if Royal Slider installed and activated
if ( !function_exists( 'bestdeals_exists_royalslider' ) ) {
	function bestdeals_exists_royalslider() {
		return class_exists("NewRoyalSliderMain");
	}
}
?>