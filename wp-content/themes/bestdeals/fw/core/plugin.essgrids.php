<?php
/* Essential Grid support functions
------------------------------------------------------------------------------- */

// Check if Ess. Grid installed and activated
if ( !function_exists( 'bestdeals_exists_essgrids' ) ) {
	function bestdeals_exists_essgrids() {
		return defined('EG_PLUGIN_PATH');
	}
}
?>