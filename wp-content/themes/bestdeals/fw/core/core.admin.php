<?php
/**
 * BestDEALS Framework: Admin functions
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

/* Admin actions and filters:
------------------------------------------------------------------------ */


if (is_admin()) {

	/* Theme setup section
	-------------------------------------------------------------------- */

	if ( !function_exists( 'bestdeals_admin_theme_setup' ) ) {
		add_action( 'bestdeals_action_before_init_theme', 'bestdeals_admin_theme_setup', 11 );
		function bestdeals_admin_theme_setup() {
			if ( is_admin() ) {
				add_action("admin_footer",			'bestdeals_admin_prepare_scripts', 9);
				add_action("admin_enqueue_scripts",	'bestdeals_admin_load_scripts');
				add_action('tgmpa_register',		'bestdeals_admin_register_plugins');

				// AJAX: Get terms for specified post type
				add_action('wp_ajax_bestdeals_admin_change_post_type', 		'bestdeals_callback_admin_change_post_type');
			}
		}
	}

	// Load required styles and scripts for admin mode
	if ( !function_exists( 'bestdeals_admin_load_scripts' ) ) {
		//Handler of add_action("admin_enqueue_scripts", 'bestdeals_admin_load_scripts');
		function bestdeals_admin_load_scripts() {
            if (bestdeals_get_theme_option('debug_mode')=='yes') {
			wp_enqueue_script( 'bestdeals-debug-script', bestdeals_get_file_url('js/core.debug.js'), array('jquery'), null, true );
            }
			wp_enqueue_style( 'bestdeals-admin-style', bestdeals_get_file_url('css/core.admin.css'), array(), null );
			wp_enqueue_script( 'bestdeals-admin-script', bestdeals_get_file_url('js/core.admin.js'), array('jquery'), null, true );
			if (bestdeals_strpos(add_query_arg(array()), 'widgets.php')!==false) {
				wp_enqueue_style( 'fontello-style', bestdeals_get_file_url('css/fontello-admin/css/fontello-admin.css'), array(), null );
				wp_enqueue_style( 'animations-style', bestdeals_get_file_url('css/fontello-admin/css/animation.css'), array(), null );
			}
		}
	}

	// Prepare required styles and scripts for admin mode
	if ( !function_exists( 'bestdeals_admin_prepare_scripts' ) ) {
		//Handler of add_action("admin_head", 'bestdeals_admin_prepare_scripts');
		function bestdeals_admin_prepare_scripts() {

			$vars = bestdeals_get_global('js_vars');
			if (empty($vars) || !is_array($vars)) $vars = array();
			$vars = array_merge($vars, array(
				'admin_mode'	=> true,
				'ajax_nonce' =>wp_create_nonce(admin_url('admin-ajax.php')),
				'ajax_url'	=> admin_url('admin-ajax.php'),
				'user_logged_in' => true,
			));

			wp_localize_script('bestdeals-admin-script', 'BESTDEALS_GLOBALS', apply_filters('bestdeals_action_add_scripts_inline', $vars));
			$code = bestdeals_get_global('js_code');
			if (!empty($js_code)) {
				$st = '<';
				$ct = '/';
				$et = '>';
				bestdeals_show_layout($code, "{$st}script{$et}jQuery(document).ready(function(){", "});{$st}{$ct}script{$et}");
			}

		}
	}

	// AJAX: Get terms for specified post type
	if ( !function_exists( 'bestdeals_callback_admin_change_post_type' ) ) {
		function bestdeals_callback_admin_change_post_type() {
			if ( !wp_verify_nonce( $_REQUEST['nonce'], 'ajax_nonce' ) )
				wp_die();
			$post_type = bestdeals_get_value_gp('post_type');
			$terms = bestdeals_get_list_terms(false, bestdeals_get_taxonomy_categories_by_post_type($post_type));
			$terms = bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals')), $terms);
			$response = array(
				'error' => '',
				'data' => array(
					'ids' => array_keys($terms),
					'titles' => array_values($terms)
				)
			);
			echo json_encode($response);
			wp_die();
		}
	}

	// Return current post type in dashboard
	if ( !function_exists( 'bestdeals_admin_get_current_post_type' ) ) {
		function bestdeals_admin_get_current_post_type() {
			global $post, $typenow, $current_screen;
			if ( $post && $post->post_type )							//we have a post so we can just get the post type from that
				return $post->post_type;
			else if ( $typenow )										//check the global $typenow — set in admin.php
				return $typenow;
			else if ( $current_screen && $current_screen->post_type )	//check the global $current_screen object — set in sceen.php
				return $current_screen->post_type;
			else if ( isset( $_REQUEST['post_type'] ) )					//check the post_type querystring
				return sanitize_key( $_REQUEST['post_type'] );
			else if ( isset( $_REQUEST['post'] ) ) {					//lastly check the post id querystring
				$post = get_post( sanitize_key( $_REQUEST['post'] ) );
				return !empty($post->post_type) ? $post->post_type : '';
			} else														//we do not know the post type!
				return '';
		}
	}

	// Add admin menu pages
	if ( !function_exists( 'bestdeals_admin_add_menu_item' ) ) {
		function bestdeals_admin_add_menu_item($mode, $item, $pos='100') {
			static $shift = 0;
			if ($pos=='100') $pos .= '.'.$shift++;
			$fn = join('_', array('add', $mode, 'page'));
			if (empty($item['parent']))
				$fn($item['page_title'], $item['menu_title'], $item['capability'], $item['menu_slug'], $item['callback'], $item['icon'], $pos);
			else
				$fn($item['parent'], $item['page_title'], $item['menu_title'], $item['capability'], $item['menu_slug'], $item['callback'], $item['icon'], $pos);
		}
	}



	// Register optional plugins
	if ( !function_exists( 'bestdeals_admin_register_plugins' ) ) {
		function bestdeals_admin_register_plugins() {

			$plugins = apply_filters('bestdeals_filter_required_plugins', array(
				array(
					'name' 		=> 'Bestdeals Utilities',
					'slug' 		=> 'bestdeals-utils',
					'version'   => '2.2',
					'source'	=> bestdeals_get_file_dir('plugins/bestdeals-utils.zip'),
					'required' 	=> true
				),
				array(
					'name' 		=> 'Essential Grid',
					'slug' 		=> 'essential-grid',
                    'version' 	=> '2.3.2',
					'source'	=> bestdeals_get_file_dir('plugins/essential-grid.zip'),
					'required' 	=> false
				),
				array(
					'name' 		=> 'WPBakery PageBuilder',
					'slug' 		=> 'js_composer',
                    'version' 	=> '6.0.5',
					'source'	=> bestdeals_get_file_dir('plugins/js_composer.zip'),
					'required' 	=> false
				),
				array(
					'name' 		=> 'Revolution Slider',
					'slug' 		=> 'revslider',
                    'version' 	=> '6.0.9',
					'source'	=> bestdeals_get_file_dir('plugins/revslider.zip'),
					'required' 	=> false
				),
				array(
					'name' 		=> 'Booked',
					'slug' 		=> 'booked',
                    'version' 	=> '2.2.5',
					'source'	=> bestdeals_get_file_dir('plugins/booked.zip'),
					'required' 	=> false
				),
				array(
					'name'     => esc_html__( 'Booked Calendar Feeds', 'bestdeals' ),
					'slug'     => 'booked-calendar-feeds',
                    'version' 	=> '1.1.6',
					'source'   => bestdeals_get_file_dir('plugins/booked-calendar-feeds.zip'),
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'Booked Front-End Agents', 'bestdeals' ),
					'slug'     => 'booked-frontend-agents',
                    'version' 	=> '1.1.16',
					'source'   => bestdeals_get_file_dir('plugins/booked-frontend-agents.zip'),
					'required' => false,
				),
				array(
					'name' 		=> 'WP GDPR Complience',
					'slug' 		=> 'wp-gdpr-compliance',
					'required' 	=> false
				),
				array(
					'name' 		=> 'Contact Form 7',
					'slug' 		=> 'contact-form-7',
					'required' 	=> false
				),
			));
			$config = array(
				'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
				'default_path' => '',                      // Default absolute path to bundled plugins.
				'menu'         => 'tgmpa-install-plugins', // Menu slug.
				'parent_slug'  => 'themes.php',            // Parent menu slug.
				'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
				'has_notices'  => true,                    // Show admin notices or not.
				'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
				'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
				'is_automatic' => true,                    // Automatically activate plugins after installation or not.
				'message'      => ''                       // Message to output right before the plugins table.
			);

			tgmpa( $plugins, $config );
		}
	}

	require_once( bestdeals_get_file_dir('lib/tgm/class-tgm-plugin-activation.php') );
}

?>