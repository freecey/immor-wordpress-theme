<?php
/**
 * BestDEALS Framework: Property post type settings
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Theme init
if (!function_exists('bestdeals_property_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_property_theme_setup' );
	function bestdeals_property_theme_setup() {
		
		// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
		add_filter('bestdeals_filter_get_blog_type',			'bestdeals_property_get_blog_type', 9, 2);
		add_filter('bestdeals_filter_get_blog_title',		'bestdeals_property_get_blog_title', 9, 2);
		add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_property_get_current_taxonomy', 9, 2);
		add_filter('bestdeals_filter_is_taxonomy',			'bestdeals_property_is_taxonomy', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_property_get_stream_page_title', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_property_get_stream_page_link', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_property_get_stream_page_id', 9, 2);
		add_filter('bestdeals_filter_query_add_filters',		'bestdeals_property_query_add_filters', 9, 2);
		add_filter('bestdeals_filter_detect_inheritance_key','bestdeals_property_detect_inheritance_key', 9, 1);
		add_filter('bestdeals_filter_post_save_custom_options',	'bestdeals_property_post_save_custom_options', 10, 3);

		// Extra column for property lists
		if (bestdeals_get_theme_option('show_overriden_posts')=='yes') {
			add_filter('manage_edit-property_columns',			'bestdeals_post_add_options_column', 9);
			add_filter('manage_property_posts_custom_column',	'bestdeals_post_fill_options_column', 9, 2);
		}

		// Add shortcodes [trx_property] and [trx_property_item]
		add_action('bestdeals_action_shortcodes_list',		'bestdeals_property_reg_shortcodes');
		add_action('bestdeals_action_shortcodes_list_vc',	'bestdeals_property_reg_shortcodes_vc');
		
		if (function_exists('bestdeals_require_data')) {
			// Prepare type "Property"
			bestdeals_require_data( 'post_type', 'property', array(
				'label'               => esc_html__( 'Property', 'bestdeals' ),
				'description'         => esc_attr__( 'Property Description', 'bestdeals' ),
				'labels'              => array(
					'name'                => esc_html_x( 'Property', 'Post Type General Name', 'bestdeals' ),
					'singular_name'       => esc_html_x( 'Property item', 'Post Type Singular Name', 'bestdeals' ),
					'menu_name'           => esc_html__( 'Property', 'bestdeals' ),
					'parent_item_colon'   => esc_html__( 'Property Item:', 'bestdeals' ),
					'all_items'           => esc_html__( 'All Property', 'bestdeals' ),
					'view_item'           => esc_html__( 'View Item', 'bestdeals' ),
					'add_new_item'        => esc_html__( 'Add New Property', 'bestdeals' ),
					'add_new'             => esc_html__( 'Add New', 'bestdeals' ),
					'edit_item'           => esc_html__( 'Edit Item', 'bestdeals' ),
					'update_item'         => esc_html__( 'Update Item', 'bestdeals' ),
					'search_items'        => esc_html__( 'Search Item', 'bestdeals' ),
					'not_found'           => esc_html__( 'Not found', 'bestdeals' ),
					'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'bestdeals' ),
				),
				'supports'            => array( 'title', 'excerpt', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields'),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'menu_icon'			  => 'dashicons-admin-home',
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 28,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'query_var'           => true,
				'capability_type'     => 'page',
				'rewrite'             => true
				)
			);
			
			// Prepare taxonomy for property
			bestdeals_require_data( 'taxonomy', 'property_group', array(
				'post_type'			=> array( 'property' ),
				'hierarchical'      => true,
				'labels'            => array(
					'name'              => esc_html_x( 'Property Group', 'taxonomy general name', 'bestdeals' ),
					'singular_name'     => esc_html_x( 'Group', 'taxonomy singular name', 'bestdeals' ),
					'search_items'      => esc_html__( 'Search Groups', 'bestdeals' ),
					'all_items'         => esc_html__( 'All Groups', 'bestdeals' ),
					'parent_item'       => esc_html__( 'Parent Group', 'bestdeals' ),
					'parent_item_colon' => esc_html__( 'Parent Group:', 'bestdeals' ),
					'edit_item'         => esc_html__( 'Edit Group', 'bestdeals' ),
					'update_item'       => esc_html__( 'Update Group', 'bestdeals' ),
					'add_new_item'      => esc_html__( 'Add New Group', 'bestdeals' ),
					'new_item_name'     => esc_html__( 'New Group Name', 'bestdeals' ),
					'menu_name'         => esc_html__( 'Property Group', 'bestdeals' ),
				),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'property_group' ),
				)
			);
		}
		
	}
}



if ( !function_exists( 'bestdeals_property_settings_theme_setup2' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_property_settings_theme_setup2', 3 );
	function bestdeals_property_settings_theme_setup2() {
		// Add post type 'property' and taxonomy 'property_group' into theme inheritance list
		bestdeals_add_theme_inheritance( array('property' => array(
			'stream_template' => 'blog-property',
			'single_template' => 'single-property',
			'taxonomy' => array('property_group'),
			'taxonomy_tags' => array(),
			'post_type' => array('property'),
			'override' => 'page'
			) )
		);
	}
}



// Return true, if current page is property page
if ( !function_exists( 'bestdeals_is_property_page' ) ) {
	function bestdeals_is_property_page() {
		return get_query_var('post_type')=='property' || is_tax('property_group') || (is_page() && bestdeals_get_template_page_id('blog-property')==get_the_ID());
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'bestdeals_property_detect_inheritance_key' ) ) {
	//Handler of add_filter('bestdeals_filter_detect_inheritance_key',	'bestdeals_property_detect_inheritance_key', 9, 1);
	function bestdeals_property_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return bestdeals_is_property_page() ? 'property' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'bestdeals_property_get_blog_type' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_type',	'bestdeals_property_get_blog_type', 9, 2);
	function bestdeals_property_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax('property_group') || is_tax('property_group'))
			$page = 'property_category';
		else if ($query && $query->get('post_type')=='property' || get_query_var('post_type')=='property')
			$page = $query && $query->is_single() || is_single() ? 'property_item' : 'property';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'bestdeals_property_get_blog_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_title',	'bestdeals_property_get_blog_title', 9, 2);
	function bestdeals_property_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( bestdeals_strpos($page, 'property')!==false ) {
			if ( $page == 'property_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'property_group' ), 'property_group', OBJECT);
				$title = $term->name;
			} else if ( $page == 'property_item' ) {
				$title = bestdeals_get_post_title();
			} else {
				$title = esc_html__('All property', 'bestdeals');
			}
		}
		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'bestdeals_property_get_stream_page_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_property_get_stream_page_title', 9, 2);
	function bestdeals_property_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (bestdeals_strpos($page, 'property')!==false) {
			if (($page_id = bestdeals_property_get_stream_page_id(0, $page=='property' ? 'blog-property' : $page)) > 0)
				$title = bestdeals_get_post_title($page_id);
			else
				$title = esc_html__('All property', 'bestdeals');				
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'bestdeals_property_get_stream_page_id' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_property_get_stream_page_id', 9, 2);
	function bestdeals_property_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (bestdeals_strpos($page, 'property')!==false) $id = bestdeals_get_template_page_id('blog-property');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'bestdeals_property_get_stream_page_link' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_property_get_stream_page_link', 9, 2);
	function bestdeals_property_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (bestdeals_strpos($page, 'property')!==false) {
			$id = bestdeals_get_template_page_id('blog-property');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'bestdeals_property_get_current_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_property_get_current_taxonomy', 9, 2);
	function bestdeals_property_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( bestdeals_strpos($page, 'property')!==false ) {
			$tax = 'property_group';
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'bestdeals_property_is_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_is_taxonomy',	'bestdeals_property_is_taxonomy', 9, 2);
	function bestdeals_property_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get('property_group')!='' || is_tax('property_group') ? 'property_group' : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'bestdeals_property_query_add_filters' ) ) {
	//Handler of add_filter('bestdeals_filter_query_add_filters',	'bestdeals_property_query_add_filters', 9, 2);
	function bestdeals_property_query_add_filters($args, $filter) {
		
		global $BESTDEALS_GLOBALS;
		
		if ($filter == 'property') {
			$args['post_type'] = 'property';
		}
		
		if ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_keyword']) ) {
			$args['s'] = $BESTDEALS_GLOBALS['blog_filters_property']['property_keyword'];
		}
		
		if ( isset($BESTDEALS_GLOBALS['blog_filters_property']) ) {
			$args['meta_query'] = array();
			
			foreach ($BESTDEALS_GLOBALS['blog_filters_property'] as $key => $value) {
				
				if ( $key == 'property-status' ) {
					$args['meta_query'][] = array (
						'key' => 'property_status_list',
						'value' => $value,
						'compare' => '='
					);
				}
				
				if ( $key == 'property_location' ) {
					$args['meta_query'][] = array (
						'key' => 'property_location',
						'value' => $value,
						'compare' => '='
					);
				}
				
				if ( $key == 'property_type_single' ) {
					$args['meta_query'][] = array (
						'key' => 'property_type_single',
						'value' => $value,
						'compare' => '='
					);
				}

			}



			if ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_bedrooms'] ))  {
				$args['meta_query'][] = array (
					'key' => 'property_bedrooms',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_bedrooms'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			}

			if ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_bathrooms'] ))  {
				$args['meta_query'][] = array (
					'key' => 'property_bathrooms',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_bathrooms'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			}

			if ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_area_min']) and isset($BESTDEALS_GLOBALS['blog_filters_property']['property_area_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_area',
					'value' => array( $BESTDEALS_GLOBALS['blog_filters_property']['property_area_min'], $BESTDEALS_GLOBALS['blog_filters_property']['property_area_max'] ),
					'compare' => 'BETWEEN',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_area_min']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_area',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_area_min'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_area_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_area',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_price_max'],
					'compare' => '<=',
					'type' => 'NUMERIC'
				);
			}

			if ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_price_min']) and isset($BESTDEALS_GLOBALS['blog_filters_property']['property_price_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_price',
					'value' => array( $BESTDEALS_GLOBALS['blog_filters_property']['property_price_min'], $BESTDEALS_GLOBALS['blog_filters_property']['property_price_max'] ),
					'compare' => 'BETWEEN',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_price_min']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_price',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_price_min'],
					'compare' => '>=',
					'type' => 'NUMERIC'
				);
			} elseif ( isset($BESTDEALS_GLOBALS['blog_filters_property']['property_price_max']) ) {
				$args['meta_query'][] = array (
					'key' => 'property_price',
					'value' => $BESTDEALS_GLOBALS['blog_filters_property']['property_price_max'],
					'compare' => '<=',
					'type' => 'NUMERIC'
				);
			}

		}
		
		return $args;
	}
}


if (!function_exists('bestdeals_property_after_theme_setup')) {
	add_action( 'bestdeals_action_after_init_theme', 'bestdeals_property_after_theme_setup' );
	function bestdeals_property_after_theme_setup() {
		// Update fields in the override options
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['post_override_options']) && $BESTDEALS_GLOBALS['post_override_options']['page']=='property') {
			// Override options fields
			$BESTDEALS_GLOBALS['post_override_options']['title'] = esc_html__('Property Options', 'bestdeals');
			$BESTDEALS_GLOBALS['post_override_options']['fields'] = array(
				"mb_partition_property" => array(
					"title" => esc_html__('Property details', 'bestdeals'),
					"override" => "page,post",
					"divider" => false,
					"icon" => "iconadmin-users",
					"type" => "partition"),
				"mb_info_property_1" => array(
					"title" => esc_html__('Property details', 'bestdeals'),
					"override" => "page,post",
					"divider" => false,
					"desc" => esc_attr__('In this section you can put details for this property', 'bestdeals'),
					"class" => "room_meta",
					"type" => "info"),
				
				"property_id_single" => array(
					"title" => esc_html__("Property ID",  'bestdeals'),
					"desc" => esc_attr__("Add unique propert ID", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_id_single",
					"std" => "",
					"type" => "text"),
				
				"property_status_list" => array( 
					"title" => esc_html__('Property status', 'bestdeals'),
					"desc" => esc_attr__('Select property status', 'bestdeals'),
					"override" => "post,page",
					"std" => "sale",
					"options" => array(
						'sale'  => esc_html__('Sale', 'bestdeals'),
						'rent' => esc_html__('Rent', 'bestdeals')
					),
					"type" => "radio"),
				
				"property_type_single" => array(
					"title" => esc_html__('Property type',  'bestdeals'),
					"desc" => esc_attr__("Select property type.", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_type_single",
					"std" => "",
					"options" => bestdeals_get_list_property_type_single(),
					"type" => "select"),
				
				"property_price" => array(
					"title" => esc_html__("Property price",  'bestdeals'),
					"desc" => esc_attr__("set your property price here(no currency sign required)", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_price",
					"std" => "0",
					"type" => "text"),
				
				"property_price_sign" => array(
					"title" => esc_html__('Currecny',  'bestdeals'),
					"desc" => esc_attr__("Select your currency", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_price_sign",
					"std" => "$",
					"options" => bestdeals_get_list_property_price_sign(),
					"type" => "select"),
				
				"property_price_per" => array(
					"title" => esc_html__('Rent period',  'bestdeals'),
					"desc" => esc_attr__("Select rent period here", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_price_per",
					"std" => "month",
					"options" => bestdeals_get_list_property_price_per(),
					"type" => "select"),
				
				"property_location" => array(
					"title" => esc_html__('Property location',  'bestdeals'),
					"desc" => esc_attr__("Select property location.", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_location",
					"std" => "",
					"options" => bestdeals_get_list_property_location(),
					"type" => "select"),
				
				
				"property_options" => array(
					"title" => esc_html__('Property options',  'bestdeals'),
					"desc" => wp_kses_data( __('Select property options.',  'bestdeals') ),
					"override" => "post,page",
					"std" => "",
					"options" => bestdeals_get_property_options_list(),
					"multiple" => true,
					"style" => "list",
					"type" => "select"),
				"property_area" => array(
					"title" => esc_html__("Property area",  'bestdeals'),
					"desc" => esc_attr__("Property area (Sq Ft)", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_area",
					"std" => "0",
					"type" => "text"),
				"property_bedrooms" => array(
					"title" => esc_html__('How many bedrooms?', 'bestdeals'),
					"desc" => esc_attr__("Set bedrooms number", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_bedrooms",
					"std" => "0",
					"min" => 0,
					"max" => 10,
					"step" => 1,
					"type" => "spinner"),
				"property_bathrooms" => array(
					"title" => esc_html__('How many bathrooms?', 'bestdeals'),
					"desc" => esc_attr__("Set bathrooms number", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_bathrooms",
					"std" => "0",
					"min" => 0,
					"max" => 10,
					"step" => 1,
					"type" => "spinner"),
				"property_garages" => array(
					"title" => esc_html__('How many garages?', 'bestdeals'),
					"desc" => esc_attr__("Set garages number", 'bestdeals'),
					"override" => "page,post",
					"class" => "property_garages",
					"std" => "0",
					"min" => 0,
					"max" => 10,
					"step" => 1,
					"type" => "spinner"),
			);
		}
	}
}



// Return property types
if ( !function_exists( 'bestdeals_get_list_property_type_single' ) ) {
	function bestdeals_get_list_property_type_single() {
		$options = get_option('bestdeals_options', array());
		$list = array(); $_list = array();
		if (!empty($options['property_type_list'])) {
			$_list = explode(',',$options['property_type_list']);
			
			foreach ($_list as $value) {
				$key = $value;
				$list[$key] = $value;
			}
			
		}
		return $list;
	}
}


// Return property location
if ( !function_exists( 'bestdeals_get_list_property_location' ) ) {
	function bestdeals_get_list_property_location() {
		$options = get_option('bestdeals_options', array());
		$list = array(); $_list = array();
		if (!empty($options['property_location_list'])) {
			$_list = explode(',',$options['property_location_list']);
			foreach ($_list as $value) {
				$list[$value] = $value;
			}
		}
		return $list;
	}
}

// Return price sign
if ( !function_exists( 'bestdeals_get_list_property_price_sign' ) ) {
	function bestdeals_get_list_property_price_sign() {
		$options = get_option('bestdeals_options', array());
		$list = array(); $_list = array();
		if (!empty($options['property_price_sign_list'])) {
			$_list = explode(',',$options['property_price_sign_list']);
			foreach ($_list as $value) {
				$list[$value] = $value;
			}
		}
		return $list;
	}
}

// Return price per
if ( !function_exists( 'bestdeals_get_list_property_price_per' ) ) {
	function bestdeals_get_list_property_price_per() {
		$options = get_option('bestdeals_options', array());
		$list = array(); $_list = array();
		if (!empty($options['property_price_per_list'])) {
			$_list = explode(',',$options['property_price_per_list']);
			foreach ($_list as $value) {
				$list[$value] = $value;
			}
		}
		return $list;
	}
}


// Return property options list
if ( !function_exists( 'bestdeals_get_property_options_list' ) ) {
	function bestdeals_get_property_options_list() {
		$options = get_option('bestdeals_options', array());
		$list = array(); $_list = array();
		if ( isset($options['property_options_list']) and !empty($options['property_options_list'])) {
			$_list = explode(',',$options['property_options_list']);
			foreach ($_list as $value) {
				$list[$value] = $value;
			}
			
		}
		return $list;
	}
}


// Before save custom options
if (!function_exists('bestdeals_property_post_save_custom_options')) {
	//Handler of add_filter('bestdeals_filter_post_save_custom_options',	'bestdeals_property_post_save_custom_options', 10, 3);
	function bestdeals_property_post_save_custom_options($custom_options, $post_type, $post_id) {

		// Save custom options - PROPERTY ID
		if ( isset($custom_options['property_id_single']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_id_single']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_id_single', $temp);
		}
		
		// Save custom options - PROPERTY STATUS (rent|sale)
		if ( isset($custom_options['property_status_list']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_status_list']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_status_list', $temp);
		}
		
		// Save custom options - PROPERTY TYPE
		if ( isset($custom_options['property_type_single']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_type_single']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_type_single', $temp);
		}
		
		// Save custom options - PROPERTY PRICE
		if ( isset($custom_options['property_price']) ) {
			if ( $custom_options['property_price'] == 'inherit' ) {
				$temp = '';
			} else {
				$temp = str_replace(" ", "", htmlspecialchars(trim($custom_options['property_price'])));
				$temp = str_replace(",", ".", $temp);
			}
			update_post_meta($post_id, 'property_price', $temp);
		}
		
		// Save custom options - PROPERTY PRICE SIGN ($,€)
		if ( isset($custom_options['property_price_sign']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_price_sign']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_price_sign', $temp);
		}
		
		// Save custom options - PROPERTY PRICE PER (month, ..)
		if ( isset($custom_options['property_price_per']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_price_per']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_price_per', $temp);
		}
		
		// Save custom options - PROPERTY LOCATION LIST
		if ( isset($custom_options['property_location']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_location']));
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_location', $temp);
		}
		
		
		// Save custom options - Property options post
		if ( isset($custom_options['property_options']) ) {
			$temp = htmlspecialchars(trim($custom_options['property_options']));			
			if ( $temp == 'inherit' ) { $temp = '';	}
			update_post_meta($post_id, 'property_options', $temp);
		}
		
		// Save custom options - PROPERTY AREA
		if ( isset($custom_options['property_area']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_area'])));
			update_post_meta($post_id, 'property_area', $temp);
		}
		
		// Save custom options - PROPERTY BEDROOMS
		if ( isset($custom_options['property_bedrooms']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_bedrooms'])));
			update_post_meta($post_id, 'property_bedrooms', $temp);
		}
		
		// Save custom options - PROPERTY BATHROOMS
		if ( isset($custom_options['property_bathrooms']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_bathrooms'])));
			update_post_meta($post_id, 'property_bathrooms', $temp);
		}

		// Save custom options - PROPERTY GARAGES
		if ( isset($custom_options['property_garages']) ) {
			$temp = (int) str_replace(" ", "", htmlspecialchars(trim($custom_options['property_garages'])));
			update_post_meta($post_id, 'property_garages', $temp);
		}
		
		return $custom_options;
	}
}


