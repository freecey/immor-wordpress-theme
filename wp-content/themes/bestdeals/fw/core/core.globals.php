<?php
/**
 * BestDEALS Framework: global variables storage
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get global variable
if (!function_exists('bestdeals_get_global')) {
	function bestdeals_get_global($var_name, $default='') {
		global $BESTDEALS_GLOBALS;
		return isset($BESTDEALS_GLOBALS[$var_name]) ? $BESTDEALS_GLOBALS[$var_name] : $default;
	}
}

// Set global variable
if (!function_exists('bestdeals_set_global')) {
	function bestdeals_set_global($var_name, $value) {
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS[$var_name] = $value;
	}
}

// Inc/Dec global variable with specified value
if (!function_exists('bestdeals_inc_global')) {
	function bestdeals_inc_global($var_name, $value=1) {
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS[$var_name] += $value;
	}
}

// Concatenate global variable with specified value
if (!function_exists('bestdeals_concat_global')) {
	function bestdeals_concat_global($var_name, $value) {
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS[$var_name] .= $value;
	}
}

// Get global array element
if (!function_exists('bestdeals_get_global_array')) {
	function bestdeals_get_global_array($var_name, $key) {
		global $BESTDEALS_GLOBALS;
		return isset($BESTDEALS_GLOBALS[$var_name][$key]) ? $BESTDEALS_GLOBALS[$var_name][$key] : '';
	}
}

// Set global array element
if (!function_exists('bestdeals_set_global_array')) {
	function bestdeals_set_global_array($var_name, $key, $value) {
		global $BESTDEALS_GLOBALS;
		if (!isset($BESTDEALS_GLOBALS[$var_name])) $BESTDEALS_GLOBALS[$var_name] = array();
		$BESTDEALS_GLOBALS[$var_name][$key] = $value;
	}
}

// Inc/Dec global array element with specified value
if (!function_exists('bestdeals_inc_global_array')) {
	function bestdeals_inc_global_array($var_name, $key, $value=1) {
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS[$var_name][$key] += $value;
	}
}

// Concatenate global array element with specified value
if (!function_exists('bestdeals_concat_global_array')) {
	function bestdeals_concat_global_array($var_name, $key, $value) {
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS[$var_name][$key] .= $value;
	}
}

// Merge two-dim array element
if (!function_exists('bestdeals_storage_merge_array')) {
	function bestdeals_storage_merge_array($var_name, $key, $arr) {
		global $BESTDEALS_GLOBALS;
		if (!isset($BESTDEALS_GLOBALS[$var_name])) $BESTDEALS_GLOBALS[$var_name] = array();
		if (!isset($BESTDEALS_GLOBALS[$var_name][$key])) $BESTDEALS_GLOBALS[$var_name][$key] = array();
		$BESTDEALS_GLOBALS[$var_name][$key] = array_merge($BESTDEALS_GLOBALS[$var_name][$key], $arr);
	}
}

// Set array element
if (!function_exists('bestdeals_storage_set_array')) {
	function bestdeals_storage_set_array($var_name, $key, $value) {
		global $BESTDEALS_GLOBALS;
		if (!isset($BESTDEALS_GLOBALS[$var_name])) $BESTDEALS_GLOBALS[$var_name] = array();
		if ($key==='')
			$BESTDEALS_GLOBALS[$var_name][] = $value;
		else
			$BESTDEALS_GLOBALS[$var_name][$key] = $value;
	}
}
?>