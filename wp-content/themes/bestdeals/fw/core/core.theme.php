<?php
/**
 * BestDEALS Framework: Theme specific actions
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */


// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_core_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_core_theme_setup', 11 );
	function bestdeals_core_theme_setup() {

		
		// Editor custom stylesheet - for user
		add_editor_style(bestdeals_get_file_url('css/editor-style.css'));	
		
		// Make theme available for translation
		// Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'bestdeals', bestdeals_get_folder_dir('languages') );


		/* Front and Admin actions and filters:
		------------------------------------------------------------------------ */

		if ( !is_admin() ) {
			
			/* Front actions and filters:
			------------------------------------------------------------------------ */

			// Get theme calendar (instead standard WP calendar) to support Events
			add_filter( 'get_calendar',						'bestdeals_get_calendar' );
	
			// Filters wp_title to print a neat <title> tag based on what is being viewed
			if (floatval(get_bloginfo('version')) < "4.1") {
				add_filter('wp_title',						'bestdeals_wp_title', 10, 2);
			}

			// Add main menu classes
			//Handler of add_filter('wp_nav_menu_objects', 			'bestdeals_add_mainmenu_classes', 10, 2);
	
			// Prepare logo text
			add_filter('bestdeals_filter_prepare_logo_text',	'bestdeals_prepare_logo_text', 10, 1);
	
			// Add class "widget_number_#' for each widget
			add_filter('dynamic_sidebar_params', 			'bestdeals_add_widget_number', 10, 1);

			// Frontend editor: Save post data
			add_action('wp_ajax_frontend_editor_save',		'bestdeals_callback_frontend_editor_save');

			// Frontend editor: Delete post
			add_action('wp_ajax_frontend_editor_delete', 	'bestdeals_callback_frontend_editor_delete');
	
			// Enqueue scripts and styles
			add_action('wp_enqueue_scripts', 				'bestdeals_core_frontend_scripts');
			add_action('wp_footer',		 					'bestdeals_core_frontend_scripts_inline', 9);
			add_filter('bestdeals_action_add_scripts_inline','bestdeals_core_add_scripts_inline');

			// Prepare theme core global variables
			add_action('bestdeals_action_prepare_globals',	'bestdeals_core_prepare_globals');

		}

		// Register theme specific nav menus
		bestdeals_register_theme_menus();

		// Register theme specific sidebars
		bestdeals_register_theme_sidebars();
	}
}




/* Theme init
------------------------------------------------------------------------ */

// Init theme template
function bestdeals_core_init_theme() {
	global $BESTDEALS_GLOBALS;
	if (!empty($BESTDEALS_GLOBALS['theme_inited'])) return;
	$BESTDEALS_GLOBALS['theme_inited'] = true;

	// Load custom options from GET and post/page/cat options
	if (isset($_GET['set']) && $_GET['set']==1) {
		foreach ($_GET as $k=>$v) {
			if (bestdeals_get_theme_option($k, null) !== null) {
				setcookie($k, $v, 0, '/');
				$_COOKIE[$k] = $v;
			}
		}
	}

	// Get custom options from current category / page / post / shop / event
	bestdeals_load_custom_options();

	// Load skin
	$skin = sanitize_file_name(bestdeals_get_custom_option('theme_skin'));
	$BESTDEALS_GLOBALS['theme_skin'] = $skin;
	if ( file_exists(bestdeals_get_file_dir('skins/'.($skin).'/skin.php')) ) {
		require_once( bestdeals_get_file_dir('skins/'.($skin).'/skin.php') );
	}

	// Fire init theme actions (after skin and custom options are loaded)
	do_action('bestdeals_action_init_theme');

	// Prepare theme core global variables
	do_action('bestdeals_action_prepare_globals');

	// Fire after init theme actions
	do_action('bestdeals_action_after_init_theme');
}


// Prepare theme global variables
if ( !function_exists( 'bestdeals_core_prepare_globals' ) ) {
	function bestdeals_core_prepare_globals() {
		if (!is_admin()) {
			// AJAX Queries settings
			global $BESTDEALS_GLOBALS;
			$BESTDEALS_GLOBALS['ajax_nonce'] = wp_create_nonce('ajax_nonce');
			$BESTDEALS_GLOBALS['ajax_url'] = admin_url('admin-ajax.php');
		
			// Logo text and slogan
			$BESTDEALS_GLOBALS['logo_text'] = apply_filters('bestdeals_filter_prepare_logo_text', bestdeals_get_custom_option('logo_text'));
			$slogan = bestdeals_get_custom_option('logo_slogan');
			if (!$slogan) $slogan = get_bloginfo ( 'description' );
			$BESTDEALS_GLOBALS['logo_slogan'] = $slogan;
			
			// Logo image and icons from skin
			$logo_side   = bestdeals_get_logo_icon('logo_side');
			$logo_fixed  = bestdeals_get_logo_icon('logo_fixed');
			$logo_footer = bestdeals_get_logo_icon('logo_footer');
			$BESTDEALS_GLOBALS['logo']        = bestdeals_get_logo_icon('logo');
			$BESTDEALS_GLOBALS['logo_icon']   = bestdeals_get_logo_icon('logo_icon');
			$BESTDEALS_GLOBALS['logo_side']   = $logo_side   ? $logo_side   : $BESTDEALS_GLOBALS['logo'];
			$BESTDEALS_GLOBALS['logo_fixed']  = $logo_fixed  ? $logo_fixed  : $BESTDEALS_GLOBALS['logo'];
			$BESTDEALS_GLOBALS['logo_footer'] = $logo_footer ? $logo_footer : $BESTDEALS_GLOBALS['logo'];
	
			$shop_mode = '';
			if (bestdeals_get_custom_option('show_mode_buttons')=='yes')
				$shop_mode = bestdeals_get_value_gpc('bestdeals_shop_mode');
			if (empty($shop_mode))
				$shop_mode = bestdeals_get_custom_option('shop_mode', '');
			if (empty($shop_mode) || !is_archive())
				$shop_mode = 'thumbs';
			$BESTDEALS_GLOBALS['shop_mode'] = $shop_mode;
		}
	}
}


// Return url for the uploaded logo image or (if not uploaded) - to image from skin folder
if ( !function_exists( 'bestdeals_get_logo_icon' ) ) {
	function bestdeals_get_logo_icon($slug) {
		$logo_icon = bestdeals_get_custom_option($slug);
		return $logo_icon;
	}
}


// Add menu locations
if ( !function_exists( 'bestdeals_register_theme_menus' ) ) {
	function bestdeals_register_theme_menus() {
		register_nav_menus(apply_filters('bestdeals_filter_add_theme_menus', array(
			'menu_main'		=> esc_html__('Main menu', 'bestdeals'),
			'menu_user'		=> esc_html__('User Menu', 'bestdeals'),
			'menu_footer'	=> esc_html__('Footer Menu', 'bestdeals'),
			'menu_side'		=> esc_html__('Side Menu', 'bestdeals')
		)));
	}
}


// Register widgetized area
if ( !function_exists( 'bestdeals_register_theme_sidebars' ) ) {
	add_action('widgets_init', 'bestdeals_register_theme_sidebars');
	function bestdeals_register_theme_sidebars($sidebars=array()) {
		global $BESTDEALS_GLOBALS;
		if (!is_array($sidebars)) $sidebars = array();
		// Custom sidebars
		$custom = bestdeals_get_theme_option('custom_sidebars');
		if (is_array($custom) && count($custom) > 0) {
			foreach ($custom as $i => $sb) {
				if (trim(chop($sb))=='') continue;
				$sidebars['sidebar_custom_'.($i)]  = $sb;
			}
		}
		$sidebars = apply_filters( 'bestdeals_filter_add_theme_sidebars', $sidebars );
		$registered = $BESTDEALS_GLOBALS['registered_sidebars'];
		if (!is_array($registered)) $registered = array();
		if (is_array($sidebars) && count($sidebars) > 0) {
			foreach ($sidebars as $id=>$name) {
				if (isset($registered[$id])) continue;
				$registered[$id] = $name;
				register_sidebar( array(
					'name'          => $name,
					'id'            => $id,
					'before_widget' => '<aside id="%1$s" class="widget %2$s">',
					'after_widget'  => '</aside>',
					'before_title'  => '<h5 class="widget_title_">',
					'after_title'   => '</h5>',
				) );
			}
		}
		$BESTDEALS_GLOBALS['registered_sidebars'] = $registered;
	}
}





/* Front actions and filters:
------------------------------------------------------------------------ */

//  Enqueue scripts and styles
if ( !function_exists( 'bestdeals_core_frontend_scripts' ) ) {
	function bestdeals_core_frontend_scripts() {

		// Enqueue styles
		//-----------------------------------------------------------------------------------------------------
		
		// Prepare custom fonts
		$fonts = bestdeals_get_list_fonts(false);
		$theme_fonts = array();
		$custom_fonts = bestdeals_get_custom_fonts();
		if (is_array($custom_fonts) && count($custom_fonts) > 0) {
			foreach ($custom_fonts as $s=>$f) {
				if (!empty($f['font-family']) && !bestdeals_is_inherit_option($f['font-family'])) $theme_fonts[$f['font-family']] = 1;
			}
		}
		// Prepare current skin fonts
		$theme_fonts = apply_filters('bestdeals_filter_used_fonts', $theme_fonts);
		// Link to selected fonts
		if (is_array($theme_fonts) && count($theme_fonts) > 0) {
			/*
			 Translators: If there are characters in your language that are not supported
			 by chosen font(s), translate this to 'off'. Do not translate into your own language.
			 */
			$google_fonts_enabled = ( 'off' !== esc_html_x( 'on', 'Google fonts: on or off', 'bestdeals' ) );
			foreach ($theme_fonts as $font=>$v) {
				if (isset($fonts[$font])) {						
					$font_name = ($pos=bestdeals_strpos($font,' ('))!==false ? bestdeals_substr($font, 0, $pos) : $font;
					$css = !empty($fonts[$font]['css']) 
						? $fonts[$font]['css'] 
						: bestdeals_get_protocol().'://fonts.googleapis.com/css?family='
							.(!empty($fonts[$font]['link']) ? $fonts[$font]['link'] : str_replace(' ', '+', $font_name).':100,100italic,300,300italic,400,400italic,700,700italic')
							.(empty($fonts[$font]['link']) || bestdeals_strpos($fonts[$font]['link'], 'subset=')===false ? '&subset=latin,latin-ext,cyrillic,cyrillic-ext' : '');
					
					if ( $google_fonts_enabled ) {
						wp_enqueue_style( 'bestdeals-font-'.str_replace(' ', '-', $font_name), $css, array(), null );
					}
				}
			}
		}
		
		// Fontello styles must be loaded before main stylesheet
		wp_enqueue_style( 'fontello-style',  bestdeals_get_file_url('css/fontello/css/fontello.css'),  array(), null);

		// Main stylesheet
		wp_enqueue_style( 'bestdeals-main-style', get_stylesheet_uri(), array(), null );
		
		// Animations
		if (bestdeals_get_theme_option('css_animation')=='yes')
			wp_enqueue_style( 'animation-style',	bestdeals_get_file_url('css/core.animation.css'), array(), null );

		// Theme skin stylesheet
		do_action('bestdeals_action_add_styles');
		
		// Theme customizer stylesheet and inline styles
		bestdeals_enqueue_custom_styles();

		// Responsive
		if (bestdeals_get_theme_option('responsive_layouts') == 'yes') {
			$suffix = bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_outer')) ? '' : '-outer';
			wp_enqueue_style( 'bestdeals-responsive-style', bestdeals_get_file_url('css/responsive'.($suffix).'.css'), array(), null );
			do_action('bestdeals_action_add_responsive');
			if (bestdeals_get_custom_option('theme_skin')!='') {
				$css = apply_filters('bestdeals_filter_add_responsive_inline', '');
				if (!empty($css)) wp_add_inline_style( 'bestdeals-responsive-style', $css );
			}
		}


		// Enqueue scripts	
		//----------------------------------------------------------------------------------------------------------------------------
		
		// Load separate theme scripts
		wp_enqueue_script( 'superfish', bestdeals_get_file_url('js/superfish.min.js'), array('jquery'), null, true );
		if (bestdeals_get_theme_option('menu_slider')=='yes') {
			wp_enqueue_script( 'slidemenu-script', bestdeals_get_file_url('js/jquery.slidemenu.js'), array('jquery'), null, true );
		}

		if ( is_single() && bestdeals_get_custom_option('show_reviews')=='yes' ) {
			wp_enqueue_script( 'bestdeals-core-reviews-script', bestdeals_get_file_url('js/core.reviews.js'), array('jquery'), null, true );
		}

		wp_enqueue_script( 'bestdeals-core-utils-script', bestdeals_get_file_url('js/core.utils.js'), array('jquery'), null, true );
		wp_enqueue_script( 'bestdeals-core-init-script', bestdeals_get_file_url('js/core.init.js'), array('jquery'), null, true );

		// Media elements library	
		global $wp_styles, $wp_scripts;
		if (bestdeals_get_theme_option('use_mediaelement')=='yes') {
			wp_enqueue_style ( 'mediaelement' );
			wp_enqueue_style ( 'wp-mediaelement' );
			wp_enqueue_script( 'mediaelement' );
			wp_enqueue_script( 'wp-mediaelement' );
		} else {
			$wp_scripts->done[]	= 'mediaelement';
			$wp_scripts->done[]	= 'wp-mediaelement';
			$wp_styles->done[]	= 'mediaelement';
			$wp_styles->done[]	= 'wp-mediaelement';
		}
		
		$wp_styles->done[] = 'cpcff_jquery_ui';
		
		// Video background
		if (bestdeals_get_custom_option('show_video_bg') == 'yes' && bestdeals_get_custom_option('video_bg_youtube_code') != '') {
			wp_enqueue_script( 'video-bg-script', bestdeals_get_file_url('js/jquery.tubular.1.0.js'), array('jquery'), null, true );
		}

		// Google map
		$api_key = bestdeals_get_theme_option('api_google');
		if ( bestdeals_get_custom_option('show_googlemap')=='yes' && !empty($api_key) ) {
			wp_enqueue_script( 'googlemap', bestdeals_get_protocol().'://maps.google.com/maps/api/js'.($api_key ? '?key='.$api_key : ''), array(), null, true );
			wp_enqueue_script( 'bestdeals-googlemap-script', bestdeals_get_file_url('js/core.googlemap.js'), array(), null, true );
		}

			
		// Social share buttons
		if (is_singular() && !bestdeals_get_global('blog_streampage') && bestdeals_get_custom_option('show_share')!='hide') {
			wp_enqueue_script( 'bestdeals-social-share-script', bestdeals_get_file_url('js/social/social-share.js'), array('jquery'), null, true );
		}

		// Comments
		if ( is_singular() && !bestdeals_get_global('blog_streampage') && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply', false, array(), null, true );
		}

		// Custom panel
		if (bestdeals_get_theme_option('show_theme_customizer') == 'yes') {
			if (file_exists(bestdeals_get_file_dir('core/core.customizer/front.customizer.css')))
				wp_enqueue_style(  'bestdeals-customizer-style',  bestdeals_get_file_url('core/core.customizer/front.customizer.css'), array(), null );
			if (file_exists(bestdeals_get_file_dir('core/core.customizer/front.customizer.js')))
				wp_enqueue_script( 'bestdeals-customizer-script', bestdeals_get_file_url('core/core.customizer/front.customizer.js'), array(), null, true );
		}
		
		//Debug utils
		if (bestdeals_get_theme_option('debug_mode')=='yes') {
			wp_enqueue_script( 'bestdeals-core-debug-script', bestdeals_get_file_url('js/core.debug.js'), array(), null, true );
		}

		// Theme skin script
		do_action('bestdeals_action_add_scripts');
	}
}

//  Enqueue Swiper Slider scripts and styles
if ( !function_exists( 'bestdeals_enqueue_slider' ) ) {
	function bestdeals_enqueue_slider($engine='all') {
		if ($engine=='all' || $engine=='swiper') {
			wp_enqueue_style( 'swiperslider-style', 				bestdeals_get_file_url('js/swiper/swiper.css'), array(), null );
			wp_enqueue_script( 'swiperslider-script', 			bestdeals_get_file_url('js/swiper/swiper.js'), array('jquery'), null, true );
			wp_enqueue_script( 'swiperslider-scrollbar-script',	bestdeals_get_file_url('js/swiper/swiper.scrollbar.js'), array('jquery'), null, true );
		}
	}
}

//  Enqueue Messages scripts and styles
if ( !function_exists( 'bestdeals_enqueue_messages' ) ) {
	function bestdeals_enqueue_messages() {
		wp_enqueue_style( 'bestdeals-messages-style',		bestdeals_get_file_url('js/core.messages/core.messages.css'), array(), null );
		wp_enqueue_script( 'bestdeals-messages-script',	bestdeals_get_file_url('js/core.messages/core.messages.js'),  array('jquery'), null, true );
	}
}

//  Enqueue Portfolio hover scripts and styles
if ( !function_exists( 'bestdeals_enqueue_portfolio' ) ) {
	function bestdeals_enqueue_portfolio($hover='') {
		wp_enqueue_style( 'bestdeals-portfolio-style',  bestdeals_get_file_url('css/core.portfolio.css'), array(), null );
		if (bestdeals_strpos($hover, 'effect_dir')!==false)
			wp_enqueue_script( 'hoverdir', bestdeals_get_file_url('js/hover/jquery.hoverdir.js'), array(), null, true );
	}
}


// Enqueue Theme Popup scripts and styles
// Link must have attribute: data-rel="popup" or data-rel="popup[gallery]"
if ( !function_exists( 'bestdeals_enqueue_popup' ) ) {
	function bestdeals_enqueue_popup($engine='') {
		if ($engine=='pretty' || (empty($engine) && bestdeals_get_theme_option('popup_engine')=='pretty')) {
			wp_enqueue_style(  'prettyphoto-style',	bestdeals_get_file_url('js/prettyphoto/css/prettyPhoto.css'), array(), null );
			wp_enqueue_script( 'prettyphoto-script',	bestdeals_get_file_url('js/prettyphoto/jquery.prettyPhoto.min.js'), array('jquery'), 'no-compose', true );
		} else if ($engine=='magnific' || (empty($engine) && bestdeals_get_theme_option('popup_engine')=='magnific')) {
			wp_enqueue_style(  'magnific-style',	bestdeals_get_file_url('js/magnific/magnific-popup.css'), array(), null );
			wp_enqueue_script( 'magnific-script',bestdeals_get_file_url('js/magnific/jquery.magnific-popup.min.js'), array('jquery'), '', true );
		} else if ($engine=='internal' || (empty($engine) && bestdeals_get_theme_option('popup_engine')=='internal')) {
			bestdeals_enqueue_messages();
		}
	}
}

//  Add inline scripts in the footer hook
if ( !function_exists( 'bestdeals_core_frontend_scripts_inline' ) ) {
	function bestdeals_core_frontend_scripts_inline() {
		global $BESTDEALS_GLOBALS;

		$vars = empty($BESTDEALS_GLOBALS['js_vars']) ? array() : $BESTDEALS_GLOBALS['js_vars'];
		if (empty($vars) || !is_array($vars)) $vars = array();
		wp_localize_script('bestdeals-core-init-script', 'BESTDEALS_GLOBALS', apply_filters('bestdeals_action_add_scripts_inline', $vars));
		$code = empty($BESTDEALS_GLOBALS['js_code']) ? '' : $BESTDEALS_GLOBALS['js_vars'];

		if (!empty($js_code)) {
			$st = '<';
			$ct = '/';
			$et = '>';
			bestdeals_show_layout($code, "{$st}script{$et}jQuery(document).ready(function(){", "});{$st}{$ct}script{$et}");
		}
	}
}

//  Add inline scripts in the footer
if (!function_exists('bestdeals_core_add_scripts_inline')) {
	function bestdeals_core_add_scripts_inline($vars = array()) {
		global $BESTDEALS_GLOBALS;

		$msg = bestdeals_get_system_message(true); 
		if (!empty($msg['message'])) bestdeals_enqueue_messages();

		$vars ['ajax_url'] = esc_url($BESTDEALS_GLOBALS['ajax_url']);
		$vars ['ajax_nonce'] = esc_attr($BESTDEALS_GLOBALS['ajax_nonce']);
		$vars ['ajax_nonce_editor'] = esc_attr(wp_create_nonce('bestdeals_editor_nonce'));
		$vars ['site_url'] = get_site_url();
		$vars ['vc_edit_mode'] = (bestdeals_vc_is_frontend() ? true : false);
		$vars ['theme_font'] = bestdeals_get_custom_font_settings('p', 'font-family');
		$vars ['theme_skin'] = esc_attr(bestdeals_get_custom_option('theme_skin'));
		$vars ['theme_skin_color'] = bestdeals_get_scheme_color('text_dark');
		$vars ['theme_skin_bg_color'] = bestdeals_get_scheme_color('bg_color');
		$vars ['slider_height'] = max(100, bestdeals_get_custom_option('slider_height'));
		$vars ['system_message'] = array(
			'message' => addslashes($msg['message']),
			'status' => addslashes($msg['status']),
			'header' => addslashes($msg['header']),
		);
		$vars ['user_logged_in'] = (is_user_logged_in() ? true : false);
			
		// Show table of content for the current page
		$vars ['toc_menu']		=  esc_attr(bestdeals_get_custom_option('menu_toc')) ;
		$vars ['toc_menu_home']	=  (bestdeals_get_custom_option('menu_toc')!='hide' && bestdeals_get_custom_option('menu_toc_home')=='yes' ? true : false) ;
		$vars ['toc_menu_top']	=  (bestdeals_get_custom_option('menu_toc')!='hide' && bestdeals_get_custom_option('menu_toc_top')=='yes' ? true : false) ;
		
		// Fix main menu
		$vars ['menu_fixed']		=  (bestdeals_get_theme_option('menu_attachment')=='fixed' ? true : false) ;
		
		// Use responsive version for main menu
		$vars ['menu_relayout']	=  max(0, (int) bestdeals_get_theme_option('menu_relayout')) ;
		$vars ['menu_responsive']	=  (bestdeals_get_theme_option('responsive_layouts') == 'yes' ? max(0, (int) bestdeals_get_theme_option('menu_responsive')) : 0) ;
		$vars ['menu_slider']     =  (bestdeals_get_theme_option('menu_slider')=='yes' ? true : false) ;

		// Right panel demo timer
		$vars ['demo_time']	= (bestdeals_get_theme_option('show_theme_customizer')=='yes' ? max(0, (int) bestdeals_get_theme_option('customizer_demo')) : 0) ;

		// Video and Audio tag wrapper
		$vars ['media_elements_enabled'] =  (bestdeals_get_theme_option('use_mediaelement')=='yes' ? true : false) ;
		
		// Use AJAX search
		$vars ['ajax_search_enabled'] 	=  (bestdeals_get_theme_option('use_ajax_search')=='yes' ? true : false) ;
		$vars ['ajax_search_min_length']	= min(3, bestdeals_get_theme_option('ajax_search_min_length')) ;
		$vars ['ajax_search_delay']		=  min(200, max(1000, bestdeals_get_theme_option('ajax_search_delay'))) ;

		// Use CSS animation
		$vars ['css_animation']      = (bestdeals_get_theme_option('css_animation')=='yes' ? true : false) ;
		$vars ['menu_animation_in']  =  esc_attr(bestdeals_get_theme_option('menu_animation_in'));
		$vars ['menu_animation_out'] = esc_attr(bestdeals_get_theme_option('menu_animation_out')) ;

		// Popup windows engine
		$vars ['popup_engine']	=  esc_attr(bestdeals_get_theme_option('popup_engine')) ;

		// E-mail mask
		$vars ['email_mask']		= '^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$';
		
		// Messages max length
		$vars ['contacts_maxlength']	= intval(bestdeals_get_theme_option('message_maxlength_contacts')) ;
		$vars ['comments_maxlength']	= intval(bestdeals_get_theme_option('message_maxlength_comments')) ;

		// Remember visitors settings
		$vars ['remember_visitors_settings']	= (bestdeals_get_theme_option('remember_visitors_settings')=='yes' ? true : false) ;

		// Internal vars - do not change it!
		// Flag for review mechanism
		$vars ['admin_mode']			= false;
		// Max scale factor for the portfolio and other isotope elements before relayout
		$vars ['isotope_resize_delta']	= 0.3;
		// jQuery object for the message box in the form
		$vars ['error_message_box']	= null;

		$vars ['viewmore_busy']		= false;
		$vars ['video_resize_inited']	= false;
		$vars ['top_panel_height']		= 0;

		return $vars;
	}
}


//  Enqueue Custom styles (main Theme options settings)
if ( !function_exists( 'bestdeals_enqueue_custom_styles' ) ) {
	function bestdeals_enqueue_custom_styles() {
		// Custom stylesheet
		$custom_css = '';
		wp_enqueue_style( 'bestdeals-custom-style', $custom_css ? $custom_css : bestdeals_get_file_url('css/custom-style.css'), array(), null );
		// Custom inline styles
		wp_add_inline_style( 'bestdeals-custom-style', bestdeals_prepare_custom_styles() );
	}
}

// Add class "widget_number_#' for each widget
if ( !function_exists( 'bestdeals_add_widget_number' ) ) {
	//Handler of add_filter('dynamic_sidebar_params', 'bestdeals_add_widget_number', 10, 1);
	function bestdeals_add_widget_number($prm) {
		global $BESTDEALS_GLOBALS;
		if (is_admin()) return $prm;
		static $num=0, $last_sidebar='', $last_sidebar_id='', $last_sidebar_columns=0, $last_sidebar_count=0, $sidebars_widgets=array();
		$cur_sidebar = !empty($BESTDEALS_GLOBALS['current_sidebar']) ? $BESTDEALS_GLOBALS['current_sidebar'] : 'undefined';
		if (count($sidebars_widgets) == 0)
			$sidebars_widgets = wp_get_sidebars_widgets();
		if ($last_sidebar != $cur_sidebar) {
			$num = 0;
			$last_sidebar = $cur_sidebar;
			$last_sidebar_id = $prm[0]['id'];
			$last_sidebar_columns = max(1, (int) bestdeals_get_custom_option('sidebar_'.($cur_sidebar).'_columns'));
			$last_sidebar_count = count($sidebars_widgets[$last_sidebar_id]);
		}
		$num++;
		$prm[0]['before_widget'] = str_replace(' class="', ' class="widget_number_'.esc_attr($num).($last_sidebar_columns > 1 ? ' column-1_'.esc_attr($last_sidebar_columns) : '').' ', $prm[0]['before_widget']);
		return $prm;
	}
}


// Filters wp_title to print a neat <title> tag based on what is being viewed.
if ( !function_exists( 'bestdeals_wp_title' ) ) {
	function bestdeals_wp_title( $title, $sep ) {
		global $page, $paged;
		if ( is_feed() ) return $title;
		// Add the blog name
		$title .= get_bloginfo( 'name' );
		// Add the blog description for the home/front page.
		if ( is_home() || is_front_page() ) {
			$site_description = bestdeals_get_custom_option('logo_slogan');
			if (empty($site_description)) 
				$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description )
				$title .= " $sep $site_description";
		}
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			$title .= " $sep " . sprintf( esc_attr__( 'Page %s', 'bestdeals' ), max( $paged, $page ) );
		return $title;
	}
}

// Add main menu classes
if ( !function_exists( 'bestdeals_add_mainmenu_classes' ) ) {
	function bestdeals_add_mainmenu_classes($items, $args) {
		if (is_admin()) return $items;
		if ($args->menu_id == 'mainmenu' && bestdeals_get_theme_option('menu_colored')=='yes' && is_array($items) && count($items) > 0) {
			foreach($items as $k=>$item) {
				if ($item->menu_item_parent==0) {
					if ($item->type=='taxonomy' && $item->object=='category') {
						$cur_tint = bestdeals_taxonomy_get_inherited_property('category', $item->object_id, 'bg_tint');
						if (!empty($cur_tint) && !bestdeals_is_inherit_option($cur_tint))
							$items[$k]->classes[] = 'bg_tint_'.esc_attr($cur_tint);
					}
				}
			}
		}
		return $items;
	}
}


// Save post data from frontend editor
if ( !function_exists( 'bestdeals_callback_frontend_editor_save' ) ) {
	function bestdeals_callback_frontend_editor_save() {
		global $_REQUEST;

		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'bestdeals_editor_nonce' ) )
			wp_die();

		$response = array('error'=>'');

		parse_str($_REQUEST['data'], $output);
		$post_id = $output['frontend_editor_post_id'];

		if ( bestdeals_get_theme_option("allow_editor")=='yes' && (current_user_can('edit_posts', $post_id) || current_user_can('edit_pages', $post_id)) ) {
			if ($post_id > 0) {
				$title   = stripslashes($output['frontend_editor_post_title']);
				$content = stripslashes($output['frontend_editor_post_content']);
				$excerpt = stripslashes($output['frontend_editor_post_excerpt']);
				$rez = wp_update_post(array(
					'ID'           => $post_id,
					'post_content' => $content,
					'post_excerpt' => $excerpt,
					'post_title'   => $title
				));
				if ($rez == 0) 
					$response['error'] = esc_html__('Post update error!', 'bestdeals');
			} else {
				$response['error'] = esc_html__('Post update error!', 'bestdeals');
			}
		} else
			$response['error'] = esc_html__('Post update denied!', 'bestdeals');
		
		echo json_encode($response);
		wp_die();
	}
}

// Delete post from frontend editor
if ( !function_exists( 'bestdeals_callback_frontend_editor_delete' ) ) {
	function bestdeals_callback_frontend_editor_delete() {
		global $_REQUEST;

		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'bestdeals_editor_nonce' ) )
			wp_die();

		$response = array('error'=>'');
		
		$post_id = bestdeals_get_value_gp('post_id');

		if ( bestdeals_get_theme_option("allow_editor")=='yes' && (current_user_can('delete_posts', $post_id) || current_user_can('delete_pages', $post_id)) ) {
			if ($post_id > 0) {
				$rez = wp_delete_post($post_id);
				if ($rez === false) 
					$response['error'] = esc_html__('Post delete error!', 'bestdeals');
			} else {
				$response['error'] = esc_html__('Post delete error!', 'bestdeals');
			}
		} else
			$response['error'] = esc_html__('Post delete denied!', 'bestdeals');

		echo json_encode($response);
		wp_die();
	}
}

//------------------------------------------------------------------------
// One-click import support
//------------------------------------------------------------------------

// Set theme specific importer options
if ( !function_exists( 'themerex_importer_set_options' ) ) {
    add_filter( 'trx_utils_filter_importer_options', 'themerex_importer_set_options', 9 );
    function themerex_importer_set_options($options=array()) {
        if (is_array($options)) {
            // Save or not installer's messages to the log-file
            $options['debug'] = false;
            // Prepare demo data
            if ( is_dir(BESTDEALS_PATH . 'demo/') ) {
                $options['demo_url'] = BESTDEALS_PATH . 'demo/';
            } else {
                $options['demo_url'] = esc_url(bestdeals_get_protocol().'://demofiles.axiomthemes.com/bestdeals/'); // Demo-site domain
            }

            // Required plugins
            $options['required_plugins'] = array(
                'js_composer',
				'essential-grid',
                'revslider',
                'booked',
            );

            $options['theme_slug'] = 'bestdeals';

            // Set number of thumbnails to regenerate when its imported (if demo data was zipped without cropped images)
            // Set 0 to prevent regenerate thumbnails (if demo data archive is already contain cropped images)
            $options['regenerate_thumbnails'] = 3;
            // Default demo
            $options['files']['default']['title'] = esc_html__('BestDeals Demo', 'bestdeals');
            $options['files']['default']['domain_dev'] = esc_url(bestdeals_get_protocol().'://bestdeals.dv.axiomthemes.com');	// Developers domain
            $options['files']['default']['domain_demo']= esc_url(bestdeals_get_protocol().'://bestdeals.axiomthemes.com');	// Demo-site domain

        }
        return $options;
    }
}


// Prepare logo text
if ( !function_exists( 'bestdeals_prepare_logo_text' ) ) {
	function bestdeals_prepare_logo_text($text) {
		$text = str_replace(array('[', ']'), array('<span class="theme_accent">', '</span>'), $text);
		$text = str_replace(array('{', '}'), array('<strong>', '</strong>'), $text);
		return $text;
	}
}
?>