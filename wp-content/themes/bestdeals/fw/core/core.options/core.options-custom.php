<?php
/**
 * BestDEALS Framework: Theme options custom fields
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_options_custom_theme_setup' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_options_custom_theme_setup' );
	function bestdeals_options_custom_theme_setup() {

		if ( is_admin() ) {
			add_action("admin_enqueue_scripts",	'bestdeals_options_custom_load_scripts');
		}
		
	}
}

// Load required styles and scripts for custom options fields
if ( !function_exists( 'bestdeals_options_custom_load_scripts' ) ) {
	//Handler of add_action("admin_enqueue_scripts", 'bestdeals_options_custom_load_scripts');
	function bestdeals_options_custom_load_scripts() {
		wp_enqueue_script( 'bestdeals-options-custom-script',	bestdeals_get_file_url('core/core.options/js/core.options-custom.js'), array(), null, true );
	}
}


// Show theme specific fields in Post (and Page) options
function bestdeals_show_custom_field($id, $field, $value) {
	$output = '';
	switch ($field['type']) {
		case 'reviews':
			$output .= '<div class="reviews_block">' . trim(bestdeals_reviews_get_markup($field, $value, true)) . '</div>';
			break;

		case 'mediamanager':
			wp_enqueue_media( );
			$output .= '<a id="'.esc_attr($id).'" class="button mediamanager"
				data-param="' . esc_attr($id) . '"
				data-choose="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'bestdeals') : esc_html__( 'Choose Image', 'bestdeals')).'"
				data-update="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Add to Gallery', 'bestdeals') : esc_html__( 'Choose Image', 'bestdeals')).'"
				data-multiple="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? 'true' : 'false').'"
				data-linked-field="'.esc_attr($field['media_field_id']).'"
				onclick="bestdeals_show_media_manager(this); return false;"
				>' . (isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'bestdeals') : esc_html__( 'Choose Image', 'bestdeals')) . '</a>';
			break;
	}
	return apply_filters('bestdeals_filter_show_custom_field', $output, $id, $field, $value);
}
?>