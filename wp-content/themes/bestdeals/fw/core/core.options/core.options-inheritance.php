<?php
//####################################################
//#### Inheritance system (for internal use only) #### 
//####################################################

// Add item to the inheritance settings
if ( !function_exists( 'bestdeals_add_theme_inheritance' ) ) {
	function bestdeals_add_theme_inheritance($options, $append=true) {
		global $BESTDEALS_GLOBALS;
		if (!isset($BESTDEALS_GLOBALS["inheritance"])) $BESTDEALS_GLOBALS["inheritance"] = array();
		$BESTDEALS_GLOBALS['inheritance'] = $append 
			? bestdeals_array_merge($BESTDEALS_GLOBALS['inheritance'], $options) 
			: bestdeals_array_merge($options, $BESTDEALS_GLOBALS['inheritance']);
	}
}



// Return inheritance settings
if ( !function_exists( 'bestdeals_get_theme_inheritance' ) ) {
	function bestdeals_get_theme_inheritance($key = '') {
		global $BESTDEALS_GLOBALS;
		return $key ? $BESTDEALS_GLOBALS['inheritance'][$key] : $BESTDEALS_GLOBALS['inheritance'];
	}
}



// Detect inheritance key for the current mode
if ( !function_exists( 'bestdeals_detect_inheritance_key' ) ) {
	function bestdeals_detect_inheritance_key() {
		static $inheritance_key = '';
		if (!empty($inheritance_key)) return $inheritance_key;
		$inheritance_key = apply_filters('bestdeals_filter_detect_inheritance_key', '');
		return $inheritance_key;
	}
}


// Return key for override parameter
if ( !function_exists( 'bestdeals_get_override_key' ) ) {
	function bestdeals_get_override_key($value, $by) {
		$key = '';
		$inheritance = bestdeals_get_theme_inheritance();
		if (!empty($inheritance) && is_array($inheritance)) {
			foreach ($inheritance as $k=>$v) {
				if (!empty($v[$by]) && in_array($value, $v[$by])) {
					$key = $by=='taxonomy' 
						? $value
						: (!empty($v['override']) ? $v['override'] : $k);
					break;
				}
			}
		}
		return $key;
	}
}


// Return taxonomy (for categories) by post_type from inheritance array
if ( !function_exists( 'bestdeals_get_taxonomy_categories_by_post_type' ) ) {
	function bestdeals_get_taxonomy_categories_by_post_type($value) {
		$key = '';
		$inheritance = bestdeals_get_theme_inheritance();
		if (!empty($inheritance) && is_array($inheritance)) {
			foreach ($inheritance as $k=>$v) {
				if (!empty($v['post_type']) && in_array($value, $v['post_type'])) {
					$key = !empty($v['taxonomy']) ? $v['taxonomy'][0] : '';
					break;
				}
			}
		}
		return $key;
	}
}


// Return taxonomy (for tags) by post_type from inheritance array
if ( !function_exists( 'bestdeals_get_taxonomy_tags_by_post_type' ) ) {
	function bestdeals_get_taxonomy_tags_by_post_type($value) {
		$key = '';
		$inheritance = bestdeals_get_theme_inheritance();
		if (!empty($inheritance) && is_array($inheritance)) {
			foreach($inheritance as $k=>$v) {
				if (!empty($v['post_type']) && in_array($value, $v['post_type'])) {
					$key = !empty($v['taxonomy_tags']) ? $v['taxonomy_tags'][0] : '';
					break;
				}
			}
		}
		return $key;
	}
}
?>