<?php
/**
 * BestDEALS Framework: return lists
 *
 * @package bestdeals
 * @since bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }



// Return styles list
if ( !function_exists( 'bestdeals_get_list_styles' ) ) {
	function bestdeals_get_list_styles($from=1, $to=2, $prepend_inherit=false) {
		$list = array();
		for ($i=$from; $i<=$to; $i++)
			$list[$i] = sprintf(esc_attr__('Style %d', 'bestdeals'), $i);
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the animations
if ( !function_exists( 'bestdeals_get_list_animations' ) ) {
	function bestdeals_get_list_animations($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_animations']))
			$list = $BESTDEALS_GLOBALS['list_animations'];
		else {
			$list = array();
			$list['none']			= esc_html__('- None -',	'bestdeals');
			$list['bounced']		= esc_html__('Bounced',		'bestdeals');
			$list['flash']			= esc_html__('Flash',		'bestdeals');
			$list['flip']			= esc_html__('Flip',		'bestdeals');
			$list['pulse']			= esc_html__('Pulse',		'bestdeals');
			$list['rubberBand']		= esc_html__('Rubber Band',	'bestdeals');
			$list['shake']			= esc_html__('Shake',		'bestdeals');
			$list['swing']			= esc_html__('Swing',		'bestdeals');
			$list['tada']			= esc_html__('Tada',		'bestdeals');
			$list['wobble']			= esc_html__('Wobble',		'bestdeals');
			$BESTDEALS_GLOBALS['list_animations'] = $list = apply_filters('bestdeals_filter_list_animations', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the enter animations
if ( !function_exists( 'bestdeals_get_list_animations_in' ) ) {
	function bestdeals_get_list_animations_in($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_animations_in']))
			$list = $BESTDEALS_GLOBALS['list_animations_in'];
		else {
			$list = array();
			$list['none']			= esc_html__('- None -',	'bestdeals');
			$list['bounceIn']		= esc_html__('Bounce In',			'bestdeals');
			$list['bounceInUp']		= esc_html__('Bounce In Up',		'bestdeals');
			$list['bounceInDown']	= esc_html__('Bounce In Down',		'bestdeals');
			$list['bounceInLeft']	= esc_html__('Bounce In Left',		'bestdeals');
			$list['bounceInRight']	= esc_html__('Bounce In Right',		'bestdeals');
			$list['fadeIn']			= esc_html__('Fade In',				'bestdeals');
			$list['fadeInUp']		= esc_html__('Fade In Up',			'bestdeals');
			$list['fadeInDown']		= esc_html__('Fade In Down',		'bestdeals');
			$list['fadeInLeft']		= esc_html__('Fade In Left',		'bestdeals');
			$list['fadeInRight']	= esc_html__('Fade In Right',		'bestdeals');
			$list['fadeInUpBig']	= esc_html__('Fade In Up Big',		'bestdeals');
			$list['fadeInDownBig']	= esc_html__('Fade In Down Big',	'bestdeals');
			$list['fadeInLeftBig']	= esc_html__('Fade In Left Big',	'bestdeals');
			$list['fadeInRightBig']	= esc_html__('Fade In Right Big',	'bestdeals');
			$list['flipInX']		= esc_html__('Flip In X',			'bestdeals');
			$list['flipInY']		= esc_html__('Flip In Y',			'bestdeals');
			$list['lightSpeedIn']	= esc_html__('Light Speed In',		'bestdeals');
			$list['rotateIn']		= esc_html__('Rotate In',			'bestdeals');
			$list['rotateInUpLeft']		= esc_html__('Rotate In Down Left',	'bestdeals');
			$list['rotateInUpRight']	= esc_html__('Rotate In Up Right',	'bestdeals');
			$list['rotateInDownLeft']	= esc_html__('Rotate In Up Left',	'bestdeals');
			$list['rotateInDownRight']	= esc_html__('Rotate In Down Right','bestdeals');
			$list['rollIn']				= esc_html__('Roll In',			'bestdeals');
			$list['slideInUp']			= esc_html__('Slide In Up',		'bestdeals');
			$list['slideInDown']		= esc_html__('Slide In Down',	'bestdeals');
			$list['slideInLeft']		= esc_html__('Slide In Left',	'bestdeals');
			$list['slideInRight']		= esc_html__('Slide In Right',	'bestdeals');
			$list['zoomIn']				= esc_html__('Zoom In',			'bestdeals');
			$list['zoomInUp']			= esc_html__('Zoom In Up',		'bestdeals');
			$list['zoomInDown']			= esc_html__('Zoom In Down',	'bestdeals');
			$list['zoomInLeft']			= esc_html__('Zoom In Left',	'bestdeals');
			$list['zoomInRight']		= esc_html__('Zoom In Right',	'bestdeals');
			$BESTDEALS_GLOBALS['list_animations_in'] = $list = apply_filters('bestdeals_filter_list_animations_in', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the out animations
if ( !function_exists( 'bestdeals_get_list_animations_out' ) ) {
	function bestdeals_get_list_animations_out($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_animations_out']))
			$list = $BESTDEALS_GLOBALS['list_animations_out'];
		else {
			$list = array();
			$list['none']			= esc_html__('- None -',	'bestdeals');
			$list['bounceOut']		= esc_html__('Bounce Out',			'bestdeals');
			$list['bounceOutUp']	= esc_html__('Bounce Out Up',		'bestdeals');
			$list['bounceOutDown']	= esc_html__('Bounce Out Down',		'bestdeals');
			$list['bounceOutLeft']	= esc_html__('Bounce Out Left',		'bestdeals');
			$list['bounceOutRight']	= esc_html__('Bounce Out Right',	'bestdeals');
			$list['fadeOut']		= esc_html__('Fade Out',			'bestdeals');
			$list['fadeOutUp']		= esc_html__('Fade Out Up',			'bestdeals');
			$list['fadeOutDown']	= esc_html__('Fade Out Down',		'bestdeals');
			$list['fadeOutLeft']	= esc_html__('Fade Out Left',		'bestdeals');
			$list['fadeOutRight']	= esc_html__('Fade Out Right',		'bestdeals');
			$list['fadeOutUpBig']	= esc_html__('Fade Out Up Big',		'bestdeals');
			$list['fadeOutDownBig']	= esc_html__('Fade Out Down Big',	'bestdeals');
			$list['fadeOutLeftBig']	= esc_html__('Fade Out Left Big',	'bestdeals');
			$list['fadeOutRightBig']= esc_html__('Fade Out Right Big',	'bestdeals');
			$list['flipOutX']		= esc_html__('Flip Out X',			'bestdeals');
			$list['flipOutY']		= esc_html__('Flip Out Y',			'bestdeals');
			$list['hinge']			= esc_html__('Hinge Out',			'bestdeals');
			$list['lightSpeedOut']	= esc_html__('Light Speed Out',		'bestdeals');
			$list['rotateOut']		= esc_html__('Rotate Out',			'bestdeals');
			$list['rotateOutUpLeft']	= esc_html__('Rotate Out Down Left',	'bestdeals');
			$list['rotateOutUpRight']	= esc_html__('Rotate Out Up Right',		'bestdeals');
			$list['rotateOutDownLeft']	= esc_html__('Rotate Out Up Left',		'bestdeals');
			$list['rotateOutDownRight']	= esc_html__('Rotate Out Down Right',	'bestdeals');
			$list['rollOut']			= esc_html__('Roll Out',		'bestdeals');
			$list['slideOutUp']			= esc_html__('Slide Out Up',		'bestdeals');
			$list['slideOutDown']		= esc_html__('Slide Out Down',	'bestdeals');
			$list['slideOutLeft']		= esc_html__('Slide Out Left',	'bestdeals');
			$list['slideOutRight']		= esc_html__('Slide Out Right',	'bestdeals');
			$list['zoomOut']			= esc_html__('Zoom Out',			'bestdeals');
			$list['zoomOutUp']			= esc_html__('Zoom Out Up',		'bestdeals');
			$list['zoomOutDown']		= esc_html__('Zoom Out Down',	'bestdeals');
			$list['zoomOutLeft']		= esc_html__('Zoom Out Left',	'bestdeals');
			$list['zoomOutRight']		= esc_html__('Zoom Out Right',	'bestdeals');
			$BESTDEALS_GLOBALS['list_animations_out'] = $list = apply_filters('bestdeals_filter_list_animations_out', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return classes list for the specified animation
if (!function_exists('bestdeals_get_animation_classes')) {
	function bestdeals_get_animation_classes($animation, $speed='normal', $loop='none') {
		return bestdeals_param_is_off($animation) ? '' : 'animated '.esc_attr($animation).' '.esc_attr($speed).(!bestdeals_param_is_off($loop) ? ' '.esc_attr($loop) : '');
	}
}


// Return list of categories
if ( !function_exists( 'bestdeals_get_list_categories' ) ) {
	function bestdeals_get_list_categories($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_categories']))
			$list = $BESTDEALS_GLOBALS['list_categories'];
		else {
			$list = array();
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false );
			$taxonomies = get_categories( $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;
				}
			}
			$BESTDEALS_GLOBALS['list_categories'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of taxonomies
if ( !function_exists( 'bestdeals_get_list_terms' ) ) {
	function bestdeals_get_list_terms($prepend_inherit=false, $taxonomy='category') {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_taxonomies_'.($taxonomy)]))
			$list = $BESTDEALS_GLOBALS['list_taxonomies_'.($taxonomy)];
		else {
			$list = array();
			$args = array(
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => $taxonomy,
				'pad_counts'               => false );
			$taxonomies = get_terms( $taxonomy, $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;
				}
			}
			$BESTDEALS_GLOBALS['list_taxonomies_'.($taxonomy)] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list of post's types
if ( !function_exists( 'bestdeals_get_list_posts_types' ) ) {
	function bestdeals_get_list_posts_types($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_posts_types']))
			$list = $BESTDEALS_GLOBALS['list_posts_types'];
		else {
			$list = array();
			// Return only theme inheritance supported post types
			$BESTDEALS_GLOBALS['list_posts_types'] = $list = apply_filters('bestdeals_filter_list_post_types', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'bestdeals_get_list_posts' ) ) {
	function bestdeals_get_list_posts($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'taxonomy'			=> 'category',
			'taxonomy_value'	=> '',
			'posts_per_page'	=> -1,
			'orderby'			=> 'post_date',
			'order'				=> 'desc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));

		global $BESTDEALS_GLOBALS;
		$hash = 'list_posts_'.($opt['post_type']).'_'.($opt['taxonomy']).'_'.($opt['taxonomy_value']).'_'.($opt['orderby']).'_'.($opt['order']).'_'.($opt['return']).'_'.($opt['posts_per_page']);
		if (isset($BESTDEALS_GLOBALS[$hash]))
			$list = $BESTDEALS_GLOBALS[$hash];
		else {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'bestdeals');
			$args = array(
				'post_type' => $opt['post_type'],
				'post_status' => $opt['post_status'],
				'posts_per_page' => $opt['posts_per_page'],
				'ignore_sticky_posts' => true,
				'orderby'	=> $opt['orderby'],
				'order'		=> $opt['order']
			);
			if (!empty($opt['taxonomy_value'])) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $opt['taxonomy'],
						'field' => (int) $opt['taxonomy_value'] > 0 ? 'id' : 'slug',
						'terms' => $opt['taxonomy_value']
					)
				);
			}
			$posts = get_posts( $args );
			if (is_array($posts) && count($posts) > 0) {
				foreach ($posts as $post) {
					$list[$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
				}
			}
			$BESTDEALS_GLOBALS[$hash] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of registered users
if ( !function_exists( 'bestdeals_get_list_users' ) ) {
	function bestdeals_get_list_users($prepend_inherit=false, $roles=array('administrator', 'editor', 'author', 'contributor', 'shop_manager')) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_users']))
			$list = $BESTDEALS_GLOBALS['list_users'];
		else {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'bestdeals');
			$args = array(
				'orderby'	=> 'display_name',
				'order'		=> 'ASC' );
			$users = get_users( $args );
			if (is_array($users) && count($users) > 0) {
				foreach ($users as $user) {
					$accept = true;
					if (is_array($user->roles)) {
						if (is_array($user->roles) && count($user->roles) > 0) {
							$accept = false;
							foreach ($user->roles as $role) {
								if (in_array($role, $roles)) {
									$accept = true;
									break;
								}
							}
						}
					}
					if ($accept) $list[$user->user_login] = $user->display_name;
				}
			}
			$BESTDEALS_GLOBALS['list_users'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return slider engines list, prepended inherit (if need)
if ( !function_exists( 'bestdeals_get_list_sliders' ) ) {
	function bestdeals_get_list_sliders($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_sliders']))
			$list = $BESTDEALS_GLOBALS['list_sliders'];
		else {
			$list = array(
				'swiper' => esc_html__("Posts slider (Swiper)", 'bestdeals')
			);
			if (bestdeals_exists_revslider())
				$list["revo"] = esc_html__("Layer slider (Revolution)", 'bestdeals');
			if (bestdeals_exists_royalslider())
				$list["royal"] = esc_html__("Layer slider (Royal)", 'bestdeals');
			$BESTDEALS_GLOBALS['list_sliders'] = $list = apply_filters('bestdeals_filter_list_sliders', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return Revo Sliders list, prepended inherit (if need)
if ( !function_exists( 'bestdeals_get_list_revo_sliders' ) ) {
	function bestdeals_get_list_revo_sliders($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_revo_sliders']))
			$list = $BESTDEALS_GLOBALS['list_revo_sliders'];
		else {
			$list = array();
			if (bestdeals_exists_revslider()) {
				global $wpdb;
                // Attention! The use of wpdb->prepare() is not required
                // because the query does not use external data substitution
				$rows = $wpdb->get_results( "SELECT alias, title FROM " . esc_sql($wpdb->prefix) . "revslider_sliders" );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->alias] = $row->title;
					}
				}
			}
			$BESTDEALS_GLOBALS['list_revo_sliders'] = $list = apply_filters('bestdeals_filter_list_revo_sliders', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return slider controls list, prepended inherit (if need)
if ( !function_exists( 'bestdeals_get_list_slider_controls' ) ) {
	function bestdeals_get_list_slider_controls($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_slider_controls']))
			$list = $BESTDEALS_GLOBALS['list_slider_controls'];
		else {
			$list = array(
				'no' => esc_html__('None', 'bestdeals'),
				'side' => esc_html__('Side', 'bestdeals'),
				'bottom' => esc_html__('Bottom', 'bestdeals'),
				'pagination' => esc_html__('Pagination', 'bestdeals')
			);
			$BESTDEALS_GLOBALS['list_slider_controls'] = $list = apply_filters('bestdeals_filter_list_slider_controls', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return slider controls classes
if ( !function_exists( 'bestdeals_get_slider_controls_classes' ) ) {
	function bestdeals_get_slider_controls_classes($controls) {
		if (bestdeals_param_is_off($controls))	$classes = 'sc_slider_nopagination sc_slider_nocontrols';
		else if ($controls=='bottom')				$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom';
		else if ($controls=='pagination')			$classes = 'sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols';
		else										$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_side';
		return $classes;
	}
}

// Return list with popup engines
if ( !function_exists( 'bestdeals_get_list_popup_engines' ) ) {
	function bestdeals_get_list_popup_engines($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_popup_engines']))
			$list = $BESTDEALS_GLOBALS['list_popup_engines'];
		else {
			$list = array();
			$list["pretty"] = esc_html__("Pretty photo", 'bestdeals');
			$list["magnific"] = esc_html__("Magnific popup", 'bestdeals');
			$BESTDEALS_GLOBALS['list_popup_engines'] = $list = apply_filters('bestdeals_filter_list_popup_engines', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return menus list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_menus' ) ) {
	function bestdeals_get_list_menus($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_menus']))
			$list = $BESTDEALS_GLOBALS['list_menus'];
		else {
			$list = array();
			$list['default'] = esc_html__("Default", 'bestdeals');
			$menus = wp_get_nav_menus();
			if (is_array($menus) && count($menus) > 0) {
				foreach ($menus as $menu) {
					$list[$menu->slug] = $menu->name;
				}
			}
			$BESTDEALS_GLOBALS['list_menus'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return custom sidebars list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'bestdeals_get_list_sidebars' ) ) {
	function bestdeals_get_list_sidebars($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_sidebars'])) {
			$list = $BESTDEALS_GLOBALS['list_sidebars'];
		} else {
			$list = isset($BESTDEALS_GLOBALS['registered_sidebars']) ? $BESTDEALS_GLOBALS['registered_sidebars'] : array();
			$BESTDEALS_GLOBALS['list_sidebars'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return sidebars positions
if ( !function_exists( 'bestdeals_get_list_sidebars_positions' ) ) {
	function bestdeals_get_list_sidebars_positions($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_sidebars_positions']))
			$list = $BESTDEALS_GLOBALS['list_sidebars_positions'];
		else {
			$list = array();
			$list['none']  = esc_html__('Hide',  'bestdeals');
			$list['left']  = esc_html__('Left',  'bestdeals');
			$list['right'] = esc_html__('Right', 'bestdeals');
			$BESTDEALS_GLOBALS['list_sidebars_positions'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return sidebars class
if ( !function_exists( 'bestdeals_get_sidebar_class' ) ) {
	function bestdeals_get_sidebar_class() {
		$sb_main = bestdeals_get_custom_option('show_sidebar_main');
		$sb_outer = bestdeals_get_custom_option('show_sidebar_outer');
		return ( ( bestdeals_param_is_off($sb_main) || is_404() ) ? 'sidebar_hide' : 'sidebar_show sidebar_'.($sb_main))
				. ' ' . (bestdeals_param_is_off($sb_outer) ? 'sidebar_outer_hide' : 'sidebar_outer_show sidebar_outer_'.($sb_outer));
	}
}

// Return body styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_body_styles' ) ) {
	function bestdeals_get_list_body_styles($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_body_styles']))
			$list = $BESTDEALS_GLOBALS['list_body_styles'];
		else {
			$list = array();
			$list['boxed']		= esc_html__('Boxed',		'bestdeals');
			$list['wide']		= esc_html__('Wide',		'bestdeals');
			if (bestdeals_get_theme_setting('allow_fullscreen')) {
				$list['fullwide']	= esc_html__('Fullwide',	'bestdeals');
				$list['fullscreen']	= esc_html__('Fullscreen',	'bestdeals');
			}
			$BESTDEALS_GLOBALS['list_body_styles'] = $list = apply_filters('bestdeals_filter_list_body_styles', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return skins list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_skins' ) ) {
	function bestdeals_get_list_skins($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_skins']))
			$list = $BESTDEALS_GLOBALS['list_skins'];
		else
			$BESTDEALS_GLOBALS['list_skins'] = $list = bestdeals_get_list_folders("skins");
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return css-themes list
if ( !function_exists( 'bestdeals_get_list_themes' ) ) {
	function bestdeals_get_list_themes($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_themes']))
			$list = $BESTDEALS_GLOBALS['list_themes'];
		else
			$BESTDEALS_GLOBALS['list_themes'] = $list = bestdeals_get_list_files("css/themes");
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return templates list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_templates' ) ) {
	function bestdeals_get_list_templates($mode='') {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_templates_'.($mode)]))
			$list = $BESTDEALS_GLOBALS['list_templates_'.($mode)];
		else {
			$list = array();
			if (is_array($BESTDEALS_GLOBALS['registered_templates']) && count($BESTDEALS_GLOBALS['registered_templates']) > 0) {
				foreach ($BESTDEALS_GLOBALS['registered_templates'] as $k=>$v) {
					if ($mode=='' || bestdeals_strpos($v['mode'], $mode)!==false)
						$list[$k] = !empty($v['icon']) 
									? $v['icon'] 
									: (!empty($v['title']) 
										? $v['title'] 
										: bestdeals_strtoproper($v['layout'])
										);
				}
			}
			$BESTDEALS_GLOBALS['list_templates_'.($mode)] = $list;
		}
		return $list;
	}
}

// Return blog styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_templates_blog' ) ) {
	function bestdeals_get_list_templates_blog($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_templates_blog']))
			$list = $BESTDEALS_GLOBALS['list_templates_blog'];
		else {
			$list = bestdeals_get_list_templates('blog');
			$BESTDEALS_GLOBALS['list_templates_blog'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return blogger styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_templates_blogger' ) ) {
	function bestdeals_get_list_templates_blogger($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_templates_blogger']))
			$list = $BESTDEALS_GLOBALS['list_templates_blogger'];
		else {
			$list = bestdeals_array_merge(bestdeals_get_list_templates('blogger'), bestdeals_get_list_templates('blog'));
			$BESTDEALS_GLOBALS['list_templates_blogger'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return single page styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_templates_single' ) ) {
	function bestdeals_get_list_templates_single($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_templates_single']))
			$list = $BESTDEALS_GLOBALS['list_templates_single'];
		else {
			$list = bestdeals_get_list_templates('single');
			$BESTDEALS_GLOBALS['list_templates_single'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return header styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_templates_header' ) ) {
	function bestdeals_get_list_templates_header($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_templates_header']))
			$list = $BESTDEALS_GLOBALS['list_templates_header'];
		else {
			$list = bestdeals_get_list_templates('header');
			$BESTDEALS_GLOBALS['list_templates_header'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return article styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_article_styles' ) ) {
	function bestdeals_get_list_article_styles($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_article_styles']))
			$list = $BESTDEALS_GLOBALS['list_article_styles'];
		else {
			$list = array();
			$list["boxed"]   = esc_html__('Boxed', 'bestdeals');
			$list["stretch"] = esc_html__('Stretch', 'bestdeals');
			$BESTDEALS_GLOBALS['list_article_styles'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return post-formats filters list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_post_formats_filters' ) ) {
	function bestdeals_get_list_post_formats_filters($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_post_formats_filters']))
			$list = $BESTDEALS_GLOBALS['list_post_formats_filters'];
		else {
			$list = array();
			$list["no"]      = esc_html__('All posts', 'bestdeals');
			$list["thumbs"]  = esc_html__('With thumbs', 'bestdeals');
			$list["reviews"] = esc_html__('With reviews', 'bestdeals');
			$list["video"]   = esc_html__('With videos', 'bestdeals');
			$list["audio"]   = esc_html__('With audios', 'bestdeals');
			$list["gallery"] = esc_html__('With galleries', 'bestdeals');
			$BESTDEALS_GLOBALS['list_post_formats_filters'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return portfolio filters list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_portfolio_filters' ) ) {
	function bestdeals_get_list_portfolio_filters($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_portfolio_filters']))
			$list = $BESTDEALS_GLOBALS['list_portfolio_filters'];
		else {
			$list = array();
			$list["hide"] = esc_html__('Hide', 'bestdeals');
			$list["tags"] = esc_html__('Tags', 'bestdeals');
			$list["categories"] = esc_html__('Categories', 'bestdeals');
			$BESTDEALS_GLOBALS['list_portfolio_filters'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return hover styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_hovers' ) ) {
	function bestdeals_get_list_hovers($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_hovers']))
			$list = $BESTDEALS_GLOBALS['list_hovers'];
		else {
			$list = array();
			$list['circle effect1']  = esc_html__('Circle Effect 1',  'bestdeals');
			$list['circle effect2']  = esc_html__('Circle Effect 2',  'bestdeals');
			$list['circle effect3']  = esc_html__('Circle Effect 3',  'bestdeals');
			$list['circle effect4']  = esc_html__('Circle Effect 4',  'bestdeals');
			$list['circle effect5']  = esc_html__('Circle Effect 5',  'bestdeals');
			$list['circle effect6']  = esc_html__('Circle Effect 6',  'bestdeals');
			$list['circle effect7']  = esc_html__('Circle Effect 7',  'bestdeals');
			$list['circle effect8']  = esc_html__('Circle Effect 8',  'bestdeals');
			$list['circle effect9']  = esc_html__('Circle Effect 9',  'bestdeals');
			$list['circle effect10'] = esc_html__('Circle Effect 10',  'bestdeals');
			$list['circle effect11'] = esc_html__('Circle Effect 11',  'bestdeals');
			$list['circle effect12'] = esc_html__('Circle Effect 12',  'bestdeals');
			$list['circle effect13'] = esc_html__('Circle Effect 13',  'bestdeals');
			$list['circle effect14'] = esc_html__('Circle Effect 14',  'bestdeals');
			$list['circle effect15'] = esc_html__('Circle Effect 15',  'bestdeals');
			$list['circle effect16'] = esc_html__('Circle Effect 16',  'bestdeals');
			$list['circle effect17'] = esc_html__('Circle Effect 17',  'bestdeals');
			$list['circle effect18'] = esc_html__('Circle Effect 18',  'bestdeals');
			$list['circle effect19'] = esc_html__('Circle Effect 19',  'bestdeals');
			$list['circle effect20'] = esc_html__('Circle Effect 20',  'bestdeals');
			$list['square effect1']  = esc_html__('Square Effect 1',  'bestdeals');
			$list['square effect2']  = esc_html__('Square Effect 2',  'bestdeals');
			$list['square effect3']  = esc_html__('Square Effect 3',  'bestdeals');
			$list['square effect5']  = esc_html__('Square Effect 5',  'bestdeals');
			$list['square effect6']  = esc_html__('Square Effect 6',  'bestdeals');
			$list['square effect7']  = esc_html__('Square Effect 7',  'bestdeals');
			$list['square effect8']  = esc_html__('Square Effect 8',  'bestdeals');
			$list['square effect9']  = esc_html__('Square Effect 9',  'bestdeals');
			$list['square effect10'] = esc_html__('Square Effect 10',  'bestdeals');
			$list['square effect11'] = esc_html__('Square Effect 11',  'bestdeals');
			$list['square effect12'] = esc_html__('Square Effect 12',  'bestdeals');
			$list['square effect13'] = esc_html__('Square Effect 13',  'bestdeals');
			$list['square effect14'] = esc_html__('Square Effect 14',  'bestdeals');
			$list['square effect15'] = esc_html__('Square Effect 15',  'bestdeals');
			$list['square effect_dir']   = esc_html__('Square Effect Dir',   'bestdeals');
			$list['square effect_shift'] = esc_html__('Square Effect Shift', 'bestdeals');
			$list['square effect_book']  = esc_html__('Square Effect Book',  'bestdeals');
			$list['square effect_more']  = esc_html__('Square Effect More',  'bestdeals');
			$list['square effect_fade']  = esc_html__('Square Effect Fade',  'bestdeals');
			$BESTDEALS_GLOBALS['list_hovers'] = $list = apply_filters('bestdeals_filter_portfolio_hovers', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return extended hover directions list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_hovers_directions' ) ) {
	function bestdeals_get_list_hovers_directions($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_hovers_directions']))
			$list = $BESTDEALS_GLOBALS['list_hovers_directions'];
		else {
			$list = array();
			$list['left_to_right'] = esc_html__('Left to Right',  'bestdeals');
			$list['right_to_left'] = esc_html__('Right to Left',  'bestdeals');
			$list['top_to_bottom'] = esc_html__('Top to Bottom',  'bestdeals');
			$list['bottom_to_top'] = esc_html__('Bottom to Top',  'bestdeals');
			$list['scale_up']      = esc_html__('Scale Up',  'bestdeals');
			$list['scale_down']    = esc_html__('Scale Down',  'bestdeals');
			$list['scale_down_up'] = esc_html__('Scale Down-Up',  'bestdeals');
			$list['from_left_and_right'] = esc_html__('From Left and Right',  'bestdeals');
			$list['from_top_and_bottom'] = esc_html__('From Top and Bottom',  'bestdeals');
			$BESTDEALS_GLOBALS['list_hovers_directions'] = $list = apply_filters('bestdeals_filter_portfolio_hovers_directions', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the label positions in the custom forms
if ( !function_exists( 'bestdeals_get_list_label_positions' ) ) {
	function bestdeals_get_list_label_positions($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_label_positions']))
			$list = $BESTDEALS_GLOBALS['list_label_positions'];
		else {
			$list = array();
			$list['top']	= esc_html__('Top',		'bestdeals');
			$list['bottom']	= esc_html__('Bottom',		'bestdeals');
			$list['left']	= esc_html__('Left',		'bestdeals');
			$list['over']	= esc_html__('Over',		'bestdeals');
			$BESTDEALS_GLOBALS['list_label_positions'] = $list = apply_filters('bestdeals_filter_label_positions', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the bg image positions
if ( !function_exists( 'bestdeals_get_list_bg_image_positions' ) ) {
	function bestdeals_get_list_bg_image_positions($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_bg_image_positions']))
			$list = $BESTDEALS_GLOBALS['list_bg_image_positions'];
		else {
			$list = array();
			$list['left top']	  = esc_html__('Left Top', 'bestdeals');
			$list['center top']   = esc_html__("Center Top", 'bestdeals');
			$list['right top']    = esc_html__("Right Top", 'bestdeals');
			$list['left center']  = esc_html__("Left Center", 'bestdeals');
			$list['center center']= esc_html__("Center Center", 'bestdeals');
			$list['right center'] = esc_html__("Right Center", 'bestdeals');
			$list['left bottom']  = esc_html__("Left Bottom", 'bestdeals');
			$list['center bottom']= esc_html__("Center Bottom", 'bestdeals');
			$list['right bottom'] = esc_html__("Right Bottom", 'bestdeals');
			$BESTDEALS_GLOBALS['list_bg_image_positions'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the bg image repeat
if ( !function_exists( 'bestdeals_get_list_bg_image_repeats' ) ) {
	function bestdeals_get_list_bg_image_repeats($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_bg_image_repeats']))
			$list = $BESTDEALS_GLOBALS['list_bg_image_repeats'];
		else {
			$list = array();
			$list['repeat']	  = esc_html__('Repeat', 'bestdeals');
			$list['repeat-x'] = esc_html__('Repeat X', 'bestdeals');
			$list['repeat-y'] = esc_html__('Repeat Y', 'bestdeals');
			$list['no-repeat']= esc_html__('No Repeat', 'bestdeals');
			$BESTDEALS_GLOBALS['list_bg_image_repeats'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the bg image attachment
if ( !function_exists( 'bestdeals_get_list_bg_image_attachments' ) ) {
	function bestdeals_get_list_bg_image_attachments($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_bg_image_attachments']))
			$list = $BESTDEALS_GLOBALS['list_bg_image_attachments'];
		else {
			$list = array();
			$list['scroll']	= esc_html__('Scroll', 'bestdeals');
			$list['fixed']	= esc_html__('Fixed', 'bestdeals');
			$list['local']	= esc_html__('Local', 'bestdeals');
			$BESTDEALS_GLOBALS['list_bg_image_attachments'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}


// Return list of the bg tints
if ( !function_exists( 'bestdeals_get_list_bg_tints' ) ) {
	function bestdeals_get_list_bg_tints($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_bg_tints']))
			$list = $BESTDEALS_GLOBALS['list_bg_tints'];
		else {
			$list = array();
			$list['white']	= esc_html__('White', 'bestdeals');
			$list['light']	= esc_html__('Light', 'bestdeals');
			$list['dark']	= esc_html__('Dark', 'bestdeals');
			$BESTDEALS_GLOBALS['list_bg_tints'] = $list = apply_filters('bestdeals_filter_bg_tints', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return custom fields types list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_field_types' ) ) {
	function bestdeals_get_list_field_types($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_field_types']))
			$list = $BESTDEALS_GLOBALS['list_field_types'];
		else {
			$list = array();
			$list['text']     = esc_html__('Text',  'bestdeals');
			$list['textarea'] = esc_html__('Text Area','bestdeals');
			$list['password'] = esc_html__('Password',  'bestdeals');
			$list['radio']    = esc_html__('Radio',  'bestdeals');
			$list['checkbox'] = esc_html__('Checkbox',  'bestdeals');
			$list['select']   = esc_html__('Select',  'bestdeals');
			$list['button']   = esc_html__('Button','bestdeals');
			$BESTDEALS_GLOBALS['list_field_types'] = $list = apply_filters('bestdeals_filter_field_types', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return Google map styles
if ( !function_exists( 'bestdeals_get_list_googlemap_styles' ) ) {
	function bestdeals_get_list_googlemap_styles($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_googlemap_styles']))
			$list = $BESTDEALS_GLOBALS['list_googlemap_styles'];
		else {
			$list = array();
			$list['default'] = esc_html__('Default', 'bestdeals');
			$list['simple'] = esc_html__('Simple', 'bestdeals');
			$list['greyscale'] = esc_html__('Greyscale', 'bestdeals');
			$list['greyscale2'] = esc_html__('Greyscale 2', 'bestdeals');
			$list['invert'] = esc_html__('Invert', 'bestdeals');
			$list['dark'] = esc_html__('Dark', 'bestdeals');
			$list['style1'] = esc_html__('Custom style 1', 'bestdeals');
			$list['style2'] = esc_html__('Custom style 2', 'bestdeals');
			$list['style3'] = esc_html__('Custom style 3', 'bestdeals');
			$BESTDEALS_GLOBALS['list_googlemap_styles'] = $list = apply_filters('bestdeals_filter_googlemap_styles', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return iconed classes list
if ( !function_exists( 'bestdeals_get_list_icons' ) ) {
	function bestdeals_get_list_icons($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_icons']))
			$list = $BESTDEALS_GLOBALS['list_icons'];
		else
			$BESTDEALS_GLOBALS['list_icons'] = $list = bestdeals_parse_icons_classes(bestdeals_get_file_dir("css/fontello/css/fontello-codes.css"));
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return socials list
if ( !function_exists( 'bestdeals_get_list_socials' ) ) {
	function bestdeals_get_list_socials($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_socials']))
			$list = $BESTDEALS_GLOBALS['list_socials'];
		else
			$BESTDEALS_GLOBALS['list_socials'] = $list = bestdeals_get_list_files("images/socials", "png");
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return flags list
if ( !function_exists( 'bestdeals_get_list_flags' ) ) {
	function bestdeals_get_list_flags($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_flags']))
			$list = $BESTDEALS_GLOBALS['list_flags'];
		else
			$BESTDEALS_GLOBALS['list_flags'] = $list = bestdeals_get_list_files("images/flags", "png");
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with 'Yes' and 'No' items
if ( !function_exists( 'bestdeals_get_list_yesno' ) ) {
	function bestdeals_get_list_yesno($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_yesno']))
			$list = $BESTDEALS_GLOBALS['list_yesno'];
		else {
			$list = array();
			$list["yes"] = esc_html__("Yes", 'bestdeals');
			$list["no"]  = esc_html__("No", 'bestdeals');
			$BESTDEALS_GLOBALS['list_yesno'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with 'On' and 'Of' items
if ( !function_exists( 'bestdeals_get_list_onoff' ) ) {
	function bestdeals_get_list_onoff($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_onoff']))
			$list = $BESTDEALS_GLOBALS['list_onoff'];
		else {
			$list = array();
			$list["on"] = esc_html__("On", 'bestdeals');
			$list["off"] = esc_html__("Off", 'bestdeals');
			$BESTDEALS_GLOBALS['list_onoff'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with 'Show' and 'Hide' items
if ( !function_exists( 'bestdeals_get_list_showhide' ) ) {
	function bestdeals_get_list_showhide($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_showhide']))
			$list = $BESTDEALS_GLOBALS['list_showhide'];
		else {
			$list = array();
			$list["show"] = esc_html__("Show", 'bestdeals');
			$list["hide"] = esc_html__("Hide", 'bestdeals');
			$BESTDEALS_GLOBALS['list_showhide'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with 'Ascending' and 'Descending' items
if ( !function_exists( 'bestdeals_get_list_orderings' ) ) {
	function bestdeals_get_list_orderings($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_orderings']))
			$list = $BESTDEALS_GLOBALS['list_orderings'];
		else {
			$list = array();
			$list["asc"] = esc_html__("Ascending", 'bestdeals');
			$list["desc"] = esc_html__("Descending", 'bestdeals');
			$BESTDEALS_GLOBALS['list_orderings'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with 'Horizontal' and 'Vertical' items
if ( !function_exists( 'bestdeals_get_list_directions' ) ) {
	function bestdeals_get_list_directions($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_directions']))
			$list = $BESTDEALS_GLOBALS['list_directions'];
		else {
			$list = array();
			$list["horizontal"] = esc_html__("Horizontal", 'bestdeals');
			$list["vertical"] = esc_html__("Vertical", 'bestdeals');
			$BESTDEALS_GLOBALS['list_directions'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with item's shapes
if ( !function_exists( 'bestdeals_get_list_shapes' ) ) {
	function bestdeals_get_list_shapes($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_shapes']))
			$list = $BESTDEALS_GLOBALS['list_shapes'];
		else {
			$list = array();
			$list["round"]  = esc_html__("Round", 'bestdeals');
			$list["square"] = esc_html__("Square", 'bestdeals');
			$BESTDEALS_GLOBALS['list_shapes'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with item's sizes
if ( !function_exists( 'bestdeals_get_list_sizes' ) ) {
	function bestdeals_get_list_sizes($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_sizes']))
			$list = $BESTDEALS_GLOBALS['list_sizes'];
		else {
			$list = array();
			$list["tiny"]   = esc_html__("Tiny", 'bestdeals');
			$list["small"]  = esc_html__("Small", 'bestdeals');
			$list["medium"] = esc_html__("Medium", 'bestdeals');
			$list["large"]  = esc_html__("Large", 'bestdeals');
			$BESTDEALS_GLOBALS['list_sizes'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with float items
if ( !function_exists( 'bestdeals_get_list_floats' ) ) {
	function bestdeals_get_list_floats($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_floats']))
			$list = $BESTDEALS_GLOBALS['list_floats'];
		else {
			$list = array();
			$list["none"] = esc_html__("None", 'bestdeals');
			$list["left"] = esc_html__("Float Left", 'bestdeals');
			$list["right"] = esc_html__("Float Right", 'bestdeals');
			$BESTDEALS_GLOBALS['list_floats'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with alignment items
if ( !function_exists( 'bestdeals_get_list_alignments' ) ) {
	function bestdeals_get_list_alignments($justify=false, $prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_alignments']))
			$list = $BESTDEALS_GLOBALS['list_alignments'];
		else {
			$list = array();
			$list["none"] = esc_html__("None", 'bestdeals');
			$list["left"] = esc_html__("Left", 'bestdeals');
			$list["center"] = esc_html__("Center", 'bestdeals');
			$list["right"] = esc_html__("Right", 'bestdeals');
			if ($justify) $list["justify"] = esc_html__("Justify", 'bestdeals');
			$BESTDEALS_GLOBALS['list_alignments'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return sorting list items
if ( !function_exists( 'bestdeals_get_list_sortings' ) ) {
	function bestdeals_get_list_sortings($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_sortings']))
			$list = $BESTDEALS_GLOBALS['list_sortings'];
		else {
			$list = array();
			$list["date"] = esc_html__("Date", 'bestdeals');
			$list["title"] = esc_html__("Alphabetically", 'bestdeals');
			$list["views"] = esc_html__("Popular (views count)", 'bestdeals');
			$list["comments"] = esc_html__("Most commented (comments count)", 'bestdeals');
			$list["author_rating"] = esc_html__("Author rating", 'bestdeals');
			$list["users_rating"] = esc_html__("Visitors (users) rating", 'bestdeals');
			$list["random"] = esc_html__("Random", 'bestdeals');
			$BESTDEALS_GLOBALS['list_sortings'] = $list = apply_filters('bestdeals_filter_list_sortings', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list with columns widths
if ( !function_exists( 'bestdeals_get_list_columns' ) ) {
	function bestdeals_get_list_columns($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_columns']))
			$list = $BESTDEALS_GLOBALS['list_columns'];
		else {
			$list = array();
			$list["none"] = esc_html__("None", 'bestdeals');
			$list["1_1"] = esc_html__("100%", 'bestdeals');
			$list["1_2"] = esc_html__("1/2", 'bestdeals');
			$list["1_3"] = esc_html__("1/3", 'bestdeals');
			$list["2_3"] = esc_html__("2/3", 'bestdeals');
			$list["1_4"] = esc_html__("1/4", 'bestdeals');
			$list["3_4"] = esc_html__("3/4", 'bestdeals');
			$list["1_5"] = esc_html__("1/5", 'bestdeals');
			$list["2_5"] = esc_html__("2/5", 'bestdeals');
			$list["3_5"] = esc_html__("3/5", 'bestdeals');
			$list["4_5"] = esc_html__("4/5", 'bestdeals');
			$list["1_6"] = esc_html__("1/6", 'bestdeals');
			$list["5_6"] = esc_html__("5/6", 'bestdeals');
			$list["1_7"] = esc_html__("1/7", 'bestdeals');
			$list["2_7"] = esc_html__("2/7", 'bestdeals');
			$list["3_7"] = esc_html__("3/7", 'bestdeals');
			$list["4_7"] = esc_html__("4/7", 'bestdeals');
			$list["5_7"] = esc_html__("5/7", 'bestdeals');
			$list["6_7"] = esc_html__("6/7", 'bestdeals');
			$list["1_8"] = esc_html__("1/8", 'bestdeals');
			$list["3_8"] = esc_html__("3/8", 'bestdeals');
			$list["5_8"] = esc_html__("5/8", 'bestdeals');
			$list["7_8"] = esc_html__("7/8", 'bestdeals');
			$list["1_9"] = esc_html__("1/9", 'bestdeals');
			$list["2_9"] = esc_html__("2/9", 'bestdeals');
			$list["4_9"] = esc_html__("4/9", 'bestdeals');
			$list["5_9"] = esc_html__("5/9", 'bestdeals');
			$list["7_9"] = esc_html__("7/9", 'bestdeals');
			$list["8_9"] = esc_html__("8/9", 'bestdeals');
			$list["1_10"]= esc_html__("1/10", 'bestdeals');
			$list["3_10"]= esc_html__("3/10", 'bestdeals');
			$list["7_10"]= esc_html__("7/10", 'bestdeals');
			$list["9_10"]= esc_html__("9/10", 'bestdeals');
			$list["1_11"]= esc_html__("1/11", 'bestdeals');
			$list["2_11"]= esc_html__("2/11", 'bestdeals');
			$list["3_11"]= esc_html__("3/11", 'bestdeals');
			$list["4_11"]= esc_html__("4/11", 'bestdeals');
			$list["5_11"]= esc_html__("5/11", 'bestdeals');
			$list["6_11"]= esc_html__("6/11", 'bestdeals');
			$list["7_11"]= esc_html__("7/11", 'bestdeals');
			$list["8_11"]= esc_html__("8/11", 'bestdeals');
			$list["9_11"]= esc_html__("9/11", 'bestdeals');
			$list["10_11"]= esc_html__("10/11", 'bestdeals');
			$list["1_12"]= esc_html__("1/12", 'bestdeals');
			$list["5_12"]= esc_html__("5/12", 'bestdeals');
			$list["7_12"]= esc_html__("7/12", 'bestdeals');
			$list["10_12"]= esc_html__("10/12", 'bestdeals');
			$list["11_12"]= esc_html__("11/12", 'bestdeals');
			$BESTDEALS_GLOBALS['list_columns'] = $list = apply_filters('bestdeals_filter_list_columns', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return list of locations for the dedicated content
if ( !function_exists( 'bestdeals_get_list_dedicated_locations' ) ) {
	function bestdeals_get_list_dedicated_locations($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_dedicated_locations']))
			$list = $BESTDEALS_GLOBALS['list_dedicated_locations'];
		else {
			$list = array();
			$list["default"] = esc_html__('As in the post defined', 'bestdeals');
			$list["center"]  = esc_html__('Above the text of the post', 'bestdeals');
			$list["left"]    = esc_html__('To the left the text of the post', 'bestdeals');
			$list["right"]   = esc_html__('To the right the text of the post', 'bestdeals');
			$list["alter"]   = esc_html__('Alternates for each post', 'bestdeals');
			$BESTDEALS_GLOBALS['list_dedicated_locations'] = $list = apply_filters('bestdeals_filter_list_dedicated_locations', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return post-format name
if ( !function_exists( 'bestdeals_get_post_format_name' ) ) {
	function bestdeals_get_post_format_name($format, $single=true) {
		$name = '';
		if ($format=='gallery')		$name = $single ? esc_html__('gallery', 'bestdeals') : esc_html__('galleries', 'bestdeals');
		else if ($format=='video')	$name = $single ? esc_html__('video', 'bestdeals') : esc_html__('videos', 'bestdeals');
		else if ($format=='audio')	$name = $single ? esc_html__('audio', 'bestdeals') : esc_html__('audios', 'bestdeals');
		else if ($format=='image')	$name = $single ? esc_html__('image', 'bestdeals') : esc_html__('images', 'bestdeals');
		else if ($format=='quote')	$name = $single ? esc_html__('quote', 'bestdeals') : esc_html__('quotes', 'bestdeals');
		else if ($format=='link')	$name = $single ? esc_html__('link', 'bestdeals') : esc_html__('links', 'bestdeals');
		else if ($format=='status')	$name = $single ? esc_html__('status', 'bestdeals') : esc_html__('statuses', 'bestdeals');
		else if ($format=='aside')	$name = $single ? esc_html__('aside', 'bestdeals') : esc_html__('asides', 'bestdeals');
		else if ($format=='chat')	$name = $single ? esc_html__('chat', 'bestdeals') : esc_html__('chats', 'bestdeals');
		else						$name = $single ? esc_html__('standard', 'bestdeals') : esc_html__('standards', 'bestdeals');
		return apply_filters('bestdeals_filter_list_post_format_name', $name, $format);
	}
}

// Return post-format icon name (from Fontello library)
if ( !function_exists( 'bestdeals_get_post_format_icon' ) ) {
	function bestdeals_get_post_format_icon($format) {
		$icon = 'icon-';
		if ($format=='gallery')		$icon .= 'pictures';
		else if ($format=='video')	$icon .= 'video';
		else if ($format=='audio')	$icon .= 'note';
		else if ($format=='image')	$icon .= 'picture';
		else if ($format=='quote')	$icon .= 'quote';
		else if ($format=='link')	$icon .= 'link';
		else if ($format=='status')	$icon .= 'comment';
		else if ($format=='aside')	$icon .= 'doc-text';
		else if ($format=='chat')	$icon .= 'chat';
		else						$icon .= 'book-open';
		return apply_filters('bestdeals_filter_list_post_format_icon', $icon, $format);
	}
}

// Return fonts styles list, prepended inherit
if ( !function_exists( 'bestdeals_get_list_fonts_styles' ) ) {
	function bestdeals_get_list_fonts_styles($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_fonts_styles']))
			$list = $BESTDEALS_GLOBALS['list_fonts_styles'];
		else {
			$list = array();
			$list['i'] = esc_html__('I','bestdeals');
			$list['u'] = esc_html__('U', 'bestdeals');
			$BESTDEALS_GLOBALS['list_fonts_styles'] = $list;
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return Google fonts list
if ( !function_exists( 'bestdeals_get_list_fonts' ) ) {
	function bestdeals_get_list_fonts($prepend_inherit=false) {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['list_fonts']))
			$list = $BESTDEALS_GLOBALS['list_fonts'];
		else {
			$list = array();
			$list = bestdeals_array_merge($list, bestdeals_get_list_font_faces());
			$list['Advent Pro'] = array('family'=>'sans-serif');
			$list['Alegreya Sans'] = array('family'=>'sans-serif');
			$list['Arimo'] = array('family'=>'sans-serif');
			$list['Asap'] = array('family'=>'sans-serif');
			$list['Averia Sans Libre'] = array('family'=>'cursive');
			$list['Averia Serif Libre'] = array('family'=>'cursive');
			$list['Bree Serif'] = array('family'=>'serif',);
			$list['Cabin'] = array('family'=>'sans-serif');
			$list['Cabin Condensed'] = array('family'=>'sans-serif');
			$list['Caudex'] = array('family'=>'serif');
			$list['Comfortaa'] = array('family'=>'cursive');
			$list['Cousine'] = array('family'=>'sans-serif');
			$list['Crimson Text'] = array('family'=>'serif');
			$list['Cuprum'] = array('family'=>'sans-serif');
			$list['Dosis'] = array('family'=>'sans-serif');
			$list['Economica'] = array('family'=>'sans-serif');
			$list['Exo'] = array('family'=>'sans-serif');
			$list['Expletus Sans'] = array('family'=>'cursive');
			$list['Karla'] = array('family'=>'sans-serif');
			$list['Lato'] = array('family'=>'sans-serif');
			$list['Lekton'] = array('family'=>'sans-serif');
			$list['Lobster Two'] = array('family'=>'cursive');
			$list['Maven Pro'] = array('family'=>'sans-serif');
			$list['Merriweather'] = array('family'=>'serif');
			$list['Montserrat'] = array('family'=>'sans-serif');
			$list['Neuton'] = array('family'=>'serif');
			$list['Noticia Text'] = array('family'=>'serif');
			$list['Old Standard TT'] = array('family'=>'serif');
			$list['Open Sans'] = array('family'=>'sans-serif');
			$list['Orbitron'] = array('family'=>'sans-serif');
			$list['Oswald'] = array('family'=>'sans-serif');
			$list['Overlock'] = array('family'=>'cursive');
			$list['Oxygen'] = array('family'=>'sans-serif');
			$list['PT Serif'] = array('family'=>'serif');
			$list['Puritan'] = array('family'=>'sans-serif');
			$list['Raleway'] = array('family'=>'sans-serif');
			$list['Roboto'] = array('family'=>'sans-serif');
			$list['Roboto Slab'] = array('family'=>'sans-serif');
			$list['Roboto Condensed'] = array('family'=>'sans-serif');
			$list['Rosario'] = array('family'=>'sans-serif');
			$list['Share'] = array('family'=>'cursive');
			$list['Signika'] = array('family'=>'sans-serif');
			$list['Signika Negative'] = array('family'=>'sans-serif');
			$list['Source Sans Pro'] = array('family'=>'sans-serif');
			$list['Tinos'] = array('family'=>'serif');
			$list['Ubuntu'] = array('family'=>'sans-serif');
			$list['Vollkorn'] = array('family'=>'serif');
			$BESTDEALS_GLOBALS['list_fonts'] = $list = apply_filters('bestdeals_filter_list_fonts', $list);
		}
		return $prepend_inherit ? bestdeals_array_merge(array('inherit' => esc_html__("Inherit", 'bestdeals')), $list) : $list;
	}
}

// Return Custom font-face list
if ( !function_exists( 'bestdeals_get_list_font_faces' ) ) {
	function bestdeals_get_list_font_faces($prepend_inherit=false) {
		static $list = false;
		if (is_array($list)) return $list;
		$list = array();
		$dir = bestdeals_get_folder_dir("css/font-face");
		if ( is_dir($dir) ) {
                $files = glob(sprintf("%s/*", $dir), GLOB_ONLYDIR);
                if ( is_array($files) ) {
                    foreach ($files as $file) {
                        $file_name = basename($file);
                        $pi = pathinfo( ($dir) . '/' . ($file_name) );
                        if ( substr($file_name, 0, 1) == '.' || ! is_dir( ($dir) . '/' . ($file_name) ) )
                            continue;
                        $css = file_exists( ($dir) . '/' . ($file_name) . '/' . ($file_name) . '.css' )
                            ? bestdeals_get_folder_url("css/font-face/".($file_name).'/'.($file_name).'.css')
                            : (file_exists( ($dir) . '/' . ($file_name) . '/stylesheet.css' )
                                ? bestdeals_get_folder_url("css/font-face/".($file_name).'/stylesheet.css')
                                : '');
                        if ($css != '')
                            $list[$file_name.' ('.esc_html__('uploaded font', 'bestdeals').')'] = array('css' => $css);
                    }
                }
            }
		return $list;
	}
}
?>