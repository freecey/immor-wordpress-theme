<?php
/**
 * BestDEALS Framework: Team post type settings
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Theme init
if (!function_exists('bestdeals_team_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_team_theme_setup' );
	function bestdeals_team_theme_setup() {

		// Add item in the admin menu
		add_filter('trx_utils_filter_override_options',	 	'bestdeals_team_add_override_options');

		// Save data from override options
		add_action('save_post',								'bestdeals_team_save_data');
		
		// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
		add_filter('bestdeals_filter_get_blog_type',			'bestdeals_team_get_blog_type', 9, 2);
		add_filter('bestdeals_filter_get_blog_title',		'bestdeals_team_get_blog_title', 9, 2);
		add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_team_get_current_taxonomy', 9, 2);
		add_filter('bestdeals_filter_is_taxonomy',			'bestdeals_team_is_taxonomy', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_team_get_stream_page_title', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_team_get_stream_page_link', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_team_get_stream_page_id', 9, 2);
		add_filter('bestdeals_filter_query_add_filters',		'bestdeals_team_query_add_filters', 9, 2);
		add_filter('bestdeals_filter_detect_inheritance_key','bestdeals_team_detect_inheritance_key', 9, 1);

		// Extra column for team members lists
		if (bestdeals_get_theme_option('show_overriden_posts')=='yes') {
			add_filter('manage_edit-team_columns',			'bestdeals_post_add_options_column', 9);
			add_filter('manage_team_posts_custom_column',	'bestdeals_post_fill_options_column', 9, 2);
		}

		// Add shortcodes [trx_team] and [trx_team_item]
		add_action('bestdeals_action_shortcodes_list',		'bestdeals_team_reg_shortcodes');
		add_action('bestdeals_action_shortcodes_list_vc',	'bestdeals_team_reg_shortcodes_vc');

		// Override options fields
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['team_override_options'] = array(
			'id' => 'team-override-options',
			'title' => esc_html__('Team Member Details', 'bestdeals'),
			'page' => 'team',
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				"team_member_position" => array(
					"title" => esc_html__('Position',  'bestdeals'),
					"desc" => esc_attr__("Position of the team member", 'bestdeals'),
					"class" => "team_member_position",
					"std" => "",
					"type" => "text"),
				"team_member_email" => array(
					"title" => esc_html__("E-mail",  'bestdeals'),
					"desc" => esc_attr__("E-mail of the team member - need to take Gravatar (if registered)", 'bestdeals'),
					"class" => "team_member_email",
					"std" => "",
					"type" => "text"),
				"team_member_link" => array(
					"title" => esc_html__('Link to profile',  'bestdeals'),
					"desc" => esc_attr__("URL of the team member profile page (if not this page)", 'bestdeals'),
					"class" => "team_member_link",
					"std" => "",
					"type" => "text"),				
				"team_member_description_bd" => array(
					"title" => esc_html__('Description',  'bestdeals'),
					"desc" => esc_attr__("Content abour team", 'bestdeals'),
					"class" => "team_member_description_bd",
					"std" => "",
					"type" => "textarea"),
				"team_member_contact_info" => array(
					"title" => esc_html__('Contact info',  'bestdeals'),
					"desc" => esc_attr__("Contact info for property type", 'bestdeals'),
					"class" => "team_member_contact_info",
					"std" => "",
					"type" => "textarea"),
				"team_member_socials" => array(
					"title" => esc_html__("Social links",  'bestdeals'),
					"desc" => esc_attr__("Links to the social profiles of the team member", 'bestdeals'),
					"class" => "team_member_email",
					"std" => "",
					"type" => "social")
			)
		);
		
		if (function_exists('bestdeals_require_data')) {
			// Prepare type "Team"
			bestdeals_require_data( 'post_type', 'team', array(
				'label'               => esc_html__( 'Team member', 'bestdeals' ),
				'description'         => esc_html__( 'Team Description', 'bestdeals' ),
				'labels'              => array(
					'name'                => esc_html_x( 'Team', 'Post Type General Name', 'bestdeals' ),
					'singular_name'       => esc_html_x( 'Team member', 'Post Type Singular Name', 'bestdeals' ),
					'menu_name'           => esc_html__( 'Team', 'bestdeals' ),
					'parent_item_colon'   => esc_html__( 'Parent Item:', 'bestdeals' ),
					'all_items'           => esc_html__( 'All Team', 'bestdeals' ),
					'view_item'           => esc_html__( 'View Item', 'bestdeals' ),
					'add_new_item'        => esc_html__( 'Add New Team member', 'bestdeals' ),
					'add_new'             => esc_html__( 'Add New', 'bestdeals' ),
					'edit_item'           => esc_html__( 'Edit Item', 'bestdeals' ),
					'update_item'         => esc_html__( 'Update Item', 'bestdeals' ),
					'search_items'        => esc_html__( 'Search Item', 'bestdeals' ),
					'not_found'           => esc_html__( 'Not found', 'bestdeals' ),
					'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'bestdeals' ),
				),
				'supports'            => array( 'title', 'excerpt', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields'),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'menu_icon'			  => 'dashicons-admin-users',
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 25,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'query_var'           => true,
				'capability_type'     => 'page',
				'rewrite'             => true
				)
			);
			
			// Prepare taxonomy for team
			bestdeals_require_data( 'taxonomy', 'team_group', array(
				'post_type'			=> array( 'team' ),
				'hierarchical'      => true,
				'labels'            => array(
					'name'              => esc_html_x( 'Team Group', 'taxonomy general name', 'bestdeals' ),
					'singular_name'     => esc_html_x( 'Group', 'taxonomy singular name', 'bestdeals' ),
					'search_items'      => esc_html__( 'Search Groups', 'bestdeals' ),
					'all_items'         => esc_html__( 'All Groups', 'bestdeals' ),
					'parent_item'       => esc_html__( 'Parent Group', 'bestdeals' ),
					'parent_item_colon' => esc_html__( 'Parent Group:', 'bestdeals' ),
					'edit_item'         => esc_html__( 'Edit Group', 'bestdeals' ),
					'update_item'       => esc_html__( 'Update Group', 'bestdeals' ),
					'add_new_item'      => esc_html__( 'Add New Group', 'bestdeals' ),
					'new_item_name'     => esc_html__( 'New Group Name', 'bestdeals' ),
					'menu_name'         => esc_html__( 'Team Group', 'bestdeals' ),
				),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'team_group' ),
				)
			);
		}
	}
}

if ( !function_exists( 'bestdeals_team_settings_theme_setup2' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_team_settings_theme_setup2', 3 );
	function bestdeals_team_settings_theme_setup2() {
		// Add post type 'team' and taxonomy 'team_group' into theme inheritance list
		bestdeals_add_theme_inheritance( array('team' => array(
			'stream_template' => 'blog-team',
			'single_template' => 'single-team',
			'taxonomy' => array('team_group'),
			'taxonomy_tags' => array(),
			'post_type' => array('team'),
			'override' => 'page'
			) )
		);
	}
}


// Add override options
if (!function_exists('bestdeals_team_add_override_options')) {
	function bestdeals_team_add_override_options($boxes = array()) {
		global $BESTDEALS_GLOBALS;
		$boxes[] = array_merge($BESTDEALS_GLOBALS['team_override_options'], array('callback' => 'bestdeals_team_show_override_options'));
		return $boxes;
	}
}

// Callback function to show fields in override options
if (!function_exists('bestdeals_team_show_override_options')) {
	function bestdeals_team_show_override_options() {
		global $post, $BESTDEALS_GLOBALS;

		// Use nonce for verification
		$data = get_post_meta($post->ID, 'team_data', true);
		$fields = $BESTDEALS_GLOBALS['team_override_options']['fields'];
		?>
		<input type="hidden" name="override_options_team_nonce" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" />
		<table class="team_area">
		<?php
		if (is_array($fields) && count($fields) > 0) {
			foreach ($fields as $id=>$field) { 
				$meta = isset($data[$id]) ? $data[$id] : '';
				?>
				<tr class="team_field <?php echo esc_attr($field['class']); ?>" valign="top">
					<td><label for="<?php echo esc_attr($id); ?>"><?php echo esc_attr($field['title']); ?></label></td>
					<td>
						<?php
						if ($id == 'team_member_socials') {
							$socials_type = bestdeals_get_theme_setting('socials_type');
							$social_list = bestdeals_get_theme_option('social_icons');
							if (is_array($social_list) && count($social_list) > 0) {
								foreach ($social_list as $soc) {
									if ($socials_type == 'icons') {
										$parts = explode('-', $soc['icon'], 2);
										$sn = isset($parts[1]) ? $parts[1] : $sn;
									} else {
										$sn = basename($soc['icon']);
										$sn = bestdeals_substr($sn, 0, bestdeals_strrpos($sn, '.'));
										if (($pos=bestdeals_strrpos($sn, '_'))!==false)
											$sn = bestdeals_substr($sn, 0, $pos);
									}   
									$link = isset($meta[$sn]) ? $meta[$sn] : '';
									?>
									<label for="<?php echo esc_attr(($id).'_'.($sn)); ?>"><?php echo esc_attr(bestdeals_strtoproper($sn)); ?></label><br>
									<input type="text" name="<?php echo esc_attr($id); ?>[<?php echo esc_attr($sn); ?>]" id="<?php echo esc_attr(($id).'_'.($sn)); ?>" value="<?php echo esc_attr($link); ?>" size="30" /><br>
									<?php
								}
							}
						} else {
							
							if ( isset($field['type']) and $field['type'] == "textarea" ) { ?>
								
									<textarea rows="10" cols="80" name="<?php echo esc_attr($id); ?>" id="<?php echo esc_attr($id); ?>"><?php echo esc_attr($meta); ?></textarea>
									
								<?php
							} else { ?>
								
									<input type="text" name="<?php echo esc_attr($id); ?>" id="<?php echo esc_attr($id); ?>" value="<?php echo esc_attr($meta); ?>" size="30" />
									
								<?php
							}
						}
						?>
						<br><small><?php echo esc_attr($field['desc']); ?></small>
					</td>
				</tr>
				<?php
			}
		}
		?>
		</table>
		<?php
	}
}


// Save data from override options
if (!function_exists('bestdeals_team_save_data')) {
	//Handler of add_action('save_post', 'bestdeals_team_save_data');
	function bestdeals_team_save_data($post_id) {
		// verify nonce
		if (!isset($_POST['override_options_team_nonce']) || !wp_verify_nonce($_POST['override_options_team_nonce'], basename(__FILE__))) {
			return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		// check permissions
		if ($_POST['post_type']!='team' || !current_user_can('edit_post', $post_id)) {
			return $post_id;
		}

		global $BESTDEALS_GLOBALS;

		$data = array();

		$fields = $BESTDEALS_GLOBALS['team_override_options']['fields'];

		// Post type specific data handling
		if (is_array($fields) && count($fields) > 0) {
			foreach ($fields as $id=>$field) {
                $social_temp = array();
                if (isset($_POST[$id])) {
                    if (is_array($_POST[$id]) && count($_POST[$id]) > 0) {
                        foreach ($_POST[$id] as $sn=>$link) {
                            $social_temp[$sn] = stripslashes($link);
                        }
                        $data[$id] = $social_temp;
					} else {
						$data[$id] = stripslashes($_POST[$id]);
					}
				}
			}
		}

		update_post_meta($post_id, 'team_data', $data);
	}
}



// Return true, if current page is team member page
if ( !function_exists( 'bestdeals_is_team_page' ) ) {
	function bestdeals_is_team_page() {
		return get_query_var('post_type')=='team' || is_tax('team_group') || (is_page() && bestdeals_get_template_page_id('blog-team')==get_the_ID());
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'bestdeals_team_detect_inheritance_key' ) ) {
	//Handler of add_filter('bestdeals_filter_detect_inheritance_key',	'bestdeals_team_detect_inheritance_key', 9, 1);
	function bestdeals_team_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return bestdeals_is_team_page() ? 'team' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'bestdeals_team_get_blog_type' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_type',	'bestdeals_team_get_blog_type', 9, 2);
	function bestdeals_team_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax('team_group') || is_tax('team_group'))
			$page = 'team_category';
		else if ($query && $query->get('post_type')=='team' || get_query_var('post_type')=='team')
			$page = $query && $query->is_single() || is_single() ? 'team_item' : 'team';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'bestdeals_team_get_blog_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_title',	'bestdeals_team_get_blog_title', 9, 2);
	function bestdeals_team_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( bestdeals_strpos($page, 'team')!==false ) {
			if ( $page == 'team_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'team_group' ), 'team_group', OBJECT);
				$title = $term->name;
			} else if ( $page == 'team_item' ) {
				$title = bestdeals_get_post_title();
			} else {
				$title = esc_html__('All team', 'bestdeals');
			}
		}

		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'bestdeals_team_get_stream_page_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_team_get_stream_page_title', 9, 2);
	function bestdeals_team_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (bestdeals_strpos($page, 'team')!==false) {
			if (($page_id = bestdeals_team_get_stream_page_id(0, $page=='team' ? 'blog-team' : $page)) > 0)
				$title = bestdeals_get_post_title($page_id);
			else
				$title = esc_html__('All team', 'bestdeals');				
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'bestdeals_team_get_stream_page_id' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_team_get_stream_page_id', 9, 2);
	function bestdeals_team_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (bestdeals_strpos($page, 'team')!==false) $id = bestdeals_get_template_page_id('blog-team');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'bestdeals_team_get_stream_page_link' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_team_get_stream_page_link', 9, 2);
	function bestdeals_team_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (bestdeals_strpos($page, 'team')!==false) {
			$id = bestdeals_get_template_page_id('blog-team');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'bestdeals_team_get_current_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_team_get_current_taxonomy', 9, 2);
	function bestdeals_team_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( bestdeals_strpos($page, 'team')!==false ) {
			$tax = 'team_group';
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'bestdeals_team_is_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_is_taxonomy',	'bestdeals_team_is_taxonomy', 9, 2);
	function bestdeals_team_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get('team_group')!='' || is_tax('team_group') ? 'team_group' : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'bestdeals_team_query_add_filters' ) ) {
	//Handler of add_filter('bestdeals_filter_query_add_filters',	'bestdeals_team_query_add_filters', 9, 2);
	function bestdeals_team_query_add_filters($args, $filter) {
		if ($filter == 'team') {
			$args['post_type'] = 'team';
		}
		return $args;
	}
}

?>