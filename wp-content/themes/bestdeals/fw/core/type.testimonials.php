<?php
/**
 * BestDEALS Framework: Testimonial post type settings
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Theme init
if (!function_exists('bestdeals_testimonial_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_testimonial_theme_setup' );
	function bestdeals_testimonial_theme_setup() {
	
		// Add item in the admin menu
		add_filter('trx_utils_filter_override_options',	'bestdeals_testimonial_add_override_options');

		// Save data from override options
		add_action('save_post',	'bestdeals_testimonial_save_data');

		// Add shortcodes [trx_testimonials] and [trx_testimonials_item]
		add_action('bestdeals_action_shortcodes_list',		'bestdeals_testimonials_reg_shortcodes');
		add_action('bestdeals_action_shortcodes_list_vc',	'bestdeals_testimonials_reg_shortcodes_vc');

		// Override options fields
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['testimonial_override_options'] = array(
			'id' => 'testimonial-override-options',
			'title' => esc_html__('Testimonial Details', 'bestdeals'),
			'page' => 'testimonial',
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				"testimonial_author" => array(
					"title" => esc_html__('Testimonial author',  'bestdeals'),
					"desc" => esc_attr__("Name of the testimonial's author", 'bestdeals'),
					"class" => "testimonial_author",
					"std" => "",
					"type" => "text"),
				"testimonial_position" => array(
					"title" => esc_html__("Author's position",  'bestdeals'),
					"desc" => esc_attr__("Position of the testimonial's author", 'bestdeals'),
					"class" => "testimonial_author",
					"std" => "",
					"type" => "text"),
				"testimonial_email" => array(
					"title" => esc_html__("Author's e-mail",  'bestdeals'),
					"desc" => esc_attr__("E-mail of the testimonial's author - need to take Gravatar (if registered)", 'bestdeals'),
					"class" => "testimonial_email",
					"std" => "",
					"type" => "text"),
				"testimonial_link" => array(
					"title" => esc_html__('Testimonial link',  'bestdeals'),
					"desc" => esc_attr__("URL of the testimonial source or author profile page", 'bestdeals'),
					"class" => "testimonial_link",
					"std" => "",
					"type" => "text")
			)
		);
		
		if (function_exists('bestdeals_require_data')) {
			// Prepare type "Testimonial"
			bestdeals_require_data( 'post_type', 'testimonial', array(
				'label'               => esc_html__( 'Testimonial', 'bestdeals' ),
				'description'         => esc_html__( 'Testimonial Description', 'bestdeals' ),
				'labels'              => array(
					'name'                => esc_html_x( 'Testimonials', 'Post Type General Name', 'bestdeals' ),
					'singular_name'       => esc_html_x( 'Testimonial', 'Post Type Singular Name', 'bestdeals' ),
					'menu_name'           => esc_html__( 'Testimonials', 'bestdeals' ),
					'parent_item_colon'   => esc_html__( 'Parent Item:', 'bestdeals' ),
					'all_items'           => esc_html__( 'All Testimonials', 'bestdeals' ),
					'view_item'           => esc_html__( 'View Item', 'bestdeals' ),
					'add_new_item'        => esc_html__( 'Add New Testimonial', 'bestdeals' ),
					'add_new'             => esc_html__( 'Add New', 'bestdeals' ),
					'edit_item'           => esc_html__( 'Edit Item', 'bestdeals' ),
					'update_item'         => esc_html__( 'Update Item', 'bestdeals' ),
					'search_items'        => esc_html__( 'Search Item', 'bestdeals' ),
					'not_found'           => esc_html__( 'Not found', 'bestdeals' ),
					'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'bestdeals' ),
				),
				'supports'            => array( 'title', 'editor', 'author', 'thumbnail'),
				'hierarchical'        => false,
				'public'              => false,
				'show_ui'             => true,
				'menu_icon'			  => 'dashicons-cloud',
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 26,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => true,
				'publicly_queryable'  => false,
				'capability_type'     => 'page',
				)
			);
			
			// Prepare taxonomy for testimonial
			bestdeals_require_data( 'taxonomy', 'testimonial_group', array(
				'post_type'			=> array( 'testimonial' ),
				'hierarchical'      => true,
				'labels'            => array(
					'name'              => esc_html_x( 'Testimonials Group', 'taxonomy general name', 'bestdeals' ),
					'singular_name'     => esc_html_x( 'Group', 'taxonomy singular name', 'bestdeals' ),
					'search_items'      => esc_html__( 'Search Groups', 'bestdeals' ),
					'all_items'         => esc_html__( 'All Groups', 'bestdeals' ),
					'parent_item'       => esc_html__( 'Parent Group', 'bestdeals' ),
					'parent_item_colon' => esc_html__( 'Parent Group:', 'bestdeals' ),
					'edit_item'         => esc_html__( 'Edit Group', 'bestdeals' ),
					'update_item'       => esc_html__( 'Update Group', 'bestdeals' ),
					'add_new_item'      => esc_html__( 'Add New Group', 'bestdeals' ),
					'new_item_name'     => esc_html__( 'New Group Name', 'bestdeals' ),
					'menu_name'         => esc_html__( 'Testimonial Group', 'bestdeals' ),
				),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'testimonial_group' ),
				)
			);
		}
	}
}


// Add override options
if (!function_exists('bestdeals_testimonial_add_override_options')) {
	function bestdeals_testimonial_add_override_options($boxes = array()) {
		global $BESTDEALS_GLOBALS;
		$boxes[] = array_merge( $BESTDEALS_GLOBALS['testimonial_override_options'], array('callback' =>'bestdeals_testimonial_show_override_options' ) );
		return $boxes;
	}
}

// Callback function to show fields in override options
if (!function_exists('bestdeals_testimonial_show_override_options')) {
	function bestdeals_testimonial_show_override_options() {
		global $post, $BESTDEALS_GLOBALS;

		// Use nonce for verification
		echo '<input type="hidden" name="override_options_testimonial_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
		
		$data = get_post_meta($post->ID, 'testimonial_data', true);
	
		$fields = $BESTDEALS_GLOBALS['testimonial_override_options']['fields'];
		?>
		<table class="testimonial_area">
		<?php
		if (is_array($fields) && count($fields) > 0) {
			foreach ($fields as $id=>$field) { 
				$meta = isset($data[$id]) ? $data[$id] : '';
				?>
				<tr class="testimonial_field <?php echo esc_attr($field['class']); ?>" valign="top">
					<td><label for="<?php echo esc_attr($id); ?>"><?php echo esc_attr($field['title']); ?></label></td>
					<td><input type="text" name="<?php echo esc_attr($id); ?>" id="<?php echo esc_attr($id); ?>" value="<?php echo esc_attr($meta); ?>" size="30" />
						<br><small><?php echo esc_attr($field['desc']); ?></small></td>
				</tr>
				<?php
			}
		}
		?>
		</table>
		<?php
	}
}


// Save data from override options
if (!function_exists('bestdeals_testimonial_save_data')) {
	//Handler of add_action('save_post', 'bestdeals_testimonial_save_data');
	function bestdeals_testimonial_save_data($post_id) {
		// verify nonce
		if (!isset($_POST['override_options_testimonial_nonce']) || !wp_verify_nonce($_POST['override_options_testimonial_nonce'], basename(__FILE__))) {
			return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		// check permissions
		if ($_POST['post_type']!='testimonial' || !current_user_can('edit_post', $post_id)) {
			return $post_id;
		}

		global $BESTDEALS_GLOBALS;

		$data = array();

		$fields = $BESTDEALS_GLOBALS['testimonial_override_options']['fields'];

		// Post type specific data handling
		if (is_array($fields) && count($fields) > 0) {
			foreach ($fields as $id=>$field) {
				if (isset($_POST[$id])) 
					$data[$id] = stripslashes($_POST[$id]);
			}
		}

		update_post_meta($post_id, 'testimonial_data', $data);
	}
}




?>