<?php
/**
 * BestDEALS Framework: Supported post types settings
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Theme init
if (!function_exists('bestdeals_post_type_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_post_type_theme_setup', 9 );
	function bestdeals_post_type_theme_setup() {
		if ( !bestdeals_options_is_used() ) return;
		$post_type = bestdeals_admin_get_current_post_type();
		if (empty($post_type)) $post_type = 'post';
		$override_key = bestdeals_get_override_key($post_type, 'post_type');
		if ($override_key) {
			// Set post type action
			add_action('save_post',				'bestdeals_post_type_save_options');
			add_filter('trx_utils_filter_override_options',	'bestdeals_post_type_add_override_options');
			add_action('admin_enqueue_scripts', 'bestdeals_post_type_admin_scripts');
			// Create override options
			global $BESTDEALS_GLOBALS;
			$BESTDEALS_GLOBALS['post_override_options'] = array(
				'id' => 'post-override-options',
				'title' => esc_html__('Post Options', 'bestdeals'),
				'page' => $post_type,
				'context' => 'normal',
				'priority' => 'high',
				'fields' => array()
			);
		}
	}
}


// Admin scripts
if (!function_exists('bestdeals_post_type_admin_scripts')) {
	//Handler of add_action('admin_enqueue_scripts', 'bestdeals_post_type_admin_scripts');
	function bestdeals_post_type_admin_scripts() {
	}
}


// Add override options
if (!function_exists('bestdeals_post_type_add_override_options')) {
	function bestdeals_post_type_add_override_options($boxes = array()) {
		global $BESTDEALS_GLOBALS;
		$boxes[] = array_merge( $BESTDEALS_GLOBALS['post_override_options'], array('callback' =>'bestdeals_post_type_show_override_options' ) );
		return $boxes;
	}
}

// Callback function to show fields in override options
if (!function_exists('bestdeals_post_type_show_override_options')) {
	function bestdeals_post_type_show_override_options() {
		global $post, $BESTDEALS_GLOBALS;
		
		$post_type = bestdeals_admin_get_current_post_type();
		$override_key = bestdeals_get_override_key($post_type, 'post_type');
		
		// Use nonce for verification
		echo '<input type="hidden" name="override_options_post_nonce" value="' .esc_attr(wp_create_nonce(basename(__FILE__))).'" />';
		echo '<input type="hidden" name="override_options_post_type" value="'.esc_attr($post_type).'" />';
	
		$custom_options = apply_filters('bestdeals_filter_post_load_custom_options', get_post_meta($post->ID, 'post_custom_options', true), $post_type, $post->ID);

		$mb = $BESTDEALS_GLOBALS['post_override_options'];
		$post_options = bestdeals_array_merge($BESTDEALS_GLOBALS['options'], $mb['fields']);

		bestdeals_set_global_array('js_vars', 'ajax_url', admin_url('admin-ajax.php'));
		bestdeals_set_global_array('js_vars','ajax_nonce', wp_create_nonce('ajax_nonce'));

		do_action('bestdeals_action_post_before_show_override_options', $post_type, $post->ID);
	
		bestdeals_options_page_start(array(
			'data' => $post_options,
			'add_inherit' => true,
			'create_form' => false,
			'buttons' => array('import', 'export'),
			'override' => $override_key
		));

		if (is_array($post_options) && count($post_options) > 0) {
			foreach ($post_options as $id=>$option) { 
				if (!isset($option['override']) || !in_array($override_key, explode(',', $option['override']))) continue;

				$option = apply_filters('bestdeals_filter_post_show_custom_field_option', $option, $id, $post_type, $post->ID);
				$meta = isset($custom_options[$id]) ? apply_filters('bestdeals_filter_post_show_custom_field_value', $custom_options[$id], $option, $id, $post_type, $post->ID) : '';

				do_action('bestdeals_action_post_before_show_custom_field', $post_type, $post->ID, $option, $id, $meta);

				bestdeals_options_show_field($id, $option, $meta);

				do_action('bestdeals_action_post_after_show_custom_field', $post_type, $post->ID, $option, $id, $meta);
			}
		}
	
		bestdeals_options_page_stop();
		
		do_action('bestdeals_action_post_after_show_override_options', $post_type, $post->ID);
		
	}
}


// Save data from override options
if (!function_exists('bestdeals_post_type_save_options')) {
	//Handler of add_action('save_post', 'bestdeals_post_type_save_options');
	function bestdeals_post_type_save_options($post_id) {
		// verify nonce
		if (!isset($_POST['override_options_post_nonce']) || !wp_verify_nonce($_POST['override_options_post_nonce'], basename(__FILE__))) {
			return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		$post_type = isset($_POST['override_options_post_type']) ? bestdeals_get_value_gp('override_options_post_type') : bestdeals_get_value_gp('post_type');
		$override_key = bestdeals_get_override_key($post_type, 'post_type');

		// check permissions
		$capability = 'page';
		$post_types = get_post_types( array( 'name' => $post_type), 'objects' );
		if (!empty($post_types) && is_array($post_types)) {
			foreach ($post_types  as $type) {
				$capability = $type->capability_type;
				break;
			}
		}
		if (!current_user_can('edit_'.($capability), $post_id)) {
			return $post_id;
		}

		global $BESTDEALS_GLOBALS;

		$custom_options = array();

		$post_options = array_merge($BESTDEALS_GLOBALS['options'], $BESTDEALS_GLOBALS['post_override_options']['fields']);

		if (bestdeals_options_merge_new_values($post_options, $custom_options, $_POST, 'save', $override_key)) {
			update_post_meta($post_id, 'post_custom_options', apply_filters('bestdeals_filter_post_save_custom_options', $custom_options, $post_type, $post_id));
		}
		
		
		global $post;
		if ( !empty($post->ID) && $post_id==$post->ID ) {
		 bestdeals_get_post_views($post_id);
		 bestdeals_get_post_likes($post_id);
		}
		
	}
}
?>