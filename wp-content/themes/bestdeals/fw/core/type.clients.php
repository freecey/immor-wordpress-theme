<?php
/**
 * BestDEALS Framework: Clients post type settings
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Theme init
if (!function_exists('bestdeals_clients_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_clients_theme_setup' );
	function bestdeals_clients_theme_setup() {

		// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
		add_filter('bestdeals_filter_get_blog_type',			'bestdeals_clients_get_blog_type', 9, 2);
		add_filter('bestdeals_filter_get_blog_title',		'bestdeals_clients_get_blog_title', 9, 2);
		add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_clients_get_current_taxonomy', 9, 2);
		add_filter('bestdeals_filter_is_taxonomy',			'bestdeals_clients_is_taxonomy', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_clients_get_stream_page_title', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_clients_get_stream_page_link', 9, 2);
		add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_clients_get_stream_page_id', 9, 2);
		add_filter('bestdeals_filter_query_add_filters',		'bestdeals_clients_query_add_filters', 9, 2);
		add_filter('bestdeals_filter_detect_inheritance_key','bestdeals_clients_detect_inheritance_key', 9, 1);

		// Extra column for clients lists
		if (bestdeals_get_theme_option('show_overriden_posts')=='yes') {
			add_filter('manage_edit-clients_columns',			'bestdeals_post_add_options_column', 9);
			add_filter('manage_clients_posts_custom_column',	'bestdeals_post_fill_options_column', 9, 2);
		}

		// Add shortcodes [trx_clients] and [trx_clients_item] in the shortcodes list
		add_action('bestdeals_action_shortcodes_list',		'bestdeals_clients_reg_shortcodes');
		add_action('bestdeals_action_shortcodes_list_vc',	'bestdeals_clients_reg_shortcodes_vc');
		
		if (function_exists('bestdeals_require_data')) {
			// Prepare type "Clients"
			bestdeals_require_data( 'post_type', 'clients', array(
				'label'               => esc_html__( 'Clients', 'bestdeals' ),
				'description'         => esc_attr__( 'Clients Description', 'bestdeals' ),
				'labels'              => array(
					'name'                => esc_html_x( 'Clients', 'Post Type General Name', 'bestdeals' ),
					'singular_name'       => esc_html_x( 'Client', 'Post Type Singular Name', 'bestdeals' ),
					'menu_name'           => esc_html__( 'Clients', 'bestdeals' ),
					'parent_item_colon'   => esc_html__( 'Parent Item:', 'bestdeals' ),
					'all_items'           => esc_html__( 'All Clients', 'bestdeals' ),
					'view_item'           => esc_html__( 'View Item', 'bestdeals' ),
					'add_new_item'        => esc_html__( 'Add New Client', 'bestdeals' ),
					'add_new'             => esc_html__( 'Add New', 'bestdeals' ),
					'edit_item'           => esc_html__( 'Edit Item', 'bestdeals' ),
					'update_item'         => esc_html__( 'Update Item', 'bestdeals' ),
					'search_items'        => esc_html__( 'Search Item', 'bestdeals' ),
					'not_found'           => esc_html__( 'Not found', 'bestdeals' ),
					'not_found_in_trash'  => esc_html__( 'Not found in Trash', 'bestdeals' ),
				),
				'supports'            => array( 'title', 'excerpt', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields'),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'menu_icon'			  => 'dashicons-admin-users',
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 25,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'query_var'           => true,
				'capability_type'     => 'page',
				'rewrite'             => true
				)
			);
			
			// Prepare taxonomy for clients
			bestdeals_require_data( 'taxonomy', 'clients_group', array(
				'post_type'			=> array( 'clients' ),
				'hierarchical'      => true,
				'labels'            => array(
					'name'              => esc_html_x( 'Clients Group', 'taxonomy general name', 'bestdeals' ),
					'singular_name'     => esc_html_x( 'Group', 'taxonomy singular name', 'bestdeals' ),
					'search_items'      => esc_html__( 'Search Groups', 'bestdeals' ),
					'all_items'         => esc_html__( 'All Groups', 'bestdeals' ),
					'parent_item'       => esc_html__( 'Parent Group', 'bestdeals' ),
					'parent_item_colon' => esc_html__( 'Parent Group:', 'bestdeals' ),
					'edit_item'         => esc_html__( 'Edit Group', 'bestdeals' ),
					'update_item'       => esc_html__( 'Update Group', 'bestdeals' ),
					'add_new_item'      => esc_html__( 'Add New Group', 'bestdeals' ),
					'new_item_name'     => esc_html__( 'New Group Name', 'bestdeals' ),
					'menu_name'         => esc_html__( 'Clients Group', 'bestdeals' ),
				),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'clients_group' ),
				)
			);
		}
	}
}

if ( !function_exists( 'bestdeals_clients_settings_theme_setup2' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_clients_settings_theme_setup2', 3 );
	function bestdeals_clients_settings_theme_setup2() {
		// Add post type 'clients' and taxonomy 'clients_group' into theme inheritance list
		bestdeals_add_theme_inheritance( array('clients' => array(
			'stream_template' => 'blog-clients',
			'single_template' => 'single-client',
			'taxonomy' => array('clients_group'),
			'taxonomy_tags' => array(),
			'post_type' => array('clients'),
			'override' => 'page'
			) )
		);
	}
}


if (!function_exists('bestdeals_clients_after_theme_setup')) {
	add_action( 'bestdeals_action_after_init_theme', 'bestdeals_clients_after_theme_setup' );
	function bestdeals_clients_after_theme_setup() {
		// Update fields in the override options
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['post_override_options']) && $BESTDEALS_GLOBALS['post_override_options']['page']=='clients') {
			// Override options fields
			$BESTDEALS_GLOBALS['post_override_options']['title'] = esc_html__('Client Options', 'bestdeals');
			$BESTDEALS_GLOBALS['post_override_options']['fields'] = array(
				"mb_partition_clients" => array(
					"title" => esc_html__('Clients', 'bestdeals'),
					"override" => "page,post",
					"divider" => false,
					"icon" => "iconadmin-users",
					"type" => "partition"),
				"mb_info_clients_1" => array(
					"title" => esc_html__('Client details', 'bestdeals'),
					"override" => "page,post",
					"divider" => false,
					"desc" => esc_html__('In this section you can put details for this client', 'bestdeals'),
					"class" => "course_meta",
					"type" => "info"),
				"client_name" => array(
					"title" => esc_html__('Contact name',  'bestdeals'),
					"desc" => esc_html__("Name of the contacts manager", 'bestdeals'),
					"override" => "page,post",
					"class" => "client_name",
					"std" => '',
					"type" => "text"),
				"client_position" => array(
					"title" => esc_html__('Position',  'bestdeals'),
					"desc" => esc_attr__("Position of the contacts manager", 'bestdeals'),
					"override" => "page,post",
					"class" => "client_position",
					"std" => '',
					"type" => "text"),
				"client_show_link" => array(
					"title" => esc_html__('Show link',  'bestdeals'),
					"desc" => esc_attr__("Show link to client page", 'bestdeals'),
					"override" => "page,post",
					"class" => "client_show_link",
					"std" => "no",
					"options" => bestdeals_get_list_yesno(),
					"type" => "switch"),
				"client_link" => array(
					"title" => esc_html__('Link',  'bestdeals'),
					"desc" => esc_attr__("URL of the client's site. If empty - use link to this page", 'bestdeals'),
					"override" => "page,post",
					"class" => "client_link",
					"std" => '',
					"type" => "text")
			);
		}
	}
}


// Return true, if current page is clients page
if ( !function_exists( 'bestdeals_is_clients_page' ) ) {
	function bestdeals_is_clients_page() {
		return get_query_var('post_type')=='clients' || is_tax('clients_group') || (is_page() && bestdeals_get_template_page_id('blog-clients')==get_the_ID());
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'bestdeals_clients_detect_inheritance_key' ) ) {
	//Handler of add_filter('bestdeals_filter_detect_inheritance_key',	'bestdeals_clients_detect_inheritance_key', 9, 1);
	function bestdeals_clients_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return bestdeals_is_clients_page() ? 'clients' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'bestdeals_clients_get_blog_type' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_type',	'bestdeals_clients_get_blog_type', 9, 2);
	function bestdeals_clients_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax('clients_group') || is_tax('clients_group'))
			$page = 'clients_category';
		else if ($query && $query->get('post_type')=='clients' || get_query_var('post_type')=='clients')
			$page = $query && $query->is_single() || is_single() ? 'clients_item' : 'clients';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'bestdeals_clients_get_blog_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_blog_title',	'bestdeals_clients_get_blog_title', 9, 2);
	function bestdeals_clients_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( bestdeals_strpos($page, 'clients')!==false ) {
			if ( $page == 'clients_category' ) {
				$term = get_term_by( 'slug', get_query_var( 'clients_group' ), 'clients_group', OBJECT);
				$title = $term->name;
			} else if ( $page == 'clients_item' ) {
				$title = bestdeals_get_post_title();
			} else {
				$title = esc_html__('All clients', 'bestdeals');
			}
		}

		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'bestdeals_clients_get_stream_page_title' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_title',	'bestdeals_clients_get_stream_page_title', 9, 2);
	function bestdeals_clients_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (bestdeals_strpos($page, 'clients')!==false) {
			if (($page_id = bestdeals_clients_get_stream_page_id(0, $page=='clients' ? 'blog-clients' : $page)) > 0)
				$title = bestdeals_get_post_title($page_id);
			else
				$title = esc_html__('All clients', 'bestdeals');				
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'bestdeals_clients_get_stream_page_id' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_id',	'bestdeals_clients_get_stream_page_id', 9, 2);
	function bestdeals_clients_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (bestdeals_strpos($page, 'clients')!==false) $id = bestdeals_get_template_page_id('blog-clients');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'bestdeals_clients_get_stream_page_link' ) ) {
	//Handler of add_filter('bestdeals_filter_get_stream_page_link',	'bestdeals_clients_get_stream_page_link', 9, 2);
	function bestdeals_clients_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (bestdeals_strpos($page, 'clients')!==false) {
			$id = bestdeals_get_template_page_id('blog-clients');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'bestdeals_clients_get_current_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_get_current_taxonomy',	'bestdeals_clients_get_current_taxonomy', 9, 2);
	function bestdeals_clients_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( bestdeals_strpos($page, 'clients')!==false ) {
			$tax = 'clients_group';
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'bestdeals_clients_is_taxonomy' ) ) {
	//Handler of add_filter('bestdeals_filter_is_taxonomy',	'bestdeals_clients_is_taxonomy', 9, 2);
	function bestdeals_clients_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get('clients_group')!='' || is_tax('clients_group') ? 'clients_group' : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'bestdeals_clients_query_add_filters' ) ) {
	//Handler of add_filter('bestdeals_filter_query_add_filters',	'bestdeals_clients_query_add_filters', 9, 2);
	function bestdeals_clients_query_add_filters($args, $filter) {
		if ($filter == 'clients') {
			$args['post_type'] = 'clients';
		}
		return $args;
	}
}



?>