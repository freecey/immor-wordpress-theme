<?php
/**
 * BestDEALS Framework: messages subsystem
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('bestdeals_messages_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_messages_theme_setup' );
	function bestdeals_messages_theme_setup() {
		// Core messages strings
		add_filter('bestdeals_action_add_scripts_inline', 'bestdeals_messages_add_scripts_inline');
	}
}


/* Session messages
------------------------------------------------------------------------------------- */

if (!function_exists('bestdeals_get_error_msg')) {
	function bestdeals_get_error_msg() {
		global $BESTDEALS_GLOBALS;
		return !empty($BESTDEALS_GLOBALS['error_msg']) ? $BESTDEALS_GLOBALS['error_msg'] : '';
	}
}

if (!function_exists('bestdeals_set_error_msg')) {
	function bestdeals_set_error_msg($msg) {
		global $BESTDEALS_GLOBALS;
		$msg2 = bestdeals_get_error_msg();
		$BESTDEALS_GLOBALS['error_msg'] = $msg2 . ($msg2=='' ? '' : '<br />') . ($msg);
	}
}

if (!function_exists('bestdeals_get_success_msg')) {
	function bestdeals_get_success_msg() {
		global $BESTDEALS_GLOBALS;
		return !empty($BESTDEALS_GLOBALS['success_msg']) ? $BESTDEALS_GLOBALS['success_msg'] : '';
	}
}

if (!function_exists('bestdeals_set_success_msg')) {
	function bestdeals_set_success_msg($msg) {
		global $BESTDEALS_GLOBALS;
		$msg2 = bestdeals_get_success_msg();
		$BESTDEALS_GLOBALS['success_msg'] = $msg2 . ($msg2=='' ? '' : '<br />') . ($msg);
	}
}

if (!function_exists('bestdeals_get_notice_msg')) {
	function bestdeals_get_notice_msg() {
		global $BESTDEALS_GLOBALS;
		return !empty($BESTDEALS_GLOBALS['notice_msg']) ? $BESTDEALS_GLOBALS['notice_msg'] : '';
	}
}

if (!function_exists('bestdeals_set_notice_msg')) {
	function bestdeals_set_notice_msg($msg) {
		global $BESTDEALS_GLOBALS;
		$msg2 = bestdeals_get_notice_msg();
		$BESTDEALS_GLOBALS['notice_msg'] = $msg2 . ($msg2=='' ? '' : '<br />') . ($msg);
	}
}


/* System messages (save when page reload)
------------------------------------------------------------------------------------- */
if (!function_exists('bestdeals_set_system_message')) {
	function bestdeals_set_system_message($msg, $status='info', $hdr='') {
		update_option('bestdeals_message', array('message' => $msg, 'status' => $status, 'header' => $hdr));
	}
}

if (!function_exists('bestdeals_get_system_message')) {
	function bestdeals_get_system_message($del=false) {
		$msg = get_option('bestdeals_message', false);
		if (!$msg)
			$msg = array('message' => '', 'status' => '', 'header' => '');
		else if ($del)
			bestdeals_del_system_message();
		return $msg;
	}
}

if (!function_exists('bestdeals_del_system_message')) {
	function bestdeals_del_system_message() {
		delete_option('bestdeals_message');
	}
}


/* Messages strings
------------------------------------------------------------------------------------- */
// Strings for translation
if (!function_exists('bestdeals_messages_add_scripts_inline')) {
	function bestdeals_messages_add_scripts_inline($vars = array()) {
		global $BESTDEALS_GLOBALS;

		$vars["strings"] = array(
				'bookmark_add' => addslashes(esc_html__('Add the bookmark', 'bestdeals')),
				'bookmark_added' =>	addslashes(esc_html__('Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab \'Bookmarks\'', 'bestdeals')) , 
				'bookmark_del' => addslashes(esc_html__('Delete this bookmark', 'bestdeals')) , 
				'bookmark_title' =>	 addslashes(esc_html__('Enter bookmark title', 'bestdeals')) , 
				'bookmark_exists' =>	 addslashes(esc_html__('Current page already exists in the bookmarks list', 'bestdeals')) , 
				'search_error' =>	 addslashes(esc_html__('Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.', 'bestdeals')) , 
				'email_confirm' =>	 addslashes(wp_kses( __('On the e-mail address <b>%s</b> we sent a confirmation email.<br>Please, open it and click on the link.', 'bestdeals'), $BESTDEALS_GLOBALS['allowed_tags'])) , 
				'reviews_vote' =>	 addslashes(esc_html__('Thanks for your vote! New average rating is:', 'bestdeals')) , 
				'reviews_error' =>	 addslashes(esc_html__('Error saving your vote! Please, try again later.', 'bestdeals')) , 
				'error_like' =>		 addslashes(esc_html__('Error saving your like! Please, try again later.', 'bestdeals')) , 
				'error_global' =>	 addslashes(esc_html__('Global error text', 'bestdeals')) , 
				'name_empty' =>		 addslashes(esc_html__('The name can\'t be empty', 'bestdeals')) , 
				'name_long' =>		 addslashes(esc_html__('Too long name', 'bestdeals')) , 
				'email_empty' =>		 addslashes(esc_html__('Too short (or empty) email address', 'bestdeals')) , 
				'email_long' =>		 addslashes(esc_html__('Too long email address', 'bestdeals')) , 
				'email_not_valid' =>	 addslashes(esc_html__('Invalid email address', 'bestdeals')) , 
				'subject_empty' =>	 addslashes(esc_html__('The subject can\'t be empty', 'bestdeals')) , 
				'subject_long' =>	 addslashes(esc_html__('Too long subject', 'bestdeals')) , 
				'text_empty' =>		 addslashes(esc_html__('The message text can\'t be empty', 'bestdeals')) , 
				'text_long' =>		 addslashes(esc_html__('Too long message text', 'bestdeals')) , 
				'send_complete' =>	 addslashes(esc_html__("Send message complete!", 'bestdeals')) , 
				'send_error' =>		 addslashes(esc_html__('Transmit failed!', 'bestdeals')) , 
				'login_empty' =>		 addslashes(esc_html__('The Login field can\'t be empty', 'bestdeals')) , 
				'login_long' =>		 addslashes(esc_html__('Too long login field', 'bestdeals')) , 
				'login_success' =>	 addslashes(esc_html__('Login success! The page will be reloaded in 3 sec.', 'bestdeals')) , 
				'login_failed' =>	 addslashes(esc_html__('Login failed!', 'bestdeals')) , 
				'password_empty' =>	 addslashes(esc_html__('The password can\'t be empty and shorter then 4 characters', 'bestdeals')) , 
				'password_long' =>	 addslashes(esc_html__('Too long password', 'bestdeals')) , 
				'password_not_equal' => addslashes(esc_html__('The passwords in both fields are not equal', 'bestdeals')) , 
				'registration_success' => addslashes(esc_html__('Registration success! Please log in!', 'bestdeals')) ,
				'registration_failed' => addslashes(esc_html__('Registration failed!', 'bestdeals')) , 
				'geocode_error' =>	 addslashes(esc_html__('Geocode was not successful for the following reason:', 'bestdeals')) , 
				'googlemap_not_avail' => addslashes(esc_html__('Google map API not available!', 'bestdeals')) , 
				'editor_save_success' => addslashes(esc_html__("Post content saved!", 'bestdeals')) , 
				'editor_save_error' => addslashes(esc_html__("Error saving post data!", 'bestdeals')) , 
				'editor_delete_post' => addslashes(esc_html__("You really want to delete the current post?", 'bestdeals')) , 
				'editor_delete_post_header' => addslashes(esc_html__("Delete post", 'bestdeals')) ,
				'editor_delete_success' => addslashes(esc_html__("Post deleted!", 'bestdeals')) , 
				'editor_delete_error' =>	 addslashes(esc_html__("Error deleting post!", 'bestdeals')) , 
				'editor_caption_cancel' => addslashes(esc_html__('Cancel', 'bestdeals')) , 
				'editor_caption_close' => addslashes(esc_html__('Close', 'bestdeals')) . '"'
		);
		
		return $vars;
	}
}
?>