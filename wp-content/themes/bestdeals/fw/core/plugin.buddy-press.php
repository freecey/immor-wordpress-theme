<?php
/* BuddyPress support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('bestdeals_buddypress_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_buddypress_theme_setup' );
	function bestdeals_buddypress_theme_setup() {
		if (bestdeals_is_buddypress_page()) {
			add_action( 'bestdeals_action_add_styles', 'bestdeals_buddypress_frontend_scripts' );
			// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
			add_filter('bestdeals_filter_detect_inheritance_key',	'bestdeals_buddypress_detect_inheritance_key', 9, 1);
		}
	}
}
if ( !function_exists( 'bestdeals_buddypress_settings_theme_setup2' ) ) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_buddypress_settings_theme_setup2', 3 );
	function bestdeals_buddypress_settings_theme_setup2() {
		if (bestdeals_exists_buddypress()) {
			bestdeals_add_theme_inheritance( array('buddypress' => array(
				'stream_template' => 'buddypress',
				'single_template' => '',
				'taxonomy' => array(),
				'taxonomy_tags' => array(),
				'post_type' => array(),
				'override' => 'page'
				) )
			);
		}
	}
}

// Check if BuddyPress installed and activated
if ( !function_exists( 'bestdeals_exists_buddypress' ) ) {
	function bestdeals_exists_buddypress() {
		return class_exists( 'BuddyPress' );
	}
}

// Check if current page is BuddyPress page
if ( !function_exists( 'bestdeals_is_buddypress_page' ) ) {
	function bestdeals_is_buddypress_page() {
		return  bestdeals_is_bbpress_page() || (function_exists('is_buddypress') && is_buddypress());
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'bestdeals_buddypress_detect_inheritance_key' ) ) {
	//Handler of add_filter('bestdeals_filter_detect_inheritance_key',	'bestdeals_buddypress_detect_inheritance_key', 9, 1);
	function bestdeals_buddypress_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return bestdeals_is_buddypress_page() ? 'buddypress' : '';
	}
}

// Enqueue BuddyPress custom styles
if ( !function_exists( 'bestdeals_buddypress_frontend_scripts' ) ) {
	//Handler of add_action( 'bestdeals_action_add_styles', 'bestdeals_buddypress_frontend_scripts' );
	function bestdeals_buddypress_frontend_scripts() {
		wp_enqueue_style( 'bestdeals-buddypress-style',  bestdeals_get_file_url('css/buddypress-style.css'), array(), null );
	}
}

?>