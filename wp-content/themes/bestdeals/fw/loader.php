<?php
/**
 * BestDEALS Framework
 *
 * @package bestdeals
 * @since bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Framework directory path from theme root
if ( ! defined( 'BESTDEALS_FW_DIR' ) )		define( 'BESTDEALS_FW_DIR', '/fw/' );
if ( ! defined( 'BESTDEALS_PATH' ) ) define( 'BESTDEALS_PATH', 	trailingslashit( get_template_directory() ) );
// Theme timing
if ( ! defined( 'BESTDEALS_START_TIME' ) )	define( 'BESTDEALS_START_TIME', microtime());			// Framework start time
if ( ! defined( 'BESTDEALS_START_MEMORY' ) )	define( 'BESTDEALS_START_MEMORY', memory_get_usage());	// Memory usage before core loading

// Global variables storage
$BESTDEALS_GLOBALS = array(
	'page_template'	=> '',
    'allowed_tags'	=> array(		// Allowed tags list (with attributes) in translations
    	'b' => array(),
    	'strong' => array(),
    	'i' => array(),
    	'em' => array(),
    	'u' => array(),
    	'a' => array(
			'href' => array(),
			'title' => array(),
			'target' => array(),
			'id' => array(),
			'class' => array()
		),
    	'span' => array(
			'id' => array(),
			'class' => array()
		)
    )	
);

/* Theme setup section
-------------------------------------------------------------------- */
if ( !function_exists( 'bestdeals_loader_theme_setup' ) ) {
	add_action( 'after_setup_theme', 'bestdeals_loader_theme_setup', 20 );
	function bestdeals_loader_theme_setup() {
		// Before init theme
		do_action('bestdeals_action_before_init_theme');

		// Load current values for main theme options
		bestdeals_load_main_options();

		// Theme core init - only for admin side. In frontend it called from header.php
		if ( is_admin() ) {
			bestdeals_core_init_theme();
		}
	}
}


/* Include core parts
------------------------------------------------------------------------ */

// Manual load important libraries before load all rest files
// core.strings must be first - we use bestdeals_str...() in the bestdeals_get_file_dir()
require_once( (file_exists(get_stylesheet_directory().(BESTDEALS_FW_DIR).'core/core.strings.php') ? get_stylesheet_directory() : get_template_directory()).(BESTDEALS_FW_DIR).'core/core.strings.php' );
// core.files must be first - we use bestdeals_get_file_dir() to include all rest parts
require_once( (file_exists(get_stylesheet_directory().(BESTDEALS_FW_DIR).'core/core.files.php') ? get_stylesheet_directory() : get_template_directory()).(BESTDEALS_FW_DIR).'core/core.files.php' );

// Include theme storage support
require_once( bestdeals_get_file_dir('core/core.globals.php') );

// Include core files
bestdeals_autoload_folder( 'core' );

do_action('bestdeals_setup_widgets');

// Include custom theme files
bestdeals_autoload_folder( 'includes' );

// Include theme templates
bestdeals_autoload_folder( 'templates' );

?>