<?php
/**
Template Name: Property list
 */
global $BESTDEALS_GLOBALS;
$BESTDEALS_GLOBALS['blog_filters'] = 'property';

if ( isset($_GET['search_keyword']) ) {
	$_searchKeyword = htmlspecialchars(trim($_GET['search_keyword']));
	if ( strlen($_searchKeyword) > 2 ) {
		$BESTDEALS_GLOBALS['blog_filters_property']['property_keyword'] = $_searchKeyword;
	}
}

if ( isset($_GET['search_location']) ) {
	$_searchLocation = htmlspecialchars(trim($_GET['search_location']));
	if ( $_searchLocation != "-1" ) {
		$BESTDEALS_GLOBALS['blog_filters_property']['property_location'] = $_searchLocation;
	}
}

if ( isset($_GET['search_type_single']) ) {
	$_searchTypeSingle = htmlspecialchars(trim($_GET['search_type_single']));
	if ( $_searchTypeSingle != "-1" ) {
		$_searchTypeSingle = str_replace('_', ' ', $_searchTypeSingle);
		$BESTDEALS_GLOBALS['blog_filters_property']['property_type_single'] = $_searchTypeSingle;
	}
}

if ( isset($_GET['search_bedrooms']) ) {
	$_searchBedrooms = htmlspecialchars(trim($_GET['search_bedrooms']));
	if ( $_searchBedrooms != "-1" ) {
		$BESTDEALS_GLOBALS['blog_filters_property']['property_bedrooms'] = $_searchBedrooms;
	}
}

if ( isset($_GET['search_bathrooms']) ) {
	$_searchBathrooms = htmlspecialchars(trim($_GET['search_bathrooms']));
	if ( $_searchBathrooms != "-1" ) {
		$BESTDEALS_GLOBALS['blog_filters_property']['property_bathrooms'] = $_searchBathrooms;
	}
}

$_searchAreaMin = 0; $_searchAreaMax = 0;
if ( isset($_GET['search_area_min']) ) {
	$_searchAreaMin = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_area_min'])));
}
if ( isset($_GET['search_area_max']) ) {
	$_searchAreaMax = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_area_max'])));
}
if ( $_searchAreaMin > 0 ) {
	$BESTDEALS_GLOBALS['blog_filters_property']['property_area_min'] = $_searchAreaMin;
}
if ( $_searchAreaMax > 0 ) {
	$BESTDEALS_GLOBALS['blog_filters_property']['property_area_max'] = $_searchAreaMax;
}
$_searchPriceMin = 0; $_searchPriceMax = 0;
if ( isset($_GET['search_price_min']) ) {
	$_searchPriceMin = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_price_min'])));
}
if ( isset($_GET['search_price_max']) ) {
	$_searchPriceMax = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_price_max'])));
}
if ( $_searchPriceMin > 0 ) {
	$BESTDEALS_GLOBALS['blog_filters_property']['property_price_min'] = $_searchPriceMin;
}
if ( $_searchPriceMax > 0 ) {
	$BESTDEALS_GLOBALS['blog_filters_property']['property_price_max'] = $_searchPriceMax;
}

if ( isset($_GET['search_status']) ) {
	$_searchStatus = htmlspecialchars(trim($_GET['search_status']));
	$BESTDEALS_GLOBALS['blog_filters_property']['property-status'] = $_searchStatus;
}

get_template_part('blog');
?>