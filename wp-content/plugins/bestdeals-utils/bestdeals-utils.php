<?php
/*
Plugin Name: bestdeals Utilities
Plugin URI: http://axiomthemes.com
Author: bestdeals
Author URI: http://axiomthemes.com
Description: Utils for files, directories, post type and taxonomies manipulations
Version: 2.2
Language: bestdeals-utils
*/

// Don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Current version
if ( ! defined( 'TRX_UTILS_VERSION' ) ) {
	define( 'TRX_UTILS_VERSION', '2.2' );
}

// Plugin's storage
if (!defined('TRX_UTILS_PLUGIN_DIR'))	define('TRX_UTILS_PLUGIN_DIR', plugin_dir_path(__FILE__));
if (!defined('TRX_UTILS_PLUGIN_URL'))	define('TRX_UTILS_PLUGIN_URL', plugin_dir_url(__FILE__));
if (!defined('TRX_UTILS_PLUGIN_BASE'))	define('TRX_UTILS_PLUGIN_BASE', dirname(plugin_basename(__FILE__)));

global $TRX_UTILS_STORAGE;
$TRX_UTILS_STORAGE = array (
	'plugin_active' => false,
);


/* Types and taxonomies 
------------------------------------------------------ */

// Register theme required types and taxes
if (!function_exists('bestdeals_require_data')) {	
	function bestdeals_require_data($type, $name, $args) {
		if ($type == 'taxonomy')
			register_taxonomy($name, $args['post_type'], $args);
		else
			register_post_type($name, $args);
	}
}

// Load plugin's translation file
// Attention! It must be loaded before the first call of any translation function
if ( !function_exists( 'trx_utils_load_plugin_textdomain' ) ) {
    add_action( 'plugins_loaded', 'trx_utils_load_plugin_textdomain');
    function trx_utils_load_plugin_textdomain() {
        static $loaded = false;
        if ( $loaded ) return true;
        $domain = 'bestdeals-utils';
        if ( is_textdomain_loaded( $domain ) && !is_a( $GLOBALS['l10n'][ $domain ], 'NOOP_Translations' ) ) return true;
        $loaded = true;
        load_plugin_textdomain( $domain, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }
}

/* Twitter API
------------------------------------------------------ */
if (!function_exists('bestdeals_twitter_acquire_data')) {
	function bestdeals_twitter_acquire_data($cfg) {
		if (empty($cfg['mode'])) $cfg['mode'] = 'user_timeline';
		$data = get_transient("twitter_data_".($cfg['mode']));
		if (!$data) {
			require_once( plugin_dir_path( __FILE__ ) . 'lib/tmhOAuth/tmhOAuth.php' );
			$tmhOAuth = new tmhOAuth(array(
				'consumer_key'    => $cfg['consumer_key'],
				'consumer_secret' => $cfg['consumer_secret'],
				'token'           => $cfg['token'],
				'secret'          => $cfg['secret']
			));
			$code = $tmhOAuth->user_request(array(
				'url' => $tmhOAuth->url(bestdeals_twitter_mode_url($cfg['mode']))
			));
			if ($code == 200) {
				$data = json_decode($tmhOAuth->response['response'], true);
				if (isset($data['status'])) {
					$code = $tmhOAuth->user_request(array(
						'url' => $tmhOAuth->url(bestdeals_twitter_mode_url($cfg['oembed'])),
						'params' => array(
							'id' => $data['status']['id_str']
						)
					));
					if ($code == 200)
						$data = json_decode($tmhOAuth->response['response'], true);
				}
				set_transient("twitter_data_".($cfg['mode']), $data, 60*60);
			}
		} else if (!is_array($data) && substr($data, 0, 2)=='a:') {
			$data = unserialize($data);
		}
		return $data;
	}
}

if (!function_exists('bestdeals_twitter_mode_url')) {
	function bestdeals_twitter_mode_url($mode) {
		$url = '/1.1/statuses/';
		if ($mode == 'user_timeline')
			$url .= $mode;
		else if ($mode == 'home_timeline')
			$url .= $mode;
		return $url;
	}
}



// Theme init
if (!function_exists('bestdeals_wp_theme_regsetup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_wp_theme_regsetup' );
	function bestdeals_wp_theme_regsetup() {

		// AJAX: New user registration
		add_action('wp_ajax_registration_user',				'bestdeals_callback_registration_user');
		add_action('wp_ajax_nopriv_registration_user',		'bestdeals_callback_registration_user');

		// AJAX: User login
		add_action('wp_ajax_login_user',					'bestdeals_callback_login_user');
		add_action('wp_ajax_nopriv_login_user',				'bestdeals_callback_login_user');
	}
}

// AJAX: New user registration
// add_action('wp_ajax_registration_user',			'bestdeals_callback_registration_user');
// add_action('wp_ajax_nopriv_registration_user',	'bestdeals_callback_registration_user');
if ( !function_exists( 'bestdeals_callback_registration_user' ) ) {
	function bestdeals_callback_registration_user() {
		global $_REQUEST;

		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'ajax_nonce' ) ) {
			die();
		}

		$user_name  = bestdeals_substr($_REQUEST['user_name'], 0, 20);
		$user_email = bestdeals_substr($_REQUEST['user_email'], 0, 60);
		$user_pwd   = bestdeals_substr($_REQUEST['user_pwd'], 0, 20);

		$response = array('error' => '');

		$id = wp_insert_user( array ('user_login' => $user_name, 'user_pass' => $user_pwd, 'user_email' => $user_email) );
		if ( is_wp_error($id) ) {
			$response['error'] = $id->get_error_message();
		} else if (($notify = bestdeals_get_theme_option('notify_about_new_registration'))!='no' && (($contact_email = bestdeals_get_theme_option('contact_email')) || ($contact_email = bestdeals_get_theme_option('admin_email')))) {
			$mail = bestdeals_get_theme_option('mail_function');
			if (in_array($notify, array('both', 'admin', 'yes'))) {
				$subj = sprintf(esc_attr__('Site %s - New user registration: %s', 'bestdeals-utils'), esc_html(get_bloginfo('site_name')), esc_html($user_name));
				$msg = "\n".esc_html__('New registration:', 'bestdeals-utils')
					."\n".esc_html__('Name:', 'bestdeals-utils').' '.esc_html($user_name)
					."\n".esc_html__('E-mail:', 'bestdeals-utils').' '.esc_html($user_email)
					."\n\n............ " . esc_html(get_bloginfo('site_name')) . " (" . esc_html(home_url('/')) . ") ............";
				$head = "Content-Type: text/plain; charset=\"utf-8\"\n"
					. "X-Mailer: PHP/" . phpversion() . "\n"
					. "Reply-To: " . sanitize_text_field($user_email) . "\n"
					. "To: " . sanitize_text_field($contact_email) . "\n"
					. "From: " . sanitize_text_field($user_email) . "\n"
					. "Subject: " . sanitize_text_field($subj) . "\n";
				@$mail($contact_email, $subj, $msg, $head);
			}
			if (in_array($notify, array('both', 'user', 'yes'))) {
				$subj = sprintf(esc_attr__('Welcome to "%s"', 'bestdeals-utils'), get_bloginfo('site_name'));
				$msg = "\n".esc_html__('Your registration data:', 'bestdeals-utils')
					."\n".esc_html__('Name:', 'bestdeals-utils').' '.esc_html($user_name)
					."\n".esc_html__('E-mail:', 'bestdeals-utils').' '.esc_html($user_email)
					."\n".esc_html__('Password:', 'bestdeals-utils').' '.esc_html($user_pwd)
					."\n\n............ " . esc_html(get_bloginfo('site_name')) . " (<a href=\"" . esc_url(home_url('/')) . "\">" . esc_html(home_url('/')) . "</a>) ............";
				$head = "Content-Type: text/plain; charset=\"utf-8\"\n"
					. "X-Mailer: PHP/" . phpversion() . "\n"
					. "Reply-To: " . sanitize_text_field($contact_email) . "\n"
					. "To: " . sanitize_text_field($user_email) . "\n"
					. "From: " . sanitize_text_field($contact_email) . "\n"
					. "Subject: " . sanitize_text_field($subj) . "\n";
				@$mail($user_email, $subj, $msg, $head);
			}
		}

		echo json_encode($response);
		die();
	}
}



// AJAX: Login user
// add_action('wp_ajax_login_user',			'bestdeals_callback_login_user');
// add_action('wp_ajax_nopriv_login_user',	'bestdeals_callback_login_user');
if ( !function_exists( 'bestdeals_callback_login_user' ) ) {
	function bestdeals_callback_login_user() {
		global $_REQUEST;

		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'ajax_nonce' ) ) {
			die();
		}

		$user_log = bestdeals_substr($_REQUEST['user_log'], 0, 60);
		$user_pwd = bestdeals_substr($_REQUEST['user_pwd'], 0, 20);
		$remember = bestdeals_substr($_REQUEST['remember'], 0, 7)=='forever';

		$response = array('error' => '');

		if ( is_email( $user_log ) ) {
			$user = get_user_by('email', $user_log );
			if ( $user ) $user_log = $user->user_login;
		}

		$rez = wp_signon( array(
			'user_login' => $user_log,
			'user_password' => $user_pwd,
			'remember' => $remember
		), false );

		if ( is_wp_error($rez) ) {
			$response['error'] = $rez->get_error_message();
		}

		echo json_encode($response);
		die();
	}
}


/* LESS compilers
------------------------------------------------------ */

// Compile less-files
if (!function_exists('bestdeals_less_compiler')) {	
	function bestdeals_less_compiler($list, $opt) {

		$success = true;

		// Load and create LESS Parser
		if ($opt['compiler'] == 'lessc') {
			// 1: Compiler Lessc
			require_once( plugin_dir_path( __FILE__ ) . 'lib/lessc/lessc.inc.php' );
		} else {
			// 2: Compiler Less
			require_once( plugin_dir_path( __FILE__ ) . 'lib/less/Less.php' );
		}

		foreach($list as $file) {
			if (empty($file) || !file_exists($file)) continue;
			$file_css = substr_replace($file , 'css', strrpos($file , '.') + 1);
				
			// Check if time of .css file after .less - skip current .less
			if (!empty($opt['check_time']) && file_exists($file_css)) {
				$css_time = filemtime($file_css);
				if ($css_time >= filemtime($file) && ($opt['utils_time']==0 || $css_time > $opt['utils_time'])) continue;
			}
				
			// Compile current .less file
			try {
				// Create Parser
				if ($opt['compiler'] == 'lessc') {
					$parser = new lessc;
					//$parser->registerFunction("replace", "bestdeals_less_func_replace");
					if ($opt['compressed']) $parser->setFormatter("compressed");
				} else {
					if ($opt['compressed'])
						$args = array('compress' => true);
					else {
						$args = array('compress' => false);
						if ($opt['map'] != 'no') {
							$args['sourceMap'] = true;
							if ($opt['map'] == 'external') {
								$args['sourceMapWriteTo'] = $file.'.map';
								$args['sourceMapURL'] = str_replace(
									array(get_template_directory(), get_stylesheet_directory()),
									array(get_template_directory_uri(), get_stylesheet_directory_uri()),
									$file) . '.map';
							}
						}
					}
					$parser = new Less_Parser($args);
				}

				// Parse main file
				$css = '';
				if ($opt['map'] != 'no') {
				// Parse main file
					$parser->parseFile( $file, '');
					// Parse less utils
					if (is_array($opt['utils']) && count($opt['utils']) > 0) {
						foreach($opt['utils'] as $utility) {
							$parser->parseFile( $utility, '');
						}
					}
					// Parse less vars (from Theme Options)
					if (!empty($opt['vars'])) {
						$parser->parse($opt['vars']);
					}
					// Get compiled CSS code
					$css = $parser->getCss();
					// Reset LESS engine
					$parser->Reset();
				} else if (($text = file_get_contents($file))!='') {
					$parts = $opt['separator'] != '' ? explode($opt['separator'], $text) : array($text);
					for ($i=0; $i<count($parts); $i++) {
						$text = $parts[$i]
							. (!empty($opt['utils']) ? $opt['utils'] : '')			// Add less utils
							. (!empty($opt['vars']) ? $opt['vars'] : '');			// Add less vars (from Theme Options)
						// Get compiled CSS code
						if ($opt['compiler']=='lessc')
							$css .= $parser->compile($text);
						else {
							$parser->parse($text);
							$css .= $parser->getCss();
							$parser->Reset();
						}
					}
				}
				if ($css) {
					if ($opt['map']=='no') {
						// If it main theme style - append CSS after header comments
						if ($file == get_template_directory(). '/style.less') {
							// Append to the main Theme Style CSS
							$theme_css = file_get_contents( get_template_directory() . '/style.css' );
							$css = substr($theme_css, 0, strpos($theme_css, '*/')+2) . "\n\n" . $css;
						} else {
							$css =	"/*"
									. "\n"
									. __('Attention! Do not modify this .css-file!', 'bestdeals-utils')
									. "\n"
									. __('Please, make all necessary changes in the corresponding .less-file!', 'bestdeals-utils')
									. "\n"
									. "*/"
									. "\n"
									. '@charset "utf-8";'
									. "\n\n"
									. $css;
						}
					}
					// Save compiled CSS
					file_put_contents( $file_css, $css);
				}
			} catch (Exception $e) {
				if (function_exists('dfl')) dfl($e->getMessage());
				$success = false;
			}
		}
		return $success;
	}
}

// Return encode
if (!function_exists('bestdeals_convert')) {	
	function bestdeals_convert($str) {
		return base64_encode($str);
	}
}
// Return decode
if (!function_exists('bestdeals_deconvert')) {	
	function bestdeals_deconvert($str) {
		return base64_decode($str);
	}
}

/* Support for meta boxes
--------------------------------------------------- */
if (!function_exists('trx_utils_meta_box_add')) {
	add_action('add_meta_boxes', 'trx_utils_meta_box_add');
	function trx_utils_meta_box_add() {
		// Custom theme-specific meta-boxes
		$boxes = apply_filters('trx_utils_filter_override_options', array());
		if (is_array($boxes)) {
			foreach ($boxes as $box) {
				$box = array_merge(array('id' => '',
					'title' => '',
					'callback' => '',
					'page' => null,        // screen
					'context' => 'advanced',
					'priority' => 'default',
					'callbacks' => null
				),
					$box);
				add_meta_box($box['id'], $box['title'], $box['callback'], $box['page'], $box['context'], $box['priority'], $box['callbacks']);
			}
		}
	}
}

// Return text for the Privacy Policy checkbox
if (!function_exists('trx_utils_get_privacy_text')) {
	function trx_utils_get_privacy_text() {
		$page = get_option('wp_page_for_privacy_policy');
		return apply_filters( 'trx_utils_filter_privacy_text', wp_kses_post(
				__( 'I agree that my submitted data is being collected and stored.', 'bestdeals-utils' )
				. ( '' != $page
					// Translators: Add url to the Privacy Policy page
					? ' ' . sprintf(__('For further details on handling user data, see our %s', 'bestdeals-utils'),
						'<a href="' . esc_url(get_permalink($page)) . '" target="_blank">'
						. __('Privacy Policy', 'bestdeals-utils')
						. '</a>')
					: ''
				)
			)
		);
	}
}


// Prepare required styles and scripts for admin mode
if ( !function_exists( 'themerex_admin_prepare_scripts' ) ) {
    add_action("admin_head", 'themerex_admin_prepare_scripts');
    function themerex_admin_prepare_scripts() {
        ?>
        <script>
            if (typeof TRX_UTILS_GLOBALS == 'undefined') var TRX_UTILS_GLOBALS = {};
            jQuery(document).ready(function() {
                TRX_UTILS_GLOBALS['admin_mode']	= true;
                TRX_UTILS_GLOBALS['ajax_nonce'] = "<?php echo wp_create_nonce('ajax_nonce'); ?>";
                TRX_UTILS_GLOBALS['ajax_url']	= "<?php echo admin_url('admin-ajax.php'); ?>";
				TRX_UTILS_GLOBALS['ajax_error']   = "<?php _e('Ajax Error!!!', EG_TEXTDOMAIN); ?>";
                TRX_UTILS_GLOBALS['user_logged_in'] = true;
				TRX_UTILS_GLOBALS['msg_importer_full_alert'] = "<?php
					echo esc_html__("ATTENTION!\\n\\nIn this case ALL THE OLD DATA WILL BE ERASED\\nand YOU WILL GET A NEW SET OF POSTS, pages and menu items.", 'bestdeals-utils')
						. "\\n\\n"
						. esc_html__("It is strongly recommended only for new installations of WordPress\\n(without posts, pages and any other data)!", 'bestdeals-utils')
						. "\\n\\n"
						. esc_html__("Press OK to continue or Cancel to return to a partial installation", 'bestdeals-utils');
					?>";
            });
        </script>
        <?php
    }
}


// File functions
if (file_exists(TRX_UTILS_PLUGIN_DIR . 'includes/plugin.files.php')) {
    require_once TRX_UTILS_PLUGIN_DIR . 'includes/plugin.files.php';
}

// Third-party plugins support
if (file_exists(TRX_UTILS_PLUGIN_DIR . 'api/api.php')) {
    require_once TRX_UTILS_PLUGIN_DIR . 'api/api.php';
}


// Demo data import/export
if (file_exists(TRX_UTILS_PLUGIN_DIR . 'importer/importer.php')) {
    require_once TRX_UTILS_PLUGIN_DIR . 'importer/importer.php';
}


// LESS function
/*
if (!function_exists('bestdeals_less_func_replace')) {	
	function bestdeals_less_func_replace($arg) {
    	return $arg;
	}
}
*/


/**
 *
 */


if (is_admin()) {
	require_once( trx_utils_get_file_dir('includes/tools/emailer/emailer.php') );
	require_once( trx_utils_get_file_dir('includes/tools/po_composer/po_composer.php') );
}

// Shortcodes init
if (!function_exists('trx_utils_sc_init')) {
	add_action( 'bestdeals_setup_widgets', 'trx_utils_sc_init', 11 );
	function trx_utils_sc_init() {
		global $TRX_UTILS_STORAGE;
		$TRX_UTILS_STORAGE['plugin_active'] = apply_filters('trx_utils_active', $TRX_UTILS_STORAGE['plugin_active']);
		if ( !($TRX_UTILS_STORAGE['plugin_active']) ) return;

		if (file_exists(TRX_UTILS_PLUGIN_DIR . 'includes/core.socials.php')) {
			require_once trx_utils_get_file_dir('includes/core.socials.php');
		}

		if (file_exists(TRX_UTILS_PLUGIN_DIR . 'includes/custom_shortcodes.php')) {
			require_once trx_utils_get_file_dir('includes/custom_shortcodes.php');
		}
		// Include shortcodes
//		trx_utils_autoload_folder('includes/core.shortcodes');


		require_once trx_utils_get_file_dir('includes/core.shortcodes/core.shortcodes.php');
		require_once trx_utils_get_file_dir('includes/core.shortcodes/shortcodes_settings.php');
		require_once trx_utils_get_file_dir('includes/core.shortcodes/shortcodes.php');
		if (class_exists('Vc_Manager')) {
			require_once( trx_utils_get_file_dir('includes/core.shortcodes/shortcodes_vc.php') );
		}

		
	}
}


// Widgets init
if (!function_exists('trx_utils_setup_widgets')) {
	add_action( 'bestdeals_setup_widgets', 'trx_utils_setup_widgets', 9 );
	function trx_utils_setup_widgets() {
		global $TRX_UTILS_STORAGE;
		$TRX_UTILS_STORAGE['plugin_active'] = apply_filters('trx_utils_active', $TRX_UTILS_STORAGE['plugin_active']);
		if ( !($TRX_UTILS_STORAGE['plugin_active']) ) return;

		// Include widgets
		trx_utils_autoload_folder('includes/widgets');
	}
}

?>