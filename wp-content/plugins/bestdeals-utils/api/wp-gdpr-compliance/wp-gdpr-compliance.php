<?php
/**
 * Plugin support: WP GDPR Compliance
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.5
 */

// Don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Check if plugin is installed and activated
if ( !function_exists( 'trx_utils_exists_wp_gdpr_compliance' ) ) {
	function trx_utils_exists_wp_gdpr_compliance() {
		return class_exists( 'WPGDPRC\WPGDPRC' );
	}
}


// One-click import support
//------------------------------------------------------------------------

// Check plugin in the required plugins
if ( !function_exists( 'trx_utils_wp_gdpr_compliance_importer_required_plugins' ) ) {
	if (is_admin()) add_filter( 'trx_utils_filter_importer_required_plugins',	'trx_utils_wp_gdpr_compliance_importer_required_plugins', 10, 2 );
	function trx_utils_wp_gdpr_compliance_importer_required_plugins($not_installed='', $list='') {
		if (strpos($list, 'wp-gdpr-compliance')!==false && !trx_utils_exists_wp_gdpr_compliance() )
			$not_installed .= '<br>' . esc_html__('WP GDPR Compliance', 'bestdeals-utils');
		return $not_installed;
	}
}

// Set plugin's specific importer options
if ( !function_exists( 'trx_utils_wp_gdpr_compliance_importer_set_options' ) ) {
	if (is_admin()) add_filter( 'trx_utils_filter_importer_options', 'trx_utils_wp_gdpr_compliance_importer_set_options', 10, 1 );
	function trx_utils_wp_gdpr_compliance_importer_set_options($options=array()) {
		if ( trx_utils_exists_wp_gdpr_compliance() && in_array('wp-gdpr-compliance', $options['required_plugins']) ) {
			if (is_array($options)) {
				$options['additional_options'][] = 'wpgdprc_%';
			}
		}
		return $options;
	}
}
?>