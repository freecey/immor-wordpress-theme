<?php
/**
 * File system manipulations
 *
 * @package WordPress
 * @subpackage ThemeREX Utilities
 * @since v3.0
 */

// Don't load directly
if ( ! defined( 'TRX_UTILS_VERSION' ) ) {
	die( '-1' );
}


// Return current site protocol
if (!function_exists('trx_utils_get_protocol')) {
    function trx_utils_get_protocol() {
        return is_ssl() ? 'https' : 'http';
    }
}

// Check value for "on" | "off" | "inherit" values
if (!function_exists('trx_utils_get_theme_option')) {
    function trx_utils_get_theme_option($prm) {
        if (function_exists('themerex_get_theme_option')) {
            return themerex_get_theme_option($prm);
        }
        if (function_exists('axiom_get_theme_option')) {
            return axiom_get_theme_option($prm);
        }
        if (function_exists('whiterabbit_get_theme_option')) {
            return whiterabbit_get_theme_option($prm);
        }
        return null;
    }
}

// Check value for "on" | "off" | "inherit" values
if (!function_exists('trx_utils_is_on')) {
    function trx_utils_is_on($prm) {
        return $prm>0 || in_array(strtolower($prm), array('true', 'on', 'yes', 'show'));
    }
}
if (!function_exists('trx_utils_is_off')) {
    function trx_utils_is_off($prm) {
        return empty($prm) || $prm===0 || in_array(strtolower($prm), array('false', 'off', 'no', 'none', 'hide'));
    }
}

/* Check if file/folder present in the child theme and return path (url) to it. 
   Else - path (url) to file in the main theme dir
------------------------------------------------------------------------------------- */

// Autoload templates, widgets, etc.
// Scan subfolders and require() file with same name in each folder
if (!function_exists('trx_utils_autoload_folder')) {
	function trx_utils_autoload_folder($folder, $from_subfolders=true) {
		if ($folder[0]==DIRECTORY_SEPARATOR) $folder = substr($folder, 1);;
		$theme_dir = get_template_directory();
		$child_dir = get_stylesheet_directory();
		$dirs = array(
			($child_dir).DIRECTORY_SEPARATOR. TRX_UTILS_PLUGIN_BASE .DIRECTORY_SEPARATOR.($folder),
			($theme_dir).DIRECTORY_SEPARATOR. TRX_UTILS_PLUGIN_BASE .DIRECTORY_SEPARATOR.($folder),
			TRX_UTILS_PLUGIN_DIR . ($folder)
		);
		$loaded = array();
		foreach($dirs as $dir) {
			if ( is_dir($dir) ) {
				$hdir = @opendir( $dir );
				if ( $hdir ) {
					while ( ($file = readdir($hdir)) !== false ) {
						if (substr($file, 0, 1) == '.' || in_array($file, $loaded))
							continue;
						if ( is_dir( ($dir) . DIRECTORY_SEPARATOR . ($file) ) ) {
							if ($from_subfolders && file_exists( ($dir) . DIRECTORY_SEPARATOR . ($file) . DIRECTORY_SEPARATOR . ($file) . '.php' ) ) {
								$loaded[] = $file;
								require_once( ($dir) . DIRECTORY_SEPARATOR . ($file) . DIRECTORY_SEPARATOR . ($file) . '.php' );
							}
						} else {
							$loaded[] = $file;
							if (strpos( $file,'.php') !== false) {
								require_once( ($dir) . DIRECTORY_SEPARATOR . ($file) );
							}
						}
					}
					@closedir( $hdir );
				}
			}
		}
	}
}


if (!function_exists('trx_utils_get_file_dir')) {	
	function trx_utils_get_file_dir($file, $return_url=false) {
		if ($file[0]==DIRECTORY_SEPARATOR) $file = substr($file, 1);
		$theme_dir = get_template_directory().DIRECTORY_SEPARATOR.TRX_UTILS_PLUGIN_BASE;
		$theme_url = get_template_directory_uri().DIRECTORY_SEPARATOR.TRX_UTILS_PLUGIN_BASE;
		$child_dir = get_stylesheet_directory().DIRECTORY_SEPARATOR.TRX_UTILS_PLUGIN_BASE;
		$child_url = get_stylesheet_directory_uri().DIRECTORY_SEPARATOR.TRX_UTILS_PLUGIN_BASE;
		$dir = '';
		if (file_exists(($child_dir).($file)))
			$dir = ($return_url ? $child_url : $child_dir).($file);
		else if (file_exists(($theme_dir).($file)))
			$dir = ($return_url ? $theme_url : $theme_dir).($file);
		else if (file_exists((TRX_UTILS_PLUGIN_DIR . $file)))
			$dir = ($return_url ? TRX_UTILS_PLUGIN_URL : TRX_UTILS_PLUGIN_DIR).($file);
		return $dir;
	}
}

if (!function_exists('trx_utils_get_file_url')) {	
	function trx_utils_get_file_url($file) {
		return trx_utils_get_file_dir($file, true);
	}
}

// Return file extension from full name/path
if (!function_exists('trx_utils_get_file_ext')) {	
	function trx_utils_get_file_ext($file) {
		$parts = pathinfo($file);
		return $parts['extension'];
	}
}


// Get domain part from URL
if (!function_exists('trx_utils_get_domain_from_url')) {
	function trx_utils_get_domain_from_url($url) {
		if (($pos=strpos($url, '://'))!==false) $url = substr($url, $pos+3);
		if (($pos=strpos($url, DIRECTORY_SEPARATOR))!==false) $url = substr($url, 0, $pos);
		return $url;
	}
}

// Detect folder location (in the child theme or in the main theme)
if (!function_exists('trx_utils_get_folder_dir')) {	
	function trx_utils_get_folder_dir($folder, $return_url=false) {
		if ($folder[0]==DIRECTORY_SEPARATOR) $folder = substr($folder, 1);
		$theme_dir = get_template_directory();
		$theme_url = get_template_directory_uri();
		$child_dir = get_stylesheet_directory();
		$child_url = get_stylesheet_directory_uri();
		$dir = '';
		if (is_dir(($child_dir).DIRECTORY_SEPARATOR.($folder)))
			$dir = ($return_url ? $child_url : $child_dir).DIRECTORY_SEPARATOR.($folder);
		else if (is_dir(($theme_dir).'/'.($folder)))
			$dir = ($return_url ? $theme_url : $theme_dir).'/'.($folder);
		else if (is_dir((TRX_UTILS_PLUGIN_DIR).($folder)))
			$dir = ($return_url ? TRX_UTILS_PLUGIN_URL : TRX_UTILS_PLUGIN_DIR).($folder);
		return $dir;
	}
}

if (!function_exists('trx_utils_get_folder_url')) {	
	function trx_utils_get_folder_url($folder) {
		return trx_utils_get_folder_dir($folder, true);
	}
}

// Return list files in the folder
if (!function_exists('trx_utils_get_folder_list')) {	
	function trx_utils_get_folder_list($folder, $ext='', $only_names=false) {
		$dir = trx_utils_get_folder_dir($folder);
		$url = trx_utils_get_folder_url($folder);
		$list = array();
		if ( is_dir($dir) ) {
			$files = @glob(sprintf("%s/%s", $dir, $ext ? "*.{$ext}" : '*.*'));
			if ( is_array($files) ) {
				foreach ($files as $file) {
					if ( substr($file, 0, 1) == '.' || is_dir( $file ) )
						continue;
					$file = basename($file);
					$key = substr($file, 0, strrpos($file, '.'));
					if (substr($key, -4)=='.min') $key = substr($file, 0, strrpos($key, '.'));
					$list[$key] = $only_names ? ucfirst(str_replace('_', ' ', $key)) : ($url) . DIRECTORY_SEPARATOR . ($file);
				}
			}
		}
		return $list;
	}
}



/* CSS & JS minify
-------------------------------------------------------------------------------- */

// Minify CSS string
if (!function_exists('trx_utils_minify_css')) {
	function trx_utils_minify_css($css) {
		$css = preg_replace("/\r*\n*/", "", $css);
		$css = preg_replace("/\s{2,}/", " ", $css);
        //$css = str_ireplace('@CHARSET "UTF-8";', "", $css);
		$css = preg_replace("/\s*>\s*/", ">", $css);
		$css = preg_replace("/\s*:\s*/", ":", $css);
		$css = preg_replace("/\s*{\s*/", "{", $css);
		$css = preg_replace("/\s*;*\s*}\s*/", "}", $css);
        $css = str_replace(', ', ',', $css);
        $css = preg_replace("/(\/\*[\w\'\s\r\n\*\+\,\"\-\.]*\*\/)/", "", $css);
        return $css;
	}
}

// Minify JS string
if (!function_exists('trx_utils_minify_js')) {
	function trx_utils_minify_js($js) {
		// Remove multi-row comments
		//$js = preg_replace('/(\/\*)[^(\*\/)]*(\*\/)/', '', $js);
		$pos = 0;
		while (($pos = strpos($js, '/*', $pos))!==false) {
			if (($pos2 = strpos($js, '*/', $pos))!==false)
				$js = substr($js, 0, $pos) . substr($js, $pos2+2);
			else
				break;
		}
		// Remove single-line comments
		//$js = preg_replace('/\s*\/\/[^\n]*\n/', '', $js);
		$pos = -1;
		while (($pos = strpos($js, '//', $pos+1))!==false) {
			if ($js[$pos-1]!='\\' && $js[$pos-1]!=':') {
				$pos2 = strpos($js, "\n", $pos);
				if ($pos2==false) $pos2 = strlen($js);
				$js = substr($js, 0, $pos) . substr($js, $pos2);
			}
		}
		// Remove spaces before/after {}()
		$js = preg_replace('/\s+/', ' ', $js);
		$js = preg_replace('/([;}{\)\(])\s+/', '$1 ', $js);
		$js = preg_replace('/\s+([;}{\)\(])/', ' $1', $js);
		$js = preg_replace('/(else)\s+/', '$1 ', $js);
//		$js = preg_replace('/([}])\s+(else)/', '$1else', $js);
//		$js = preg_replace('/([}])\s+(var)/', '$1;var', $js);
//		$js = preg_replace('/([{};])\s+(\$)/', '$1\$', $js);
		return $js;
	}
}


/* Init WP Filesystem before the plugins and theme init
------------------------------------------------------------------- */
if (!function_exists('trx_utils_init_filesystem')) {
	add_action( 'after_setup_theme', 'trx_utils_init_filesystem', 0);
	function trx_utils_init_filesystem() {
        if( !function_exists('WP_Filesystem') ) {
            require_once( ABSPATH .'/wp-admin/includes/file.php' );
        }
		if (is_admin()) {
			$url = admin_url();
			$creds = false;
			// First attempt to get credentials.
			if ( function_exists('request_filesystem_credentials') && false === ( $creds = request_filesystem_credentials( $url, '', false, false, array() ) ) ) {
				// If we comes here - we don't have credentials
				// so the request for them is displaying no need for further processing
				return false;
			}
	
			// Now we got some credentials - try to use them.
			if ( !WP_Filesystem( $creds ) ) {
				// Incorrect connection data - ask for credentials again, now with error message.
				if ( function_exists('request_filesystem_credentials') ) request_filesystem_credentials( $url, '', true, false );
				return false;
			}
			
			return true; // Filesystem object successfully initiated.
		} else {
            WP_Filesystem();
		}
		return true;
	}
}


// Put data into specified file
if (!function_exists('trx_utils_fpc')) {	
	function trx_utils_fpc($file, $data, $flag=0) {
		global $wp_filesystem;
		if (!empty($file)) {
			if (isset($wp_filesystem) && is_object($wp_filesystem)) {
				$file = str_replace(ABSPATH, $wp_filesystem->abspath(), $file);
				// Attention! WP_Filesystem can't append the content to the file!
				// That's why we have to read the contents of the file into a string,
				// add new content to this string and re-write it to the file if parameter $flag == FILE_APPEND!
				return $wp_filesystem->put_contents($file, ($flag==FILE_APPEND ? $wp_filesystem->get_contents($file) : '') . $data, false);
			} else {
				if (WP_DEBUG)
					throw new Exception(sprintf(esc_html__('WP Filesystem is not initialized! Put contents to the file "%s" failed', 'bestdeals-utils'), $file));
			}
		}
		return false;
	}
}

// Get text from specified file
if (!function_exists('trx_utils_fgc')) {	
	function trx_utils_fgc($file, $unpack=false) {
		global $wp_filesystem;
		if (!empty($file)) {
			if (isset($wp_filesystem) && is_object($wp_filesystem)) {
				$file = str_replace(ABSPATH, $wp_filesystem->abspath(), $file);
				if ($unpack && trx_utils_get_file_ext($file) == 'zip') {
					$tmp_cont = $wp_filesystem->get_contents($file);
					$tmp_name = 'tmp-'.rand().'.zip';
					$tmp = wp_upload_bits($tmp_name, null, $tmp_cont);
					if ($tmp['error'])
						$tmp_cont = '';
					else {
						unzip_file($tmp['file'], dirname($tmp['file']));
						$file_name = dirname($tmp['file']) . DIRECTORY_SEPARATOR . basename($file, '.zip') . '.txt';
						$tmp_cont = trx_utils_fgc($file_name);
						unlink($tmp['file']);
						unlink($file_name);
					}
					return $tmp_cont;
				} else {
					return $wp_filesystem->get_contents($file);
				}
			} else {
				if (WP_DEBUG)
					throw new Exception(sprintf(esc_html__('WP Filesystem is not initialized! Get contents from the file "%s" failed', 'bestdeals-utils'), $file));
			}
		}
		return '';
	}
}

// Get array with rows from specified file
if (!function_exists('trx_utils_fga')) {	
	function trx_utils_fga($file) {
		global $wp_filesystem;
		if (!empty($file)) {
			if (isset($wp_filesystem) && is_object($wp_filesystem)) {
				$file = str_replace(ABSPATH, $wp_filesystem->abspath(), $file);
				return $wp_filesystem->get_contents_array($file);
			} else {
				if (WP_DEBUG)
					throw new Exception(sprintf(esc_html__('WP Filesystem is not initialized! Get rows from the file "%s" failed', 'bestdeals-utils'), $file));
			}
		}
		return array();
	}
}


// Unserialize string (try replace \n with \r\n)
if (!function_exists('trx_utils_unserialize')) {
    function trx_utils_unserialize($str) {
        if ( !empty($str) && is_serialized($str) ) {
            try {
                $data = unserialize($str);
            } catch (Exception $e) {
                dcl($e->getMessage());
                $data = false;
            }
            if ($data===false) {
                try {
                    $data = @unserialize(str_replace("\n", "\r\n", $str));
                } catch (Exception $e) {
                    dcl($e->getMessage());
                    $data = false;
                }
            }
            //if ($data===false) $data = @unserialize(str_replace(array("\n", "\r"), array('\\n','\\r'), $str));
            return $data;
        } else
            return $str;
    }
}


// Return first key (by default) or value from associative array
if (!function_exists('trx_utils_array_get_first')) {
    function trx_utils_array_get_first(&$arr, $key=true) {
        foreach ($arr as $k=>$v) break;
        return $key ? $k : $v;
    }
}


?>