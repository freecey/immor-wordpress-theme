<?php
/**
 * BestDEALS Framework: social networks
 *
 * @package	bestdeals
 * @since	bestdeals 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('bestdeals_socials_theme_setup')) {
	add_action( 'bestdeals_action_before_init_theme', 'bestdeals_socials_theme_setup' );
	function bestdeals_socials_theme_setup() {

		if ( !is_admin() ) {
			// Add og:image meta tag for facebook
			add_action( 'wp_head', 					'bestdeals_facebook_og_tags', 5 );
		} else {
            add_action( 'show_user_profile',		'bestdeals_add_fields_in_user_profile' );
            add_action( 'edit_user_profile',		'bestdeals_add_fields_in_user_profile' );

            // Save / update additional fields from profile
            add_action( 'personal_options_update',	'bestdeals_save_fields_in_user_profile' );
            add_action( 'edit_user_profile_update',	'bestdeals_save_fields_in_user_profile' );
        }
	}
}

/* Social Share and Profile links
-------------------------------------------------------------------------------- */

if (!function_exists('bestdeals_socials_theme_setup1')) {
	add_action('bestdeals_action_before_init_theme', 'bestdeals_socials_theme_setup1', 1);
	function bestdeals_socials_theme_setup1() {
// List of social networks for site sharing and user profiles
		$BESTDEALS_GLOBALS['share_links'] = array(
			'blogger' =>		'http://www.blogger.com/blog_this.pyra?t&u={link}&n={title}',
			'bobrdobr' =>		'http://bobrdobr.ru/add.html?url={link}&title={title}&desc={descr}',
			'delicious' =>		'http://delicious.com/save?url={link}&title={title}&note={descr}',
			'designbump' =>		'http://designbump.com/node/add/drigg/?url={link}&title={title}',
			'designfloat' =>	'http://www.designfloat.com/submit.php?url={link}',
			'digg' =>			'http://digg.com/submit?url={link}',
			'evernote' =>		'https://www.evernote.com/clip.action?url={link}&title={title}',
			'facebook' =>		'http://www.facebook.com/sharer.php?s=100&p[url]={link}&p[title]={title}&p[summary]={descr}&p[images][0]={image}',
			'friendfeed' =>		'http://www.friendfeed.com/share?title={title} - {link}',
			'google' =>			'http://www.google.com/bookmarks/mark?op=edit&output=popup&bkmk={link}&title={title}&annotation={descr}',
			'gplus' => 			'https://plus.google.com/share?url={link}',
			'identi' => 		'http://identi.ca/notice/new?status_textarea={title} - {link}',
			'juick' => 			'http://www.juick.com/post?body={title} - {link}',
			'linkedin' => 		'http://www.linkedin.com/shareArticle?mini=true&url={link}&title={title}',
			'liveinternet' =>	'http://www.liveinternet.ru/journal_post.php?action=n_add&cnurl={link}&cntitle={title}',
			'livejournal' =>	'http://www.livejournal.com/update.bml?event={link}&subject={title}',
			'mail' =>			'http://connect.mail.ru/share?url={link}&title={title}&description={descr}&imageurl={image}',
			'memori' =>			'http://memori.ru/link/?sm=1&u_data[url]={link}&u_data[name]={title}',
			'mister-wong' =>	'http://www.mister-wong.ru/index.php?action=addurl&bm_url={link}&bm_description={title}',
			'mixx' =>			'http://chime.in/chimebutton/compose/?utm_source=bookmarklet&utm_medium=compose&utm_campaign=chime&chime[url]={link}&chime[title]={title}&chime[body]={descr}',
			'moykrug' =>		'http://share.yandex.ru/go.xml?service=moikrug&url={link}&title={title}&description={descr}',
			'myspace' =>		'http://www.myspace.com/Modules/PostTo/Pages/?u={link}&t={title}&c={descr}',
			'newsvine' =>		'http://www.newsvine.com/_tools/seed&save?u={link}&h={title}',
			'odnoklassniki' =>	'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl={link}&title={title}',
			'pikabu' =>			'http://pikabu.ru/add_story.php?story_url={link}',
			'pinterest' =>		'json:{"link": "http://pinterest.com/pin/create/button/", "script": "//assets.pinterest.com/js/pinit.js", "style": "", "attributes": {"data-pin-do": "buttonPin", "data-pin-media": "{image}", "data-pin-url": "{link}", "data-pin-description": "{title}", "data-pin-custom": "true","nopopup": "true"}}',
			'posterous' =>		'http://posterous.com/share?linkto={link}&title={title}',
			'postila' =>		'http://postila.ru/publish/?url={link}&agregator=bestdeals',
			'reddit' =>			'http://reddit.com/submit?url={link}&title={title}',
			'rutvit' =>			'http://rutvit.ru/tools/widgets/share/popup?url={link}&title={title}',
			'stumbleupon' =>	'http://www.stumbleupon.com/submit?url={link}&title={title}',
			'surfingbird' =>	'http://surfingbird.ru/share?url={link}',
			'technorati' =>		'http://technorati.com/faves?add={link}&title={title}',
			'tumblr' =>			'http://www.tumblr.com/share?v=3&u={link}&t={title}&s={descr}',
			'twitter' =>		'https://twitter.com/intent/tweet?text={title}&url={link}',
			'vk' =>				'http://vk.com/share.php?url={link}&title={title}&description={descr}',
			'vk2' =>			'http://vk.com/share.php?url={link}&title={title}&description={descr}',
			'webdiscover' =>	'http://webdiscover.ru/share.php?url={link}',
			'yahoo' =>			'http://bookmarks.yahoo.com/toolbar/savebm?u={link}&t={title}&d={descr}',
			'yandex' =>			'http://zakladki.yandex.ru/newlink.xml?url={link}&name={title}&descr={descr}',
			'ya' =>				'http://my.ya.ru/posts_add_link.xml?URL={link}&title={title}&body={descr}',
			'yosmi' =>			'http://yosmi.ru/index.php?do=share&url={link}'
		);
	}
}

// Add social network
// Example: 1) add_share_link('pinterest', 'url');
//			2) add_share_link(array('pinterest'=>'url', 'dribble'=>'url'));
if (!function_exists('bestdeals_add_share_link')) {
	function bestdeals_add_share_link($soc, $url='') {
		if (!is_array($soc)) $soc = array($soc => $url);
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['share_links'] = array_merge( $BESTDEALS_GLOBALS['share_links'], $soc );
	}
}



// Return (and show) share social links
if (!function_exists('bestdeals_show_share_links')) {
	function bestdeals_show_share_links($args) {

		$args = array_merge(array(
			'post_id' => 0,                        // post ID
			'post_link' => '',                    // post link
			'post_title' => '',                    // post title
			'post_descr' => '',                    // post descr
			'post_thumb' => '',                    // post featured image
			'size' => 'tiny',                    // icons size: tiny|small|medium|big
			'style' => bestdeals_get_theme_setting('socials_type')=='images' ? 'bg' : 'icons',	// style for show icons: icons|images|bg
			'type' => 'block',					// share block type: list|block|drop
			'popup' => true,					// open share url in new window or in popup window
			'counters' => bestdeals_get_custom_option('show_share_counters')=='yes',	// show share counters
			'direction' => bestdeals_get_custom_option('show_share'),				// share block direction
			'caption' => bestdeals_get_custom_option('share_caption'),				// share block caption
			'share' => bestdeals_get_theme_option('share_buttons'),					// list of allowed socials
			'echo' => true,						// if true - show on page, else - only return as string
			'before' => '',                        // HTML-code before the share links
			'after' => '',                        // HTML-code after the share links
		), $args);


		if (empty($args['post_id']))    $args['post_id'] = get_the_ID();
		if (empty($args['post_link']))    $args['post_link'] = get_permalink();
		if (empty($args['post_title']))    $args['post_title'] = get_the_title();
		if (empty($args['post_descr']))    $args['post_descr'] = strip_tags(get_the_excerpt());

		$output = '';

		$upload_info = wp_upload_dir();
		$upload_url = $upload_info['baseurl'];
		if (count($args['share'])==0 || implode('', $args['share'][0])=='') return '';


		if (is_array($args['share']) && count($args['share']) > 0) {
			foreach ($args['share'] as $social) {
				$sn = $social['name'];
				$fn = $args['style']=='icons' || bestdeals_strpos($social['icon'], $upload_url)!==false ? $social['icon'] : bestdeals_get_socials_url(basename($social['icon']));
				if ($args['style'] == 'icons') {
					$parts = explode('-', $social['icon'], 2);
					$cl = isset($parts[1]) ? $parts[1] : $parts[0];
				} else {
					$cl = basename($social['icon']);
					$cl = bestdeals_substr($cl, 0, bestdeals_strrpos($cl, '.'));
					if (($pos=bestdeals_strrpos($cl, '_'))!==false)
						$cl = bestdeals_substr($cl, 0, $pos);
				}

				$title = !empty($social['title']) ? $social['title'] : ucfirst($fn);
				$url = $social['url'];
				if (substr($url, 0, 5) == 'json:') {
					$url = json_decode(substr($url, 5), true);
					if (is_null($url))
						continue;
				} else {
					$url = array('link' => $url);
				}

				if (!isset($url['attributes']))
					$url['attributes'] = array();
				$url['attributes']['href'] = $url['link'];
				$email = strpos($url['link'], 'mailto:')!==false;
				$popup = !empty($args['popup']) && !$email && empty($url['attributes']['nopopup']);
				if (!empty($popup))
					$url['attributes']['data-link'] = esc_url($url['link']);
				else
					$url['attributes']['target'] = '_blank';
				if ($args['counters'])
					$url['attributes']['data-count'] = $fn;
				$output .= '<div class="sc_socials_item">'
						. '<a class="social_icons social_icons_a social_'. esc_attr( $cl ) .'"';
				foreach($url['attributes'] as $k=>$v) {
					$v = str_replace(
						array('{id}', '{link}', '{title}', '{descr}', '{image}'),
						array(
							$k=='href' ? urlencode($args['post_id']) : $args['post_id'],
							$k=='href' ? urlencode($args['post_link']) : $args['post_link'],
							$k=='href' && !$email ? urlencode(strip_tags($args['post_title'])) : strip_tags($args['post_title']),
							$k=='href' && !$email ? urlencode(strip_tags($args['post_descr'])) : strip_tags($args['post_descr']),
							$k=='href' ? urlencode($args['post_thumb']) : $args['post_thumb']
						),
						$v);
					$output .= " {$k}=\"" . ($k=='href' ? esc_url($v) : esc_attr($v)) . '"';
				}
				$output .= '>'
//					. '<span class="social_icon social_icon_'.esc_attr($fn).'"'
                    . '<span class="social_icon '.esc_attr($fn).' social_icon_share"'
					. ($args['style']=='bg' ? ' style="background-image: url('.esc_url($sn).');"' : '')
					. '>'
					. ($args['style']=='icons'
						? '<span class="' . esc_attr($sn) . '"></span>'
						: ($args['style']=='images'
							? '<img src="'.esc_url($sn).'" alt="'.esc_attr($title).'" />'
							: '<span class="social_hover" style="background-image: url('.esc_url($sn).');"></span>'
						)
					)
					//. ($args['counters'] ? '<span class="share_counter">0</span>' : '')
					. ($args['type']=='drop' ? '<i>' . trim($title) . '</i>' : '')
					. '</span>'
					. '</a>'
                    . '</div>';
			}
		}

		if (!empty($output)) {
			$output = $args['before']
				. '<div class="sc_socials sc_socials_size_'.esc_attr($args['size']).' sc_socials_share' . ($args['type']=='drop' ? ' sc_socials_drop' : ' sc_socials_dir_' . esc_attr($args['direction'])) . '">'
				. ($args['caption']!='' ? '<span class="share_caption">'.($args['caption']).'</span>' : '')
				. $output
				. '</div>'
				. $args['after'];
			if ($args['echo']) trx_utils_show_layout($output);
		}

		return $output;
	}
}




// OLD Way ( no Pinterest )Return (and show) share social links
if ( false && !function_exists('bestdeals_show_share_links')) {
	function bestdeals_show_share_links($args) {
		if ( bestdeals_get_custom_option('show_share')=='hide' ) return '';

		$args = array_merge(array(
			'post_id' => 0,						// post ID
			'post_link' => '',					// post link
			'post_title' => '',					// post title
			'post_descr' => '',					// post descr
			'post_thumb' => '',					// post featured image
			'size' => 'small',					// icons size: tiny|small|big
			'style' => bestdeals_get_theme_setting('socials_type')=='images' ? 'bg' : 'icons',	// style for show icons: icons|images|bg
			'type' => 'block',					// share block type: list|block|drop
			'popup' => true,					// open share url in new window or in popup window
			'counters' => bestdeals_get_custom_option('show_share_counters')=='yes',	// show share counters
			'direction' => bestdeals_get_custom_option('show_share'),				// share block direction
			'caption' => bestdeals_get_custom_option('share_caption'),				// share block caption
			'share' => bestdeals_get_theme_option('share_buttons'),					// list of allowed socials
			'echo' => true						// if true - show on page, else - only return as string
			), $args);

		if (count($args['share'])==0 || implode('', $args['share'][0])=='') return '';
		
		global $BESTDEALS_GLOBALS;

		$upload_info = wp_upload_dir();
		$upload_url = $upload_info['baseurl'];

		$output = '<div class="sc_socials sc_socials_size_'.esc_attr($args['size']).' sc_socials_share' . ($args['type']=='drop' ? ' sc_socials_drop' : ' sc_socials_dir_' . esc_attr($args['direction'])) . '">'
			. ($args['caption']!='' ? '<span class="share_caption">'.($args['caption']).'</span>' : '');

		if (is_array($args['share']) && count($args['share']) > 0) {
			foreach ($args['share'] as $soc) {
				$icon = $args['style']=='icons' || bestdeals_strpos($soc['icon'], $upload_url)!==false ? $soc['icon'] : bestdeals_get_socials_url(basename($soc['icon']));
				if ($args['style'] == 'icons') {
					$parts = explode('-', $soc['icon'], 2);
					$sn = isset($parts[1]) ? $parts[1] : $parts[0];
				} else {
					$sn = basename($soc['icon']);
					$sn = bestdeals_substr($sn, 0, bestdeals_strrpos($sn, '.'));
					if (($pos=bestdeals_strrpos($sn, '_'))!==false)
						$sn = bestdeals_substr($sn, 0, $pos);
				}
				$url = empty($soc['url']) && !empty($BESTDEALS_GLOBALS['share_links'][$sn]) ? $BESTDEALS_GLOBALS['share_links'][$sn] : $soc['url'];

				if (substr($url, 0, 5) == 'json:') {
					$url = json_decode(substr($url, 5), true);
					if (is_null($url))
						continue;
				} else {
					$url = array('link' => $url);
				}

				$link = str_replace(
					array('{id}', '{link}', '{title}', '{descr}', '{image}'),
					array(
						urlencode($args['post_id']),
						urlencode($args['post_link']),
						urlencode(strip_tags($args['post_title'])),
						urlencode(strip_tags($args['post_descr'])),
						urlencode($args['post_thumb'])
						),
					$url);
				$output .= '<div class="sc_socials_item">'
						. '<a href="'.esc_url($soc['url']).'"'
						. ' class="social_icons social_'.esc_attr($sn).'"'
						. ($args['popup'] ? ' onclick="window.open(\'' . esc_url($link) .'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=480, height=400, toolbar=0, status=0\'); return false;"' : ' target="_blank"')
						. ($args['style']=='bg' ? ' style="background-image: url('.esc_url($icon).');"' : '')
						. ($args['counters'] ? ' data-count="'.esc_attr($sn).'"' : '') 
					. '>'
					. ($args['style']=='icons' 
						? '<span class="' . esc_attr($soc['icon']) . '"></span>' 
						: ($args['style']=='images' 
							? '<img src="'.esc_url($icon).'" alt="'.esc_attr($sn).'" />' 
							: '<span class="sc_socials_hover" style="background-image: url('.esc_url($icon).');"></span>'
							)
						)
					. '</a>'
					//. ($args['counters'] ? '<span class="share_counter">0</span>' : '') 
					. ($args['type']=='drop' ? '<i>' . trim(bestdeals_strtoproper($sn)) . '</i>' : '')
					. '</div>';
			}
		}
		$output .= '</div>';
		if ($args['echo']) echo ($output);
		return $output;
	}
}


// Return social icons links
if (!function_exists('bestdeals_prepare_socials')) {
	function bestdeals_prepare_socials($list, $style='') {
		if (empty($style)) $style = bestdeals_get_theme_setting('socials_type')=='images' ? 'bg' : 'icons';
		$output = '';
		$upload_info = wp_upload_dir();
		$upload_url = $upload_info['baseurl'];
		if (is_array($list) && count($list) > 0) {
			foreach ($list as $soc) {
				if (empty($soc['url'])) continue;
				$icon = $style=='icons' || bestdeals_strpos($soc['icon'], $upload_url)!==false ? $soc['icon'] : bestdeals_get_socials_url(basename($soc['icon']));
				if ($style == 'icons') {
					$parts = explode('-', $soc['icon'], 2);
					$sn = isset($parts[1]) ? $parts[1] : $parts[0];
				} else {
					$sn = basename($soc['icon']);
					$sn = bestdeals_substr($sn, 0, bestdeals_strrpos($sn, '.'));
					if (($pos=bestdeals_strrpos($sn, '_'))!==false)
						$sn = bestdeals_substr($sn, 0, $pos);
				}
				$output .= '<div class="sc_socials_item">'
						. '<a href="'.esc_url($soc['url']).'" target="_blank" class="social_icons social_'.esc_attr($sn).'"'
						. ($style=='bg' ? ' style="background-image: url('.esc_url($icon).');"' : '')
						. '>'
						. ($style=='icons' 
							? '<span class="icon-' . esc_attr($sn) . '"></span>' 
							: ($style=='images' 
								? '<img src="'.esc_url($icon).'" alt="" />' 
								: '<span class="sc_socials_hover" style="background-image: url('.esc_url($icon).');"></span>'))
						. '</a>'
						. '</div>';
			}
		}
		return $output;
	}
}
	
	
/* Twitter
-------------------------------------------------------------------------------- */

if (!function_exists('bestdeals_get_twitter_data')) {
	function bestdeals_get_twitter_data($cfg) {
		return function_exists('bestdeals_twitter_acquire_data') 
				? bestdeals_twitter_acquire_data(array(
						'mode'            => 'user_timeline',
						'consumer_key'    => $cfg['consumer_key'],
						'consumer_secret' => $cfg['consumer_secret'],
						'token'           => $cfg['token'],
						'secret'          => $cfg['secret']
					))
				: '';
	}
}

if (!function_exists('bestdeals_prepare_twitter_text')) {
	function bestdeals_prepare_twitter_text($tweet) {
		$text = $tweet['text'];
		if (!empty($tweet['entities']['urls']) && count($tweet['entities']['urls']) > 0) {
			foreach ($tweet['entities']['urls'] as $url) {
				$text = str_replace($url['url'], '<a href="'.esc_url($url['expanded_url']).'" target="_blank">' . ($url['display_url']) . '</a>', $text);
			}
		}
		if (!empty($tweet['entities']['media']) && count($tweet['entities']['media']) > 0) {
			foreach ($tweet['entities']['media'] as $url) {
				$text = str_replace($url['url'], '<a href="'.esc_url($url['expanded_url']).'" target="_blank">' . ($url['display_url']) . '</a>', $text);
			}
		}
		return $text;
	}
}

// Return Twitter followers count
if (!function_exists('bestdeals_get_twitter_followers')) {
	function bestdeals_get_twitter_followers($cfg) {
		$data = bestdeals_get_twitter_data($cfg); 
		return $data && isset($data[0]['user']['followers_count']) ? $data[0]['user']['followers_count'] : 0;
	}
}



/* Facebook
-------------------------------------------------------------------------------- */

if (!function_exists('bestdeals_get_facebook_likes')) {
	function bestdeals_get_facebook_likes($account) {
		$fb = get_transient("facebooklikes");
		if ($fb !== false) return $fb;
		$fb = '?';
		$url = esc_url(bestdeals_get_protocol().'http://graph.facebook.com/'.($account));
		$headers = get_headers($url);
		if (bestdeals_strpos($headers[0], '200')) {
			$json = bestdeals_fgc($url);
			$rez = json_decode($json, true);
			if (isset($rez['likes']) ) {
				$fb = $rez['likes'];
				set_transient("facebooklikes", $fb, 60*60);
			}
		}
		return $fb;
	}
}


// Add facebook meta tags for post/page sharing
function bestdeals_facebook_og_tags() {
	global $post;
	if ( !is_singular() || bestdeals_get_global('blog_streampage')) return;
	if (has_post_thumbnail( $post->ID )) {
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>' . "\n";
	}
	//echo '<meta property="og:title" content="' . esc_attr( strip_tags( get_the_title() ) ) . '" />' . "\n"
	//	.'<meta property="og:description" content="' . esc_attr( strip_tags( strip_shortcodes( get_the_excerpt()) ) ) . '" />' . "\n"
	//	.'<meta property="og:url" content="' . esc_attr( get_permalink() ) . '" />';
}

// Show additional fields in the user profile
if (!function_exists('bestdeals_add_fields_in_user_profile')) {
    function bestdeals_add_fields_in_user_profile( $user ) {
        ?>
        <h3><?php esc_html_e('User Position', 'bestdeals'); ?></h3>
        <table class="form-table">
            <tr>
                <th><label for="user_position"><?php esc_html_e('User position', 'bestdeals'); ?>:</label></th>
                <td><input type="text" name="user_position" id="user_position" size="55" value="<?php echo esc_attr(get_the_author_meta('user_position', $user->ID)); ?>" />
                    <span class="description"><?php esc_html_e('Please, enter your position in the company', 'bestdeals'); ?></span>
                </td>
            </tr>
        </table>

        <h3><?php esc_html_e('Social links', 'bestdeals'); ?></h3>
        <table class="form-table">
            <?php
            $socials_type = bestdeals_get_theme_setting('socials_type');
            $social_list = bestdeals_get_theme_option('social_icons');
            if (is_array($social_list) && count($social_list) > 0) {
                foreach ($social_list as $soc) {
                    if ($socials_type == 'icons') {
                        $parts = explode('-', $soc['icon'], 2);
                        $sn = isset($parts[1]) ? $parts[1] : $sn;
                    } else {
                        $sn = basename($soc['icon']);
                        $sn = bestdeals_substr($sn, 0, bestdeals_strrpos($sn, '.'));
                        if (($pos=bestdeals_strrpos($sn, '_'))!==false)
                            $sn = bestdeals_substr($sn, 0, $pos);
                    }
                    if (!empty($sn)) {
                        ?>
                        <tr>
                            <th><label for="user_<?php echo esc_attr($sn); ?>"><?php bestdeals_show_layout(bestdeals_strtoproper($sn)); ?>:</label></th>
                            <td><input type="text" name="user_<?php echo esc_attr($sn); ?>" id="user_<?php echo esc_attr($sn); ?>" size="55" value="<?php echo esc_attr(get_the_author_meta('user_'.($sn), $user->ID)); ?>" />
                                <span class="description"><?php echo sprintf(esc_attr__('Please, enter your %s link', 'bestdeals'), bestdeals_strtoproper($sn)); ?></span>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
        </table>
        <?php
    }
}

// Save / update additional fields
if (!function_exists('bestdeals_save_fields_in_user_profile')) {
    function bestdeals_save_fields_in_user_profile( $user_id ) {
        if ( !current_user_can( 'edit_user', $user_id ) )
            return false;
        update_user_meta( $user_id, 'user_position', $_POST['user_position'] );
        $socials_type = bestdeals_get_theme_setting('socials_type');
        $social_list = bestdeals_get_theme_option('social_icons');
        if (is_array($social_list) && count($social_list) > 0) {
            foreach ($social_list as $soc) {
                if ($socials_type == 'icons') {
                    $parts = explode('-', $soc['icon'], 2);
                    $sn = isset($parts[1]) ? $parts[1] : $sn;
                } else {
                    $sn = basename($soc['icon']);
                    $sn = bestdeals_substr($sn, 0, bestdeals_strrpos($sn, '.'));
                    if (($pos=bestdeals_strrpos($sn, '_'))!==false)
                        $sn = bestdeals_substr($sn, 0, $pos);
                }
                update_user_meta( $user_id, 'user_'.($sn), $_POST['user_'.($sn)] );
            }
        }
    }
}


/* Feedburner
-------------------------------------------------------------------------------- */

if (!function_exists('bestdeals_get_feedburner_counter')) {
	function bestdeals_get_feedburner_counter($account) {
		$rss = get_transient("feedburnercounter");
		if ($rss !== false) return $rss;
		$rss = '?';
		$url = esc_url(bestdeals_get_protocol().'://feedburner.google.com/api/awareness/1.0/GetFeedData?uri='.($account));
		$headers = get_headers($url);
		if (bestdeals_strpos($headers[0], '200')) {
			$xml = bestdeals_fgc($url);
			preg_match('/circulation="(\d+)"/', $xml, $match);
			if ($match[1] != 0) {
				$rss = $match[1];
				set_transient("feedburnercounter", $rss, 60*60);
			}
		}
		return $rss;
	}
}
?>