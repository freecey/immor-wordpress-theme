<?php
/*
 * Support for the Courses and Lessons
 */



// Register custom post type
if (!function_exists('trx_utils_support_courses_post_type')) {
	add_action( 'trx_utils_custom_post_type', 'trx_utils_support_courses_post_type', 10, 2 );
	function trx_utils_support_courses_post_type($name, $args=false) {
		
		if ($name=='courses') {

			if ($args===false) {
				$args = array(
					'label'               => __( 'Course item', 'bestdeals-utils' ),
					'description'         => __( 'Course Description', 'bestdeals-utils' ),
					'labels'              => array(
						'name'                => _x( 'Courses', 'Post Type General Name', 'bestdeals-utils' ),
						'singular_name'       => _x( 'Course item', 'Post Type Singular Name', 'bestdeals-utils' ),
						'menu_name'           => __( 'Courses', 'bestdeals-utils' ),
						'parent_item_colon'   => __( 'Parent Item:', 'bestdeals-utils' ),
						'all_items'           => __( 'All Courses', 'bestdeals-utils' ),
						'view_item'           => __( 'View Item', 'bestdeals-utils' ),
						'add_new_item'        => __( 'Add New Course item', 'bestdeals-utils' ),
						'add_new'             => __( 'Add New', 'bestdeals-utils' ),
						'edit_item'           => __( 'Edit Item', 'bestdeals-utils' ),
						'update_item'         => __( 'Update Item', 'bestdeals-utils' ),
						'search_items'        => __( 'Search Item', 'bestdeals-utils' ),
						'not_found'           => __( 'Not found', 'bestdeals-utils' ),
						'not_found_in_trash'  => __( 'Not found in Trash', 'bestdeals-utils' ),
					),
					'supports'            => array( 'title', 'excerpt', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields'),
					'hierarchical'        => false,
					'public'              => true,
					'show_ui'             => true,
					'menu_icon'			  => 'dashicons-format-chat',
					'show_in_menu'        => true,
					'show_in_nav_menus'   => true,
					'show_in_admin_bar'   => true,
					'menu_position'       => '52.5',
					'can_export'          => true,
					'has_archive'         => false,
					'exclude_from_search' => false,
					'publicly_queryable'  => true,
					'query_var'           => true,
					'capability_type'     => 'page',
					'rewrite'             => true
					);
			}
			register_post_type( $name, $args );
			trx_utils_add_rewrite_rules($name);

		} else if ($name=='lesson') {

			if ($args===false) {
				$args = array(
					'label'               => __( 'Lesson', 'bestdeals-utils' ),
					'description'         => __( 'Lesson Description', 'bestdeals-utils' ),
					'labels'              => array(
						'name'                => _x( 'Lessons', 'Post Type General Name', 'bestdeals-utils' ),
						'singular_name'       => _x( 'Lesson', 'Post Type Singular Name', 'bestdeals-utils' ),
						'menu_name'           => __( 'Lessons', 'bestdeals-utils' ),
						'parent_item_colon'   => __( 'Parent Item:', 'bestdeals-utils' ),
						'all_items'           => __( 'All lessons', 'bestdeals-utils' ),
						'view_item'           => __( 'View Item', 'bestdeals-utils' ),
						'add_new_item'        => __( 'Add New lesson', 'bestdeals-utils' ),
						'add_new'             => __( 'Add New', 'bestdeals-utils' ),
						'edit_item'           => __( 'Edit Item', 'bestdeals-utils' ),
						'update_item'         => __( 'Update Item', 'bestdeals-utils' ),
						'search_items'        => __( 'Search Item', 'bestdeals-utils' ),
						'not_found'           => __( 'Not found', 'bestdeals-utils' ),
						'not_found_in_trash'  => __( 'Not found in Trash', 'bestdeals-utils' ),
					),
					'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
					'hierarchical'        => false,
					'public'              => true,
					'show_ui'             => true,
					'menu_icon'			  => 'dashicons-format-chat',
					'show_in_menu'        => true,
					'show_in_nav_menus'   => true,
					'show_in_admin_bar'   => true,
					'menu_position'       => '52.6',
					'can_export'          => true,
					'has_archive'         => false,
					'exclude_from_search' => true,
					'publicly_queryable'  => true,
					'capability_type'     => 'page'
					);
			}
			register_post_type( $name, $args );
		}
	}
}
		

// Register custom taxonomy
if (!function_exists('trx_utils_support_courses_taxonomy')) {
	add_action( 'trx_utils_custom_taxonomy', 'trx_utils_support_courses_taxonomy', 10, 2 );
	function trx_utils_support_courses_taxonomy($name, $args=false) {
		
		if ($name=='courses_group') {

			if ($args===false) {
				$args = array(
					'post_type' 		=> 'courses',
					'hierarchical'      => true,
					'labels'            => array(
						'name'              => _x( 'Courses Groups', 'taxonomy general name', 'bestdeals-utils' ),
						'singular_name'     => _x( 'Courses Group', 'taxonomy singular name', 'bestdeals-utils' ),
						'search_items'      => __( 'Search Groups', 'bestdeals-utils' ),
						'all_items'         => __( 'All Groups', 'bestdeals-utils' ),
						'parent_item'       => __( 'Parent Group', 'bestdeals-utils' ),
						'parent_item_colon' => __( 'Parent Group:', 'bestdeals-utils' ),
						'edit_item'         => __( 'Edit Group', 'bestdeals-utils' ),
						'update_item'       => __( 'Update Group', 'bestdeals-utils' ),
						'add_new_item'      => __( 'Add New Group', 'bestdeals-utils' ),
						'new_item_name'     => __( 'New Group Name', 'bestdeals-utils' ),
						'menu_name'         => __( 'Courses Groups', 'bestdeals-utils' ),
					),
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => 'courses_group' )
					);
			}
			register_taxonomy( $name, $args['post_type'], $args);

		} else if ($name=='courses_tag') {

			if ($args===false) {
				$args = array(
					'post_type' 		=> 'courses',
					'hierarchical'      => true,
					'labels'            => array(
						'name'              => _x( 'Courses Tags', 'taxonomy general name', 'bestdeals-utils' ),
						'singular_name'     => _x( 'Courses Tag', 'taxonomy singular name', 'bestdeals-utils' ),
						'search_items'      => __( 'Search Tags', 'bestdeals-utils' ),
						'all_items'         => __( 'All Tags', 'bestdeals-utils' ),
						'parent_item'       => __( 'Parent Tag', 'bestdeals-utils' ),
						'parent_item_colon' => __( 'Parent Tag:', 'bestdeals-utils' ),
						'edit_item'         => __( 'Edit Tag', 'bestdeals-utils' ),
						'update_item'       => __( 'Update Tag', 'bestdeals-utils' ),
						'add_new_item'      => __( 'Add New Tag', 'bestdeals-utils' ),
						'new_item_name'     => __( 'New Tag Name', 'bestdeals-utils' ),
						'menu_name'         => __( 'Courses Tags', 'bestdeals-utils' ),
					),
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => 'courses_tag' )
				);
			}
			register_taxonomy( $name, $args['post_type'], $args);
		}
	}
}
?>