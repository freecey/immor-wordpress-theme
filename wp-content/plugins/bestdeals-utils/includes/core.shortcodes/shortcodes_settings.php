<?php

// Check if shortcodes settings are now used
if ( !function_exists( 'bestdeals_shortcodes_is_used' ) ) {
	function bestdeals_shortcodes_is_used() {
		return bestdeals_options_is_used() 															// All modes when Theme Options are used
			|| (is_admin() && isset($_POST['action']) 
					&& in_array($_POST['action'], array('vc_edit_form', 'wpb_show_edit_form')))		// AJAX query when save post/page
			|| bestdeals_vc_is_frontend();															// VC Frontend editor mode
	}
}

// Width and height params
if ( !function_exists( 'bestdeals_shortcodes_width' ) ) {
	function bestdeals_shortcodes_width($w="") {
		return array(
			"title" => esc_html__("Width", 'bestdeals-utils'),
			"divider" => true,
			"value" => $w,
			"type" => "text"
		);
	}
}
if ( !function_exists( 'bestdeals_shortcodes_height' ) ) {
	function bestdeals_shortcodes_height($h='') {
		return array(
			"title" => esc_html__("Height", 'bestdeals-utils'),
			"desc" => esc_attr__("Width (in pixels or percent) and height (only in pixels) of element", 'bestdeals-utils'),
			"value" => $h,
			"type" => "text"
		);
	}
}

/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_shortcodes_settings_theme_setup' ) ) {
//	if ( bestdeals_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'bestdeals_action_before_init_theme', 'bestdeals_shortcodes_settings_theme_setup', 20 );
	else
		add_action( 'bestdeals_action_after_init_theme', 'bestdeals_shortcodes_settings_theme_setup' );
	function bestdeals_shortcodes_settings_theme_setup() {
		if (bestdeals_shortcodes_is_used()) {
			global $BESTDEALS_GLOBALS;

			// Prepare arrays 
			$BESTDEALS_GLOBALS['sc_params'] = array(
			
				// Current element id
				'id' => array(
					"title" => esc_html__("Element ID", 'bestdeals-utils'),
					"desc" => esc_attr__("ID for current element", 'bestdeals-utils'),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				// Current element class
				'class' => array(
					"title" => esc_html__("Element CSS class", 'bestdeals-utils'),
					"desc" => esc_attr__("CSS class for current element (optional)", 'bestdeals-utils'),
					"value" => "",
					"type" => "text"
				),
			
				// Current element style
				'css' => array(
					"title" => esc_html__("CSS styles", 'bestdeals-utils'),
					"desc" => esc_attr__("Any additional CSS rules (if need)", 'bestdeals-utils'),
					"value" => "",
					"type" => "text"
				),
			
				// Margins params
				'top' => array(
					"title" => esc_html__("Top margin", 'bestdeals-utils'),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				'bottom' => array(
					"title" => esc_html__("Bottom margin", 'bestdeals-utils'),
					"value" => "",
					"type" => "text"
				),
			
				'left' => array(
					"title" => esc_html__("Left margin", 'bestdeals-utils'),
					"value" => "",
					"type" => "text"
				),
			
				'right' => array(
					"title" => esc_html__("Right margin", 'bestdeals-utils'),
					"desc" => esc_attr__("Margins around list (in pixels).", 'bestdeals-utils'),
					"value" => "",
					"type" => "text"
				),
			
				// Switcher choises
				'list_styles' => array(
					'ul'	=> esc_html__('Unordered', 'bestdeals-utils'),
					'ol'	=> esc_html__('Ordered', 'bestdeals-utils'),
					'iconed'=> esc_html__('Iconed', 'bestdeals-utils')
				),
				'yes_no'	=> bestdeals_get_list_yesno(),
				'on_off'	=> bestdeals_get_list_onoff(),
				'dir' 		=> bestdeals_get_list_directions(),
				'align'		=> bestdeals_get_list_alignments(),
				'float'		=> bestdeals_get_list_floats(),
				'show_hide'	=> bestdeals_get_list_showhide(),
				'sorting' 	=> bestdeals_get_list_sortings(),
				'ordering' 	=> bestdeals_get_list_orderings(),
				'shapes'	=> bestdeals_get_list_shapes(),
				'sizes'		=> bestdeals_get_list_sizes(),
				'sliders'	=> bestdeals_get_list_sliders(),
				'revo_sliders' => bestdeals_get_list_revo_sliders(),
				'categories'=> is_admin() && bestdeals_get_value_gp('action')=='vc_edit_form' && substr(bestdeals_get_value_gp('tag'), 0, 4)=='trx_' && isset($_POST['params']['post_type']) && $_POST['params']['post_type']!='post'
					? bestdeals_get_list_terms(false, bestdeals_get_taxonomy_categories_by_post_type($_POST['params']['post_type']))
					: bestdeals_get_list_categories(),
				'columns'	=> bestdeals_get_list_columns(),
				'images'	=> array_merge(array('none'=>"none"), bestdeals_get_list_files("images/icons", "png")),
				'icons'		=> array_merge(array("inherit", "none"), bestdeals_get_list_icons()),
				'locations'	=> bestdeals_get_list_dedicated_locations(),
				'filters'	=> bestdeals_get_list_portfolio_filters(),
				'formats'	=> bestdeals_get_list_post_formats_filters(),
				'hovers'	=> bestdeals_get_list_hovers(true),
				'hovers_dir'=> bestdeals_get_list_hovers_directions(true),
				'schemes'	=> bestdeals_get_list_color_schemes(true),
				'animations'=> bestdeals_get_list_animations_in(),
				'blogger_styles'	=> bestdeals_get_list_templates_blogger(),
				'posts_types'		=> bestdeals_get_list_posts_types(),
				'googlemap_styles'	=> bestdeals_get_list_googlemap_styles(),
				'field_types'		=> bestdeals_get_list_field_types(),
				'label_positions'	=> bestdeals_get_list_label_positions()
			);

			$BESTDEALS_GLOBALS['sc_params']['animation'] = array(
				"title" => esc_html__("Animation",  'bestdeals-utils'),
				"desc" => esc_attr__('Select animation while object enter in the visible area of page',  'bestdeals-utils'),
				"value" => "none",
				"type" => "select",
				"options" => $BESTDEALS_GLOBALS['sc_params']['animations']
			);
	
			// Shortcodes list
			//------------------------------------------------------------------
			$BESTDEALS_GLOBALS['shortcodes'] = array(
			
				// Accordion
				"trx_accordion" => array(
					"title" => esc_html__("Accordion", 'bestdeals-utils'),
					"desc" => esc_attr__("Accordion items", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Accordion style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style for display accordion", 'bestdeals-utils'),
							"value" => 1,
							"options" => bestdeals_get_list_styles(1, 2),
							"type" => "radio"
						),
						"counter" => array(
							"title" => esc_html__("Counter", 'bestdeals-utils'),
							"desc" => esc_attr__("Display counter before each accordion title", 'bestdeals-utils'),
							"value" => "off",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['on_off']
						),
						"initial" => array(
							"title" => esc_html__("Initially opened item", 'bestdeals-utils'),
							"desc" => esc_attr__("Number of initially opened item", 'bestdeals-utils'),
							"value" => 1,
							"min" => 0,
							"type" => "spinner"
						),
						"icon_closed" => array(
							"title" => esc_html__("Icon while closed",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the closed accordion item from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"icon_opened" => array(
							"title" => esc_html__("Icon while opened",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the opened accordion item from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_accordion_item",
						"title" => esc_html__("Item", 'bestdeals-utils'),
						"desc" => esc_attr__("Accordion item", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Accordion item title", 'bestdeals-utils'),
								"desc" => esc_attr__("Title for current accordion item", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"icon_closed" => array(
								"title" => esc_html__("Icon while closed",  'bestdeals-utils'),
								"desc" => esc_attr__('Select icon for the closed accordion item from Fontello icons set',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"icon_opened" => array(
								"title" => esc_html__("Icon while opened",  'bestdeals-utils'),
								"desc" => esc_attr__('Select icon for the opened accordion item from Fontello icons set',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"_content_" => array(
								"title" => esc_html__("Accordion item content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current accordion item content", 'bestdeals-utils'),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Anchor
				"trx_anchor" => array(
					"title" => esc_html__("Anchor", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert anchor for the TOC (table of content)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"icon" => array(
							"title" => esc_html__("Anchor's icon",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the anchor from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"title" => array(
							"title" => esc_html__("Short title", 'bestdeals-utils'),
							"desc" => esc_attr__("Short title of the anchor (for the table of content)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Long description", 'bestdeals-utils'),
							"desc" => esc_attr__("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"url" => array(
							"title" => esc_html__("External URL", 'bestdeals-utils'),
							"desc" => esc_attr__("External URL for this TOC item", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"separator" => array(
							"title" => esc_html__("Add separator", 'bestdeals-utils'),
							"desc" => esc_attr__("Add separator under item in the TOC", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"id" => $BESTDEALS_GLOBALS['sc_params']['id']
					)
				),
			
			
				// Audio
				"trx_audio" => array(
					"title" => esc_html__("Audio", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert audio player", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for audio file", 'bestdeals-utils'),
							"desc" => esc_attr__("URL for audio file", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose audio', 'bestdeals-utils'),
								'action' => 'media_upload',
								'type' => 'audio',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose audio file', 'bestdeals-utils'),
									'update' => esc_html__('Select audio file', 'bestdeals-utils')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"image" => array(
							"title" => esc_html__("Cover image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for audio cover", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title of the audio file", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"author" => array(
							"title" => esc_html__("Author", 'bestdeals-utils'),
							"desc" => esc_attr__("Author of the audio file", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"controls" => array(
							"title" => esc_html__("Show controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Show controls in audio player", 'bestdeals-utils'),
							"divider" => true,
							"size" => "medium",
							"value" => "show",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['show_hide']
						),
						"autoplay" => array(
							"title" => esc_html__("Autoplay audio", 'bestdeals-utils'),
							"desc" => esc_attr__("Autoplay audio on page load", 'bestdeals-utils'),
							"value" => "off",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['on_off']
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select block alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Block
				"trx_block" => array(
					"title" => esc_html__("Block container", 'bestdeals-utils'),
					"desc" => esc_attr__("Container for any block ([section] analog - to enable nesting)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"dedicated" => array(
							"title" => esc_html__("Dedicated", 'bestdeals-utils'),
							"desc" => esc_attr__("Use this block as dedicated content - show it before post title on single page", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select block alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"columns" => array(
							"title" => esc_html__("Columns emulation", 'bestdeals-utils'),
							"desc" => esc_attr__("Select width for columns emulation", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['columns']
						), 
						"pan" => array(
							"title" => esc_html__("Use pan effect", 'bestdeals-utils'),
							"desc" => esc_attr__("Use pan effect to show section content", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", 'bestdeals-utils'),
							"desc" => esc_attr__("Use scroller to show section content", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scroll_dir" => array(
							"title" => esc_html__("Scroll direction", 'bestdeals-utils'),
							"desc" => esc_attr__("Scroll direction (if Use scroller = yes)", 'bestdeals-utils'),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['dir']
						),
						"scroll_controls" => array(
							"title" => esc_html__("Scroll controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Show scroll controls (if Use scroller = yes)", 'bestdeals-utils'),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Fore color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any color for objects in this section", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", 'bestdeals-utils'),
							"desc" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", 'bestdeals-utils'),
							"desc" => esc_attr__("Predefined texture style from 1 to 11. 0 - without texture.", 'bestdeals-utils'),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", 'bestdeals-utils'),
							"desc" => esc_attr__("Font size of the text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", 'bestdeals-utils'),
							"desc" => esc_attr__("Font weight of the text", 'bestdeals-utils'),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'bestdeals-utils'),
								'300' => esc_html__('Light (300)', 'bestdeals-utils'),
								'400' => esc_html__('Normal (400)', 'bestdeals-utils'),
								'700' => esc_html__('Bold (700)', 'bestdeals-utils')
							)
						),
						"_content_" => array(
							"title" => esc_html__("Container content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for section container", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Blogger
				"trx_blogger" => array(
					"title" => esc_html__("Blogger", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert posts (pages) in many styles from desired categories or directly from ids", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Posts output style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired style for posts output", 'bestdeals-utils'),
							"value" => "regular",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['blogger_styles']
						),
						"filters" => array(
							"title" => esc_html__("Show filters", 'bestdeals-utils'),
							"desc" => esc_attr__("Use post's tags or categories as filter buttons", 'bestdeals-utils'),
							"value" => "no",
							"dir" => "horizontal",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['filters']
						),
						"hover" => array(
							"title" => esc_html__("Hover effect", 'bestdeals-utils'),
							"desc" => esc_attr__("Select hover effect (only if style=Portfolio)", 'bestdeals-utils'),
							"dependency" => array(
								'style' => array('portfolio','grid','square','short','colored')
							),
							"value" => "",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['hovers']
						),
						"hover_dir" => array(
							"title" => esc_html__("Hover direction", 'bestdeals-utils'),
							"desc" => esc_attr__("Select hover direction (only if style=Portfolio and hover=Circle|Square)", 'bestdeals-utils'),
							"dependency" => array(
								'style' => array('portfolio','grid','square','short','colored'),
								'hover' => array('square','circle')
							),
							"value" => "left_to_right",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['hovers_dir']
						),
						"dir" => array(
							"title" => esc_html__("Posts direction", 'bestdeals-utils'),
							"desc" => esc_attr__("Display posts in horizontal or vertical direction", 'bestdeals-utils'),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['dir']
						),
						"post_type" => array(
							"title" => esc_html__("Post type", 'bestdeals-utils'),
							"desc" => esc_attr__("Select post type to show", 'bestdeals-utils'),
							"value" => "post",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['posts_types']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"cat" => array(
							"title" => esc_html__("Categories list", 'bestdeals-utils'),
							"desc" => esc_attr__("Select the desired categories. If not selected - show posts from any category or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'ids' => array('is_empty'),
								'post_type' => array('refresh')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $BESTDEALS_GLOBALS['sc_params']['categories'])
						),
						"count" => array(
							"title" => esc_html__("Total posts to show", 'bestdeals-utils'),
							"desc" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'ids' => array('is_empty')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns used to show posts? If empty or 0 - equal to posts number", 'bestdeals-utils'),
							"dependency" => array(
								'dir' => array('horizontal')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'ids' => array('is_empty')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"only" => array(
							"title" => esc_html__("Select posts only", 'bestdeals-utils'),
							"desc" => esc_attr__("Select posts only with reviews, videos, audios, thumbs or galleries", 'bestdeals-utils'),
							"value" => "no",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['formats']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", 'bestdeals-utils'),
							"desc" => esc_attr__("Use scroller to show all posts", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Show slider controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Show arrows to control scroll slider", 'bestdeals-utils'),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"location" => array(
							"title" => esc_html__("Dedicated content location", 'bestdeals-utils'),
							"desc" => esc_attr__("Select position for dedicated content (only for style=excerpt)", 'bestdeals-utils'),
							"divider" => true,
							"dependency" => array(
								'style' => array('excerpt')
							),
							"value" => "default",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['locations']
						),
						"rating" => array(
							"title" => esc_html__("Show rating stars", 'bestdeals-utils'),
							"desc" => esc_attr__("Show rating stars under post's header", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"info" => array(
							"title" => esc_html__("Show post info block", 'bestdeals-utils'),
							"desc" => esc_attr__("Show post info block (author, date, tags, etc.)", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"links" => array(
							"title" => esc_html__("Allow links on the post", 'bestdeals-utils'),
							"desc" => esc_attr__("Allow links on the post from each blogger item", 'bestdeals-utils'),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"descr" => array(
							"title" => esc_html__("Description length", 'bestdeals-utils'),
							"desc" => esc_attr__("How many characters are displayed from post excerpt? If 0 - don't show description", 'bestdeals-utils'),
							"value" => 0,
							"min" => 0,
							"step" => 10,
							"type" => "spinner"
						),
						"readmore" => array(
							"title" => esc_html__("More link text", 'bestdeals-utils'),
							"desc" => esc_attr__("Read more link text. If empty - show 'More', else - used as link text", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Br
				"trx_br" => array(
					"title" => esc_html__("Break", 'bestdeals-utils'),
					"desc" => esc_attr__("Line break with clear floating (if need)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"clear" => 	array(
							"title" => esc_html__("Clear floating", 'bestdeals-utils'),
							"desc" => esc_attr__("Clear floating (if need)", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => array(
								'none' => esc_html__('None', 'bestdeals-utils'),
								'left' => esc_html__('Left', 'bestdeals-utils'),
								'right' => esc_html__('Right', 'bestdeals-utils'),
								'both' => esc_html__('Both', 'bestdeals-utils')
							)
						)
					)
				),
				
				// property info
				"trx_property_info" => array(
					"title" => esc_html__("Property info box", 'bestdeals-utils'),
					"desc" => "",
					"decorate" => false,
					"container" => false,
					"params" => array()
				),
				
				// property options
				"trx_property_options" => array(
					"title" => esc_html__("Property options box", 'bestdeals-utils'),
					"desc" => "",
					"decorate" => false,
					"container" => false,
					"params" => array(
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for property list", 'bestdeals-utils'),
							"value" => 1,
							"min" => 1,
							"max" => 3,
							"type" => "spinner"
						)
					)
				),
				
				
				// Property search
				"trx_property_search" => array(
					"title" => esc_html__("Property search", 'bestdeals-utils'),
					"desc" => esc_attr__("Property search box", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"status" => 	array(
							"title" => esc_html__("Property search status", 'bestdeals-utils'),
							"desc" => "",
							"value" => "none",
							"type" => "checklist",
							"options" => array(
								'none' => esc_html__('All property', 'bestdeals-utils'),
								'sale' => esc_html__('Sale', 'bestdeals-utils'),
								'rent' => esc_html__('Rent', 'bestdeals-utils')
							)
						)
					)
				),
				
				
				
				
				
				
				
			
				// Button
				"trx_button" => array(
					"title" => esc_html__("Button", 'bestdeals-utils'),
					"desc" => esc_attr__("Button with link", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Button caption", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
//						"type" => array(
//							"title" => esc_html__("Button's shape", 'bestdeals-utils'),
//							"desc" => esc_attr__("Select button's shape", 'bestdeals-utils'),
//							"value" => "square",
//							"size" => "medium",
//							"options" => array(
//								'square' => esc_html__('Square', 'bestdeals-utils'),
//								'round' => esc_html__('Round', 'bestdeals-utils')
//							),
//							"type" => "switch"
//						), 
//						"style" => array(
//							"title" => esc_html__("Button's style", 'bestdeals-utils'),
//							"desc" => esc_attr__("Select button's style", 'bestdeals-utils'),
//							"value" => "default",
//							"dir" => "horizontal",
//							"options" => array(
//								'filled' => esc_html__('Filled', 'bestdeals-utils'),
//								'border' => esc_html__('Border', 'bestdeals-utils')
//							),
//							"type" => "checklist"
//						), 
						"size" => array(
							"title" => esc_html__("Button's size", 'bestdeals-utils'),
							"desc" => esc_attr__("Select button's size", 'bestdeals-utils'),
							"value" => "small",
							"dir" => "horizontal",
							"options" => array(
								'small' => esc_html__('Small', 'bestdeals-utils'),
								'medium' => esc_html__('Medium', 'bestdeals-utils'),
								'large' => esc_html__('Large', 'bestdeals-utils')
							),
							"type" => "checklist"
						), 
						"icon" => array(
							"title" => esc_html__("Button's icon",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the title from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Button's text color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any color for button's caption", 'bestdeals-utils'),
							"std" => "",
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Button's backcolor", 'bestdeals-utils'),
							"desc" => esc_attr__("Any color for button's background", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"align" => array(
							"title" => esc_html__("Button's alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align button to left, center or right", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"link" => array(
							"title" => esc_html__("Link URL", 'bestdeals-utils'),
							"desc" => esc_attr__("URL for link on button click", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"target" => array(
							"title" => esc_html__("Link target", 'bestdeals-utils'),
							"desc" => esc_attr__("Target for link on button click", 'bestdeals-utils'),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"popup" => array(
							"title" => esc_html__("Open link in popup", 'bestdeals-utils'),
							"desc" => esc_attr__("Open link target in popup window", 'bestdeals-utils'),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						), 
						"rel" => array(
							"title" => esc_html__("Rel attribute", 'bestdeals-utils'),
							"desc" => esc_attr__("Rel attribute for button's link (if need)", 'bestdeals-utils'),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),




				// Call to Action block
				"trx_call_to_action" => array(
					"title" => esc_html__("Call to action", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert call to action block in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style to display block", 'bestdeals-utils'),
							"value" => "1",
							"type" => "checklist",
							"options" => bestdeals_get_list_styles(1, 2)
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Alignment elements in the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"accent" => array(
							"title" => esc_html__("Accented", 'bestdeals-utils'),
							"desc" => esc_attr__("Fill entire block with Accent1 color from current color scheme", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_attr__("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"image" => array(
							"title" => esc_html__("Image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site to include image into this block", 'bestdeals-utils'),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"video" => array(
							"title" => esc_html__("URL for video file", 'bestdeals-utils'),
							"desc" => esc_attr__("Select video from media library or paste URL for video file from other site to include video into this block", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'bestdeals-utils'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'bestdeals-utils'),
									'update' => esc_html__('Select video file', 'bestdeals-utils')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link2" => array(
							"title" => esc_html__("Button 2 URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL for the second button at the bottom of the block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link2_caption" => array(
							"title" => esc_html__("Button 2 caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Caption for the second button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Chat
				"trx_chat" => array(
					"title" => esc_html__("Chat", 'bestdeals-utils'),
					"desc" => esc_attr__("Chat message", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Item title", 'bestdeals-utils'),
							"desc" => esc_attr__("Chat item title", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"photo" => array(
							"title" => esc_html__("Item photo", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the item photo (avatar)", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"link" => array(
							"title" => esc_html__("Item link", 'bestdeals-utils'),
							"desc" => esc_attr__("Chat item link", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Chat item content", 'bestdeals-utils'),
							"desc" => esc_attr__("Current chat item content", 'bestdeals-utils'),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Columns
				"trx_columns" => array(
					"title" => esc_html__("Columns", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert up to 5 columns in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"fluid" => array(
							"title" => esc_html__("Fluid columns", 'bestdeals-utils'),
							"desc" => esc_attr__("To squeeze the columns when reducing the size of the window (fluid=yes) or to rebuild them (fluid=no)", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						), 
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_column_item",
						"title" => esc_html__("Column", 'bestdeals-utils'),
						"desc" => esc_attr__("Column item", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"span" => array(
								"title" => esc_html__("Merge columns", 'bestdeals-utils'),
								"desc" => esc_attr__("Count merged columns from current", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"align" => array(
								"title" => esc_html__("Alignment", 'bestdeals-utils'),
								"desc" => esc_attr__("Alignment text in the column", 'bestdeals-utils'),
								"value" => "",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $BESTDEALS_GLOBALS['sc_params']['align']
							),
							"color" => array(
								"title" => esc_html__("Fore color", 'bestdeals-utils'),
								"desc" => esc_attr__("Any color for objects in this column", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"bg_color" => array(
								"title" => esc_html__("Background color", 'bestdeals-utils'),
								"desc" => esc_attr__("Any background color for this column", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"bg_image" => array(
								"title" => esc_html__("URL for background image file", 'bestdeals-utils'),
								"desc" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
							"_content_" => array(
								"title" => esc_html__("Column item content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current column item content", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Contact form
				"trx_contact_form" => array(
					"title" => esc_html__("Contact form", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert contact form", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style of the contact form", 'bestdeals-utils'),
							"value" => 1,
							"options" => bestdeals_get_list_styles(1, 2),
							"type" => "checklist"
						), 
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_attr__("Use custom fields or create standard contact form (ignore info from 'Field' tabs)", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						), 
						"action" => array(
							"title" => esc_html__("Action", 'bestdeals-utils'),
							"desc" => esc_attr__("Contact form action (URL to handle form data). If empty - use internal action", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select form alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"width" => bestdeals_shortcodes_width(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_form_item",
						"title" => esc_html__("Field", 'bestdeals-utils'),
						"desc" => esc_attr__("Custom field", 'bestdeals-utils'),
						"container" => false,
						"params" => array(
							"type" => array(
								"title" => esc_html__("Type", 'bestdeals-utils'),
								"desc" => esc_attr__("Type of the custom field", 'bestdeals-utils'),
								"value" => "text",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $BESTDEALS_GLOBALS['sc_params']['field_types']
							), 
							"name" => array(
								"title" => esc_html__("Name", 'bestdeals-utils'),
								"desc" => esc_attr__("Name of the custom field", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"value" => array(
								"title" => esc_html__("Default value", 'bestdeals-utils'),
								"desc" => esc_attr__("Default value of the custom field", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"options" => array(
								"title" => esc_html__("Options", 'bestdeals-utils'),
								"desc" => esc_attr__("Field options. For example: big=My daddy|middle=My brother|small=My little sister", 'bestdeals-utils'),
								"dependency" => array(
									'type' => array('radio', 'checkbox', 'select')
								),
								"value" => "",
								"type" => "text"
							),
							"label" => array(
								"title" => esc_html__("Label", 'bestdeals-utils'),
								"desc" => esc_attr__("Label for the custom field", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"label_position" => array(
								"title" => esc_html__("Label position", 'bestdeals-utils'),
								"desc" => esc_attr__("Label position relative to the field", 'bestdeals-utils'),
								"value" => "top",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $BESTDEALS_GLOBALS['sc_params']['label_positions']
							), 
							"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
							"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
							"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
							"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Content block on fullscreen page
				"trx_content" => array(
					"title" => esc_html__("Content block", 'bestdeals-utils'),
					"desc" => esc_attr__("Container for main content block with desired class and style (use it only on fullscreen pages)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"_content_" => array(
							"title" => esc_html__("Container content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for section container", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Countdown
				"trx_countdown" => array(
					"title" => esc_html__("Countdown", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert countdown object", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"date" => array(
							"title" => esc_html__("Date", 'bestdeals-utils'),
							"desc" => esc_attr__("Upcoming date (format: yyyy-mm-dd)", 'bestdeals-utils'),
							"value" => "",
							"format" => "yy-mm-dd",
							"type" => "date"
						),
						"time" => array(
							"title" => esc_html__("Time", 'bestdeals-utils'),
							"desc" => esc_attr__("Upcoming time (format: HH:mm:ss)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Countdown style", 'bestdeals-utils'),
							"value" => "1",
							"type" => "checklist",
							"options" => bestdeals_get_list_styles(1, 2)
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align counter to left, center or right", 'bestdeals-utils'),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Dropcaps
				"trx_dropcaps" => array(
					"title" => esc_html__("Dropcaps", 'bestdeals-utils'),
					"desc" => esc_attr__("Make first letter as dropcaps", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Dropcaps style", 'bestdeals-utils'),
							"value" => "1",
							"type" => "checklist",
							"options" => bestdeals_get_list_styles(1, 4)
						),
						"_content_" => array(
							"title" => esc_html__("Paragraph content", 'bestdeals-utils'),
							"desc" => esc_attr__("Paragraph with dropcaps content", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Emailer
				"trx_emailer" => array(
					"title" => esc_html__("E-mail collector", 'bestdeals-utils'),
					"desc" => esc_attr__("Collect the e-mail address into specified group", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"group" => array(
							"title" => esc_html__("Group", 'bestdeals-utils'),
							"desc" => esc_attr__("The name of group to collect e-mail address", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"open" => array(
							"title" => esc_html__("Open", 'bestdeals-utils'),
							"desc" => esc_attr__("Initially open the input field on show object", 'bestdeals-utils'),
							"divider" => true,
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align object to left, center or right", 'bestdeals-utils'),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Gap
				"trx_gap" => array(
					"title" => esc_html__("Gap", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert gap (fullwidth area) in the post content. Attention! Use the gap only in the posts (pages) without left or right sidebar", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Gap content", 'bestdeals-utils'),
							"desc" => esc_attr__("Gap inner content", 'bestdeals-utils'),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						)
					)
				),
			
			
			
			
			
				// Google map
				"trx_googlemap" => array(
					"title" => esc_html__("Google map", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert Google map with specified markers", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"zoom" => array(
							"title" => esc_html__("Zoom", 'bestdeals-utils'),
							"desc" => esc_attr__("Map zoom factor", 'bestdeals-utils'),
							"divider" => true,
							"value" => 16,
							"min" => 1,
							"max" => 20,
							"type" => "spinner"
						),
						"style" => array(
							"title" => esc_html__("Map style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select map style", 'bestdeals-utils'),
							"value" => "default",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['googlemap_styles']
						),
						"width" => bestdeals_shortcodes_width('100%'),
						"height" => bestdeals_shortcodes_height(240),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_googlemap_marker",
						"title" => esc_html__("Google map marker", 'bestdeals-utils'),
						"desc" => esc_attr__("Google map marker", 'bestdeals-utils'),
						"decorate" => false,
						"container" => true,
						"params" => array(
							"address" => array(
								"title" => esc_html__("Address", 'bestdeals-utils'),
								"desc" => esc_attr__("Address of this marker", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"latlng" => array(
								"title" => esc_html__("Latitude and Longitude", 'bestdeals-utils'),
								"desc" => esc_attr__("Comma separated marker's coorditanes (instead Address)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"point" => array(
								"title" => esc_html__("URL for marker image file", 'bestdeals-utils'),
								"desc" => esc_attr__("Select or upload image or write URL from other site for this marker. If empty - use default marker", 'bestdeals-utils'),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
							"title" => array(
								"title" => esc_html__("Title", 'bestdeals-utils'),
								"desc" => esc_attr__("Title for this marker", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("Description", 'bestdeals-utils'),
								"desc" => esc_attr__("Description for this marker", 'bestdeals-utils'),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id']
						)
					)
				),
			
			
			
				// Hide or show any block
				"trx_hide" => array(
					"title" => esc_html__("Hide/Show any block", 'bestdeals-utils'),
					"desc" => esc_attr__("Hide or Show any block with desired CSS-selector", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"selector" => array(
							"title" => esc_html__("Selector", 'bestdeals-utils'),
							"desc" => esc_attr__("Any block's CSS-selector", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"hide" => array(
							"title" => esc_html__("Hide or Show", 'bestdeals-utils'),
							"desc" => esc_attr__("New state for the block: hide or show", 'bestdeals-utils'),
							"value" => "yes",
							"size" => "small",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						)
					)
				),
			
			
			
				// Highlght text
				"trx_highlight" => array(
					"title" => esc_html__("Highlight text", 'bestdeals-utils'),
					"desc" => esc_attr__("Highlight text with selected color, background color and other styles", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"type" => array(
							"title" => esc_html__("Type", 'bestdeals-utils'),
							"desc" => esc_attr__("Highlight type", 'bestdeals-utils'),
							"value" => "1",
							"type" => "checklist",
							"options" => array(
								0 => esc_html__('Custom', 'bestdeals-utils'),
								1 => esc_html__('Type 1', 'bestdeals-utils'),
								2 => esc_html__('Type 2', 'bestdeals-utils'),
								3 => esc_html__('Type 3', 'bestdeals-utils')
							)
						),
						"color" => array(
							"title" => esc_html__("Color", 'bestdeals-utils'),
							"desc" => esc_attr__("Color for the highlighted text", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Background color for the highlighted text", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", 'bestdeals-utils'),
							"desc" => esc_attr__("Font size of the highlighted text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Highlighting content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for highlight", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Icon
				"trx_icon" => array(
					"title" => esc_html__("Icon", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert icon", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"icon" => array(
							"title" => esc_html__('Icon',  'bestdeals-utils'),
							"desc" => esc_attr__('Select font icon from the Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Icon's color", 'bestdeals-utils'),
							"desc" => esc_attr__("Icon's color", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "color"
						),
						"bg_shape" => array(
							"title" => esc_html__("Background shape", 'bestdeals-utils'),
							"desc" => esc_attr__("Shape of the icon background", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "none",
							"type" => "radio",
							"options" => array(
								'none' => esc_html__('None', 'bestdeals-utils'),
								'round' => esc_html__('Round', 'bestdeals-utils'),
								'square' => esc_html__('Square', 'bestdeals-utils')
							)
						),
						"bg_color" => array(
							"title" => esc_html__("Icon's background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Icon's background color", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty'),
								'background' => array('round','square')
							),
							"value" => "",
							"type" => "color"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", 'bestdeals-utils'),
							"desc" => esc_attr__("Icon's font size", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "spinner",
							"min" => 8,
							"max" => 240
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", 'bestdeals-utils'),
							"desc" => esc_attr__("Icon font weight", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'bestdeals-utils'),
								'300' => esc_html__('Light (300)', 'bestdeals-utils'),
								'400' => esc_html__('Normal (400)', 'bestdeals-utils'),
								'700' => esc_html__('Bold (700)', 'bestdeals-utils')
							)
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Icon text alignment", 'bestdeals-utils'),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"link" => array(
							"title" => esc_html__("Link URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL from this icon (if not empty)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Image
				"trx_image" => array(
					"title" => esc_html__("Image", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert image into your post (page)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for image file", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'sizes' => true		// If you want allow user select thumb size for image. Otherwise, thumb size is ignored - image fullsize used
							)
						),
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Image title (if need)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"icon" => array(
							"title" => esc_html__("Icon before title",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the title from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"align" => array(
							"title" => esc_html__("Float image", 'bestdeals-utils'),
							"desc" => esc_attr__("Float image to left or right side", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						), 
						"shape" => array(
							"title" => esc_html__("Image Shape", 'bestdeals-utils'),
							"desc" => esc_attr__("Shape of the image: square (rectangle) or round", 'bestdeals-utils'),
							"value" => "square",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								"square" => esc_html__('Square', 'bestdeals-utils'),
								"round" => esc_html__('Round', 'bestdeals-utils')
							)
						), 
						"link" => array(
							"title" => esc_html__("Link", 'bestdeals-utils'),
							"desc" => esc_attr__("The link URL from the image", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Infobox
				"trx_infobox" => array(
					"title" => esc_html__("Infobox", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert infobox into your post (page)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Infobox style", 'bestdeals-utils'),
							"value" => "regular",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'regular' => esc_html__('Regular', 'bestdeals-utils'),
								'info' => esc_html__('Info', 'bestdeals-utils'),
								'success' => esc_html__('Success', 'bestdeals-utils'),
								'error' => esc_html__('Error', 'bestdeals-utils')
							)
						),
						"closeable" => array(
							"title" => esc_html__("Closeable box", 'bestdeals-utils'),
							"desc" => esc_attr__("Create closeable box (with close button)", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"icon" => array(
							"title" => esc_html__("Custom icon",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the infobox from Fontello icons set. If empty - use default icon',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Text color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any color for text and headers", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any background color for this infobox", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"_content_" => array(
							"title" => esc_html__("Infobox content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for infobox", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Line
				"trx_line" => array(
					"title" => esc_html__("Line", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert Line into your post (page)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Line style", 'bestdeals-utils'),
							"value" => "solid",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'solid' => esc_html__('Solid', 'bestdeals-utils'),
								'dashed' => esc_html__('Dashed', 'bestdeals-utils'),
								'dotted' => esc_html__('Dotted', 'bestdeals-utils'),
								'double' => esc_html__('Double', 'bestdeals-utils')
							)
						),
						"color" => array(
							"title" => esc_html__("Color", 'bestdeals-utils'),
							"desc" => esc_attr__("Line color", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// List
				"trx_list" => array(
					"title" => esc_html__("List", 'bestdeals-utils'),
					"desc" => esc_attr__("List items with specific bullets", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Bullet's style", 'bestdeals-utils'),
							"desc" => esc_attr__("Bullet's style for each list item", 'bestdeals-utils'),
							"value" => "ul",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['list_styles']
						), 
						"color" => array(
							"title" => esc_html__("Color", 'bestdeals-utils'),
							"desc" => esc_attr__("List items color", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"icon" => array(
							"title" => esc_html__('List icon',  'bestdeals-utils'),
							"desc" => esc_attr__("Select list icon from Fontello icons set (only for style=Iconed)",  'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"icon_color" => array(
							"title" => esc_html__("Icon color", 'bestdeals-utils'),
							"desc" => esc_attr__("List icons color", 'bestdeals-utils'),
							"value" => "",
							"dependency" => array(
								'style' => array('iconed')
							),
							"type" => "color"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_list_item",
						"title" => esc_html__("Item", 'bestdeals-utils'),
						"desc" => esc_attr__("List item with specific bullet", 'bestdeals-utils'),
						"decorate" => false,
						"container" => true,
						"params" => array(
							"_content_" => array(
								"title" => esc_html__("List item content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current list item content", 'bestdeals-utils'),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"title" => array(
								"title" => esc_html__("List item title", 'bestdeals-utils'),
								"desc" => esc_attr__("Current list item title (show it as tooltip)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"color" => array(
								"title" => esc_html__("Color", 'bestdeals-utils'),
								"desc" => esc_attr__("Text color for this item", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"icon" => array(
								"title" => esc_html__('List icon',  'bestdeals-utils'),
								"desc" => esc_attr__("Select list item icon from Fontello icons set (only for style=Iconed)",  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"icon_color" => array(
								"title" => esc_html__("Icon color", 'bestdeals-utils'),
								"desc" => esc_attr__("Icon color for this item", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"link" => array(
								"title" => esc_html__("Link URL", 'bestdeals-utils'),
								"desc" => esc_attr__("Link URL for the current list item", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"target" => array(
								"title" => esc_html__("Link target", 'bestdeals-utils'),
								"desc" => esc_attr__("Link target for the current list item", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
				
				
				// List Educ
				"trx_list_educ" => array(
					"title" => esc_html__("List Educ", 'bestdeals-utils'),
					"desc" => esc_attr__("Education list", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_list_educ_item",
						"title" => esc_html__("Item", 'bestdeals-utils'),
						"desc" => esc_attr__("List item with specific bullet", 'bestdeals-utils'),
						"decorate" => false,
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("List item title", 'bestdeals-utils'),
								"desc" => esc_attr__("Current list item title (show it as tooltip)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("List item content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current list item content", 'bestdeals-utils'),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
				
				
				
				
				
			
			
			
				// Number
				"trx_number" => array(
					"title" => esc_html__("Number", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert number or any word as set separate characters", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"value" => array(
							"title" => esc_html__("Value", 'bestdeals-utils'),
							"desc" => esc_attr__("Number or any word", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select block alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Parallax
				"trx_parallax" => array(
					"title" => esc_html__("Parallax", 'bestdeals-utils'),
					"desc" => esc_attr__("Create the parallax container (with asinc background image)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"gap" => array(
							"title" => esc_html__("Create gap", 'bestdeals-utils'),
							"desc" => esc_attr__("Create gap around parallax container", 'bestdeals-utils'),
							"value" => "no",
							"size" => "small",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						), 
						"dir" => array(
							"title" => esc_html__("Dir", 'bestdeals-utils'),
							"desc" => esc_attr__("Scroll direction for the parallax background", 'bestdeals-utils'),
							"value" => "up",
							"size" => "medium",
							"options" => array(
								'up' => esc_html__('Up', 'bestdeals-utils'),
								'down' => esc_html__('Down', 'bestdeals-utils')
							),
							"type" => "switch"
						), 
						"speed" => array(
							"title" => esc_html__("Speed", 'bestdeals-utils'),
							"desc" => esc_attr__("Image motion speed (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0.3",
							"type" => "spinner"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Text color", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color for text object inside parallax block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color for parallax background", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the parallax background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_image_x" => array(
							"title" => esc_html__("Image X position", 'bestdeals-utils'),
							"desc" => esc_attr__("Image horizontal position (as background of the parallax block) - in percent", 'bestdeals-utils'),
							"min" => "0",
							"max" => "100",
							"value" => "50",
							"type" => "spinner"
						),
						"bg_video" => array(
							"title" => esc_html__("Video background", 'bestdeals-utils'),
							"desc" => esc_attr__("Select video from media library or paste URL for video file from other site to show it as parallax background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'bestdeals-utils'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'bestdeals-utils'),
									'update' => esc_html__('Select video file', 'bestdeals-utils')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"bg_video_ratio" => array(
							"title" => esc_html__("Video ratio", 'bestdeals-utils'),
							"desc" => esc_attr__("Specify ratio of the video background. For example: 16:9 (default), 4:3, etc.", 'bestdeals-utils'),
							"value" => "16:9",
							"type" => "text"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", 'bestdeals-utils'),
							"desc" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", 'bestdeals-utils'),
							"desc" => esc_attr__("Predefined texture style from 1 to 11. 0 - without texture.", 'bestdeals-utils'),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"_content_" => array(
							"title" => esc_html__("Content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for the parallax container", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Popup
				"trx_popup" => array(
					"title" => esc_html__("Popup window", 'bestdeals-utils'),
					"desc" => esc_attr__("Container for any html-block with desired class and style for popup window", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Container content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for section container", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Price
				"trx_price" => array(
					"title" => esc_html__("Price", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert price with decoration", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"money" => array(
							"title" => esc_html__("Money", 'bestdeals-utils'),
							"desc" => esc_attr__("Money value (dot or comma separated)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"currency" => array(
							"title" => esc_html__("Currency", 'bestdeals-utils'),
							"desc" => esc_attr__("Currency character", 'bestdeals-utils'),
							"value" => "$",
							"type" => "text"
						),
						"period" => array(
							"title" => esc_html__("Period", 'bestdeals-utils'),
							"desc" => esc_attr__("Period text (if need). For example: monthly, daily, etc.", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align price to left or right side", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						), 
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Price block
				"trx_price_block" => array(
					"title" => esc_html__("Price block", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert price block with title, price and description", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Block title", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Link URL", 'bestdeals-utils'),
							"desc" => esc_attr__("URL for link from button (at bottom of the block)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_text" => array(
							"title" => esc_html__("Link text", 'bestdeals-utils'),
							"desc" => esc_attr__("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"icon" => array(
							"title" => esc_html__("Icon",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon from Fontello icons set (placed before/instead price)',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"money" => array(
							"title" => esc_html__("Money", 'bestdeals-utils'),
							"desc" => esc_attr__("Money value (dot or comma separated)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"currency" => array(
							"title" => esc_html__("Currency", 'bestdeals-utils'),
							"desc" => esc_attr__("Currency character", 'bestdeals-utils'),
							"value" => "$",
							"type" => "text"
						),
						"period" => array(
							"title" => esc_html__("Period", 'bestdeals-utils'),
							"desc" => esc_attr__("Period text (if need). For example: monthly, daily, etc.", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align price to left or right side", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						), 
						"_content_" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Description for this price block", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Quote
				"trx_quote" => array(
					"title" => esc_html__("Quote", 'bestdeals-utils'),
					"desc" => esc_attr__("Quote text", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"cite" => array(
							"title" => esc_html__("Quote cite", 'bestdeals-utils'),
							"desc" => esc_attr__("URL for quote cite", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"title" => array(
							"title" => esc_html__("Title (author)", 'bestdeals-utils'),
							"desc" => esc_attr__("Quote title (author name)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Quote content", 'bestdeals-utils'),
							"desc" => esc_attr__("Quote content", 'bestdeals-utils'),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Reviews
				"trx_reviews" => array(
					"title" => esc_html__("Reviews", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert reviews block in the single post", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Align counter to left, center or right", 'bestdeals-utils'),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Search
				"trx_search" => array(
					"title" => esc_html__("Search", 'bestdeals-utils'),
					"desc" => esc_attr__("Show search form", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style to display search field", 'bestdeals-utils'),
							"value" => "regular",
							"options" => array(
								"regular" => esc_html__('Regular', 'bestdeals-utils'),
								"rounded" => esc_html__('Rounded', 'bestdeals-utils')
							),
							"type" => "checklist"
						),
						"state" => array(
							"title" => esc_html__("State", 'bestdeals-utils'),
							"desc" => esc_attr__("Select search field initial state", 'bestdeals-utils'),
							"value" => "fixed",
							"options" => array(
								"fixed"  => esc_html__('Fixed',  'bestdeals-utils'),
								"opened" => esc_html__('Opened', 'bestdeals-utils'),
								"closed" => esc_html__('Closed', 'bestdeals-utils')
							),
							"type" => "checklist"
						),
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title (placeholder) for the search field", 'bestdeals-utils'),
							"value" => esc_html__("Search &hellip;", 'bestdeals-utils'),
							"type" => "text"
						),
						"ajax" => array(
							"title" => esc_html__("AJAX", 'bestdeals-utils'),
							"desc" => esc_attr__("Search via AJAX or reload page", 'bestdeals-utils'),
							"value" => "yes",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Section
				"trx_section" => array(
					"title" => esc_html__("Section container", 'bestdeals-utils'),
					"desc" => esc_attr__("Container for any block with desired class and style", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"dedicated" => array(
							"title" => esc_html__("Dedicated", 'bestdeals-utils'),
							"desc" => esc_attr__("Use this block as dedicated content - show it before post title on single page", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select block alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"columns" => array(
							"title" => esc_html__("Columns emulation", 'bestdeals-utils'),
							"desc" => esc_attr__("Select width for columns emulation", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['columns']
						), 
						"pan" => array(
							"title" => esc_html__("Use pan effect", 'bestdeals-utils'),
							"desc" => esc_attr__("Use pan effect to show section content", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", 'bestdeals-utils'),
							"desc" => esc_attr__("Use scroller to show section content", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scroll_dir" => array(
							"title" => esc_html__("Scroll and Pan direction", 'bestdeals-utils'),
							"desc" => esc_attr__("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", 'bestdeals-utils'),
							"dependency" => array(
								'pan' => array('yes'),
								'scroll' => array('yes')
							),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['dir']
						),
						"scroll_controls" => array(
							"title" => esc_html__("Scroll controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Show scroll controls (if Use scroller = yes)", 'bestdeals-utils'),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Fore color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any color for objects in this section", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", 'bestdeals-utils'),
							"desc" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", 'bestdeals-utils'),
							"desc" => esc_attr__("Predefined texture style from 1 to 11. 0 - without texture.", 'bestdeals-utils'),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", 'bestdeals-utils'),
							"desc" => esc_attr__("Font size of the text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", 'bestdeals-utils'),
							"desc" => esc_attr__("Font weight of the text", 'bestdeals-utils'),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'bestdeals-utils'),
								'300' => esc_html__('Light (300)', 'bestdeals-utils'),
								'400' => esc_html__('Normal (400)', 'bestdeals-utils'),
								'700' => esc_html__('Bold (700)', 'bestdeals-utils')
							)
						),
						"_content_" => array(
							"title" => esc_html__("Container content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content for section container", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Skills
				"trx_skills" => array(
					"title" => esc_html__("Skills", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert skills diagramm in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"max_value" => array(
							"title" => esc_html__("Max value", 'bestdeals-utils'),
							"desc" => esc_attr__("Max value for skills items", 'bestdeals-utils'),
							"value" => 100,
							"min" => 1,
							"type" => "spinner"
						),
						"type" => array(
							"title" => esc_html__("Skills type", 'bestdeals-utils'),
							"desc" => esc_attr__("Select type of skills block", 'bestdeals-utils'),
							"value" => "bar",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'bar' => esc_html__('Bar', 'bestdeals-utils'),
								'pie' => esc_html__('Pie chart', 'bestdeals-utils'),
								'counter' => esc_html__('Counter', 'bestdeals-utils'),
								'arc' => esc_html__('Arc', 'bestdeals-utils')
							)
						), 
						"layout" => array(
							"title" => esc_html__("Skills layout", 'bestdeals-utils'),
							"desc" => esc_attr__("Select layout of skills block", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('counter','pie','bar')
							),
							"value" => "rows",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'rows' => esc_html__('Rows', 'bestdeals-utils'),
								'columns' => esc_html__('Columns', 'bestdeals-utils')
							)
						),
						"dir" => array(
							"title" => esc_html__("Direction", 'bestdeals-utils'),
							"desc" => esc_attr__("Select direction of skills block", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('counter','pie','bar')
							),
							"value" => "horizontal",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['dir']
						), 
						"style" => array(
							"title" => esc_html__("Counters style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style of skills items (only for type=counter)", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('counter')
							),
							"value" => 1,
							"options" => bestdeals_get_list_styles(1, 4),
							"type" => "checklist"
						), 
						// "columns" - autodetect, not set manual
						"color" => array(
							"title" => esc_html__("Skills items color", 'bestdeals-utils'),
							"desc" => esc_attr__("Color for all skills items", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Background color for all skills items (only for type=pie)", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "",
							"type" => "color"
						),
						"border_color" => array(
							"title" => esc_html__("Border color", 'bestdeals-utils'),
							"desc" => esc_attr__("Border color for all skills items (only for type=pie)", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "",
							"type" => "color"
						),
						"align" => array(
							"title" => esc_html__("Align skills block", 'bestdeals-utils'),
							"desc" => esc_attr__("Align skills block to left or right side", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						), 
						"arc_caption" => array(
							"title" => esc_html__("Arc Caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Arc caption - text in the center of the diagram", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('arc')
							),
							"value" => "",
							"type" => "text"
						),
						"pie_compact" => array(
							"title" => esc_html__("Pie compact", 'bestdeals-utils'),
							"desc" => esc_attr__("Show all skills in one diagram or as separate diagrams", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"pie_cutout" => array(
							"title" => esc_html__("Pie cutout", 'bestdeals-utils'),
							"desc" => esc_attr__("Pie cutout (0-99). 0 - without cutout, 99 - max cutout", 'bestdeals-utils'),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => 0,
							"min" => 0,
							"max" => 99,
							"type" => "spinner"
						),
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_skills_item",
						"title" => esc_html__("Skill", 'bestdeals-utils'),
						"desc" => esc_attr__("Skills item", 'bestdeals-utils'),
						"container" => false,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Title", 'bestdeals-utils'),
								"desc" => esc_attr__("Current skills item title", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"value" => array(
								"title" => esc_html__("Value", 'bestdeals-utils'),
								"desc" => esc_attr__("Current skills level", 'bestdeals-utils'),
								"value" => 0,
								"min" => 0,
								"step" => 1,
								"type" => "spinner"
							),
							"color" => array(
								"title" => esc_html__("Color", 'bestdeals-utils'),
								"desc" => esc_attr__("Current skills item color", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"bg_color" => array(
								"title" => esc_html__("Background color", 'bestdeals-utils'),
								"desc" => esc_attr__("Current skills item background color (only for type=pie)", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"border_color" => array(
								"title" => esc_html__("Border color", 'bestdeals-utils'),
								"desc" => esc_attr__("Current skills item border color (only for type=pie)", 'bestdeals-utils'),
								"value" => "",
								"type" => "color"
							),
							"style" => array(
								"title" => esc_html__("Counter style", 'bestdeals-utils'),
								"desc" => esc_attr__("Select style for the current skills item (only for type=counter)", 'bestdeals-utils'),
								"value" => 1,
								"options" => bestdeals_get_list_styles(1, 4),
								"type" => "checklist"
							), 
							"icon" => array(
								"title" => esc_html__("Counter icon",  'bestdeals-utils'),
								"desc" => esc_attr__('Select icon from Fontello icons set, placed above counter (only for type=counter)',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Slider
				"trx_slider" => array(
					"title" => esc_html__("Slider", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert slider into your post (page)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array_merge(array(
						"engine" => array(
							"title" => esc_html__("Slider engine", 'bestdeals-utils'),
							"desc" => esc_attr__("Select engine for slider. Attention! Swiper is built-in engine, all other engines appears only if corresponding plugings are installed", 'bestdeals-utils'),
							"value" => "swiper",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sliders']
						),
						"align" => array(
							"title" => esc_html__("Float slider", 'bestdeals-utils'),
							"desc" => esc_attr__("Float slider to left or right side", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						),
						"custom" => array(
							"title" => esc_html__("Custom slides", 'bestdeals-utils'),
							"desc" => esc_attr__("Make custom slides from inner shortcodes (prepare it on tabs) or prepare slides from posts thumbnails", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						)
						),
						bestdeals_exists_revslider() ? array(
						"alias" => array(
							"title" => esc_html__("Revolution slider alias", 'bestdeals-utils'),
							"desc" => esc_attr__("Select Revolution slider to display", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('revo')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['revo_sliders']
						)) : array(), array(
						"cat" => array(
							"title" => esc_html__("Swiper: Category list", 'bestdeals-utils'),
							"desc" => esc_attr__("Select category to show post's images. If empty - select posts from any category or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $BESTDEALS_GLOBALS['sc_params']['categories'])
						),
						"count" => array(
							"title" => esc_html__("Swiper: Number of posts", 'bestdeals-utils'),
							"desc" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Swiper: Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Swiper: Post order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "date",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Swiper: Post order", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Swiper: Post IDs list", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "",
							"type" => "text"
						),
						"controls" => array(
							"title" => esc_html__("Swiper: Show slider controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Show arrows inside slider", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"pagination" => array(
							"title" => esc_html__("Swiper: Show slider pagination", 'bestdeals-utils'),
							"desc" => esc_attr__("Show bullets for switch slides", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "no",
							"type" => "checklist",
							"options" => array(
								'no'   => esc_html__('None', 'bestdeals-utils'),
								'yes'  => esc_html__('Dots', 'bestdeals-utils'),
								'full' => esc_html__('Side Titles', 'bestdeals-utils'),
								'over' => esc_html__('Over Titles', 'bestdeals-utils')
							)
						),
						"titles" => array(
							"title" => esc_html__("Swiper: Show titles section", 'bestdeals-utils'),
							"desc" => esc_attr__("Show section with post's title and short post's description", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "no",
							"type" => "checklist",
							"options" => array(
								"no"    => esc_html__('Not show', 'bestdeals-utils'),
								"slide" => esc_html__('Show/Hide info', 'bestdeals-utils'),
								"fixed" => esc_html__('Fixed info', 'bestdeals-utils')
							)
						),
						"descriptions" => array(
							"title" => esc_html__("Swiper: Post descriptions", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"desc" => esc_attr__("Show post's excerpt max length (characters)", 'bestdeals-utils'),
							"value" => 0,
							"min" => 0,
							"max" => 1000,
							"step" => 10,
							"type" => "spinner"
						),
						"links" => array(
							"title" => esc_html__("Swiper: Post's title as link", 'bestdeals-utils'),
							"desc" => esc_attr__("Make links from post's titles", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"crop" => array(
							"title" => esc_html__("Swiper: Crop images", 'bestdeals-utils'),
							"desc" => esc_attr__("Crop images in each slide or live it unchanged", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"autoheight" => array(
							"title" => esc_html__("Swiper: Autoheight", 'bestdeals-utils'),
							"desc" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"slides_per_view" => array(
							"title" => esc_html__("Swiper: Slides per view", 'bestdeals-utils'),
							"desc" => esc_attr__("Slides per view showed in this slider", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 1,
							"min" => 1,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"slides_space" => array(
							"title" => esc_html__("Swiper: Space between slides", 'bestdeals-utils'),
							"desc" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Swiper: Slides change interval", 'bestdeals-utils'),
							"desc" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 5000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)),
					"children" => array(
						"name" => "trx_slider_item",
						"title" => esc_html__("Slide", 'bestdeals-utils'),
						"desc" => esc_attr__("Slider item", 'bestdeals-utils'),
						"container" => false,
						"params" => array(
							"src" => array(
								"title" => esc_html__("URL (source) for image file", 'bestdeals-utils'),
								"desc" => esc_attr__("Select or upload image or write URL from other site for the current slide", 'bestdeals-utils'),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Socials
				"trx_socials" => array(
					"title" => esc_html__("Social icons", 'bestdeals-utils'),
					"desc" => esc_attr__("List of social icons (with hovers)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"type" => array(
							"title" => esc_html__("Icon's type", 'bestdeals-utils'),
							"desc" => esc_attr__("Type of the icons - images or font icons", 'bestdeals-utils'),
							"value" => bestdeals_get_theme_setting('socials_type'),
							"options" => array(
								'icons' => esc_html__('Icons', 'bestdeals-utils'),
								'images' => esc_html__('Images', 'bestdeals-utils')
							),
							"type" => "checklist"
						), 
						"size" => array(
							"title" => esc_html__("Icon's size", 'bestdeals-utils'),
							"desc" => esc_attr__("Size of the icons", 'bestdeals-utils'),
							"value" => "small",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sizes'],
							"type" => "checklist"
						), 
						"shape" => array(
							"title" => esc_html__("Icon's shape", 'bestdeals-utils'),
							"desc" => esc_attr__("Shape of the icons", 'bestdeals-utils'),
							"value" => "square",
							"options" => $BESTDEALS_GLOBALS['sc_params']['shapes'],
							"type" => "checklist"
						), 
						"socials" => array(
							"title" => esc_html__("Manual socials list", 'bestdeals-utils'),
							"desc" => esc_attr__("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebooc.com/my_profile. If empty - use socials from Theme options.", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"custom" => array(
							"title" => esc_html__("Custom socials", 'bestdeals-utils'),
							"desc" => esc_attr__("Make custom icons from inner shortcodes (prepare it on tabs)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_social_item",
						"title" => esc_html__("Custom social item", 'bestdeals-utils'),
						"desc" => esc_attr__("Custom social item: name, profile url and icon url", 'bestdeals-utils'),
						"decorate" => false,
						"container" => false,
						"params" => array(
							"name" => array(
								"title" => esc_html__("Social name", 'bestdeals-utils'),
								"desc" => esc_attr__("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"url" => array(
								"title" => esc_html__("Your profile URL", 'bestdeals-utils'),
								"desc" => esc_attr__("URL of your profile in specified social network", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"icon" => array(
								"title" => esc_html__("URL (source) for icon file", 'bestdeals-utils'),
								"desc" => esc_attr__("Select or upload image or write URL from other site for the current social icon", 'bestdeals-utils'),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							)
						)
					)
				),
			
			
			
			
				// Table
				"trx_table" => array(
					"title" => esc_html__("Table", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert a table into post (page). ", 'bestdeals-utils'),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"align" => array(
							"title" => esc_html__("Content alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Select alignment for each table cell", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"_content_" => array(
							"title" => esc_html__("Table content", 'bestdeals-utils'),
							"desc" => esc_attr__("Content, created with any table-generator", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 8,
							"value" => "Paste here table content, generated on one of many public internet resources, for example: http://www.impressivewebs.com/html-table-code-generator/ or http://html-tables.com/",
							"type" => "textarea"
						),
						"width" => bestdeals_shortcodes_width(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Tabs
				"trx_tabs" => array(
					"title" => esc_html__("Tabs", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert tabs in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Tabs style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style for tabs items", 'bestdeals-utils'),
							"value" => 1,
							"options" => bestdeals_get_list_styles(1, 2),
							"type" => "radio"
						),
						"initial" => array(
							"title" => esc_html__("Initially opened tab", 'bestdeals-utils'),
							"desc" => esc_attr__("Number of initially opened tab", 'bestdeals-utils'),
							"divider" => true,
							"value" => 1,
							"min" => 0,
							"type" => "spinner"
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", 'bestdeals-utils'),
							"desc" => esc_attr__("Use scroller to show tab content (height parameter required)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_tab",
						"title" => esc_html__("Tab", 'bestdeals-utils'),
						"desc" => esc_attr__("Tab item", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Tab title", 'bestdeals-utils'),
								"desc" => esc_attr__("Current tab title", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("Tab content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current tab content", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			


				
			
			
				// Title
				"trx_title" => array(
					"title" => esc_html__("Title", 'bestdeals-utils'),
					"desc" => esc_attr__("Create header tag (1-6 level) with many styles", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Title content", 'bestdeals-utils'),
							"desc" => esc_attr__("Title content", 'bestdeals-utils'),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"type" => array(
							"title" => esc_html__("Title type", 'bestdeals-utils'),
							"desc" => esc_attr__("Title type (header level)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "1",
							"type" => "select",
							"options" => array(
								'1' => esc_html__('Header 1', 'bestdeals-utils'),
								'2' => esc_html__('Header 2', 'bestdeals-utils'),
								'3' => esc_html__('Header 3', 'bestdeals-utils'),
								'4' => esc_html__('Header 4', 'bestdeals-utils'),
								'5' => esc_html__('Header 5', 'bestdeals-utils'),
								'6' => esc_html__('Header 6', 'bestdeals-utils'),
							)
						),
						"style" => array(
							"title" => esc_html__("Title style", 'bestdeals-utils'),
							"desc" => esc_attr__("Title style", 'bestdeals-utils'),
							"value" => "regular",
							"type" => "select",
							"options" => array(
								'regular' => esc_html__('Regular', 'bestdeals-utils'),
								'underline' => esc_html__('Underline', 'bestdeals-utils'),
								'divider' => esc_html__('Divider', 'bestdeals-utils'),
								'iconed' => esc_html__('With icon (image)', 'bestdeals-utils')
							)
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Title text alignment", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						), 
						"font_size" => array(
							"title" => esc_html__("Font_size", 'bestdeals-utils'),
							"desc" => esc_attr__("Custom font size. If empty - use theme default", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", 'bestdeals-utils'),
							"desc" => esc_attr__("Custom font weight. If empty or inherit - use theme default", 'bestdeals-utils'),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'inherit' => esc_html__('Default', 'bestdeals-utils'),
								'100' => esc_html__('Thin (100)', 'bestdeals-utils'),
								'300' => esc_html__('Light (300)', 'bestdeals-utils'),
								'400' => esc_html__('Normal (400)', 'bestdeals-utils'),
								'600' => esc_html__('Semibold (600)', 'bestdeals-utils'),
								'700' => esc_html__('Bold (700)', 'bestdeals-utils'),
								'900' => esc_html__('Black (900)', 'bestdeals-utils')
							)
						),
						"color" => array(
							"title" => esc_html__("Title color", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color for the title", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"icon" => array(
							"title" => esc_html__('Title font icon',  'bestdeals-utils'),
							"desc" => esc_attr__("Select font icon for the title from Fontello icons set (if style=iconed)",  'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"image" => array(
							"title" => esc_html__('or image icon',  'bestdeals-utils'),
							"desc" => esc_attr__("Select image icon for the title instead icon above (if style=iconed)",  'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "images",
							"size" => "small",
							"options" => $BESTDEALS_GLOBALS['sc_params']['images']
						),
						"picture" => array(
							"title" => esc_html__('or URL for image file', 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site (if style=iconed)", 'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"image_size" => array(
							"title" => esc_html__('Image (picture) size', 'bestdeals-utils'),
							"desc" => esc_attr__("Select image (picture) size (if style='iconed')", 'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "small",
							"type" => "checklist",
							"options" => array(
								'small' => esc_html__('Small', 'bestdeals-utils'),
								'medium' => esc_html__('Medium', 'bestdeals-utils'),
								'large' => esc_html__('Large', 'bestdeals-utils')
							)
						),
						"position" => array(
							"title" => esc_html__('Icon (image) position', 'bestdeals-utils'),
							"desc" => esc_attr__("Select icon (image) position (if style=iconed)", 'bestdeals-utils'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "left",
							"type" => "checklist",
							"options" => array(
								'top' => esc_html__('Top', 'bestdeals-utils'),
								'left' => esc_html__('Left', 'bestdeals-utils')
							)
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Toggles
				"trx_toggles" => array(
					"title" => esc_html__("Toggles", 'bestdeals-utils'),
					"desc" => esc_attr__("Toggles items", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Toggles style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style for display toggles", 'bestdeals-utils'),
							"value" => 1,
							"options" => bestdeals_get_list_styles(1, 2),
							"type" => "radio"
						),
						"counter" => array(
							"title" => esc_html__("Counter", 'bestdeals-utils'),
							"desc" => esc_attr__("Display counter before each toggles title", 'bestdeals-utils'),
							"value" => "off",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['on_off']
						),
						"icon_closed" => array(
							"title" => esc_html__("Icon while closed",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the closed toggles item from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"icon_opened" => array(
							"title" => esc_html__("Icon while opened",  'bestdeals-utils'),
							"desc" => esc_attr__('Select icon for the opened toggles item from Fontello icons set',  'bestdeals-utils'),
							"value" => "",
							"type" => "icons",
							"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
						),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_toggles_item",
						"title" => esc_html__("Toggles item", 'bestdeals-utils'),
						"desc" => esc_attr__("Toggles item", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Toggles item title", 'bestdeals-utils'),
								"desc" => esc_attr__("Title for current toggles item", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"open" => array(
								"title" => esc_html__("Open on show", 'bestdeals-utils'),
								"desc" => esc_attr__("Open current toggles item on show", 'bestdeals-utils'),
								"value" => "no",
								"type" => "switch",
								"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
							),
							"icon_closed" => array(
								"title" => esc_html__("Icon while closed",  'bestdeals-utils'),
								"desc" => esc_attr__('Select icon for the closed toggles item from Fontello icons set',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"icon_opened" => array(
								"title" => esc_html__("Icon while opened",  'bestdeals-utils'),
								"desc" => esc_attr__('Select icon for the opened toggles item from Fontello icons set',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"_content_" => array(
								"title" => esc_html__("Toggles item content", 'bestdeals-utils'),
								"desc" => esc_attr__("Current toggles item content", 'bestdeals-utils'),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
			
				// Tooltip
				"trx_tooltip" => array(
					"title" => esc_html__("Tooltip", 'bestdeals-utils'),
					"desc" => esc_attr__("Create tooltip for selected text", 'bestdeals-utils'),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Tooltip title (required)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Tipped content", 'bestdeals-utils'),
							"desc" => esc_attr__("Highlighted content with tooltip", 'bestdeals-utils'),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Twitter
				"trx_twitter" => array(
					"title" => esc_html__("Twitter", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert twitter feed into post (page)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"user" => array(
							"title" => esc_html__("Twitter Username", 'bestdeals-utils'),
							"desc" => esc_attr__("Your username in the twitter account. If empty - get it from Theme Options.", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"consumer_key" => array(
							"title" => esc_html__("Consumer Key", 'bestdeals-utils'),
							"desc" => esc_attr__("Consumer Key from the twitter account", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"consumer_secret" => array(
							"title" => esc_html__("Consumer Secret", 'bestdeals-utils'),
							"desc" => esc_attr__("Consumer Secret from the twitter account", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"token_key" => array(
							"title" => esc_html__("Token Key", 'bestdeals-utils'),
							"desc" => esc_attr__("Token Key from the twitter account", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"token_secret" => array(
							"title" => esc_html__("Token Secret", 'bestdeals-utils'),
							"desc" => esc_attr__("Token Secret from the twitter account", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"count" => array(
							"title" => esc_html__("Tweets number", 'bestdeals-utils'),
							"desc" => esc_attr__("Tweets number to show", 'bestdeals-utils'),
							"divider" => true,
							"value" => 3,
							"max" => 20,
							"min" => 1,
							"type" => "spinner"
						),
						"controls" => array(
							"title" => esc_html__("Show arrows", 'bestdeals-utils'),
							"desc" => esc_attr__("Show control buttons", 'bestdeals-utils'),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"interval" => array(
							"title" => esc_html__("Tweets change interval", 'bestdeals-utils'),
							"desc" => esc_attr__("Tweets change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Alignment of the tweets block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'bestdeals-utils'),
							"desc" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", 'bestdeals-utils'),
							"desc" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", 'bestdeals-utils'),
							"desc" => esc_attr__("Predefined texture style from 1 to 11. 0 - without texture.", 'bestdeals-utils'),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Video
				"trx_video" => array(
					"title" => esc_html__("Video", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert video player", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for video file", 'bestdeals-utils'),
							"desc" => esc_attr__("Select video from media library or paste URL for video file from other site", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'bestdeals-utils'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'bestdeals-utils'),
									'update' => esc_html__('Select video file', 'bestdeals-utils')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"ratio" => array(
							"title" => esc_html__("Ratio", 'bestdeals-utils'),
							"desc" => esc_attr__("Ratio of the video", 'bestdeals-utils'),
							"value" => "16:9",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								"16:9" => esc_html__("16:9", 'bestdeals-utils'),
								"4:3" => esc_html__("4:3", 'bestdeals-utils')
							)
						),
						"autoplay" => array(
							"title" => esc_html__("Autoplay video", 'bestdeals-utils'),
							"desc" => esc_attr__("Autoplay video on page load", 'bestdeals-utils'),
							"value" => "off",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['on_off']
						),
						"align" => array(
							"title" => esc_html__("Align", 'bestdeals-utils'),
							"desc" => esc_attr__("Select block alignment", 'bestdeals-utils'),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"image" => array(
							"title" => esc_html__("Cover image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for video preview", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for video background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", 'bestdeals-utils'),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_top" => array(
							"title" => esc_html__("Top offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Top offset (padding) inside background image to video block (in percent). For example: 3%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_bottom" => array(
							"title" => esc_html__("Bottom offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Bottom offset (padding) inside background image to video block (in percent). For example: 3%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_left" => array(
							"title" => esc_html__("Left offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Left offset (padding) inside background image to video block (in percent). For example: 20%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_right" => array(
							"title" => esc_html__("Right offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Right offset (padding) inside background image to video block (in percent). For example: 12%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Zoom
				"trx_zoom" => array(
					"title" => esc_html__("Zoom", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert the image with zoom/lens effect", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"effect" => array(
							"title" => esc_html__("Effect", 'bestdeals-utils'),
							"desc" => esc_attr__("Select effect to display overlapping image", 'bestdeals-utils'),
							"value" => "lens",
							"size" => "medium",
							"type" => "switch",
							"options" => array(
								"lens" => esc_html__('Lens', 'bestdeals-utils'),
								"zoom" => esc_html__('Zoom', 'bestdeals-utils')
							)
						),
						"url" => array(
							"title" => esc_html__("Main image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload main image", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"over" => array(
							"title" => esc_html__("Overlaping image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload overlaping image", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"align" => array(
							"title" => esc_html__("Float zoom", 'bestdeals-utils'),
							"desc" => esc_attr__("Float zoom to left or right side", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['float']
						), 
						"bg_image" => array(
							"title" => esc_html__("Background image", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for zoom block background. Attention! If you use background image - specify paddings below from background margins to zoom block in percents!", 'bestdeals-utils'),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_top" => array(
							"title" => esc_html__("Top offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Top offset (padding) inside background image to zoom block (in percent). For example: 3%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_bottom" => array(
							"title" => esc_html__("Bottom offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Bottom offset (padding) inside background image to zoom block (in percent). For example: 3%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_left" => array(
							"title" => esc_html__("Left offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Left offset (padding) inside background image to zoom block (in percent). For example: 20%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_right" => array(
							"title" => esc_html__("Right offset", 'bestdeals-utils'),
							"desc" => esc_attr__("Right offset (padding) inside background image to zoom block (in percent). For example: 12%", 'bestdeals-utils'),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					)
				)
			);
	
			// Woocommerce Shortcodes list
			//------------------------------------------------------------------
			if (bestdeals_exists_woocommerce()) {
				
				// WooCommerce - Cart
				$BESTDEALS_GLOBALS['shortcodes']["woocommerce_cart"] = array(
					"title" => esc_html__("Woocommerce: Cart", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show Cart page", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Checkout
				$BESTDEALS_GLOBALS['shortcodes']["woocommerce_checkout"] = array(
					"title" => esc_html__("Woocommerce: Checkout", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show Checkout page", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - My Account
				$BESTDEALS_GLOBALS['shortcodes']["woocommerce_my_account"] = array(
					"title" => esc_html__("Woocommerce: My Account", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show My Account page", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Order Tracking
				$BESTDEALS_GLOBALS['shortcodes']["woocommerce_order_tracking"] = array(
					"title" => esc_html__("Woocommerce: Order Tracking", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show Order Tracking page", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Shop Messages
				$BESTDEALS_GLOBALS['shortcodes']["shop_messages"] = array(
					"title" => esc_html__("Woocommerce: Shop Messages", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show shop messages", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Product Page
				$BESTDEALS_GLOBALS['shortcodes']["product_page"] = array(
					"title" => esc_html__("Woocommerce: Product Page", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: display single product page", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"sku" => array(
							"title" => esc_html__("SKU", 'bestdeals-utils'),
							"desc" => esc_attr__("SKU code of displayed product", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"id" => array(
							"title" => esc_html__("ID", 'bestdeals-utils'),
							"desc" => esc_attr__("ID of displayed product", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"posts_per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => "1",
							"min" => 1,
							"type" => "spinner"
						),
						"post_type" => array(
							"title" => esc_html__("Post type", 'bestdeals-utils'),
							"desc" => esc_attr__("Post type for the WP query (leave 'product')", 'bestdeals-utils'),
							"value" => "product",
							"type" => "text"
						),
						"post_status" => array(
							"title" => esc_html__("Post status", 'bestdeals-utils'),
							"desc" => esc_attr__("Display posts only with this status", 'bestdeals-utils'),
							"value" => "publish",
							"type" => "select",
							"options" => array(
								"publish" => esc_html__('Publish', 'bestdeals-utils'),
								"protected" => esc_html__('Protected', 'bestdeals-utils'),
								"private" => esc_html__('Private', 'bestdeals-utils'),
								"pending" => esc_html__('Pending', 'bestdeals-utils'),
								"draft" => esc_html__('Draft', 'bestdeals-utils')
							)
						)
					)
				);
				
				// WooCommerce - Product
				$BESTDEALS_GLOBALS['shortcodes']["product"] = array(
					"title" => esc_html__("Woocommerce: Product", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: display one product", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"sku" => array(
							"title" => esc_html__("SKU", 'bestdeals-utils'),
							"desc" => esc_attr__("SKU code of displayed product", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"id" => array(
							"title" => esc_html__("ID", 'bestdeals-utils'),
							"desc" => esc_attr__("ID of displayed product", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						)
					)
				);
				
				// WooCommerce - Best Selling Products
				$BESTDEALS_GLOBALS['shortcodes']["best_selling_products"] = array(
					"title" => esc_html__("Woocommerce: Best Selling Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show best selling products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						)
					)
				);
				
				// WooCommerce - Recent Products
				$BESTDEALS_GLOBALS['shortcodes']["recent_products"] = array(
					"title" => esc_html__("Woocommerce: Recent Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show recent products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Related Products
				$BESTDEALS_GLOBALS['shortcodes']["related_products"] = array(
					"title" => esc_html__("Woocommerce: Related Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show related products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"posts_per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						)
					)
				);
				
				// WooCommerce - Featured Products
				$BESTDEALS_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Featured Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show featured products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Top Rated Products
				$BESTDEALS_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Top Rated Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show top rated products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Sale Products
				$BESTDEALS_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Sale Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: list products on sale", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Product Category
				$BESTDEALS_GLOBALS['shortcodes']["product_category"] = array(
					"title" => esc_html__("Woocommerce: Products from category", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: list products in specified category(-ies)", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"category" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated category slugs", 'bestdeals-utils'),
							"value" => '',
							"type" => "text"
						),
						"operator" => array(
							"title" => esc_html__("Operator", 'bestdeals-utils'),
							"desc" => esc_attr__("Categories operator", 'bestdeals-utils'),
							"value" => "IN",
							"type" => "checklist",
							"size" => "medium",
							"options" => array(
								"IN" => esc_html__('IN', 'bestdeals-utils'),
								"NOT IN" => esc_html__('NOT IN', 'bestdeals-utils'),
								"AND" => esc_html__('AND', 'bestdeals-utils')
							)
						)
					)
				);
				
				// WooCommerce - Products
				$BESTDEALS_GLOBALS['shortcodes']["products"] = array(
					"title" => esc_html__("Woocommerce: Products", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: list all products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"skus" => array(
							"title" => esc_html__("SKUs", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated SKU codes of products", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"ids" => array(
							"title" => esc_html__("IDs", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated ID of products", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Product attribute
				$BESTDEALS_GLOBALS['shortcodes']["product_attribute"] = array(
					"title" => esc_html__("Woocommerce: Products by Attribute", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show products with specified attribute", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"attribute" => array(
							"title" => esc_html__("Attribute", 'bestdeals-utils'),
							"desc" => esc_attr__("Attribute name", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"filter" => array(
							"title" => esc_html__("Filter", 'bestdeals-utils'),
							"desc" => esc_attr__("Attribute value", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						)
					)
				);
				
				// WooCommerce - Products Categories
				$BESTDEALS_GLOBALS['shortcodes']["product_categories"] = array(
					"title" => esc_html__("Woocommerce: Product Categories", 'bestdeals-utils'),
					"desc" => esc_attr__("WooCommerce shortcode: show categories with products", 'bestdeals-utils'),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"number" => array(
							"title" => esc_html__("Number", 'bestdeals-utils'),
							"desc" => esc_attr__("How many categories showed", 'bestdeals-utils'),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns per row use for categories output", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'bestdeals-utils'),
								"title" => esc_html__('Title', 'bestdeals-utils')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", 'bestdeals-utils'),
							"desc" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"parent" => array(
							"title" => esc_html__("Parent", 'bestdeals-utils'),
							"desc" => esc_attr__("Parent category slug", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"ids" => array(
							"title" => esc_html__("IDs", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated ID of products", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"hide_empty" => array(
							"title" => esc_html__("Hide empty", 'bestdeals-utils'),
							"desc" => esc_attr__("Hide empty categories", 'bestdeals-utils'),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						)
					)
				);

			}
			
			do_action('bestdeals_action_shortcodes_list');

		}
	}
}
?>