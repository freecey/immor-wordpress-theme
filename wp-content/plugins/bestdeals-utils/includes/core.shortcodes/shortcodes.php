<?php
/**
 * BestDEALS Shortcodes
*/


// ---------------------------------- [trx_accordion] ---------------------------------------

/*
[trx_accordion style="1" counter="off" initial="1"]
	[trx_accordion_item title="Accordion Title 1"]Lorem ipsum dolor sit amet, consectetur adipisicing elit[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 2"]Proin dignissim commodo magna at luctus. Nam molestie justo augue, nec eleifend urna laoreet non.[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 3 with custom icons" icon_closed="icon-check" icon_opened="icon-delete"]Curabitur tristique tempus arcu a placerat.[/trx_accordion_item]
[/trx_accordion]
*/
if (!function_exists('bestdeals_sc_accordion')) {
	function bestdeals_sc_accordion($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"initial" => "1",
			"counter" => "off",
			"icon_closed" => "icon-plus",
			"icon_opened" => "icon-minus",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$style = max(1, min(2, $style));
		$initial = max(0, (int) $initial);
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_accordion_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_accordion_show_counter'] = bestdeals_param_is_on($counter);
		$BESTDEALS_GLOBALS['sc_accordion_icon_closed'] = empty($icon_closed) || bestdeals_param_is_inherit($icon_closed) ? "icon-plus" : $icon_closed;
		$BESTDEALS_GLOBALS['sc_accordion_icon_opened'] = empty($icon_opened) || bestdeals_param_is_inherit($icon_opened) ? "icon-minus" : $icon_opened;
		wp_enqueue_script('jquery-ui-accordion');
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_accordion sc_accordion_style_'.esc_attr($style)
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. (bestdeals_param_is_on($counter) ? ' sc_show_counter' : '') 
				. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. ' data-active="' . ($initial-1) . '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
				. do_shortcode($content)
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_accordion', $atts, $content);
	}
	add_shortcode('trx_accordion', 'bestdeals_sc_accordion');
}


if (!function_exists('bestdeals_sc_accordion_item')) {	
	function bestdeals_sc_accordion_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"icon_closed" => "",
			"icon_opened" => "",
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_accordion_counter']++;
		if (empty($icon_closed) || bestdeals_param_is_inherit($icon_closed)) $icon_closed = $BESTDEALS_GLOBALS['sc_accordion_icon_closed'] ? $BESTDEALS_GLOBALS['sc_accordion_icon_closed'] : "icon-plus";
		if (empty($icon_opened) || bestdeals_param_is_inherit($icon_opened)) $icon_opened = $BESTDEALS_GLOBALS['sc_accordion_icon_opened'] ? $BESTDEALS_GLOBALS['sc_accordion_icon_opened'] : "icon-minus";
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_accordion_item' 
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. ($BESTDEALS_GLOBALS['sc_accordion_counter'] % 2 == 1 ? ' odd' : ' even') 
				. ($BESTDEALS_GLOBALS['sc_accordion_counter'] == 1 ? ' first' : '') 
				. '">'
				. '<h5 class="sc_accordion_title">'
				. (!bestdeals_param_is_off($icon_closed) ? '<span class="sc_accordion_icon sc_accordion_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
				. (!bestdeals_param_is_off($icon_opened) ? '<span class="sc_accordion_icon sc_accordion_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
				. ($BESTDEALS_GLOBALS['sc_accordion_show_counter'] ? '<span class="sc_items_counter">'.($BESTDEALS_GLOBALS['sc_accordion_counter']).'</span>' : '')
				. ($title)
				. '</h5>'
				. '<div class="sc_accordion_content"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
					. do_shortcode($content) 
				. '</div>'
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_accordion_item', $atts, $content);
	}
	add_shortcode('trx_accordion_item', 'bestdeals_sc_accordion_item');
}
// ---------------------------------- [/trx_accordion] ---------------------------------------


// ---------------------------------- [trx_anchor] ---------------------------------------

						
/*
[trx_anchor id="unique_id" description="Anchor description" title="Short Caption" icon="icon-class"]
*/

if (!function_exists('bestdeals_sc_anchor')) {	
	function bestdeals_sc_anchor($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"description" => '',
			"icon" => '',
			"url" => "",
			"separator" => "no",
			// Common params
			"id" => ""
		), $atts)));
		$output = $id 
			? '<a id="'.esc_attr($id).'"'
				. ' class="sc_anchor"' 
				. ' title="' . ($title ? esc_attr($title) : '') . '"'
				. ' data-description="' . ($description ? esc_attr(bestdeals_strmacros($description)) : ''). '"'
				. ' data-icon="' . ($icon ? $icon : '') . '"' 
				. ' data-url="' . ($url ? esc_attr($url) : '') . '"' 
				. ' data-separator="' . (bestdeals_param_is_on($separator) ? 'yes' : 'no') . '"'
				. '></a>'
			: '';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_anchor', $atts, $content);
	}
	add_shortcode("trx_anchor", "bestdeals_sc_anchor");
}
// ---------------------------------- [/trx_anchor] ---------------------------------------





// ---------------------------------- [trx_audio] ---------------------------------------

/*
[trx_audio url="http://trex2.bestdeals.dnw/wp-content/uploads/2014/12/Dream-Music-Relax.mp3" image="http://trex2.bestdeals.dnw/wp-content/uploads/2014/10/post_audio.jpg" title="Insert Audio Title Here" author="Lily Hunter" controls="show" autoplay="off"]
*/

if (!function_exists('bestdeals_sc_audio')) {	
	function bestdeals_sc_audio($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"author" => "",
			"image" => "",
			"mp3" => '',
			"wav" => '',
			"src" => '',
			"url" => '',
			"align" => '',
			"controls" => "",
			"autoplay" => "",
			"frame" => "on",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => '',
			"height" => '',
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		if ($src=='' && $url=='' && isset($atts[0])) {
			$src = $atts[0];
		}
		if ($src=='') {
			if ($url) $src = $url;
			else if ($mp3) $src = $mp3;
			else if ($wav) $src = $wav;
		}
		if ($image > 0) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$data = ($title != ''  ? ' data-title="'.esc_attr($title).'"'   : '')
				. ($author != '' ? ' data-author="'.esc_attr($author).'"' : '')
				. ($image != ''  ? ' data-image="'.esc_url($image).'"'   : '')
				. ($align && $align!='none' ? ' data-align="'.esc_attr($align).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '');
		$audio = '<audio'
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_audio' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ' src="'.esc_url($src).'"'
			. (bestdeals_param_is_on($controls) ? ' controls="controls"' : '')
			. (bestdeals_param_is_on($autoplay) && is_single() ? ' autoplay="autoplay"' : '')
			. ' width="'.esc_attr($width).'" height="'.esc_attr($height).'"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. ($data)
			. '></audio>';
		if ( bestdeals_get_custom_option('substitute_audio')=='no') {
			if (bestdeals_param_is_on($frame)) {
				$audio = bestdeals_get_audio_frame($audio, $image, $s);
			}
		} else {
			if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
				$audio = bestdeals_substitute_audio($audio, false);
			}
		}
		if (bestdeals_get_theme_option('use_mediaelement')=='yes')
			wp_enqueue_script('wp-mediaelement');
		return apply_filters('bestdeals_shortcode_output', $audio, 'trx_audio', $atts, $content);
	}
	add_shortcode("trx_audio", "bestdeals_sc_audio");
}
// ---------------------------------- [/trx_audio] ---------------------------------------





// ---------------------------------- [trx_blogger] ---------------------------------------

/*
[trx_blogger id="unique_id" ids="comma_separated_list" cat="id|slug" orderby="date|views|comments" order="asc|desc" count="5" descr="0" dir="horizontal|vertical" style="regular|date|image_large|image_medium|image_small|accordion|list" border="0"]
*/
global $BESTDEALS_GLOBALS;
$BESTDEALS_GLOBALS['sc_blogger_busy'] = false;

if (!function_exists('bestdeals_sc_blogger')) {	
	function bestdeals_sc_blogger($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger(true)) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "accordion-1",
			"filters" => "no",
			"post_type" => "post",
			"ids" => "",
			"cat" => "",
			"count" => "3",
			"columns" => "",
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"only" => "no",
			"descr" => "",
			"readmore" => "",
			"loadmore" => "no",
			"location" => "default",
			"dir" => "horizontal",
			"hover" => bestdeals_get_theme_option('hover_style'),
			"hover_dir" => bestdeals_get_theme_option('hover_dir'),
			"scroll" => "no",
			"controls" => "no",
			"rating" => "no",
			"info" => "yes",
			"links" => "yes",
			"date_format" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link" => '',
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		$width  = bestdeals_prepare_css_value($width);
		$height = bestdeals_prepare_css_value($height);
	
		global $post, $BESTDEALS_GLOBALS;
	
		$BESTDEALS_GLOBALS['sc_blogger_busy'] = true;
		$BESTDEALS_GLOBALS['sc_blogger_counter'] = 0;
	
		if (empty($id)) $id = "sc_blogger_".str_replace('.', '', mt_rand());
		
		if ($style=='date' && empty($date_format)) $date_format = 'd.m+Y';
	
		if (!empty($ids)) {
			$posts = explode(',', str_replace(' ', '', $ids));
			$count = count($posts);
		}
		
		if ($descr == '') $descr = bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : ''));
	
		if (!bestdeals_param_is_off($scroll)) {
			bestdeals_enqueue_slider();
			if (empty($id)) $id = 'sc_blogger_'.str_replace('.', '', mt_rand());
		}
		
		$class = apply_filters('bestdeals_filter_blog_class',
					'sc_blogger'
					. ' layout_'.esc_attr($style)
					. ' template_'.esc_attr(bestdeals_get_template_name($style))
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
					. ' sc_blogger_' . ($dir=='vertical' ? 'vertical' : 'horizontal')
					. (bestdeals_param_is_on($scroll) && bestdeals_param_is_on($controls) ? ' sc_scroll_controls sc_scroll_controls_type_top sc_scroll_controls_'.esc_attr($dir) : '')
					. ($descr == 0 ? ' no_description' : ''),
					array('style'=>$style, 'dir'=>$dir, 'descr'=>$descr)
		);
	
		$container = apply_filters('bestdeals_filter_blog_container', bestdeals_get_template_property($style, 'container'), array('style'=>$style, 'dir'=>$dir));
		$container_start = $container_end = '';
		if (!empty($container)) {
			$container = explode('%s', $container);
			$container_start = !empty($container[0]) ? $container[0] : '';
			$container_end = !empty($container[1]) ? $container[1] : '';
		}
	
		$output = '<div'
				. ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="'.($style=='list' ? 'sc_list sc_list_style_iconed ' : '') . esc_attr($class).'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
			. ($container_start)
			. (!empty($subtitle) ? '<h6 class="sc_blogger_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
			. (!empty($title) ? '<h2 class="sc_blogger_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
			. (!empty($description) ? '<div class="sc_blogger_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
			. ($style=='list' ? '<ul class="sc_list sc_list_style_iconed">' : '')
			. ($dir=='horizontal' && $columns > 1 && bestdeals_get_template_property($style, 'need_columns') ? '<div class="columns_wrap">' : '')
			. (bestdeals_param_is_on($scroll) 
				? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($dir).' sc_slider_noresize swiper-slider-container scroll-container"'
					. ' style="'.($dir=='vertical' ? 'height:'.($height != '' ? $height : "230px").';' : 'width:'.($width != '' ? $width.';' : "100%;")).'"'
					. '>'
					. '<div class="sc_scroll_wrapper swiper-wrapper">' 
						. '<div class="sc_scroll_slide swiper-slide">' 
				: '');
	
		if (bestdeals_get_template_property($style, 'need_isotope')) {
			if (!bestdeals_param_is_off($filters))
				$output .= '<div class="isotope_filters"></div>';
			if ($columns<1) $columns = bestdeals_substr($style, -1);
			$output .= '<div class="isotope_wrap" data-columns="'.max(1, min(12, $columns)).'">';
		}
	
		$args = array(
			'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish',
			'posts_per_page' => $count,
			'ignore_sticky_posts' => true,
			'order' => $order=='asc' ? 'asc' : 'desc',
			'orderby' => 'date',
		);
	
		if ($offset > 0 && empty($ids)) {
			$args['offset'] = $offset;
		}
	
		$args = bestdeals_query_add_sort_order($args, $orderby, $order);
		if (!bestdeals_param_is_off($only)) $args = bestdeals_query_add_filters($args, $only);
		$args = bestdeals_query_add_posts_and_cats($args, $ids, $post_type, $cat);
	
		$query = new WP_Query( $args );
	
		$flt_ids = array();
	
		while ( $query->have_posts() ) { $query->the_post();
	
			$BESTDEALS_GLOBALS['sc_blogger_counter']++;
	
			$args = array(
				'layout' => $style,
				'show' => false,
				'number' => $BESTDEALS_GLOBALS['sc_blogger_counter'],
				'add_view_more' => false,
				'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
				// Additional options to layout generator
				"location" => $location,
				"descr" => $descr,
				"readmore" => $readmore,
				"loadmore" => $loadmore,
				"reviews" => bestdeals_param_is_on($rating),
				"dir" => $dir,
				"scroll" => bestdeals_param_is_on($scroll),
				"info" => bestdeals_param_is_on($info),
				"links" => bestdeals_param_is_on($links),
				"orderby" => $orderby,
				"columns_count" => $columns,
				"date_format" => $date_format,
				// Get post data
				'strip_teaser' => false,
				'content' => bestdeals_get_template_property($style, 'need_content'),
				'terms_list' => !bestdeals_param_is_off($filters) || bestdeals_get_template_property($style, 'need_terms'),
				'filters' => bestdeals_param_is_off($filters) ? '' : $filters,
				'hover' => $hover,
				'hover_dir' => $hover_dir
			);
			$post_data = bestdeals_get_post_data($args);
			$output .= bestdeals_show_post_layout($args, $post_data);
		
			if (!bestdeals_param_is_off($filters)) {
				if ($filters == 'tags') {			// Use tags as filter items
					if (!empty($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms) && is_array($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms)) {
						foreach ($post_data['post_terms'][$post_data['post_taxonomy_tags']]->terms as $tag) {
							$flt_ids[$tag->term_id] = $tag->name;
						}
					}
				}
			}
	
		}
	
		wp_reset_postdata();
	
		// Close isotope wrapper
		if (bestdeals_get_template_property($style, 'need_isotope'))
			$output .= '</div>';
	
		// Isotope filters list
		if (!bestdeals_param_is_off($filters)) {
			$filters_list = '';
			if ($filters == 'categories') {			// Use categories as filter items
				$taxonomy = bestdeals_get_taxonomy_categories_by_post_type($post_type);
				$portfolio_parent = $cat ? max(0, bestdeals_get_parent_taxonomy_by_property($cat, 'show_filters', 'yes', true, $taxonomy)) : 0;
				$args2 = array(
					'type'			=> $post_type,
					'child_of'		=> $portfolio_parent,
					'orderby'		=> 'name',
					'order'			=> 'ASC',
					'hide_empty'	=> 1,
					'hierarchical'	=> 0,
					'exclude'		=> '',
					'include'		=> '',
					'number'		=> '',
					'taxonomy'		=> $taxonomy,
					'pad_counts'	=> false
				);
				$portfolio_list = get_categories($args2);
				if (is_array($portfolio_list) && count($portfolio_list) > 0) {
					$filters_list .= '<a href="#" data-filter="*" class="theme_button active">'.esc_html__('All', 'bestdeals-utils').'</a>';
					foreach ($portfolio_list as $cat) {
						$filters_list .= '<a href="#" data-filter=".flt_'.esc_attr($cat->term_id).'" class="theme_button">'.($cat->name).'</a>';
					}
				}
			} else {								// Use tags as filter items
				if (is_array($flt_ids) && count($flt_ids) > 0) {
					$filters_list .= '<a href="#" data-filter="*" class="theme_button active">'.esc_html__('All', 'bestdeals-utils').'</a>';
					foreach ($flt_ids as $flt_id=>$flt_name) {
						$filters_list .= '<a href="#" data-filter=".flt_'.esc_attr($flt_id).'" class="theme_button">'.($flt_name).'</a>';
					}
				}
			}
			if ($filters_list) {
				$output .= '<script type="text/javascript">'
					. 'jQuery(document).ready(function () {'
						. 'jQuery("#'.esc_attr($id).' .isotope_filters").append("'.addslashes($filters_list).'");'
					. '});'
					. '</script>';
			}
		}
		$output	.= (bestdeals_param_is_on($scroll) 
				? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
					. (!bestdeals_param_is_off($controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
				: '')
			. ($dir=='horizontal' && $columns > 1 && bestdeals_get_template_property($style, 'need_columns') ? '</div>' : '')
			. ($style == 'list' ? '</ul>' : '')
			. (!empty($link) ? '<div class="sc_blogger_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
			. ($container_end)
			. '</div>';
	
		// Add template specific scripts and styles
		do_action('bestdeals_action_blog_scripts', $style);
		
		$BESTDEALS_GLOBALS['sc_blogger_busy'] = false;
	
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_blogger', $atts, $content);
	}
	add_shortcode('trx_blogger', 'bestdeals_sc_blogger');
}
// ---------------------------------- [/trx_blogger] ---------------------------------------





// ---------------------------------- [trx_br] ---------------------------------------
						
/*
[trx_br clear="left|right|both"]
*/

if (!function_exists('bestdeals_sc_br')) {	
	function bestdeals_sc_br($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			"clear" => ""
		), $atts)));
		$output = in_array($clear, array('left', 'right', 'both', 'all')) 
			? '<div class="clearfix" style="clear:' . str_replace('all', 'both', $clear) . '"></div>'
			: '<br />';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_br', $atts, $content);
	}
	add_shortcode("trx_br", "bestdeals_sc_br");
}
// ---------------------------------- [/trx_br] ---------------------------------------




// ---------------------------------- [trx_property_info] ---------------------------------------
						
/*
[trx_property_info]
*/

if (!function_exists('bestdeals_sc_property_info')) {	
	function bestdeals_sc_property_info($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		$output = '';
		
		$post_id = get_the_ID();
		$meta_values = get_post_meta($post_id, '');
		
		if ( isset($meta_values) ) {
			
			$_property_status_list = '';
			if ( isset($meta_values["property_status_list"]) ) {
				$_property_status_list = $meta_values["property_status_list"][0];
			}
			
			$_property_price_sign = '';
			$_property_price = 0;
			$_property_price_per = '';
			if ( isset($meta_values["property_price_sign"]) ) {
				$_property_price_sign = $meta_values["property_price_sign"][0];
			}
			if ( isset($meta_values["property_price"]) ) {
				$_property_price = (int) $meta_values["property_price"][0];
			}
			if ( isset($meta_values["property_price_per"]) ) {
				$_property_price_per = $meta_values["property_price_per"][0];
			}
			
			$_property_area = 0;
			if ( isset($meta_values["property_area"]) ) {
				$_property_area = $meta_values["property_area"][0];
				$_property_area_print = number_format($_property_area, 0, ',', ' ');
			}
			
			$_property_bedrooms = 0;
			if ( isset($meta_values["property_bedrooms"]) ) {
				$_property_bedrooms = (int) $meta_values["property_bedrooms"][0];
			}
			
			$_property_bathrooms = 0;
			if ( isset($meta_values["property_bathrooms"]) ) {
				$_property_bathrooms = (int) $meta_values["property_bathrooms"][0];
			}
			
			$_property_garages = 0;
			if ( isset($meta_values["property_garages"]) ) {
				$_property_garages = (int) $meta_values["property_garages"][0];
			}
			
			
			
			
			$output .= '<div class="sc_property_info_box">';
			
			if ( $_property_price > 0 ) {
				$output .= '<div class="sc_property_info_box_price">';
				if (strlen($_property_price_sign) > 0 ) {
					$output .= esc_html($_property_price_sign) . ' ';
				}
				$output .= esc_html(number_format($_property_price, 0, ',', ' '));
				if ( ($_property_status_list == 'rent') and (strlen($_property_price_per) > 0) ) {
					$output .= '<span> / '.esc_html__('per', 'bestdeals-utils').' '. esc_html($_property_price_per) .'</span>';
				}
				$output .= '</div>';
			}
			
			
			$output .= '<div class="sc_property_info_area_meta">';
			
			if ( $_property_area_print !='0' ) $output .= '<div class="sc_property_info_item_area">';
			if ( $_property_area_print !='0' ) $output .= esc_html($_property_area_print).'&nbsp;'.esc_html__('Sq Ft', 'bestdeals-utils');
			if ( $_property_area_print !='0' ) $output .= '</div>';
			
			$output .= '<div class="sc_property_item_meta">';			
			if ( $_property_bedrooms !=0 ) $output .= '<span class="bedrooms">' . esc_html($_property_bedrooms) . ' '.esc_html__('Bedrooms', 'bestdeals-utils').'</span>';
			if ( $_property_bathrooms !=0 ) $output .= '<span class="bathrooms">' . esc_html($_property_bathrooms) . ' '.esc_html__('Bathrooms', 'bestdeals-utils').'</span>';
			if ( $_property_garages !=0 ) $output .= '<span class="garages">' . esc_html($_property_garages) . ' '.esc_html__('Garages', 'bestdeals-utils').'</span>';
			$output .= '</div>';
			$output .= '</div>';
			
			
			
			
			$output .= '<div class="cL"></div>';
			
			$output .= '</div>';
			
		}
				
		
		
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_property_info', $atts, $content);
	}
	add_shortcode("trx_property_info", "bestdeals_sc_property_info");
}
// ---------------------------------- [/trx_property_info] ---------------------------------------





// ---------------------------------- [trx_property_options] ---------------------------------------
/*
[trx_property_options]
*/
if (!function_exists('bestdeals_sc_property_options')) {	
	function bestdeals_sc_property_options($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"columns" => "1",
		), $atts)));
		
		$col = (int) $columns;
		
		if ( $col > 3 ) $col = 3;
		
		$output = '';
		
		$post_id = get_the_ID();
		$meta_values = get_post_meta($post_id, '');
		
		if ( isset($meta_values["property_options"]) ) {
			$property_options = $meta_values["property_options"][0];
			
			$property_options_list = array();
			if (!empty($property_options)) {
				$property_options_list = explode(',', $property_options);
				
				if ($col > 1) {
					$output .= '<div class="columns_wrap sc_columns">';
					
					for ($i=1; $i<=$col; $i++) {
						$c[$i] = '';
						$c[$i] .= '<div class="column-1_'.$col.' sc_column_item sc_column_item_'.$i.'">';
						$c[$i] .= '<ul class="sc_list sc_list_style_iconed">';
					}
					
					$i = 1;
					foreach ($property_options_list as $value) {
						$c[$i] .= '<li class="sc_list_item">';
						$c[$i] .= '<span class="sc_list_icon icon-checkbox"></span>';
						$c[$i] .= esc_html($value);
						$c[$i] .= '</li>';
						$i = $i + 1;
						if ( $i > $col ) { $i = 1; }
					}
					
					for ($i=1; $i<=$col; $i++) {
						$c[$i] .= '</ul>';
						$c[$i] .= '</div>';
					}
					
					for ($i=1; $i<=$col; $i++) { $output .= $c[$i]; }
					
				} else {
					$output .= '<ul class="sc_list sc_list_style_iconed">';
					foreach ($property_options_list as $value) {
						$output .= '<li class="sc_list_item">';
						$output .= '<span class="sc_list_icon icon-checkbox"></span>';
						$output .= esc_html($value);
						$output .= '</li>';
					}
					$output .= '</ul>';
				}
				
				if ($col > 1) {$output .= '</div>';}
				
			}
		}
		
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_property_options', $atts, $content);
	}
	add_shortcode("trx_property_options", "bestdeals_sc_property_options");
}
// ---------------------------------- [/trx_property_options] ---------------------------------------











// ---------------------------------- [trx_property_search] ---------------------------------------
						
/*
[trx_property_search status="none|rent|sale"]
*/

if (!function_exists('bestdeals_sc_property_search')) {	
	function bestdeals_sc_property_search($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			"status" => "none"
		), $atts)));
		
		$url = ''; $id = '';
		$page = 'property';
		$url = bestdeals_property_get_stream_page_link($url, $page);
		$pageId = bestdeals_property_get_stream_page_id($id, $page);

		$list_property_location = bestdeals_get_list_property_location();
		$list_property_type_single = bestdeals_get_list_property_type_single();
		
		$output = '';
		
		$output .= '<div class="sc_property_search_box">';
				
		$output .= '<form method="get" action="' . esc_url($url) . '">';
		
		$output .= '<div class="sc_property_search_content">';
		
		
		/* ***** Keyword ***** */
		$_printSearchKeyword = '';
		if ( isset($_GET['search_keyword']) ) {
			$_searchKeyword = htmlspecialchars(trim($_GET['search_keyword']));
			if ( strlen($_searchKeyword) > 2 ) {
				$_printSearchKeyword = $_searchKeyword;
			} else {
				$_printSearchKeyword = '';
			}
		}
		$output .= '<label class="sc_property_search_keyword" for="sc_property_search_keyword"><span>' . esc_html__('Keyword', 'bestdeals-utils') . '</span><br>';
		$output .= '<input id="sc_property_search_keyword" type="text" name="search_keyword" placeholder="' . esc_attr__('Any', 'bestdeals-utils') . '" value="' . esc_html($_printSearchKeyword) . '" >';
		$output .= '</label>';
		
		
		/* ***** Location ***** */
		$_print = ''; $_searchLocation = '';
		if ( isset($_GET['search_location']) ) {
			$_searchLocation = htmlspecialchars(trim($_GET['search_location']));
		}
		$output .= '<label class="sc_property_search_location" for="sc_property_search_location"><span>' . esc_html__('Location', 'bestdeals-utils') . '</span><br>';
		$output .= '<select id="sc_property_search_location" name="search_location">';
		$output .= '<option value="-1">'.esc_html__('Any', 'bestdeals-utils').'</option>';
		foreach ($list_property_location as $key => $value) {
			if ( $_searchLocation == $value ) {
				$_print = 'selected';
			}
			$output .= '<option '. esc_html($_print).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
			$_print = '';
		}
		$output .= '</select>';
		$output .= '</label>';
		
		
		/* ***** Property type ***** */
		$_print = ''; $_searchTypeSingle = '';
		if ( isset($_GET['search_type_single']) ) {
			$_searchTypeSingle = htmlspecialchars(trim($_GET['search_type_single']));
		}
		$output .= '<label class="sc_property_search_type_single" for="sc_property_search_type_single"><span>' . esc_html__('Property type', 'bestdeals-utils') . '</span><br>';
		$output .= '<select id="sc_property_search_type_single" name="search_type_single">';
		$output .= '<option value="-1">'.esc_html__('Any', 'bestdeals-utils').'</option>';
		foreach ($list_property_type_single as $key => $value) {
			if ( $_searchTypeSingle == $key ) {
				$_print = 'selected';
			}
			$output .= '<option '. esc_html($_print).' value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
			$_print = '';
		}
		$output .= '</select>';
		$output .= '</label>';
		
		
		/* ***** Min bedrooms ***** */
		$_print = ''; $_searchBedrooms = -1;
		if ( isset($_GET['search_bedrooms']) ) {
			$_searchBedrooms = (int) htmlspecialchars(trim($_GET['search_bedrooms']));
		}
		$output .= '<label class="sc_property_search_bedrooms" for="sc_property_search_bedrooms"><span>' . esc_html__('Min bedrooms', 'bestdeals-utils') . '</span><br>';
		$output .= '<select id="sc_property_search_bedrooms" name="search_bedrooms">';
		$output .= '<option value="-1">'.esc_html__('Any', 'bestdeals-utils').'</option>';
		for ($i=0; $i<=10; $i++) {
			if ( $_searchBedrooms == $i ) {
				$_print = 'selected';
			}
			$output .= '<option '. esc_html($_print).' value="' . esc_html($i) . '">' . esc_html($i) . '</option>';
			$_print = '';
		}
		$output .= '</select>';
		$output .= '</label>';
		
		
		/* ***** Min bathrooms ***** */
		$_print = ''; $_searchBathrooms = -1;
		if ( isset($_GET['search_bathrooms']) ) {
			$_searchBathrooms = (int) htmlspecialchars(trim($_GET['search_bathrooms']));
		}
		$output .= '<label class="sc_property_search_bathrooms" for="sc_property_search_bathrooms"><span>' . esc_html__('Min bathrooms', 'bestdeals-utils') . '</span><br>';
		$output .= '<select id="sc_property_search_bathrooms" name="search_bathrooms">';
		$output .= '<option value="-1">'.esc_html__('Any', 'bestdeals-utils').'</option>';
		for ($i=0; $i<=10; $i++) {
			if ( $_searchBathrooms == $i ) {
				$_print = 'selected';
			}
			$output .= '<option '. esc_html($_print).' value="' . esc_html($i) . '">' . esc_html($i) . '</option>';
			$_print = '';
		}
		$output .= '</select>';
		$output .= '</label>';
		
		
		/* ***** Min Area (Sq Ft) ***** */
		$_searchAreaMin = 0;
		if ( isset($_GET['search_area_min']) ) {
			$_searchAreaMin = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_area_min'])));
		}
		if ( $_searchAreaMin > 0 ) {
			$_printSearchAreaMin = $_searchAreaMin;
		} else {
			$_printSearchAreaMin = '';
		}
		$output .= '<label class="sc_property_search_area_min" for="sc_property_search_area_min"><span>' . esc_html__('Min Area (Sq Ft)', 'bestdeals-utils') . '</span><br>';
		$output .= '<input id="sc_property_search_area_min" type="text" name="search_area_min" placeholder="' . esc_attr__('Any', 'bestdeals-utils') . '" value="' . esc_html($_printSearchAreaMin) . '" >';
		$output .= '</label>';
		
		
		/* ***** Max Area (Sq Ft) ***** */
		$_searchAreaMax = 0;
		if ( isset($_GET['search_area_max']) ) {
			$_searchAreaMax = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_area_max'])));
		}
		if ( $_searchAreaMax > 0 ) {
			$_printSearchAreaMax = $_searchAreaMax;
		} else {
			$_printSearchAreaMax = '';
		}
		$output .= '<label class="sc_property_search_area_max" for="sc_property_search_area_max"><span>' . esc_html__('Max Area (Sq Ft)', 'bestdeals-utils') . '</span><br>';
		$output .= '<input id="sc_property_search_area_max" type="text" name="search_area_max" placeholder="' . esc_attr__('Any', 'bestdeals-utils') . '" value="' . esc_html($_printSearchAreaMax) . '" >';
		$output .= '</label>';
		
		
		/* ***** Min Price ***** */
		$_searchPriceMin = 0;
		if ( isset($_GET['search_price_min']) ) {
			$_searchPriceMin = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_price_min'])));
		}
		if ( $_searchPriceMin > 0 ) {
			$_printSearchPriceMin = $_searchPriceMin;
		} else {
			$_printSearchPriceMin = '';
		}
		$output .= '<label class="sc_property_search_price_min" for="sc_property_search_price_min"><span>' . esc_html__('Min Price', 'bestdeals-utils') . '</span><br>';
		$output .= '<input id="sc_property_search_price_min" type="text" name="search_price_min" placeholder="' . esc_attr__('Any', 'bestdeals-utils') . '" value="' . esc_html($_printSearchPriceMin) . '" >';
		$output .= '</label>';
		
		
		
		
		/* ***** Max Price ***** */
		$_searchPriceMax = 0;
		if ( isset($_GET['search_price_max']) ) {
			$_searchPriceMax = (int) str_replace(" ", "", htmlspecialchars(trim($_GET['search_price_max'])));
		}
		if ( $_searchPriceMax > 0 ) {
			$_printSearchPriceMax = $_searchPriceMax;
		} else {
			$_printSearchPriceMax = '';
		}
		$output .= '<label class="sc_property_search_price_max" for="sc_property_search_price_max"><span>' . esc_html__('Max Price', 'bestdeals-utils') . '</span><br>';
		$output .= '<input id="sc_property_search_price_max" type="text" name="search_price_max" placeholder="' . esc_attr__('Any', 'bestdeals-utils') . '" value="' . esc_html($_printSearchPriceMax) . '" >';
		$output .= '</label>';
		
		
		
		
		if ( $status == 'sale' ) {
			$statusText = esc_html__('Sale', 'bestdeals-utils');
		} elseif ( $status == 'rent' ) {
			$statusText = esc_html__('Rent', 'bestdeals-utils');
		}
		
		
		if ( $status == 'sale' ) {
			$output.= '<input type="hidden" name="search_status" value="sale">';
		}
		if ( $status == 'sale' ) {
			$output.= '<div class="sc_property_all_deals"><a href="' . esc_url($url) . '?search_status='.esc_html($status).'&amp;page_id='.  esc_html($pageId).'">' . esc_html__('View all deals for ', 'bestdeals-utils') . esc_html($statusText) . '</a></div>';
		}
		
		if ( $status == 'rent' ) {
			$output.= '<input type="hidden" name="search_status" value="rent">';
		}
		if ( $status == 'rent' ) {
			$output.= '<div class="sc_property_all_deals"><a href="' . esc_url($url) . '?search_status='.esc_html($status).'&amp;page_id='.  esc_html($pageId).'">' . esc_html__('View all deals for ', 'bestdeals-utils') . esc_html($statusText) . '</a></div>';
		}
		$output.= '<input type="hidden" name="page_id" value="'.  esc_html($pageId).'">';
		
		$output .= '</div>';
		
		$output .= '<input type="submit" class="sc_button_size_medium" name="property_search" value="Search">';
		
		$output .= '</form>';
		
		$output .= '</div>';
		
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_property_search', $atts, $content);
	}
	add_shortcode("trx_property_search", "bestdeals_sc_property_search");
}
// ---------------------------------- [/trx_property_search] ---------------------------------------







// ---------------------------------- [trx_button] ---------------------------------------

/*
[trx_button id="unique_id" type="square|round" fullsize="0|1" style="global|light|dark" size="mini|medium|big|huge|banner" icon="icon-name" link='#' target='']Button caption[/trx_button]
*/

if (!function_exists('bestdeals_sc_button')) {	
	function bestdeals_sc_button($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "square",
			"style" => "filled",
			"size" => "small",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"link" => "",
			"target" => "",
			"align" => "",
			"rel" => "",
			"popup" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . '; border-color:'. esc_attr($bg_color) .';' : '');
		if (bestdeals_param_is_on($popup)) bestdeals_enqueue_popup('magnific');
		$output = '<a href="' . (empty($link) ? '#' : $link) . '"'
			. (!empty($target) ? ' target="'.esc_attr($target).'"' : '')
			. (!empty($rel) ? ' rel="'.esc_attr($rel).'"' : '')
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ' class="sc_button sc_button_' . esc_attr($type) 
					. ' sc_button_style_' . esc_attr($style) 
					. ' sc_button_size_' . esc_attr($size)
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($icon!='' ? '  sc_button_iconed '. esc_attr($icon) : '') 
					. (bestdeals_param_is_on($popup) ? '  sc_popup_link' : '') 
					. '"'
			. ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
			. do_shortcode($content)
			. '</a>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_button', $atts, $content);
	}
	add_shortcode('trx_button', 'bestdeals_sc_button');
}
// ---------------------------------- [/trx_button] ---------------------------------------






// ---------------------------------- [trx_call_to_action] ---------------------------------------

/*
[trx_call_to_action id="unique_id" style="1|2" align="left|center|right"]
	[inner shortcodes]
[/trx_call_to_action]
*/

if (!function_exists('bestdeals_sc_call_to_action')) {	
	function bestdeals_sc_call_to_action($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"align" => "center",
			"custom" => "no",
			"accent" => "no",
			"image" => "",
			"video" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link" => '',
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link2" => '',
			"link2_caption" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if (empty($id)) $id = "sc_call_to_action_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
	
		if ($image > 0) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		if (!empty($image)) {
			$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => 'excerpt'));
			$image = !empty($video) 
				? bestdeals_get_resized_image_url($image, $thumb_sizes['w'], $thumb_sizes['h']) 
				: bestdeals_get_resized_image_tag($image, $thumb_sizes['w'], $thumb_sizes['h']);
		}
	
		if (!empty($video)) {
			$video = '<video' . ($id ? ' id="' . esc_attr($id.'_video') . '"' : '') 
				. ' class="sc_video"'
				. ' src="' . esc_url(bestdeals_get_video_player_url($video)) . '"'
				. ' width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"' 
				. ' data-width="' . esc_attr($width) . '" data-height="' . esc_attr($height) . '"' 
				. ' data-ratio="16:9"'
				. ($image ? ' poster="'.esc_attr($image).'" data-image="'.esc_attr($image).'"' : '') 
				. ' controls="controls" loop="loop"'
				. '>'
				. '</video>';
			if (bestdeals_get_custom_option('substitute_video')=='no') {
				$video = bestdeals_get_video_frame($video, $image, '', '');
			} else {
				if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
					$video = bestdeals_substitute_video($video, $width, $height, false);
				}
			}
			if (bestdeals_get_theme_option('use_mediaelement')=='yes')
				wp_enqueue_script('wp-mediaelement');
		}
		
		$css = bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		
		$content = do_shortcode($content);
		
		$featured = ($style==1 && (!empty($content) || !empty($image) || !empty($video))
					? '<div class="sc_call_to_action_featured column-1_2">'
						. (!empty($content) 
							? $content 
							: (!empty($video) 
								? $video 
								: $image)
							)
						. '</div>'
					: '');
	
		$need_columns = ($featured || $style==2) && !in_array($align, array('center', 'none'))
							? ($style==2 ? 4 : 2)
							: 0;
		
		$buttons = (!empty($link) || !empty($link2) 
						? '<div class="sc_call_to_action_buttons sc_item_buttons'.($need_columns && $style==2 ? ' column-1_'.esc_attr($need_columns) : '').'">'
							. (!empty($link) 
								? '<div class="sc_call_to_action_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' 
								: '')
							. (!empty($link2) 
								? '<div class="sc_call_to_action_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link2).'" icon="icon-right"]'.esc_html($link2_caption).'[/trx_button]').'</div>' 
								: '')
							. '</div>'
						: '');
	
		
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_call_to_action'
					. (bestdeals_param_is_on($accent) ? ' sc_call_to_action_accented' : '')
					. ' sc_call_to_action_style_' . esc_attr($style) 
					. ' sc_call_to_action_align_'.esc_attr($align)
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
				. (bestdeals_param_is_on($accent) ? '<div class="content_wrap">' : '')
				. ($need_columns ? '<div class="columns_wrap">' : '')
				. ($align!='right' ? $featured : '')
				. ($style==2 && $align=='right' ? $buttons : '')
				. '<div class="sc_call_to_action_info'.($need_columns ? ' column-'.esc_attr($need_columns-1).'_'.esc_attr($need_columns) : '').'">'
					. (!empty($subtitle) ? '<h6 class="sc_call_to_action_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
					. (!empty($title) ? '<h2 class="sc_call_to_action_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
					. (!empty($description) ? '<div class="sc_call_to_action_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
					. ($style==1 ? $buttons : '')
				. '</div>'
				. ($style==2 && $align!='right' ? $buttons : '')
				. ($align=='right' ? $featured : '')
				. ($need_columns ? '</div>' : '')
				. (bestdeals_param_is_on($accent) ? '</div>' : '')
			. '</div>';
	
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_call_to_action', $atts, $content);
	}
	add_shortcode('trx_call_to_action', 'bestdeals_sc_call_to_action');
}
// ---------------------------------- [/trx_call_to_action] ---------------------------------------





// ---------------------------------- [trx_chat] ---------------------------------------

/*
[trx_chat id="unique_id" link="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_chat]
[trx_chat id="unique_id" link="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_chat]
...
*/

if (!function_exists('bestdeals_sc_chat')) {	
	function bestdeals_sc_chat($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"photo" => "",
			"title" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		$title = $title=='' ? $link : $title;
		if (!empty($photo)) {
			if ($photo > 0) {
				$attach = wp_get_attachment_image_src( $photo, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$photo = $attach[0];
			}
			$photo = bestdeals_get_resized_image_tag($photo, 75, 75);
		}
		$content = do_shortcode($content);
		if (bestdeals_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_chat' . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css ? ' style="'.esc_attr($css).'"' : '') 
				. '>'
					. '<div class="sc_chat_inner">'
						. ($photo ? '<div class="sc_chat_avatar">'.($photo).'</div>' : '')
						. ($title == '' ? '' : ('<div class="sc_chat_title">' . ($link!='' ? '<a href="'.esc_url($link).'">' : '') . ($title) . ($link!='' ? '</a>' : '') . '</div>'))
						. '<div class="sc_chat_content">'.($content).'</div>'
					. '</div>'
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_chat', $atts, $content);
	}
	add_shortcode('trx_chat', 'bestdeals_sc_chat');
}
// ---------------------------------- [/trx_chat] ---------------------------------------




// ---------------------------------- [trx_columns] ---------------------------------------

/*
[trx_columns id="unique_id" count="number"]
	[trx_column_item id="unique_id" span="2 - number_columns"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta, odio arcu vut natoque dolor ut, enim etiam vut augue. Ac augue amet quis integer ut dictumst? Elit, augue vut egestas! Tristique phasellus cursus egestas a nec a! Sociis et? Augue velit natoque, amet, augue. Vel eu diam, facilisis arcu.[/trx_column_item]
	[trx_column_item]A pulvinar ut, parturient enim porta ut sed, mus amet nunc, in. Magna eros hac montes, et velit. Odio aliquam phasellus enim platea amet. Turpis dictumst ultrices, rhoncus aenean pulvinar? Mus sed rhoncus et cras egestas, non etiam a? Montes? Ac aliquam in nec nisi amet eros! Facilisis! Scelerisque in.[/trx_column_item]
	[trx_column_item]Duis sociis, elit odio dapibus nec, dignissim purus est magna integer eu porta sagittis ut, pid rhoncus facilisis porttitor porta, et, urna parturient mid augue a, in sit arcu augue, sit lectus, natoque montes odio, enim. Nec purus, cras tincidunt rhoncus proin lacus porttitor rhoncus, vut enim habitasse cum magna.[/trx_column_item]
	[trx_column_item]Nec purus, cras tincidunt rhoncus proin lacus porttitor rhoncus, vut enim habitasse cum magna. Duis sociis, elit odio dapibus nec, dignissim purus est magna integer eu porta sagittis ut, pid rhoncus facilisis porttitor porta, et, urna parturient mid augue a, in sit arcu augue, sit lectus, natoque montes odio, enim.[/trx_column_item]
[/trx_columns]
*/

if (!function_exists('bestdeals_sc_columns')) {	
	function bestdeals_sc_columns($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"count" => "2",
			"fluid" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		$count = max(1, min(12, (int) $count));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_columns_counter'] = 1;
		$BESTDEALS_GLOBALS['sc_columns_after_span2'] = false;
		$BESTDEALS_GLOBALS['sc_columns_after_span3'] = false;
		$BESTDEALS_GLOBALS['sc_columns_after_span4'] = false;
		$BESTDEALS_GLOBALS['sc_columns_count'] = $count;
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="columns_wrap sc_columns'
					. ' columns_' . (bestdeals_param_is_on($fluid) ? 'fluid' : 'nofluid') 
					. ' sc_columns_count_' . esc_attr($count)
					. (!empty($class) ? ' '.esc_attr($class) : '') 
				. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
					. do_shortcode($content)
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_columns', $atts, $content);
	}
	add_shortcode('trx_columns', 'bestdeals_sc_columns');
}


if (!function_exists('bestdeals_sc_column_item')) {	
	function bestdeals_sc_column_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"span" => "1",
			"align" => "",
			"color" => "",
			"bg_color" => "",
			"bg_image" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => ""
		), $atts)));
		$css .= ($align !== '' ? 'text-align:' . esc_attr($align) . ';' : '') 
			. ($color !== '' ? 'color:' . esc_attr($color) . ';' : '');
		$span = max(1, min(11, (int) $span));
		if (!empty($bg_image)) {
			if ($bg_image > 0) {
				$attach = wp_get_attachment_image_src( $bg_image, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$bg_image = $attach[0];
			}
		}
		global $BESTDEALS_GLOBALS;
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="column-'.($span > 1 ? esc_attr($span) : 1).'_'.esc_attr($BESTDEALS_GLOBALS['sc_columns_count']).' sc_column_item sc_column_item_'.esc_attr($BESTDEALS_GLOBALS['sc_columns_counter']) 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($BESTDEALS_GLOBALS['sc_columns_counter'] % 2 == 1 ? ' odd' : ' even') 
					. ($BESTDEALS_GLOBALS['sc_columns_counter'] == 1 ? ' first' : '') 
					. ($span > 1 ? ' span_'.esc_attr($span) : '') 
					. ($BESTDEALS_GLOBALS['sc_columns_after_span2'] ? ' after_span_2' : '') 
					. ($BESTDEALS_GLOBALS['sc_columns_after_span3'] ? ' after_span_3' : '') 
					. ($BESTDEALS_GLOBALS['sc_columns_after_span4'] ? ' after_span_4' : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. '>'
					. ($bg_color!=='' || $bg_image !== '' ? '<div class="sc_column_item_inner" style="'
							. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
							. ($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
							. '">' : '')
						. do_shortcode($content)
					. ($bg_color!=='' || $bg_image !== '' ? '</div>' : '')
					. '</div>';
		$BESTDEALS_GLOBALS['sc_columns_counter'] += $span;
		$BESTDEALS_GLOBALS['sc_columns_after_span2'] = $span==2;
		$BESTDEALS_GLOBALS['sc_columns_after_span3'] = $span==3;
		$BESTDEALS_GLOBALS['sc_columns_after_span4'] = $span==4;
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_column_item', $atts, $content);
	}
	add_shortcode('trx_column_item', 'bestdeals_sc_column_item');
}
// ---------------------------------- [/trx_columns] ---------------------------------------





// ---------------------------------- [trx_contact_form] ---------------------------------------

/*
[trx_contact_form id="unique_id" title="Contact Form" description="Mauris aliquam habitasse magna."]
*/

if (!function_exists('bestdeals_sc_contact_form')) {	
	function bestdeals_sc_contact_form($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"custom" => "no",
			"action" => "",
			"align" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if (empty($id)) $id = "sc_contact_form_".str_replace('.', '', mt_rand());
		$css = bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width);
		$trx_addons_privacy = trx_utils_get_privacy_text();

		bestdeals_enqueue_messages();	// Load core messages
	
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_contact_form_id'] = $id;
		$BESTDEALS_GLOBALS['sc_contact_form_counter'] = 0;
	
		$content = do_shortcode($content);
	
		$output = '<div ' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '')
					. ' class="sc_contact_form_wrap'
					. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
					. '">'
			.'<div ' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_contact_form'
					. ' sc_contact_form_'.($content != '' && bestdeals_param_is_on($custom) ? 'custom' : 'standard') 
					. ' sc_contact_form_style_'.($style) 
					. (!empty($align) && !bestdeals_param_is_off($align) ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
					. (!empty($subtitle) 
						? '<h6 class="sc_contact_form_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' 
						: '')
					. (!empty($title) 
						? '<h2 class="sc_contact_form_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' 
						: '')
					. (!empty($description) 
						? '<div class="sc_contact_form_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . ($style == 1 ? do_shortcode('[trx_socials size="tiny" shape="round"][/trx_socials]') : '') . '</div>' 
						: '');
		
		if ($style == 2) {
			$address_1 = bestdeals_get_theme_option('contact_address_1');
			$address_2 = bestdeals_get_theme_option('contact_address_2');
			$phone = bestdeals_get_theme_option('contact_phone');
			$fax = bestdeals_get_theme_option('contact_fax');
			$email = bestdeals_get_theme_option('contact_email');
			$open_hours = bestdeals_get_theme_option('contact_open_hours');
			$output .= '<div class="sc_columns columns_wrap">'
				. '<div class="sc_contact_form_address column-1_3">'
				. '<div class="sc_contact_form_address_field">'
					. '<span class="sc_contact_form_address_label">'.esc_html__('Address', 'bestdeals-utils').'</span>'
					. '<span class="sc_contact_form_address_data">'.trim($address_1).(!empty($address_1) && !empty($address_2) ? ', ' : '').$address_2.'</span>'
				. '</div>'
				. '<div class="sc_contact_form_address_field">'
					. '<span class="sc_contact_form_address_label">'.esc_html__('We are open', 'bestdeals-utils').'</span>'
					. '<span class="sc_contact_form_address_data">'.trim($open_hours).'</span>'
				. '</div>'
				. '<div class="sc_contact_form_address_field">'
					. '<span class="sc_contact_form_address_label">'.esc_html__('Phone', 'bestdeals-utils').'</span>'
					. '<span class="sc_contact_form_address_data">'.trim($phone).(!empty($phone) && !empty($fax) ? ', ' : '').$fax.'</span>'
				. '</div>'
				. '<div class="sc_contact_form_address_field">'
					. '<span class="sc_contact_form_address_label">'.esc_html__('E-mail', 'bestdeals-utils').'</span>'
					. '<span class="sc_contact_form_address_data">'.trim($email).'</span>'
				. '</div>'
				. do_shortcode('[trx_socials size="tiny" shape="round"][/trx_socials]')
				. '</div>'
				. '<div class="sc_contact_form_fields column-2_3">'
				;
		}
		
		$output .= '<form' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' data-formtype="'.($content ? 'custom' : 'contact').'" method="post" action="' . esc_url(!empty($action) ? $action : admin_url('admin-ajax.php')) . '">'
					. ($content != '' && bestdeals_param_is_on($custom)
						? $content 
						: '<div class="sc_contact_form_info">'
								.'<div class="sc_contact_form_item sc_contact_form_field label_over"><label class="required" for="sc_contact_form_username">' . esc_html__('Name', 'bestdeals-utils') . '</label><input id="sc_contact_form_username" type="text" name="username" placeholder="' . esc_html__('Name *', 'bestdeals-utils') . '"></div>'
								. '<div class="sc_contact_form_item sc_contact_form_field label_over"><label class="required" for="sc_contact_form_email">' . esc_html__('E-mail', 'bestdeals-utils') . '</label><input id="sc_contact_form_email" type="text" name="email" placeholder="' . esc_html__('E-mail *', 'bestdeals-utils') . '"></div>'
								.'<div class="sc_contact_form_item sc_contact_form_field label_over"><label class="required" for="sc_contact_form_subj">' . esc_html__('Subject', 'bestdeals-utils') . '</label><input id="sc_contact_form_subj" type="text" name="subject" placeholder="' . esc_html__('Subject', 'bestdeals-utils') . '"></div>'
							.'</div>'
							.'<div class="sc_contact_form_item sc_contact_form_message label_over"><label class="required" for="sc_contact_form_message">' . esc_html__('Message', 'bestdeals-utils') . '</label><textarea id="sc_contact_form_message" name="message" placeholder="' . esc_html__('Message', 'bestdeals-utils') . '"></textarea>'
							. (
							(!empty($trx_addons_privacy)) ?
								'<div class="sc_emailer_checkbox trx_utils_form_field_agree">'
								.'<input type="checkbox" value="1" id="i_agree_privacy_policy_registration" name="i_agree_privacy_policy"> <label for="i_agree_privacy_policy_registration">' . wp_kses_post($trx_addons_privacy) . '</label>'
								. '</div>'
								: ''
							)
							.'<div class="sc_contact_form_item sc_contact_form_button"><button class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium">'.esc_html__('Send Message', 'bestdeals-utils').'</button></div></div>'
							. '<div class=cL></div>'
						)
					. '<div class="result sc_infobox"></div>'
				. '</form>';
		
		if ($style==2) {
			$output .= '</div></div>';
		}
	
		$output .= '</div>'
				. '</div>';
	
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_contact_form', $atts, $content);
	}
	add_shortcode("trx_contact_form", "bestdeals_sc_contact_form");
}

if (!function_exists('bestdeals_sc_contact_form_item')) {	
	function bestdeals_sc_contact_form_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"type" => "text",
			"name" => "",
			"value" => "",
			"options" => "",
			"align" => "",
			"label" => "",
			"label_position" => "top",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_contact_form_counter']++;
	
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		if (empty($id)) $id = ($BESTDEALS_GLOBALS['sc_contact_form_id']).'_'.($BESTDEALS_GLOBALS['sc_contact_form_counter']);
	
		$label = $type!='button' && $type!='submit' && $label ? '<label for="' . esc_attr($id) . '">' . esc_attr($label) . '</label>' : $label;
	
		// Open field container
		$output = '<div class="sc_contact_form_item sc_contact_form_item_'.esc_attr($type)
						.' sc_contact_form_'.($type == 'textarea' ? 'message' : ($type == 'button' || $type == 'submit' ? 'button' : 'field'))
						.' label_'.esc_attr($label_position)
						.($class ? ' '.esc_attr($class) : '')
						.($align && $align!='none' ? ' align'.esc_attr($align) : '')
					.'"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. '>';
		
		// Label top or left
		if ($type!='button' && $type!='submit' && ($label_position=='top' || $label_position=='left'))
			$output .= $label;
		// Field
		if ($type == 'textarea')
			$output .= '<textarea id="' . esc_attr($id) . '" name="' . esc_attr($name ? $name : $id) . '">' . esc_attr($value) . '</textarea>';
		else if ($type=='button' || $type=='submit')
			$output .= '<button id="' . esc_attr($id) . '">'.($label ? $label : $value).'</button>';
		else if ($type=='radio' || $type=='checkbox') {
			if (!empty($options)) {
				$options = explode('|', $options);
				if (!empty($options)) {
					$i = 0;
					foreach ($options as $v) {
						$i++;
						$parts = explode('=', $v);
						if (count($parts)==1) $parts[1] = $parts[0];
						$output .= '<div class="sc_contact_form_element">'
										. '<input type="'.esc_attr($type) . '"'
											. ' id="' . esc_attr($id.($i>1 ? '_'.$i : '')) . '"'
											. ' name="' . esc_attr($name ? $name : $id) . (count($options) > 1 && $type=='checkbox' ? '[]' : '') . '"'
											. ' value="' . esc_attr(trim(chop($parts[0]))) . '"' 
											. (in_array($parts[0], explode(',', $value)) ? ' checked="checked"' : '') 
										. '>'
										. '<label for="' . esc_attr($id.($i>1 ? '_'.$i : '')) . '">' . trim(chop($parts[1])) . '</label>'
									. '</div>';
					}
				}
			}
		} else if ($type=='select') {
			if (!empty($options)) {
				$options = explode('|', $options);
				if (!empty($options)) {
					$output .= '<div class="sc_contact_form_select_container">'
						. '<select id="' . esc_attr($id) . '" name="' . esc_attr($name ? $name : $id) . '">';
					foreach ($options as $v) {
						$parts = explode('=', $v);
						if (count($parts)==1) $parts[1] = $parts[0];
						$output .= '<option'
										. ' value="' . esc_attr(trim(chop($parts[0]))) . '"' 
										. (in_array($parts[0], explode(',', $value)) ? ' selected="selected"' : '') 
									. '>'
									. trim(chop($parts[1]))
									. '</option>';
					}
					$output .= '</select>'
							. '</div>';
				}
			}
		} else
			$output .= '<input type="'.esc_attr($type ? $type : 'text').'" id="' . esc_attr($id) . '" name="' . esc_attr($name ? $name : $id) . '" value="' . esc_attr($value) . '">';
		// Label bottom
		if ($type!='button' && $type!='submit' && $label_position=='bottom')
			$output .= $label;
		
		// Close field container
		$output .= '</div>';
	
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_form_item', $atts, $content);
	}
	add_shortcode('trx_form_item', 'bestdeals_sc_contact_form_item');
}

// AJAX Callback: Send contact form data
if ( !function_exists( 'bestdeals_sc_contact_form_send' ) ) {
	function bestdeals_sc_contact_form_send() {
		global $_REQUEST;
	
		if ( !wp_verify_nonce( $_REQUEST['nonce'], 'ajax_nonce' ) )
			die();
	
		$response = array('error'=>'');
		if (!($contact_email = bestdeals_get_theme_option('contact_email')) && !($contact_email = bestdeals_get_theme_option('admin_email'))) 
			$response['error'] = esc_html__('Unknown admin email!', 'bestdeals-utils');
		else {
			$type = bestdeals_substr($_REQUEST['type'], 0, 7);
			parse_str($_POST['data'], $post_data);

			if ($type=='contact') {
				$user_name	= bestdeals_strshort($post_data['username'],	100);
				$user_email	= bestdeals_strshort($post_data['email'],	100);
				$user_subj	= bestdeals_strshort($post_data['subject'],	100);
				$user_msg	= bestdeals_strshort($post_data['message'],	bestdeals_get_theme_option('message_maxlength_contacts'));
		
				$subj = sprintf(esc_attr__('Site %s - Contact form message from %s', 'bestdeals-utils'), get_bloginfo('site_name'), $user_name);
				$msg = "\n".esc_html__('Name:', 'bestdeals-utils')   .' '.esc_html($user_name)
					.  "\n".esc_html__('E-mail:', 'bestdeals-utils') .' '.esc_html($user_email)
					.  "\n".esc_html__('Subject:', 'bestdeals-utils').' '.esc_html($user_subj)
					.  "\n".esc_html__('Message:', 'bestdeals-utils').' '.esc_html($user_msg);

			} else {

				$subj = sprintf(esc_attr__('Site %s - Custom form data', 'bestdeals-utils'), get_bloginfo('site_name'));
				$msg = '';
				if (is_array($post_data) && count($post_data) > 0) {
					foreach ($post_data as $k=>$v)
						$msg .= "\n{$k}: $v";
				}
			}

			$msg .= "\n\n............. " . get_bloginfo('site_name') . " (" . home_url('/') . ") ............";

			$mail = bestdeals_get_theme_option('mail_function') == 'mail' ? 'mail' : 'wp_mail';
			if (is_email($contact_email) && !@$mail($contact_email, $subj, apply_filters('bestdeals_filter_contact_form_message', $msg))) {
				$response['error'] = esc_html__('Error send message!', 'bestdeals-utils');
			}
		
			echo json_encode($response);
			die();
		}
	}
}

// ---------------------------------- [/trx_contact_form] ---------------------------------------




// ---------------------------------- [trx_content] ---------------------------------------

/*
[trx_content id="unique_id" class="class_name" style="css-styles"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_content]
*/

if (!function_exists('bestdeals_sc_content')) {	
	function bestdeals_sc_content($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values('!'.($top), '', '!'.($bottom));
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_content content_wrap' 
				. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
				. ($class ? ' '.esc_attr($class) : '') 
				. '"'
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '').'>' 
			. do_shortcode($content) 
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_content', $atts, $content);
	}
	add_shortcode('trx_content', 'bestdeals_sc_content');
}
// ---------------------------------- [/trx_content] ---------------------------------------





// ---------------------------------- [trx_countdown] ---------------------------------------

//[trx_countdown date="" time=""]

if (!function_exists('bestdeals_sc_countdown')) {	
	function bestdeals_sc_countdown($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"date" => "",
			"time" => "",
			"style" => "1",
			"align" => "center",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		if (empty($id)) $id = "sc_countdown_".str_replace('.', '', mt_rand());
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		if (empty($interval)) $interval = 1;
		wp_enqueue_script( 'bestdeals-jquery-plugin-script', bestdeals_get_file_url('js/countdown/jquery.plugin.js'), array('jquery'), null, true );
		wp_enqueue_script( 'bestdeals-countdown-script', bestdeals_get_file_url('js/countdown/jquery.countdown.js'), array('jquery'), null, true );
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_countdown sc_countdown_style_' . esc_attr(max(1, min(2, $style))) . (!empty($align) && $align!='none' ? ' align'.esc_attr($align) : '') . (!empty($class) ? ' '.esc_attr($class) : '') .'"'
			. ($css ? ' style="'.esc_attr($css).'"' : '')
			. ' data-date="'.esc_attr(empty($date) ? date('Y-m-d') : $date).'"'
			. ' data-time="'.esc_attr(empty($time) ? '00:00:00' : $time).'"'
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
				. '<div class="sc_countdown_item sc_countdown_days">'
					. '<span class="sc_countdown_digits"><span></span><span></span><span></span></span>'
					. '<span class="sc_countdown_label">'.esc_html__('Days', 'bestdeals-utils').'</span>'
				. '</div>'
				. '<div class="sc_countdown_separator">:</div>'
				. '<div class="sc_countdown_item sc_countdown_hours">'
					. '<span class="sc_countdown_digits"><span></span><span></span></span>'
					. '<span class="sc_countdown_label">'.esc_html__('Hours', 'bestdeals-utils').'</span>'
				. '</div>'
				. '<div class="sc_countdown_separator">:</div>'
				. '<div class="sc_countdown_item sc_countdown_minutes">'
					. '<span class="sc_countdown_digits"><span></span><span></span></span>'
					. '<span class="sc_countdown_label">'.esc_html__('Minutes', 'bestdeals-utils').'</span>'
				. '</div>'
				. '<div class="sc_countdown_separator">:</div>'
				. '<div class="sc_countdown_item sc_countdown_seconds">'
					. '<span class="sc_countdown_digits"><span></span><span></span></span>'
					. '<span class="sc_countdown_label">'.esc_html__('Seconds', 'bestdeals-utils').'</span>'
				. '</div>'
				. '<div class="sc_countdown_placeholder hide"></div>'
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_countdown', $atts, $content);
	}
	add_shortcode("trx_countdown", "bestdeals_sc_countdown");
}
// ---------------------------------- [/trx_countdown] ---------------------------------------



						


// ---------------------------------- [trx_dropcaps] ---------------------------------------

//[trx_dropcaps id="unique_id" style="1-6"]paragraph text[/trx_dropcaps]

if (!function_exists('bestdeals_sc_dropcaps')) {	
	function bestdeals_sc_dropcaps($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$style = min(4, max(1, $style));
		$content = do_shortcode($content);
		$output = bestdeals_substr($content, 0, 1) == '<' 
			? $content 
			: '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_dropcaps sc_dropcaps_style_' . esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. ($css ? ' style="'.esc_attr($css).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>' 
					. '<span class="sc_dropcaps_item">' . trim(bestdeals_substr($content, 0, 1)) . '</span>' . trim(bestdeals_substr($content, 1))
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_dropcaps', $atts, $content);
	}
	add_shortcode('trx_dropcaps', 'bestdeals_sc_dropcaps');
}
// ---------------------------------- [/trx_dropcaps] ---------------------------------------





// ---------------------------------- [trx_emailer] ---------------------------------------

//[trx_emailer group=""]

if (!function_exists('bestdeals_sc_emailer')) {	
	function bestdeals_sc_emailer($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"group" => "",
			"open" => "yes",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		// Load core messages
		bestdeals_enqueue_messages();

		$trx_addons_privacy = trx_utils_get_privacy_text();

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
					. ' class="sc_emailer' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (bestdeals_param_is_on($open) ? ' sc_emailer_opened' : '') . (!empty($class) ? ' '.esc_attr($class) : '') . ' "'
					. ($css ? ' style="'.esc_attr($css).'"' : '') 
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. '>'
				. '<form class="sc_emailer_form">'
				. '<h2>'.esc_html__('Sign up for Updates', 'bestdeals-utils').'</h2>'
				. '<div class="sc_emailer_fields_wrap">'
				. '<div>'
					. '<input type="text" class="sc_emailer_input" name="email" value="" placeholder="'.esc_html__('Enter Email', 'bestdeals-utils').'">'
					. '<a href="#" ' . ((empty($trx_addons_privacy)) ? '' : ' disabled="disabled"') . ' class="sc_emailer_button" title="'.esc_html__('Submit', 'bestdeals-utils').'" data-group="'.($group ? $group : esc_html__('E-mailer subscription', 'bestdeals-utils')).'">'.esc_html__('submit', 'bestdeals-utils').'</a>'
				. '</div>'
				. (
					(!empty($trx_addons_privacy)) ?
						'<div class="sc_emailer_checkbox trx_utils_form_field_agree">'
                        	.'<input type="checkbox" value="1" id="i_agree_privacy_policy_registration" name="i_agree_privacy_policy"> <label for="i_agree_privacy_policy_registration">' . wp_kses_post($trx_addons_privacy) . '</label>'
                        . '</div>'
						: ''
				)
				. '</div>'
				. '</form>'
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_emailer', $atts, $content);
	}
	add_shortcode("trx_emailer", "bestdeals_sc_emailer");
}
// ---------------------------------- [/trx_emailer] ---------------------------------------





// ---------------------------------- [trx_gap] ---------------------------------------
						
//[trx_gap]Fullwidth content[/trx_gap]

if (!function_exists('bestdeals_sc_gap')) {	
	function bestdeals_sc_gap($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		$output = bestdeals_gap_start() . do_shortcode($content) . bestdeals_gap_end();
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_gap', $atts, $content);
	}
	add_shortcode("trx_gap", "bestdeals_sc_gap");
}
// ---------------------------------- [/trx_gap] ---------------------------------------






// ---------------------------------- [trx_googlemap] ---------------------------------------

//[trx_googlemap id="unique_id" width="width_in_pixels_or_percent" height="height_in_pixels"]
//	[trx_googlemap_marker address="your_address"]
//[/trx_googlemap]

if (!function_exists('bestdeals_sc_googlemap')) {	
	function bestdeals_sc_googlemap($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"zoom" => 16,
			"style" => 'default',
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "100%",
			"height" => "400",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		if (empty($id)) $id = 'sc_googlemap_'.str_replace('.', '', mt_rand());
		if (empty($style)) $style = bestdeals_get_custom_option('googlemap_style');

		$api_key = bestdeals_get_theme_option('api_google');

		if (!empty($api_key)) {
			wp_enqueue_script( 'googlemap', bestdeals_get_protocol().'://maps.google.com/maps/api/js'.($api_key ? '?key='.$api_key : ''), array(), null, true );
			wp_enqueue_script( 'bestdeals-googlemap-script', bestdeals_get_file_url('js/core.googlemap.js'), array(), null, true );
		}

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_googlemap_markers'] = array();
		$content = do_shortcode($content);
		$output = '';
		if (count($BESTDEALS_GLOBALS['sc_googlemap_markers']) == 0) {
			$BESTDEALS_GLOBALS['sc_googlemap_markers'][] = array(
				'title' => bestdeals_get_custom_option('googlemap_title'),
				'description' => bestdeals_strmacros(bestdeals_get_custom_option('googlemap_description')),
				'latlng' => bestdeals_get_custom_option('googlemap_latlng'),
				'address' => bestdeals_get_custom_option('googlemap_address'),
				'point' => bestdeals_get_custom_option('googlemap_marker')
			);
		}
		$output .= '<div id="'.esc_attr($id).'"'
			. ' class="sc_googlemap'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ' data-zoom="'.esc_attr($zoom).'"'
			. ' data-style="'.esc_attr($style).'"'
			. '>';
		$cnt = 0;
		foreach ($BESTDEALS_GLOBALS['sc_googlemap_markers'] as $marker) {
			$cnt++;
			if (!empty($api_key)) {
				if (empty($marker['id'])) $marker['id'] = $id.'_'.$cnt;
				$output .= '<div id="'.esc_attr($marker['id']).'" class="sc_googlemap_marker"'
					. ' data-title="'.esc_attr($marker['title']).'"'
					. ' data-description="'.esc_attr(bestdeals_strmacros($marker['description'])).'"'
					. ' data-address="'.esc_attr($marker['address']).'"'
					. ' data-latlng="'.esc_attr($marker['latlng']).'"'
					. ' data-point="'.esc_attr($marker['point']).'"'
					. '></div>';
			} else {
				$output .= '<iframe src="https://maps.google.com/maps?t=m&output=embed&iwloc=near&z='.esc_attr($zoom > 0 ? $zoom : 14).'&q='
					. esc_attr(!empty($marker['address']) ? urlencode($marker['address']) : '')
					. ( !empty($marker['latlng'])
						? ( !empty($marker['address']) ? '@' : '' ) . str_replace(' ', '', $marker['latlng'])
						: esc_attr(get_field( "rue" )) . ' ' . esc_attr(get_field( "code_postal" )) . ' ' . esc_attr(get_field( "commune" ))
					)
					. '" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"'
					. ' aria-label="' . esc_attr(!empty($marker['title']) ? $marker['title'] : '') . '"></iframe>';
				break; // Remove this line if you want display separate iframe for each marker (otherwise only first marker shown)
			}


		}
		$output .= '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_googlemap', $atts, $content);
	}
	add_shortcode("trx_googlemap", "bestdeals_sc_googlemap");
}


if (!function_exists('bestdeals_sc_googlemap_marker')) {	
	function bestdeals_sc_googlemap_marker($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"address" => "",
			"latlng" => "",
			"point" => "",
			// Common params
			"id" => ""
		), $atts)));
		if (!empty($point)) {
			if ($point > 0) {
				$attach = wp_get_attachment_image_src( $point, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$point = $attach[0];
			}
		}
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_googlemap_markers'][] = array(
			'id' => $id,
			'title' => $title,
			'description' => do_shortcode($content),
			'latlng' => $latlng,
			'address' => $address,
			'point' => $point ? $point : bestdeals_get_custom_option('googlemap_marker')
		);
		return '';
	}
	add_shortcode("trx_googlemap_marker", "bestdeals_sc_googlemap_marker");
}
// ---------------------------------- [/trx_googlemap] ---------------------------------------





// ---------------------------------- [trx_hide] ---------------------------------------

/*
[trx_hide selector="unique_id"]
*/

if (!function_exists('bestdeals_sc_hide')) {	
	function bestdeals_sc_hide($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"selector" => "",
			"hide" => "on",
			"delay" => 0
		), $atts)));
		$selector = trim(chop($selector));
		$output = $selector == '' ? '' : 
			'<script type="text/javascript">
				jQuery(document).ready(function() {
					'.($delay>0 ? 'setTimeout(function() {' : '').'
					jQuery("'.esc_attr($selector).'").' . ($hide=='on' ? 'hide' : 'show') . '();
					'.($delay>0 ? '},'.($delay).');' : '').'
				});
			</script>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_hide', $atts, $content);
	}
	add_shortcode('trx_hide', 'bestdeals_sc_hide');
}
// ---------------------------------- [/trx_hide] ---------------------------------------





// ---------------------------------- [trx_highlight] ---------------------------------------

/*
[trx_highlight id="unique_id" color="fore_color's_name_or_#rrggbb" backcolor="back_color's_name_or_#rrggbb" style="custom_style"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_highlight]
*/

if (!function_exists('bestdeals_sc_highlight')) {	
	function bestdeals_sc_highlight($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"color" => "",
			"bg_color" => "",
			"font_size" => "",
			"type" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$css .= ($color != '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color != '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(bestdeals_prepare_css_value($font_size)) . '; line-height: 1em;' : '');
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_highlight'.($type>0 ? ' sc_highlight_style_'.esc_attr($type) : ''). (!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>' 
				. do_shortcode($content) 
				. '</span>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_highlight', $atts, $content);
	}
	add_shortcode('trx_highlight', 'bestdeals_sc_highlight');
}
// ---------------------------------- [/trx_highlight] ---------------------------------------





// ---------------------------------- [trx_icon] ---------------------------------------

/*
[trx_icon id="unique_id" style='round|square' icon='' color="" bg_color="" size="" weight=""]
*/

if (!function_exists('bestdeals_sc_icon')) {	
	function bestdeals_sc_icon($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"bg_shape" => "",
			"font_size" => "",
			"font_weight" => "",
			"align" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$css2 = ($font_weight != '' && !bestdeals_is_inherit_option($font_weight) ? 'font-weight:'. esc_attr($font_weight).';' : '')
			. ($font_size != '' ? 'font-size:' . esc_attr(bestdeals_prepare_css_value($font_size)) . '; line-height: ' . (!$bg_shape || bestdeals_param_is_inherit($bg_shape) ? '1' : '1.2') . 'em;' : '')
			. ($color != '' ? 'color:'.esc_attr($color).';' : '')
			. ($bg_color != '' ? 'background-color:'.esc_attr($bg_color).';border-color:'.esc_attr($bg_color).';' : '')
		;
		$output = $icon!='' 
			? ($link ? '<a href="'.esc_url($link).'"' : '<span') . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_icon '.esc_attr($icon)
					. ($bg_shape && !bestdeals_param_is_inherit($bg_shape) ? ' sc_icon_shape_'.esc_attr($bg_shape) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
				.'"'
				.($css || $css2 ? ' style="'.($css ? 'display:block;' : '') . ($css) . ($css2) . '"' : '')
				.'>'
				.($link ? '</a>' : '</span>')
			: '';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_icon', $atts, $content);
	}
	add_shortcode('trx_icon', 'bestdeals_sc_icon');
}
// ---------------------------------- [/trx_icon] ---------------------------------------





// ---------------------------------- [trx_image] ---------------------------------------

/*
[trx_image id="unique_id" src="image_url" width="width_in_pixels" height="height_in_pixels" title="image's_title" align="left|right"]
*/

if (!function_exists('bestdeals_sc_image')) {	
	function bestdeals_sc_image($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"align" => "",
			"shape" => "square",
			"src" => "",
			"url" => "",
			"icon" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values('!'.($top), '!'.($right), '!'.($bottom), '!'.($left), $width, $height);
		$src = $src!='' ? $src : $url;
		if ($src > 0) {
			$attach = wp_get_attachment_image_src( $src, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$src = $attach[0];
		}
		if (!empty($width) || !empty($height)) {
			$w = !empty($width) && strlen(intval($width)) == strlen($width) ? $width : null;
			$h = !empty($height) && strlen(intval($height)) == strlen($height) ? $height : null;
			if ($w || $h) $src = bestdeals_get_resized_image_url($src, $w, $h);
		}
		if (trim($link)) bestdeals_enqueue_popup();
		$output = empty($src) ? '' : ('<figure' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_image ' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (!empty($shape) ? ' sc_image_shape_'.esc_attr($shape) : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
				. (trim($link) ? '<a href="'.esc_url($link).'">' : '')
				. '<img src="'.esc_url($src).'" alt="" />'
				. (trim($link) ? '</a>' : '')
				. (trim($title) || trim($icon) ? '<figcaption><span'.($icon ? ' class="'.esc_attr($icon).'"' : '').'></span> ' . ($title) . '</figcaption>' : '')
			. '</figure>');
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_image', $atts, $content);
	}
	add_shortcode('trx_image', 'bestdeals_sc_image');
}
// ---------------------------------- [/trx_image] ---------------------------------------






// ---------------------------------- [trx_infobox] ---------------------------------------

/*
[trx_infobox id="unique_id" style="regular|info|success|error|result" static="0|1"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_infobox]
*/

if (!function_exists('bestdeals_sc_infobox')) {	
	function bestdeals_sc_infobox($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "regular",
			"closeable" => "no",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) .';' : '');
		if (empty($icon)) {
			if ($icon=='none')
				$icon = '';
			else if ($style=='regular')
				$icon = 'icon-cog';
			else if ($style=='success')
				$icon = 'icon-check';
			else if ($style=='error')
				$icon = 'icon-attention';
			else if ($style=='info')
				$icon = 'icon-info';
		}
		$content = do_shortcode($content);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_infobox sc_infobox_style_' . esc_attr($style) 
					. (bestdeals_param_is_on($closeable) ? ' sc_infobox_closeable' : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. ($icon!='' && !bestdeals_param_is_inherit($icon) ? ' sc_infobox_iconed '. esc_attr($icon) : '') 
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>'
				. trim($content)
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_infobox', $atts, $content);
	}
	add_shortcode('trx_infobox', 'bestdeals_sc_infobox');
}
// ---------------------------------- [/trx_infobox] ---------------------------------------





// ---------------------------------- [trx_line] ---------------------------------------

/*
[trx_line id="unique_id" style="none|solid|dashed|dotted|double|groove|ridge|inset|outset" top="margin_in_pixels" bottom="margin_in_pixels" width="width_in_pixels_or_percent" height="line_thickness_in_pixels" color="line_color's_name_or_#rrggbb"]
*/

if (!function_exists('bestdeals_sc_line')) {	
	function bestdeals_sc_line($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "solid",
			"color" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width)
			.($height !='' ? 'border-top-width:' . esc_attr($height) . 'px;' : '')
			.($style != '' ? 'border-top-style:' . esc_attr($style) . ';' : '')
			.($color != '' ? 'border-top-color:' . esc_attr($color) . ';' : '');
		$output = '<div' . ($id ? ' id="'.esc_attr($id) . '"' : '') 
				. ' class="sc_line' . ($style != '' ? ' sc_line_style_'.esc_attr($style) : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '></div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_line', $atts, $content);
	}
	add_shortcode('trx_line', 'bestdeals_sc_line');
}
// ---------------------------------- [/trx_line] ---------------------------------------





// ---------------------------------- [trx_list] ---------------------------------------

/*
[trx_list id="unique_id" style="arrows|iconed|ol|ul"]
	[trx_list_item id="unique_id" title="title_of_element"]Et adipiscing integer.[/trx_list_item]
	[trx_list_item]A pulvinar ut, parturient enim porta ut sed, mus amet nunc, in.[/trx_list_item]
	[trx_list_item]Duis sociis, elit odio dapibus nec, dignissim purus est magna integer.[/trx_list_item]
	[trx_list_item]Nec purus, cras tincidunt rhoncus proin lacus porttitor rhoncus.[/trx_list_item]
[/trx_list]
*/

if (!function_exists('bestdeals_sc_list')) {	
	function bestdeals_sc_list($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "ul",
			"icon" => "icon-right",
			"icon_color" => "",
			"color" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '');
		if (trim($style) == '' || (trim($icon) == '' && $style=='iconed')) $style = 'ul';
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_list_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_list_icon'] = empty($icon) || bestdeals_param_is_inherit($icon) ? "icon-right" : $icon;
		$BESTDEALS_GLOBALS['sc_list_icon_color'] = $icon_color;
		$BESTDEALS_GLOBALS['sc_list_style'] = $style;
		$output = '<' . ($style=='ol' ? 'ol' : 'ul')
				. ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_list sc_list_style_' . esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
				. do_shortcode($content)
				. '</' .($style=='ol' ? 'ol' : 'ul') . '>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_list', $atts, $content);
	}
	add_shortcode('trx_list', 'bestdeals_sc_list');
}


if (!function_exists('bestdeals_sc_list_item')) {	
	function bestdeals_sc_list_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"color" => "",
			"icon" => "",
			"icon_color" => "",
			"title" => "",
			"link" => "",
			"target" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_list_counter']++;
		$css .= $color !== '' ? 'color:' . esc_attr($color) .';' : '';
		if (trim($icon) == '' || bestdeals_param_is_inherit($icon)) $icon = $BESTDEALS_GLOBALS['sc_list_icon'];
		if (trim($color) == '' || bestdeals_param_is_inherit($icon_color)) $icon_color = $BESTDEALS_GLOBALS['sc_list_icon_color'];
		$output = '<li' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_list_item' 
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. ($BESTDEALS_GLOBALS['sc_list_counter'] % 2 == 1 ? ' odd' : ' even') 
			. ($BESTDEALS_GLOBALS['sc_list_counter'] == 1 ? ' first' : '')  
			. '"' 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. ($title ? ' title="'.esc_attr($title).'"' : '') 
			. '>' 
			. (!empty($link) ? '<a href="'.esc_url($link).'"' . (!empty($target) ? ' target="'.esc_attr($target).'"' : '') . '>' : '')
			. ($BESTDEALS_GLOBALS['sc_list_style']=='iconed' && $icon!='' ? '<span class="sc_list_icon '.esc_attr($icon).'"'.($icon_color !== '' ? ' style="color:'.esc_attr($icon_color).';"' : '').'></span>' : '')
			. do_shortcode($content)
			. (!empty($link) ? '</a>': '')
			. '</li>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_list_item', $atts, $content);
	}
	add_shortcode('trx_list_item', 'bestdeals_sc_list_item');
}
// ---------------------------------- [/trx_list] ---------------------------------------










// ---------------------------------- [trx_list_educ] ---------------------------------------

/*
[trx_list_educ id="unique_id"]
	[trx_list_educ_item id="unique_id" title="title_of_element"]Et adipiscing integer.[/trx_list_educ_item]
	[trx_list_educ_item]A pulvinar ut, parturient enim porta ut sed, mus amet nunc, in.[/trx_list_educ_item]
	[trx_list_educ_item]Duis sociis, elit odio dapibus nec, dignissim purus est magna integer.[/trx_list_educ_item]
	[trx_list_educ_item]Nec purus, cras tincidunt rhoncus proin lacus porttitor rhoncus.[/trx_list_educ_item]
[/trx_list_educ]
*/

if (!function_exists('bestdeals_sc_list_educ')) {	
	function bestdeals_sc_list_educ($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_list_educ_counter'] = 0;
		$output = '<div'
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_list_educ' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			.'>'
			.do_shortcode($content)
			.'</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_list_educ', $atts, $content);
	}
	add_shortcode('trx_list_educ', 'bestdeals_sc_list_educ');
}

if (!function_exists('bestdeals_sc_list_educ_item')) {	
	function bestdeals_sc_list_educ_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_list_educ_counter']++;
		$output = '<div'
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_list_educ_item' . (!empty($class) ? ' ' . esc_attr($class) : '') . '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			.'>';
		$output .= '<div class="sc_list_educ_title">'.($title ? ''.esc_attr($title).'' : '').'</div>';
		$output .= '<div class="sc_list_educ_content">'.do_shortcode($content).'</div>';
		$output .= '<div class="cL"></div>';
		$output .= '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_list_educ_item', $atts, $content);
	}
	add_shortcode('trx_list_educ_item', 'bestdeals_sc_list_educ_item');
}
// ---------------------------------- [/trx_list_educ] ---------------------------------------






// ---------------------------------- [trx_number] ---------------------------------------

/*
[trx_number id="unique_id" value="400"]
*/

if (!function_exists('bestdeals_sc_number')) {	
	function bestdeals_sc_number($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"value" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_number' 
					. (!empty($align) ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>';
		for ($i=0; $i < bestdeals_strlen($value); $i++) {
			$output .= '<span class="sc_number_item">' . trim(bestdeals_substr($value, $i, 1)) . '</span>';
		}
		$output .= '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_number', $atts, $content);
	}
	add_shortcode('trx_number', 'bestdeals_sc_number');
}
// ---------------------------------- [/trx_number] ---------------------------------------





// ---------------------------------- [trx_parallax] ---------------------------------------

/*
[trx_parallax id="unique_id" style="light|dark" dir="up|down" image="" color='']Content for parallax block[/trx_parallax]
*/

if (!function_exists('bestdeals_sc_parallax')) {	
	function bestdeals_sc_parallax($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"gap" => "no",
			"dir" => "up",
			"speed" => 0.3,
			"color" => "",
			"scheme" => "",
			"bg_color" => "",
			"bg_image" => "",
			"bg_image_x" => "",
			"bg_image_y" => "",
			"bg_video" => "",
			"bg_video_ratio" => "16:9",
			"bg_overlay" => "",
			"bg_texture" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		if ($bg_video!='') {
			$info = pathinfo($bg_video);
			$ext = !empty($info['extension']) ? $info['extension'] : 'mp4';
			$bg_video_ratio = empty($bg_video_ratio) ? "16:9" : str_replace(array('/','\\','-'), ':', $bg_video_ratio);
			$ratio = explode(':', $bg_video_ratio);
			$bg_video_width = !empty($width) && bestdeals_substr($width, -1) >= '0' && bestdeals_substr($width, -1) <= '9'  ? $width : 1280;
			$bg_video_height = round($bg_video_width / $ratio[0] * $ratio[1]);
			if (bestdeals_get_theme_option('use_mediaelement')=='yes')
				wp_enqueue_script('wp-mediaelement');
		}
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
		$bg_image_x = $bg_image_x!='' ? str_replace('%', '', $bg_image_x).'%' : "50%";
		$bg_image_y = $bg_image_y!='' ? str_replace('%', '', $bg_image_y).'%' : "50%";
		$speed = ($dir=='down' ? -1 : 1) * abs($speed);
		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = bestdeals_get_scheme_color('bg');
			$rgb = bestdeals_hex2rgb($bg_color);
		}
		$css .= bestdeals_get_css_position_from_values($top, '!'.($right), $bottom, '!'.($left), $width, $height)
			. ($color !== '' ? 'color:' . esc_attr($color) . ';' : '')
			. ($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			;
		$output = (bestdeals_param_is_on($gap) ? bestdeals_gap_start() : '')
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_parallax' 
					. ($bg_video!='' ? ' sc_parallax_with_video' : '') 
					. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"' 
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. ' data-parallax-speed="'.esc_attr($speed).'"'
				. ' data-parallax-x-pos="'.esc_attr($bg_image_x).'"'
				. ' data-parallax-y-pos="'.esc_attr($bg_image_y).'"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
			. ($bg_video!='' 
				? '<div class="sc_video_bg_wrapper"><video class="sc_video_bg"'
					. ' width="'.esc_attr($bg_video_width).'" height="'.esc_attr($bg_video_height).'" data-width="'.esc_attr($bg_video_width).'" data-height="'.esc_attr($bg_video_height).'" data-ratio="'.esc_attr($bg_video_ratio).'" data-frame="no"'
					. ' preload="metadata" autoplay="autoplay" loop="loop" src="'.esc_attr($bg_video).'"><source src="'.esc_url($bg_video).'" type="video/'.esc_attr($ext).'"></source></video></div>' 
				: '')
			. '<div class="sc_parallax_content" style="' . ($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . '); background-position:'.esc_attr($bg_image_x).' '.esc_attr($bg_image_y).';' : '').'">'
			. ($bg_overlay>0 || $bg_texture!=''
				? '<div class="sc_parallax_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
					. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
						. (bestdeals_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
						. '"'
						. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
						. '>' 
				: '')
			. do_shortcode($content)
			. ($bg_overlay > 0 || $bg_texture!='' ? '</div>' : '')
			. '</div>'
			. '</div>'
			. (bestdeals_param_is_on($gap) ? bestdeals_gap_end() : '');
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_parallax', $atts, $content);
	}
	add_shortcode('trx_parallax', 'bestdeals_sc_parallax');
}
// ---------------------------------- [/trx_parallax] ---------------------------------------




// ---------------------------------- [trx_popup] ---------------------------------------

/*
[trx_popup id="unique_id" class="class_name" style="css_styles"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_popup]
*/

if (!function_exists('bestdeals_sc_popup')) {	
	function bestdeals_sc_popup($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		bestdeals_enqueue_popup('magnific');
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_popup mfp-with-anim mfp-hide' . ($class ? ' '.esc_attr($class) : '') . '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>' 
				. do_shortcode($content) 
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_popup', $atts, $content);
	}
	add_shortcode('trx_popup', 'bestdeals_sc_popup');
}
// ---------------------------------- [/trx_popup] ---------------------------------------






// ---------------------------------- [trx_price] ---------------------------------------

/*
[trx_price id="unique_id" currency="$" money="29.99" period="monthly"]
*/

if (!function_exists('bestdeals_sc_price')) {	
	function bestdeals_sc_price($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		if (!empty($money)) {
			$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
			$m = explode('.', str_replace(',', '.', $money));
			$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price'
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. '>'
				. '<span class="sc_price_currency">'.($currency).'</span>'
				. '<span class="sc_price_money">'.($m[0]).'</span>'
				. (!empty($m[1]) ? '<span class="sc_price_info">' : '')
				. (!empty($m[1]) ? '<span class="sc_price_penny">'.($m[1]).'</span>' : '')
				. (!empty($period) ? '<span class="sc_price_period">'.($period).'</span>' : (!empty($m[1]) ? '<span class="sc_price_period_empty"></span>' : ''))
				. (!empty($m[1]) ? '</span>' : '')
				. '</div>';
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_price', $atts, $content);
	}
	add_shortcode('trx_price', 'bestdeals_sc_price');
}
// ---------------------------------- [/trx_price] ---------------------------------------





// ---------------------------------- [trx_price_block] ---------------------------------------

/*
[trx_price id="unique_id" currency="$" money="29.99" period="monthly"]
*/

if (!function_exists('bestdeals_sc_price_block')) {	
	function bestdeals_sc_price_block($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => 1,
			"title" => "",
			"link" => "",
			"link_text" => "",
			"icon" => "",
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		if ($money) $money = do_shortcode('[trx_price money="'.esc_attr($money).'" period="'.esc_attr($period).'"'.($currency ? ' currency="'.esc_attr($currency).'"' : '').']');
		$content = do_shortcode($content);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price_block sc_price_block_style_'.max(1, min(3, $style))
						. (!empty($class) ? ' '.esc_attr($class) : '')
						. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. '>'
				. (!empty($title) ? '<div class="sc_price_block_title">'.($title).'</div>' : '')
				. '<div class="sc_price_block_money">'
					. (!empty($icon) ? '<div class="sc_price_block_icon '.esc_attr($icon).'"></div>' : '')
					. ($money)
				. '</div>'
				. (!empty($content) ? '<div class="sc_price_block_description">'.($content).'</div>' : '')
				. (!empty($link_text) ? '<div class="sc_price_block_link">'.do_shortcode('[trx_button link="'.($link ? esc_url($link) : '#').'"]'.($link_text).'[/trx_button]').'</div>' : '')
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_price_block', $atts, $content);
	}
	add_shortcode('trx_price_block', 'bestdeals_sc_price_block');
}
// ---------------------------------- [/trx_price_block] ---------------------------------------




// ---------------------------------- [trx_quote] ---------------------------------------

/*
[trx_quote id="unique_id" cite="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/quote]
*/

if (!function_exists('bestdeals_sc_quote')) {	
	function bestdeals_sc_quote($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"cite" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width);
		$cite_param = $cite != '' ? ' cite="'.esc_attr($cite).'"' : '';
		$title = $title=='' ? $cite : $title;
		$content = do_shortcode($content);
		if (bestdeals_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
		$output = '<blockquote' 
			. ($id ? ' id="'.esc_attr($id).'"' : '') . ($cite_param) 
			. ' class="sc_quote'. (!empty($class) ? ' '.esc_attr($class) : '').'"' 
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
				. ($content)
				. ($title == '' ? '' : ('<p class="sc_quote_title">' . ($cite!='' ? '<a href="'.esc_url($cite).'">' : '') . ($title) . ($cite!='' ? '</a>' : '') . '</p>'))
			.'</blockquote>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_quote', $atts, $content);
	}
	add_shortcode('trx_quote', 'bestdeals_sc_quote');
}
// ---------------------------------- [/trx_quote] ---------------------------------------





// ---------------------------------- [trx_reviews] ---------------------------------------
						
/*
[trx_reviews]
*/

if (!function_exists('bestdeals_sc_reviews')) {	
	function bestdeals_sc_reviews($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"align" => "right",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$output = bestdeals_param_is_off(bestdeals_get_custom_option('show_sidebar_main'))
			? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_reviews'
							. ($align && $align!='none' ? ' align'.esc_attr($align) : '')
							. ($class ? ' '.esc_attr($class) : '')
							. '"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
						. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
						. '>'
					. trim(bestdeals_get_reviews_placeholder())
					. '</div>'
			: '';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_reviews', $atts, $content);
	}
	add_shortcode("trx_reviews", "bestdeals_sc_reviews");
}
// ---------------------------------- [/trx_reviews] ---------------------------------------




// ---------------------------------- [trx_search] ---------------------------------------

/*
[trx_search id="unique_id" open="yes|no"]
*/

if (!function_exists('bestdeals_sc_search')) {	
	function bestdeals_sc_search($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "regular",
			"state" => "fixed",
			"scheme" => "original",
			"ajax" => "",
			"title" => esc_html__('Search', 'bestdeals-utils'),
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		if (empty($ajax)) $ajax = bestdeals_get_theme_option('use_ajax_search');
		// Load core messages
		bestdeals_enqueue_messages();
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="search_wrap search_style_'.esc_attr($style).' search_state_'.esc_attr($state)
						. (bestdeals_param_is_on($ajax) ? ' search_ajax' : '')
						. ($class ? ' '.esc_attr($class) : '')
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. '>
						<div class="search_form_wrap">
							<form role="search" method="get" class="search_form" action="' . esc_url( home_url('/') ) . '">
								<input type="text" class="search_field" placeholder="' . esc_attr($title) . '" value="' . esc_attr(get_search_query()) . '" name="s" />
								<button type="submit" class="search_submit icon-search" title="' . ($state=='closed' ? esc_html__('Open search', 'bestdeals-utils') : esc_html__('Start search', 'bestdeals-utils')) . '"><span>'.esc_html__('Search', 'bestdeals-utils').'</span></button>
							</form>
						</div>
						<div class="search_results widget_area' . ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') . '"><a class="search_results_close icon-cancel"></a><div class="search_results_content"></div></div>
				</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_search', $atts, $content);
	}
	add_shortcode('trx_search', 'bestdeals_sc_search');
}
// ---------------------------------- [/trx_search] ---------------------------------------




// ---------------------------------- [trx_section] and [trx_block] ---------------------------------------

/*
[trx_section id="unique_id" class="class_name" style="css-styles" dedicated="yes|no"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_section]
*/

global $BESTDEALS_GLOBALS;
$BESTDEALS_GLOBALS['sc_section_dedicated'] = '';

if (!function_exists('bestdeals_sc_section')) {	
	function bestdeals_sc_section($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"dedicated" => "no",
			"align" => "none",
			"columns" => "none",
			"pan" => "no",
			"scroll" => "no",
			"scroll_dir" => "horizontal",
			"scroll_controls" => "no",
			"color" => "",
			"scheme" => "",
			"bg_color" => "",
			"bg_image" => "",
			"bg_overlay" => "",
			"bg_texture" => "",
			"font_size" => "",
			"font_weight" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
	
		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = bestdeals_get_scheme_color('bg');
			$rgb = bestdeals_hex2rgb($bg_color);
		}
	
		$css .= bestdeals_get_css_position_from_values('!'.($top), '!'.($right), '!'.($bottom), '!'.($left))
			.($color !== '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
			.(!bestdeals_param_is_off($pan) ? 'position:relative;' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(bestdeals_prepare_css_value($font_size)) . '; line-height: 1.3em;' : '')
			.($font_weight != '' && !bestdeals_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) . ';' : '');
		$css_dim = bestdeals_get_css_position_from_values('', '', '', '', $width, $height);
		if ($bg_image == '' && $bg_color == '' && $bg_overlay==0 && $bg_texture==0 && bestdeals_strlen($bg_texture)<2) $css .= $css_dim;
		
		$width  = bestdeals_prepare_css_value($width);
		$height = bestdeals_prepare_css_value($height);
	
		if ((!bestdeals_param_is_off($scroll) || !bestdeals_param_is_off($pan)) && empty($id)) $id = 'sc_section_'.str_replace('.', '', mt_rand());
	
		if (!bestdeals_param_is_off($scroll)) bestdeals_enqueue_slider();
	
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_section' 
					. ($class ? ' ' . esc_attr($class) : '') 
					. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($columns) && $columns!='none' ? ' column-'.esc_attr($columns) : '') 
					. (bestdeals_param_is_on($scroll) && !bestdeals_param_is_off($scroll_controls) ? ' sc_scroll_controls sc_scroll_controls_'.esc_attr($scroll_dir).' sc_scroll_controls_type_'.esc_attr($scroll_controls) : '')
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '').'>' 
				. '<div class="sc_section_inner">'
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay>0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2
						? '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
							. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
								. (bestdeals_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
								. '"'
								. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
								. '>'
								. '<div class="sc_section_content"'
									. ($css_dim)
									. '>'
						: '')
					. (bestdeals_param_is_on($scroll) 
						? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($scroll_dir).' swiper-slider-container scroll-container"'
							. ' style="'.($height != '' ? 'height:'.esc_attr($height).';' : '') . ($width != '' ? 'width:'.esc_attr($width).';' : '').'"'
							. '>'
							. '<div class="sc_scroll_wrapper swiper-wrapper">' 
							. '<div class="sc_scroll_slide swiper-slide">' 
						: '')
					. (bestdeals_param_is_on($pan) 
						? '<div id="'.esc_attr($id).'_pan" class="sc_pan sc_pan_'.esc_attr($scroll_dir).'">' 
						: '')
					. do_shortcode($content)
					. (bestdeals_param_is_on($pan) ? '</div>' : '')
					. (bestdeals_param_is_on($scroll) 
						? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($scroll_dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
							. (!bestdeals_param_is_off($scroll_controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
						: '')
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay > 0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2 ? '</div></div>' : '')
					. '</div>'
				. '</div>';
		if (bestdeals_param_is_on($dedicated)) {
			global $BESTDEALS_GLOBALS;
			if ($BESTDEALS_GLOBALS['sc_section_dedicated']=='') {
				$BESTDEALS_GLOBALS['sc_section_dedicated'] = $output;
			}
			$output = '';
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_section', $atts, $content);
	}
	add_shortcode('trx_section', 'bestdeals_sc_section');
	add_shortcode('trx_block', 'bestdeals_sc_section');
}
// ---------------------------------- [/trx_section] ---------------------------------------





// ---------------------------------- [trx_skills] ---------------------------------------

/*
[trx_skills id="unique_id" type="bar|pie|arc|counter" dir="horizontal|vertical" layout="rows|columns" count="" max_value="100" align="left|right"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
[/trx_skills]
*/

if (!function_exists('bestdeals_sc_skills')) {	
	function bestdeals_sc_skills($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"max_value" => "100",
			"type" => "bar",
			"layout" => "",
			"dir" => "",
			"style" => "1",
			"columns" => "",
			"align" => "",
			"color" => "",
			"bg_color" => "",
			"border_color" => "",
			"arc_caption" => esc_html__("Skills", 'bestdeals-utils'),
			"pie_compact" => "on",
			"pie_cutout" => 0,
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_skills_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_skills_columns'] = 0;
		$BESTDEALS_GLOBALS['sc_skills_height']  = 0;
		$BESTDEALS_GLOBALS['sc_skills_type']    = $type;
		$BESTDEALS_GLOBALS['sc_skills_pie_compact'] = $pie_compact;
		$BESTDEALS_GLOBALS['sc_skills_pie_cutout']  = max(0, min(99, $pie_cutout));
		$BESTDEALS_GLOBALS['sc_skills_color']   = $color;
		$BESTDEALS_GLOBALS['sc_skills_bg_color']= $bg_color;
		$BESTDEALS_GLOBALS['sc_skills_border_color']= $border_color;
		$BESTDEALS_GLOBALS['sc_skills_legend']  = '';
		$BESTDEALS_GLOBALS['sc_skills_data']    = '';
		bestdeals_enqueue_diagram($type);
		if ($type!='arc') {
			if ($layout=='' || ($layout=='columns' && $columns<1)) $layout = 'rows';
			if ($layout=='columns') $BESTDEALS_GLOBALS['sc_skills_columns'] = $columns;
			if ($type=='bar') {
				if ($dir == '') $dir = 'horizontal';
				if ($dir == 'vertical' && $height < 1) $height = 300;
			}
		}
		if (empty($id)) $id = 'sc_skills_diagram_'.str_replace('.','',mt_rand());
		if ($max_value < 1) $max_value = 100;
		if ($style) {
			$style = max(1, min(4, $style));
			$BESTDEALS_GLOBALS['sc_skills_style'] = $style;
		}
		$BESTDEALS_GLOBALS['sc_skills_max'] = $max_value;
		$BESTDEALS_GLOBALS['sc_skills_dir'] = $dir;
		$BESTDEALS_GLOBALS['sc_skills_height'] = bestdeals_prepare_css_value($height);
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		$content = do_shortcode($content);
		$output = '<div id="'.esc_attr($id).'"' 
					. ' class="sc_skills sc_skills_' . esc_attr($type) 
						. ($type=='bar' ? ' sc_skills_'.esc_attr($dir) : '') 
						. ($type=='pie' ? ' sc_skills_compact_'.esc_attr($pie_compact) : '') 
						. (!empty($class) ? ' '.esc_attr($class) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. ' data-type="'.esc_attr($type).'"'
					. ' data-caption="'.esc_attr($arc_caption).'"'
					. ($type=='bar' ? ' data-dir="'.esc_attr($dir).'"' : '')
				. '>'
					. (!empty($subtitle) ? '<h6 class="sc_skills_subtitle sc_item_subtitle">' . esc_html($subtitle) . '</h6>' : '')
					. (!empty($title) ? '<h2 class="sc_skills_title sc_item_title">' . esc_html($title) . '</h2>' : '')
					. (!empty($description) ? '<div class="sc_skills_descr sc_item_descr">' . trim($description) . '</div>' : '')
					. ($layout == 'columns' ? '<div class="columns_wrap sc_skills_'.esc_attr($layout).' sc_skills_columns_'.esc_attr($columns).'">' : '')
					. ($type=='arc' 
						? ('<div class="sc_skills_legend">'.($BESTDEALS_GLOBALS['sc_skills_legend']).'</div>'
							. '<div id="'.esc_attr($id).'_diagram" class="sc_skills_arc_canvas"></div>'
							. '<div class="sc_skills_data" style="display:none;">' . ($BESTDEALS_GLOBALS['sc_skills_data']) . '</div>'
						  )
						: '')
					. ($type=='pie' && bestdeals_param_is_on($pie_compact)
						? ('<div class="sc_skills_legend">'.($BESTDEALS_GLOBALS['sc_skills_legend']).'</div>'
							. '<div id="'.esc_attr($id).'_pie" class="sc_skills_item">'
								. '<canvas id="'.esc_attr($id).'_pie" class="sc_skills_pie_canvas"></canvas>'
								. '<div class="sc_skills_data" style="display:none;">' . ($BESTDEALS_GLOBALS['sc_skills_data']) . '</div>'
							. '</div>'
						  )
						: '')
					. ($content)
					. ($layout == 'columns' ? '</div>' : '')
					. (!empty($link) ? '<div class="sc_skills_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_skills', $atts, $content);
	}
	add_shortcode('trx_skills', 'bestdeals_sc_skills');
}


if (!function_exists('bestdeals_sc_skills_item')) {	
	function bestdeals_sc_skills_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"title" => "",
			"value" => "",
			"color" => "",
			"bg_color" => "",
			"border_color" => "",
			"style" => "",
			"icon" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_skills_counter']++;
		$ed = bestdeals_substr($value, -1)=='%' ? '%' : '';
		$value = str_replace('%', '', $value);
		if ($BESTDEALS_GLOBALS['sc_skills_max'] < $value) $BESTDEALS_GLOBALS['sc_skills_max'] = $value;
		$percent = round($value / $BESTDEALS_GLOBALS['sc_skills_max'] * 100);
		$start = 0;
		$stop = $value;
		$steps = 100;
		$step = max(1, round($BESTDEALS_GLOBALS['sc_skills_max']/$steps));
		$speed = mt_rand(10,40);
		$animation = round(($stop - $start) / $step * $speed);
		$title_block = '<div class="sc_skills_info"><div class="sc_skills_label">' . ($title) . '</div></div>';
		$old_color = $color;
		if (empty($color)) $color = $BESTDEALS_GLOBALS['sc_skills_color'];
		if (empty($color)) $color = bestdeals_get_scheme_color('accent1', $color);
		if (empty($bg_color)) $bg_color = $BESTDEALS_GLOBALS['sc_skills_bg_color'];
		if (empty($bg_color)) $bg_color = bestdeals_get_scheme_color('bg_color', $bg_color);
		if (empty($border_color)) $border_color = $BESTDEALS_GLOBALS['sc_skills_border_color'];
		if (empty($border_color)) $border_color = bestdeals_get_scheme_color('bd_color', $border_color);;
		if (empty($style)) $style = $BESTDEALS_GLOBALS['sc_skills_style'];
		$style = max(1, min(4, $style));
		$output = '';
		if ($BESTDEALS_GLOBALS['sc_skills_type'] == 'arc' || ($BESTDEALS_GLOBALS['sc_skills_type'] == 'pie' && bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_skills_pie_compact']))) {
			if ($BESTDEALS_GLOBALS['sc_skills_type'] == 'arc' && empty($old_color)) {
				$rgb = bestdeals_hex2rgb($color);
				$color = 'rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.(1 - 0.1*($BESTDEALS_GLOBALS['sc_skills_counter']-1)).')';
			}
			$BESTDEALS_GLOBALS['sc_skills_legend'] .= '<div class="sc_skills_legend_item"><span class="sc_skills_legend_marker" style="background-color:'.esc_attr($color).'"></span><span class="sc_skills_legend_title">' . ($title) . '</span><span class="sc_skills_legend_value">' . ($value) . '</span></div>';
			$BESTDEALS_GLOBALS['sc_skills_data'] .= '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_type']).'"'
				. ($BESTDEALS_GLOBALS['sc_skills_type']=='pie'
					? ( ' data-start="'.esc_attr($start).'"'
						. ' data-stop="'.esc_attr($stop).'"'
						. ' data-step="'.esc_attr($step).'"'
						. ' data-steps="'.esc_attr($steps).'"'
						. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
						. ' data-speed="'.esc_attr($speed).'"'
						. ' data-duration="'.esc_attr($animation).'"'
						. ' data-color="'.esc_attr($color).'"'
						. ' data-bg_color="'.esc_attr($bg_color).'"'
						. ' data-border_color="'.esc_attr($border_color).'"'
						. ' data-cutout="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_pie_cutout']).'"'
						. ' data-easing="easeOutCirc"'
						. ' data-ed="'.esc_attr($ed).'"'
						)
					: '')
				. '><input type="hidden" class="text" value="'.esc_attr($title).'" /><input type="hidden" class="percent" value="'.esc_attr($percent).'" /><input type="hidden" class="color" value="'.esc_attr($color).'" /></div>';
		} else {
			$output .= ($BESTDEALS_GLOBALS['sc_skills_columns'] > 0 ? '<div class="sc_skills_column column-1_'.esc_attr($BESTDEALS_GLOBALS['sc_skills_columns']).'">' : '')
					. ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $BESTDEALS_GLOBALS['sc_skills_dir']=='horizontal' ? $title_block : '')
					. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_skills_item' . ($style ? ' sc_skills_style_'.esc_attr($style) : '') 
							. (!empty($class) ? ' '.esc_attr($class) : '')
							. ($BESTDEALS_GLOBALS['sc_skills_counter'] % 2 == 1 ? ' odd' : ' even') 
							. ($BESTDEALS_GLOBALS['sc_skills_counter'] == 1 ? ' first' : '') 
							. '"'
						. ($BESTDEALS_GLOBALS['sc_skills_height'] !='' || $css ? ' style="height: '.esc_attr($BESTDEALS_GLOBALS['sc_skills_height']).';'.($css).'"' : '')
					. '>'
					. (!empty($icon) ? '<div class="sc_skills_icon '.esc_attr($icon).'"></div>' : '');
			if (in_array($BESTDEALS_GLOBALS['sc_skills_type'], array('bar', 'counter'))) {
				$output .= '<div class="sc_skills_count"' . ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $color ? ' style="background-color:' . esc_attr($color) . '; border-color:' . esc_attr($color) . '"' : '') . '>'
							. '<div class="sc_skills_total"'
								. ' data-start="'.esc_attr($start).'"'
								. ' data-stop="'.esc_attr($stop).'"'
								. ' data-step="'.esc_attr($step).'"'
								. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
								. ' data-speed="'.esc_attr($speed).'"'
								. ' data-duration="'.esc_attr($animation).'"'
								. ' data-ed="'.esc_attr($ed).'">'
								. ($start) . ($ed)
							.'</div>'
						. '</div>';
			} else if ($BESTDEALS_GLOBALS['sc_skills_type']=='pie') {
				if (empty($id)) $id = 'sc_skills_canvas_'.str_replace('.','',mt_rand());
				$output .= '<canvas id="'.esc_attr($id).'"></canvas>'
					. '<div class="sc_skills_total"'
						. ' data-start="'.esc_attr($start).'"'
						. ' data-stop="'.esc_attr($stop).'"'
						. ' data-step="'.esc_attr($step).'"'
						. ' data-steps="'.esc_attr($steps).'"'
						. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
						. ' data-speed="'.esc_attr($speed).'"'
						. ' data-duration="'.esc_attr($animation).'"'
						. ' data-color="'.esc_attr($color).'"'
						. ' data-bg_color="'.esc_attr($bg_color).'"'
						. ' data-border_color="'.esc_attr($border_color).'"'
						. ' data-cutout="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_pie_cutout']).'"'
						. ' data-easing="easeOutCirc"'
						. ' data-ed="'.esc_attr($ed).'">'
						. ($start) . ($ed)
					.'</div>';
			}
			$output .= 
					  ($BESTDEALS_GLOBALS['sc_skills_type']=='counter' ? $title_block : '')
					. '</div>'
					. ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $BESTDEALS_GLOBALS['sc_skills_dir']=='vertical' || $BESTDEALS_GLOBALS['sc_skills_type'] == 'pie' ? $title_block : '')
					. ($BESTDEALS_GLOBALS['sc_skills_columns'] > 0 ? '</div>' : '');
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_skills_item', $atts, $content);
	}
	add_shortcode('trx_skills_item', 'bestdeals_sc_skills_item');
}
// ---------------------------------- [/trx_skills] ---------------------------------------






// ---------------------------------- [trx_slider] ---------------------------------------

/*
[trx_slider id="unique_id" engine="revo|royal|flex|swiper|chop" alias="revolution_slider_alias|royal_slider_id" titles="no|slide|fixed" cat="id|slug" count="posts_number" ids="comma_separated_id_list" offset="" width="" height="" align="" top="" bottom=""]
[trx_slider_item src="image_url"]
[/trx_slider]
*/

if (!function_exists('bestdeals_sc_slider')) {	
	function bestdeals_sc_slider($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"engine" => 'swiper',
			"custom" => "no",
			"alias" => "",
			"post_type" => "post",
			"ids" => "",
			"cat" => "",
			"count" => "0",
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"controls" => "no",
			"pagination" => "no",
			"slides_space" => 0,
			"slides_per_view" => 1,
			"titles" => "no",
			"descriptions" => bestdeals_get_custom_option('slider_info_descriptions'),
			"links" => "no",
			"align" => "",
			"interval" => "",
			"date_format" => "",
			"crop" => "yes",
			"autoheight" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($width) && $pagination!='full') $width = "100%";
		if (empty($height) && ($pagination=='full' || $pagination=='over')) $height = 250;
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "off";
		if (empty($interval)) $interval = mt_rand(5000, 10000);
		if (empty($custom)) $custom = 'no';
		if (empty($controls)) $controls = 'no';
		if (empty($pagination)) $pagination = 'no';
		if (empty($titles)) $titles = 'no';
		if (empty($links)) $links = 'no';
		if (empty($autoheight)) $autoheight = 'no';
		if (empty($crop)) $crop = 'no';

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_slider_engine'] = $engine;
		$BESTDEALS_GLOBALS['sc_slider_width']  = bestdeals_prepare_css_value($width);
		$BESTDEALS_GLOBALS['sc_slider_height'] = bestdeals_prepare_css_value($height);
		$BESTDEALS_GLOBALS['sc_slider_links']  = bestdeals_param_is_on($links);
		$BESTDEALS_GLOBALS['sc_slider_bg_image'] = bestdeals_get_theme_setting('slides_type')=='bg';
		$BESTDEALS_GLOBALS['sc_slider_crop_image'] = $crop;
	
		if (empty($id)) $id = "sc_slider_".str_replace('.', '', mt_rand());
		
		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
	
		$css .= (!in_array($pagination, array('full', 'over')) ? $ms : '') . ($hs) . ($ws);
		
		if ($engine!='swiper' && in_array($pagination, array('full', 'over'))) $pagination = 'yes';
		
		$output = (in_array($pagination, array('full', 'over')) 
					? '<div class="sc_slider_pagination_area sc_slider_pagination_'.esc_attr($pagination)
							. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
							. '"'
						. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
						. (($ms).($hs) ? ' style="'.esc_attr(($ms).($hs)).'"' : '') 
						.'>' 
					: '')
				. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_slider sc_slider_' . esc_attr($engine)
					. ($engine=='swiper' ? ' swiper-slider-container' : '')
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
					. ($hs ? ' sc_slider_height_fixed' : '')
					. (bestdeals_param_is_on($controls) ? ' sc_slider_controls' : ' sc_slider_nocontrols')
					. (bestdeals_param_is_on($pagination) ? ' sc_slider_pagination' : ' sc_slider_nopagination')
					. ($BESTDEALS_GLOBALS['sc_slider_bg_image'] ? ' sc_slider_bg' : ' sc_slider_images')
					. (!in_array($pagination, array('full', 'over')) && $align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
					. '"'
				. (!in_array($pagination, array('full', 'over')) && !bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
				. ($slides_per_view > 1 ? ' data-slides-per_view="' . esc_attr($slides_per_view) . '"' : '')
				. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
				. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
				. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>';
	
		bestdeals_enqueue_slider($engine);
	
		if ($engine=='revo') {
			if (bestdeals_exists_revslider() && !empty($alias))
				$output .= do_shortcode('[rev_slider '.esc_attr($alias).']');
			else
				$output = '';
		} else if ($engine=='swiper') {
			
			$caption = '';
	
			$output .= '<div class="slides'
				.($engine=='swiper' ? ' swiper-wrapper' : '').'"'
				.($engine=='swiper' && $BESTDEALS_GLOBALS['sc_slider_bg_image'] ? ' style="'.esc_attr($hs).'"' : '')
				.'>';
	
			$content = do_shortcode($content);
			
			if (bestdeals_param_is_on($custom) && $content) {
				$output .= $content;
			} else {
				global $post;
		
				if (!empty($ids)) {
					$posts = explode(',', $ids);
					$count = count($posts);
				}
			
				$args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => $count,
					'ignore_sticky_posts' => true,
					'order' => $order=='asc' ? 'asc' : 'desc',
				);
		
				if ($offset > 0 && empty($ids)) {
					$args['offset'] = $offset;
				}
		
				$args = bestdeals_query_add_sort_order($args, $orderby, $order);
				$args = bestdeals_query_add_filters($args, 'thumbs');
				$args = bestdeals_query_add_posts_and_cats($args, $ids, $post_type, $cat);
	
				$query = new WP_Query( $args );
	
				$post_number = 0;
				$pagination_items = '';
				$show_image 	= 1;
				$show_types 	= 0;
				$show_date 		= 1;
				$show_author 	= 0;
				$show_links 	= 0;
				$show_counters	= 'views';	//comments | rating
				
				while ( $query->have_posts() ) { 
					$query->the_post();
					$post_number++;
					$post_id = get_the_ID();
					$post_type = get_post_type();
					$post_title = get_the_title();
					$post_link = get_permalink();
					$post_date = get_the_date(!empty($date_format) ? $date_format : 'd.m.y');
					$post_attachment = wp_get_attachment_url(get_post_thumbnail_id($post_id));
					if (bestdeals_param_is_on($crop)) {
						$post_attachment = $BESTDEALS_GLOBALS['sc_slider_bg_image']
							? bestdeals_get_resized_image_url($post_attachment, !empty($width) && (float) $width.' ' == $width.' ' ? $width : null, !empty($height) && (float) $height.' ' == $height.' ' ? $height : null)
							: bestdeals_get_resized_image_tag($post_attachment, !empty($width) && (float) $width.' ' == $width.' ' ? $width : null, !empty($height) && (float) $height.' ' == $height.' ' ? $height : null);
					} else if (!$BESTDEALS_GLOBALS['sc_slider_bg_image']) {
						$post_attachment = '<img src="'.esc_url($post_attachment).'" alt="">';
					}
					$post_accent_color = '';
					$post_category = '';
					$post_category_link = '';
	
					if (in_array($pagination, array('full', 'over'))) {
						$old_output = $output;
						$output = '';
						if (file_exists(bestdeals_get_file_dir('templates/_parts/widgets-posts.php'))) {
							require(bestdeals_get_file_dir('templates/_parts/widgets-posts.php'));
						}
						$pagination_items .= $output;
						$output = $old_output;
					}
					$output .= '<div' 
						. ' class="'.esc_attr($engine).'-slide"'
						. ' data-style="'.esc_attr(($ws).($hs)).'"'
						. ' style="'
							. ($BESTDEALS_GLOBALS['sc_slider_bg_image'] ? 'background-image:url(' . esc_url($post_attachment) . ');' : '') . ($ws) . ($hs)
							. '"'
						. '>' 
						. (bestdeals_param_is_on($links) ? '<a href="'.esc_url($post_link).'" title="'.esc_attr($post_title).'">' : '')
						. (!$BESTDEALS_GLOBALS['sc_slider_bg_image'] ? $post_attachment : '')
						;
					$caption = $engine=='swiper' ? '' : $caption;
					if (!bestdeals_param_is_off($titles)) {
						$post_hover_bg  = bestdeals_get_scheme_color('accent1');
						$post_bg = '';
						if ($post_hover_bg!='' && !bestdeals_is_inherit_option($post_hover_bg)) {
							$rgb = bestdeals_hex2rgb($post_hover_bg);
							$post_hover_ie = str_replace('#', '', $post_hover_bg);
							$post_bg = "background-color: rgba({$rgb['r']},{$rgb['g']},{$rgb['b']},0.8);";
						}
						$caption .= '<div class="sc_slider_info' . ($titles=='fixed' ? ' sc_slider_info_fixed' : '') . ($engine=='swiper' ? ' content-slide' : '') . '"'.($post_bg!='' ? ' style="'.esc_attr($post_bg).'"' : '').'>';
						$post_descr = bestdeals_get_post_excerpt();
						if (bestdeals_get_custom_option("slider_info_category")=='yes') { // || empty($cat)) {
							// Get all post's categories
							$post_tax = bestdeals_get_taxonomy_categories_by_post_type($post_type);
							if (!empty($post_tax)) {
								$post_terms = bestdeals_get_terms_by_post_id(array('post_id'=>$post_id, 'taxonomy'=>$post_tax));
								if (!empty($post_terms[$post_tax])) {
									if (!empty($post_terms[$post_tax]->closest_parent)) {
										$post_category = $post_terms[$post_tax]->closest_parent->name;
										$post_category_link = $post_terms[$post_tax]->closest_parent->link;
									}
									if ($post_category!='') {
										$caption .= '<div class="sc_slider_category"'.(bestdeals_substr($post_accent_color, 0, 1)=='#' ? ' style="background-color: '.esc_attr($post_accent_color).'"' : '').'><a href="'.esc_url($post_category_link).'">'.($post_category).'</a></div>';
									}
								}
							}
						}
						$output_reviews = '';
						if (bestdeals_get_custom_option('show_reviews')=='yes' && bestdeals_get_custom_option('slider_info_reviews')=='yes') {
							$avg_author = bestdeals_reviews_marks_to_display(get_post_meta($post_id, 'reviews_avg'.((bestdeals_get_theme_option('reviews_first')=='author' && $orderby != 'users_rating') || $orderby == 'author_rating' ? '' : '2'), true));
							if ($avg_author > 0) {
								$output_reviews .= '<div class="sc_slider_reviews post_rating reviews_summary blog_reviews' . (bestdeals_get_custom_option("slider_info_category")=='yes' ? ' after_category' : '') . '">'
									. '<div class="criteria_summary criteria_row">' . trim(bestdeals_reviews_get_summary_stars($avg_author, false, false, 5)) . '</div>'
									. '</div>';
							}
						}
						if (bestdeals_get_custom_option("slider_info_category")=='yes') $caption .= $output_reviews;
						$caption .= '<h3 class="sc_slider_subtitle"><a href="'.esc_url($post_link).'">'.($post_title).'</a></h3>';
						if (bestdeals_get_custom_option("slider_info_category")!='yes') $caption .= $output_reviews;
						if ($descriptions > 0) {
							$caption .= '<div class="sc_slider_descr">'.trim(bestdeals_strshort($post_descr, $descriptions)).'</div>';
						}
						$caption .= '</div>';
					}
					$output .= ($engine=='swiper' ? $caption : '') . (bestdeals_param_is_on($links) ? '</a>' : '' ) . '</div>';
				}
				wp_reset_postdata();
			}
	
			$output .= '</div>';
			if ($engine=='swiper') {
				if (bestdeals_param_is_on($controls))
					$output .= '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>';
				if (bestdeals_param_is_on($pagination))
					$output .= '<div class="sc_slider_pagination_wrap"></div>';
			}
		
		} else
			$output = '';
		
		if (!empty($output)) {
			$output .= '</div>';
			if (!empty($pagination_items)) {
				$output .= '
					<div class="sc_slider_pagination widget_area"'.($hs ? ' style="'.esc_attr($hs).'"' : '').'>
						<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_vertical swiper-slider-container scroll-container"'.($hs ? ' style="'.esc_attr($hs).'"' : '').'>
							<div class="sc_scroll_wrapper swiper-wrapper">
								<div class="sc_scroll_slide swiper-slide">
									'.($pagination_items).'
								</div>
							</div>
							<div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_vertical"></div>
						</div>
					</div>';
				$output .= '</div>';
			}
		}
	
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_slider', $atts, $content);
	}
	add_shortcode('trx_slider', 'bestdeals_sc_slider');
}


if (!function_exists('bestdeals_sc_slider_item')) {	
	function bestdeals_sc_slider_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"src" => "",
			"url" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$src = $src!='' ? $src : $url;
		if ($src > 0) {
			$attach = wp_get_attachment_image_src( $src, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$src = $attach[0];
		}
	
		if ($src && bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_slider_crop_image'])) {
			$src = $BESTDEALS_GLOBALS['sc_slider_bg_image']
				? bestdeals_get_resized_image_url($src, !empty($BESTDEALS_GLOBALS['sc_slider_width']) && bestdeals_strpos($BESTDEALS_GLOBALS['sc_slider_width'], '%')===false ? $BESTDEALS_GLOBALS['sc_slider_width'] : null, !empty($BESTDEALS_GLOBALS['sc_slider_height']) && bestdeals_strpos($BESTDEALS_GLOBALS['sc_slider_height'], '%')===false ? $BESTDEALS_GLOBALS['sc_slider_height'] : null)
				: bestdeals_get_resized_image_tag($src, !empty($BESTDEALS_GLOBALS['sc_slider_width']) && bestdeals_strpos($BESTDEALS_GLOBALS['sc_slider_width'], '%')===false ? $BESTDEALS_GLOBALS['sc_slider_width'] : null, !empty($BESTDEALS_GLOBALS['sc_slider_height']) && bestdeals_strpos($BESTDEALS_GLOBALS['sc_slider_height'], '%')===false ? $BESTDEALS_GLOBALS['sc_slider_height'] : null);
		} else if ($src && !$BESTDEALS_GLOBALS['sc_slider_bg_image']) {
			$src = '<img src="'.esc_url($src).'" alt="">';
		}
	
		$css .= ($BESTDEALS_GLOBALS['sc_slider_bg_image'] ? 'background-image:url(' . esc_url($src) . ');' : '')
				. (!empty($BESTDEALS_GLOBALS['sc_slider_width'])  ? 'width:'  . esc_attr($BESTDEALS_GLOBALS['sc_slider_width'])  . ';' : '')
				. (!empty($BESTDEALS_GLOBALS['sc_slider_height']) ? 'height:' . esc_attr($BESTDEALS_GLOBALS['sc_slider_height']) . ';' : '');
	
		$content = do_shortcode($content);
	
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '').' class="'.esc_attr($BESTDEALS_GLOBALS['sc_slider_engine']).'-slide' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. ($css ? ' style="'.esc_attr($css).'"' : '')
				.'>' 
				. ($src && bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_slider_links']) ? '<a href="'.esc_url($src).'">' : '')
				. ($src && !$BESTDEALS_GLOBALS['sc_slider_bg_image'] ? $src : $content)
				. ($src && bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_slider_links']) ? '</a>' : '')
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_slider_item', $atts, $content);
	}
	add_shortcode('trx_slider_item', 'bestdeals_sc_slider_item');
}
// ---------------------------------- [/trx_slider] ---------------------------------------





// ---------------------------------- [trx_socials] ---------------------------------------

/*
[trx_socials id="unique_id" size="small"]
	[trx_social_item name="facebook" url="profile url" icon="path for the icon"]
	[trx_social_item name="twitter" url="profile url"]
[/trx_socials]
*/

if (!function_exists('bestdeals_sc_socials')) {	
	function bestdeals_sc_socials($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"size" => "small",		// tiny | small | medium | large
			"shape" => "square",	// round | square
			"type" => bestdeals_get_theme_setting('socials_type'),	// icons | images
			"socials" => "",
			"custom" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_social_icons'] = false;
		$BESTDEALS_GLOBALS['sc_social_type'] = $type;
		if (!empty($socials)) {
			$allowed = explode('|', $socials);
			$list = array();
			for ($i=0; $i<count($allowed); $i++) {
				$s = explode('=', $allowed[$i]);
				if (!empty($s[1])) {
					$list[] = array(
						'icon'	=> $type=='images' ? bestdeals_get_socials_url($s[0]) : 'icon-'.$s[0],
						'url'	=> $s[1]
						);
				}
			}
			if (count($list) > 0) $BESTDEALS_GLOBALS['sc_social_icons'] = $list;
		} else if (bestdeals_param_is_off($custom))
			$content = do_shortcode($content);
		if ($BESTDEALS_GLOBALS['sc_social_icons']===false) $BESTDEALS_GLOBALS['sc_social_icons'] = bestdeals_get_custom_option('social_icons');
		$output = bestdeals_prepare_socials($BESTDEALS_GLOBALS['sc_social_icons']);
		$output = $output
			? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_socials sc_socials_type_' . esc_attr($type) . ' sc_socials_shape_' . esc_attr($shape) . ' sc_socials_size_' . esc_attr($size) . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>' 
				. ($output)
				. '</div>'
			: '';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_socials', $atts, $content);
	}
	add_shortcode('trx_socials', 'bestdeals_sc_socials');
}


if (!function_exists('bestdeals_sc_social_item')) {	
	function bestdeals_sc_social_item($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"name" => "",
			"url" => "",
			"icon" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		if (!empty($name) && empty($icon)) {
			$type = $BESTDEALS_GLOBALS['sc_social_type'];
			if ($type=='images') {
				if (file_exists(bestdeals_get_socials_dir($name.'.png')))
					$icon = bestdeals_get_socials_url($name.'.png');
			} else
				$icon = 'icon-'.esc_attr($name);
		}
		if (!empty($icon) && !empty($url)) {
			if ($BESTDEALS_GLOBALS['sc_social_icons']===false) $BESTDEALS_GLOBALS['sc_social_icons'] = array();
			$BESTDEALS_GLOBALS['sc_social_icons'][] = array(
				'icon' => $icon,
				'url' => $url
			);
		}
		return '';
	}
	add_shortcode('trx_social_item', 'bestdeals_sc_social_item');
}
// ---------------------------------- [/trx_socials] ---------------------------------------





// ---------------------------------- [trx_table] ---------------------------------------

/*
[trx_table id="unique_id" style="1"]
Table content, generated on one of many public internet resources, for example: http://www.impressivewebs.com/html-table-code-generator/
[/trx_table]
*/

if (!function_exists('bestdeals_sc_table')) {	
	function bestdeals_sc_table($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "100%"
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width);
		$content = str_replace(
					array('<p><table', 'table></p>', '><br />'),
					array('<table', 'table>', '>'),
					html_entity_decode($content, ENT_COMPAT, 'UTF-8'));
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_table' 
					. (!empty($align) && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				.'>' 
				. do_shortcode($content) 
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_table', $atts, $content);
	}
	add_shortcode('trx_table', 'bestdeals_sc_table');
}
// ---------------------------------- [/trx_table] ---------------------------------------






// ---------------------------------- [trx_trx_pr] ---------------------------------------

/*
[trx_tabs id="unique_id" tab_names="Planning|Development|Support" style="1|2" initial="1 - num_tabs"]
	[trx_tab]Randomised words which don't look even slightly believable. If you are going to use a passage. You need to be sure there isn't anything embarrassing hidden in the middle of text established fact that a reader will be istracted by the readable content of a page when looking at its layout.[/trx_tab]
	[trx_tab]Fact reader will be distracted by the <a href="#" class="main_link">readable content</a> of a page when. Looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here, content here, making it look like readable English will uncover many web sites still in their infancy. Various versions have evolved over. There are many variations of passages of Lorem Ipsum available, but the majority.[/trx_tab]
	[trx_tab]Distracted by the  readable content  of a page when. Looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here, content here, making it look like readable English will uncover many web sites still in their infancy. Various versions have  evolved over.  There are many variations of passages of Lorem Ipsum available.[/trx_tab]
[/trx_tabs]
*/

if (!function_exists('bestdeals_sc_tabs')) {	
	function bestdeals_sc_tabs($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"initial" => "1",
			"scroll" => "no",
			"style" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width);
	
		if (!bestdeals_param_is_off($scroll)) bestdeals_enqueue_slider();
		if (empty($id)) $id = 'sc_tabs_'.str_replace('.', '', mt_rand());
	
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_tab_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_tab_scroll'] = $scroll;
		$BESTDEALS_GLOBALS['sc_tab_height'] = bestdeals_prepare_css_value($height);
		$BESTDEALS_GLOBALS['sc_tab_id']     = $id;
		$BESTDEALS_GLOBALS['sc_tab_titles'] = array();
	
		$content = do_shortcode($content);
	
		$sc_tab_titles = $BESTDEALS_GLOBALS['sc_tab_titles'];
	
		$initial = max(1, min(count($sc_tab_titles), (int) $initial));
	
		$tabs_output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
							. ' class="sc_tabs sc_tabs_style_'.esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
							. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
							. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. ' data-active="' . ($initial-1) . '"'
							. '>'
						.'<ul class="sc_tabs_titles">';
		$titles_output = '';
		for ($i = 0; $i < count($sc_tab_titles); $i++) {
			$classes = array('sc_tabs_title');
			if ($i == 0) $classes[] = 'first';
			else if ($i == count($sc_tab_titles) - 1) $classes[] = 'last';
			$titles_output .= '<li class="'.join(' ', $classes).'">'
								. '<a href="#'.esc_attr($sc_tab_titles[$i]['id']).'" class="theme_button" id="'.esc_attr($sc_tab_titles[$i]['id']).'_tab">' . ($sc_tab_titles[$i]['title']) . '</a>'
								. '</li>';
		}
	
		wp_enqueue_script('jquery-ui-tabs');
		wp_enqueue_script('jquery-effects-fade');
	
		$tabs_output .= $titles_output
			. '</ul>' 
			. ($content)
			.'</div>';
		return apply_filters('bestdeals_shortcode_output', $tabs_output, 'trx_tabs', $atts, $content);
	}
	add_shortcode("trx_tabs", "bestdeals_sc_tabs");
}


if (!function_exists('bestdeals_sc_tab')) {	
	function bestdeals_sc_tab($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"tab_id" => "",		// get it from VC
			"title" => "",		// get it from VC
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_tab_counter']++;
		if (empty($id))
			$id = !empty($tab_id) ? $tab_id : ($BESTDEALS_GLOBALS['sc_tab_id']).'_'.($BESTDEALS_GLOBALS['sc_tab_counter']);
		$sc_tab_titles = $BESTDEALS_GLOBALS['sc_tab_titles'];
		if (isset($sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1])) {
			$sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1]['id'] = $id;
			if (!empty($title))
				$sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1]['title'] = $title;
		} else {
			$sc_tab_titles[] = array(
				'id' => $id,
				'title' => $title
			);
		}
		$BESTDEALS_GLOBALS['sc_tab_titles'] = $sc_tab_titles;
		$output = '<div id="'.esc_attr($id).'"'
					.' class="sc_tabs_content' 
						. ($BESTDEALS_GLOBALS['sc_tab_counter'] % 2 == 1 ? ' odd' : ' even') 
						. ($BESTDEALS_GLOBALS['sc_tab_counter'] == 1 ? ' first' : '') 
						. (!empty($class) ? ' '.esc_attr($class) : '') 
						. '"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. '>' 
				. (bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_tab_scroll']) 
					? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_vertical" style="height:'.($BESTDEALS_GLOBALS['sc_tab_height'] != '' ? $BESTDEALS_GLOBALS['sc_tab_height'] : '200px').';"><div class="sc_scroll_wrapper swiper-wrapper"><div class="sc_scroll_slide swiper-slide">' 
					: '')
				. do_shortcode($content) 
				. (bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_tab_scroll']) 
					? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_vertical '.esc_attr($id).'_scroll_bar"></div></div>' 
					: '')
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_tab', $atts, $content);
	}
	add_shortcode("trx_tab", "bestdeals_sc_tab");
}
// ---------------------------------- [/trx_tabs] ---------------------------------------





// ---------------------------------- [trx_title] ---------------------------------------

/*
[trx_title id="unique_id" style='regular|iconed' icon='' image='' background="on|off" type="1-6"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_title]
*/

if (!function_exists('bestdeals_sc_title')) {	
	function bestdeals_sc_title($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "1",
			"style" => "regular",
			"align" => "",
			"font_weight" => "",
			"font_size" => "",
			"color" => "",
			"icon" => "",
			"image" => "",
			"picture" => "",
			"image_size" => "small",
			"position" => "left",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width)
			.($align && $align!='none' && !bestdeals_param_is_inherit($align) ? 'text-align:' . esc_attr($align) .';' : '')
			.($color ? 'color:' . esc_attr($color) .';' : '')
			.($font_weight && !bestdeals_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) .';' : '')
			.($font_size   ? 'font-size:' . esc_attr($font_size) .';' : '')
			;
		$type = min(6, max(1, $type));
		if ($picture > 0) {
			$attach = wp_get_attachment_image_src( $picture, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$picture = $attach[0];
		}
		$pic = $style!='iconed' 
			? '' 
			: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '').'"'.'>'
				.($picture ? '<img src="'.esc_url($picture).'" alt="" />' : '')
				.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(bestdeals_strpos($image, 'http:')!==false ? $image : bestdeals_get_file_url('images/icons/'.($image).'.png')).'" alt="" />' : '')
				.'</span>';
		$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_title sc_title_'.esc_attr($style)
					.($align && $align!='none' && !bestdeals_param_is_inherit($align) ? ' sc_align_' . esc_attr($align) : '')
					.(!empty($class) ? ' '.esc_attr($class) : '')
					.'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
					. ($pic)
					. ($style=='divider' ? '<span class="sc_title_divider_before"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
					. do_shortcode($content) 
					. ($style=='divider' ? '<span class="sc_title_divider_after"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
				. '</h' . esc_attr($type) . '>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_title', $atts, $content);
	}
	add_shortcode('trx_title', 'bestdeals_sc_title');
}
// ---------------------------------- [/trx_title] ---------------------------------------






// ---------------------------------- [trx_toggles] ---------------------------------------

if (!function_exists('bestdeals_sc_toggles')) {	
	function bestdeals_sc_toggles($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"counter" => "off",
			"icon_closed" => "icon-plus",
			"icon_opened" => "icon-minus",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_toggle_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_toggle_style']   = max(1, min(2, $style));
		$BESTDEALS_GLOBALS['sc_toggle_show_counter'] = bestdeals_param_is_on($counter);
		$BESTDEALS_GLOBALS['sc_toggles_icon_closed'] = empty($icon_closed) || bestdeals_param_is_inherit($icon_closed) ? "icon-plus" : $icon_closed;
		$BESTDEALS_GLOBALS['sc_toggles_icon_opened'] = empty($icon_opened) || bestdeals_param_is_inherit($icon_opened) ? "icon-minus" : $icon_opened;
		wp_enqueue_script('jquery-effects-slide');
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_toggles sc_toggles_style_'.esc_attr($style)
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. (bestdeals_param_is_on($counter) ? ' sc_show_counter' : '') 
					. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. '>'
				. do_shortcode($content)
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_toggles', $atts, $content);
	}
	add_shortcode('trx_toggles', 'bestdeals_sc_toggles');
}


if (!function_exists('bestdeals_sc_toggles_item')) {	
	function bestdeals_sc_toggles_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"title" => "",
			"open" => "",
			"icon_closed" => "",
			"icon_opened" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_toggle_counter']++;
		if (empty($icon_closed) || bestdeals_param_is_inherit($icon_closed)) $icon_closed = $BESTDEALS_GLOBALS['sc_toggles_icon_closed'] ? $BESTDEALS_GLOBALS['sc_toggles_icon_closed'] : "icon-plus";
		if (empty($icon_opened) || bestdeals_param_is_inherit($icon_opened)) $icon_opened = $BESTDEALS_GLOBALS['sc_toggles_icon_opened'] ? $BESTDEALS_GLOBALS['sc_toggles_icon_opened'] : "icon-minus";
		$css .= bestdeals_param_is_on($open) ? 'display:block;' : '';
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_toggles_item'.(bestdeals_param_is_on($open) ? ' sc_active' : '')
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($BESTDEALS_GLOBALS['sc_toggle_counter'] % 2 == 1 ? ' odd' : ' even') 
					. ($BESTDEALS_GLOBALS['sc_toggle_counter'] == 1 ? ' first' : '')
					. '">'
					. '<h5 class="sc_toggles_title'.(bestdeals_param_is_on($open) ? ' ui-state-active' : '').'">'
					. (!bestdeals_param_is_off($icon_closed) ? '<span class="sc_toggles_icon sc_toggles_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
					. (!bestdeals_param_is_off($icon_opened) ? '<span class="sc_toggles_icon sc_toggles_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
					. ($BESTDEALS_GLOBALS['sc_toggle_show_counter'] ? '<span class="sc_items_counter">'.($BESTDEALS_GLOBALS['sc_toggle_counter']).'</span>' : '')
					. ($title) 
					. '</h5>'
					. '<div class="sc_toggles_content"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						.'>' 
						. do_shortcode($content) 
					. '</div>'
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_toggles_item', $atts, $content);
	}
	add_shortcode('trx_toggles_item', 'bestdeals_sc_toggles_item');
}
// ---------------------------------- [/trx_toggles] ---------------------------------------





// ---------------------------------- [trx_tooltip] ---------------------------------------

/*
[trx_tooltip id="unique_id" title="Tooltip text here"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/tooltip]
*/

if (!function_exists('bestdeals_sc_tooltip')) {	
	function bestdeals_sc_tooltip($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_tooltip_parent'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
						. do_shortcode($content)
						. '<span class="sc_tooltip">' . ($title) . '</span>'
					. '</span>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_tooltip', $atts, $content);
	}
	add_shortcode('trx_tooltip', 'bestdeals_sc_tooltip');
}
// ---------------------------------- [/trx_tooltip] ---------------------------------------






// ---------------------------------- [trx_twitter] ---------------------------------------

/*
[trx_twitter id="unique_id" user="username" consumer_key="" consumer_secret="" token_key="" token_secret=""]
*/

if (!function_exists('bestdeals_sc_twitter')) {	
	function bestdeals_sc_twitter($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"user" => "",
			"consumer_key" => "",
			"consumer_secret" => "",
			"token_key" => "",
			"token_secret" => "",
			"count" => "3",
			"controls" => "yes",
			"interval" => "",
			"autoheight" => "no",
			"align" => "",
			"scheme" => "",
			"bg_color" => "",
			"bg_image" => "",
			"bg_overlay" => "",
			"bg_texture" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		$twitter_username = $user ? $user : bestdeals_get_theme_option('twitter_username');
		$twitter_consumer_key = $consumer_key ? $consumer_key : bestdeals_get_theme_option('twitter_consumer_key');
		$twitter_consumer_secret = $consumer_secret ? $consumer_secret : bestdeals_get_theme_option('twitter_consumer_secret');
		$twitter_token_key = $token_key ? $token_key : bestdeals_get_theme_option('twitter_token_key');
		$twitter_token_secret = $token_secret ? $token_secret : bestdeals_get_theme_option('twitter_token_secret');
		$twitter_count = max(1, $count ? $count : intval(bestdeals_get_theme_option('twitter_count')));
	
		if (empty($id)) $id = "sc_testimonials_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);
	
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
	
		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = bestdeals_get_scheme_color('bg');
			$rgb = bestdeals_hex2rgb($bg_color);
		}
		
		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
	
		$css .= ($ms) . ($hs) . ($ws);
	
		$output = '';
	
		if (!empty($twitter_consumer_key) && !empty($twitter_consumer_secret) && !empty($twitter_token_key) && !empty($twitter_token_secret)) {
			$data = bestdeals_get_twitter_data(array(
				'mode'            => 'user_timeline',
				'consumer_key'    => $twitter_consumer_key,
				'consumer_secret' => $twitter_consumer_secret,
				'token'           => $twitter_token_key,
				'secret'          => $twitter_token_secret
				)
			);
			if ($data && isset($data[0]['text'])) {
				bestdeals_enqueue_slider('swiper');
				$output = ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2 || ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme))
						? '<div class="sc_twitter_wrap sc_section'
								. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
								. ($align && $align!='none' && !bestdeals_param_is_inherit($align) ? ' align' . esc_attr($align) : '')
								. '"'
							.' style="'
								. ($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
								. ($bg_image !== '' ? 'background-image:url('.esc_url($bg_image).');' : '')
								. '"'
							. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. '>'
							. '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
									. ' style="' 
										. ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
										. (bestdeals_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
										. '"'
										. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
										. '>' 
						: '')
						. '<div class="sc_twitter sc_slider_swiper sc_slider_nopagination swiper-slider-container'
								. (bestdeals_param_is_on($controls) ? ' sc_slider_controls' : ' sc_slider_nocontrols')
								. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
								. ($hs ? ' sc_slider_height_fixed' : '')
								. (!empty($class) ? ' '.esc_attr($class) : '')
								. ($bg_color=='' && $bg_image=='' && $bg_overlay==0 && ($bg_texture=='' || $bg_texture=='0') && $align && $align!='none' && !bestdeals_param_is_inherit($align) ? ' align' . esc_attr($align) : '')
								. '"'
							. ($bg_color=='' && $bg_image=='' && $bg_overlay==0 && ($bg_texture=='' || $bg_texture=='0') && !bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
							. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
							. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
							. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
							. '>'
							. '<div class="slides swiper-wrapper">';
				$cnt = 0;
				if (is_array($data) && count($data) > 0) {
					foreach ($data as $tweet) {
						if (bestdeals_substr($tweet['text'], 0, 1)=='@') continue;
							$output .= '<div class="swiper-slide" data-style="'.esc_attr(($ws).($hs)).'" style="'.esc_attr(($ws).($hs)).'">'
										. '<div class="sc_twitter_item">'
											. '<span class="sc_twitter_icon icon-twitter"></span>'
											. '<div class="sc_twitter_content">'
												. '<a href="' . esc_url('https://twitter.com/'.($twitter_username)).'" class="sc_twitter_author" target="_blank">@' . esc_html($tweet['user']['screen_name']) . '</a> '
												. force_balance_tags(bestdeals_prepare_twitter_text($tweet))
											. '</div>'
										. '</div>'
									. '</div>';
						if (++$cnt >= $twitter_count) break;
					}
				}
				$output .= '</div>'
						. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
					. '</div>'
					. ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2
						?  '</div></div>'
						: '');
			}
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_twitter', $atts, $content);
	}
	add_shortcode('trx_twitter', 'bestdeals_sc_twitter');
}
// ---------------------------------- [/trx_twitter] ---------------------------------------

						


// ---------------------------------- [trx_video] ---------------------------------------

//[trx_video id="unique_id" url="http://player.vimeo.com/video/20245032?title=0&amp;byline=0&amp;portrait=0" width="" height=""]

if (!function_exists('bestdeals_sc_video')) {	
	function bestdeals_sc_video($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"url" => '',
			"src" => '',
			"image" => '',
			"ratio" => '16:9',
			"autoplay" => 'off',
			"align" => '',
			"bg_image" => '',
			"bg_top" => '',
			"bg_bottom" => '',
			"bg_left" => '',
			"bg_right" => '',
			"frame" => "on",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => '',
			"height" => '',
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if (empty($autoplay)) $autoplay = 'off';
		
		$ratio = empty($ratio) ? "16:9" : str_replace(array('/','\\','-'), ':', $ratio);
		$ratio_parts = explode(':', $ratio);
		if (empty($height) && empty($width)) {
			$width='100%';
			if (bestdeals_param_is_off(bestdeals_get_custom_option('substitute_video'))) $height="400";
		}
		$ed = bestdeals_substr($width, -1);
		if (empty($height) && !empty($width) && $ed!='%') {
			$height = round($width / $ratio_parts[0] * $ratio_parts[1]);
		}
		if (!empty($height) && empty($width)) {
			$width = round($height * $ratio_parts[0] / $ratio_parts[1]);
		}
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$css_dim = bestdeals_get_css_position_from_values('', '', '', '', $width, $height);
		$css_bg = bestdeals_get_css_paddings_from_values($bg_top, $bg_right, $bg_bottom, $bg_left);
	
		if ($src=='' && $url=='' && isset($atts[0])) {
			$src = $atts[0];
		}
		$url = $src!='' ? $src : $url;
		if ($image!='' && bestdeals_param_is_off($image))
			$image = '';
		else {
			if (bestdeals_param_is_on($autoplay) && is_singular() && !bestdeals_get_global('blog_streampage'))
				$image = '';
			else {
				if ($image > 0) {
					$attach = wp_get_attachment_image_src( $image, 'full' );
					if (isset($attach[0]) && $attach[0]!='')
						$image = $attach[0];
				}
				if ($bg_image) {
					$thumb_sizes = bestdeals_get_thumb_sizes(array(
						'layout' => 'grid_3'
					));
					if (!is_single() || !empty($image)) $image = bestdeals_get_resized_image_url(empty($image) ? get_the_ID() : $image, $thumb_sizes['w'], $thumb_sizes['h'], null, false, false, false);
				} else
					if (!is_single() || !empty($image)) $image = bestdeals_get_resized_image_url(empty($image) ? get_the_ID() : $image, $ed!='%' ? $width : null, $height);
				if (empty($image) && (!is_singular() || bestdeals_get_global('blog_streampage')))	// || bestdeals_param_is_off($autoplay)))
					$image = bestdeals_get_video_cover_image($url);
			}
		}
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
		if ($bg_image) {
			$css_bg .= $css . 'background-image: url('.esc_url($bg_image).');';
			$css = $css_dim;
		} else {
			$css .= $css_dim;
		}
	
		$url = bestdeals_get_video_player_url($src!='' ? $src : $url);
		
		$video = '<video' . ($id ? ' id="' . esc_attr($id) . '"' : '') 
			. ' class="sc_video'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
			. ' src="' . esc_url($url) . '"'
			. ' width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"' 
			. ' data-width="' . esc_attr($width) . '" data-height="' . esc_attr($height) . '"' 
			. ' data-ratio="'.esc_attr($ratio).'"'
			. ($image ? ' poster="'.esc_attr($image).'" data-image="'.esc_attr($image).'"' : '') 
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. ($align && $align!='none' ? ' data-align="'.esc_attr($align).'"' : '')
			. ($bg_image ? ' data-bg-image="'.esc_attr($bg_image).'"' : '') 
			. ($css_bg!='' ? ' data-style="'.esc_attr($css_bg).'"' : '') 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. (($image && bestdeals_param_is_on(bestdeals_get_custom_option('substitute_video'))) || (bestdeals_param_is_on($autoplay) && is_singular() && !bestdeals_get_global('blog_streampage')) ? ' autoplay="autoplay"' : '') 
			. ' controls="controls" loop="loop"'
			. '>'
			. '</video>';
		if (bestdeals_param_is_off(bestdeals_get_custom_option('substitute_video'))) {
			if (bestdeals_param_is_on($frame)) $video = bestdeals_get_video_frame($video, $image, $css, $css_bg);
		} else {
			if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
				$video = bestdeals_substitute_video($video, $width, $height, false);
			}
		}
		if (bestdeals_get_theme_option('use_mediaelement')=='yes')
			wp_enqueue_script('wp-mediaelement');
		return apply_filters('bestdeals_shortcode_output', $video, 'trx_video', $atts, $content);
	}
	add_shortcode("trx_video", "bestdeals_sc_video");
}
// ---------------------------------- [/trx_video] ---------------------------------------






// ---------------------------------- [trx_zoom] ---------------------------------------

/*
[trx_zoom id="unique_id" border="none|light|dark"]
*/

if (!function_exists('bestdeals_sc_zoom')) {	
	function bestdeals_sc_zoom($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"effect" => "zoom",
			"src" => "",
			"url" => "",
			"over" => "",
			"align" => "",
			"bg_image" => "",
			"bg_top" => '',
			"bg_bottom" => '',
			"bg_left" => '',
			"bg_right" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		wp_enqueue_script( 'bestdeals-elevate-zoom-script', bestdeals_get_file_url('js/jquery.elevateZoom.js'), array(), null, true );
	
		$css .= bestdeals_get_css_position_from_values('!'.($top), '!'.($right), '!'.($bottom), '!'.($left));
		$css_dim = bestdeals_get_css_position_from_values('', '', '', '', $width, $height);
		$css_bg = bestdeals_get_css_paddings_from_values($bg_top, $bg_right, $bg_bottom, $bg_left);
		$width  = bestdeals_prepare_css_value($width);
		$height = bestdeals_prepare_css_value($height);
		if (empty($id)) $id = 'sc_zoom_'.str_replace('.', '', mt_rand());
		$src = $src!='' ? $src : $url;
		if ($src > 0) {
			$attach = wp_get_attachment_image_src( $src, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$src = $attach[0];
		}
		if ($over > 0) {
			$attach = wp_get_attachment_image_src( $over, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$over = $attach[0];
		}
		if ($effect=='lens' && ((int) $width > 0 && bestdeals_substr($width, -2, 2)=='px') || ((int) $height > 0 && bestdeals_substr($height, -2, 2)=='px')) {
			if ($src)
				$src = bestdeals_get_resized_image_url($src, (int) $width > 0 && bestdeals_substr($width, -2, 2)=='px' ? (int) $width : null, (int) $height > 0 && bestdeals_substr($height, -2, 2)=='px' ? (int) $height : null);
			if ($over)
				$over = bestdeals_get_resized_image_url($over, (int) $width > 0 && bestdeals_substr($width, -2, 2)=='px' ? (int) $width : null, (int) $height > 0 && bestdeals_substr($height, -2, 2)=='px' ? (int) $height : null);
		}
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
		if ($bg_image) {
			$css_bg .= $css . 'background-image: url('.esc_url($bg_image).');';
			$css = $css_dim;
		} else {
			$css .= $css_dim;
		}
		$output = empty($src) 
				? '' 
				: (
					(!empty($bg_image) 
						? '<div class="sc_zoom_wrap'
								. (!empty($class) ? ' '.esc_attr($class) : '')
								. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
								. '"'
							. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. ($css_bg!='' ? ' style="'.esc_attr($css_bg).'"' : '') 
							. '>' 
						: '')
					.'<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_zoom' 
								. (empty($bg_image) && !empty($class) ? ' '.esc_attr($class) : '') 
								. (empty($bg_image) && $align && $align!='none' ? ' align'.esc_attr($align) : '')
								. '"'
							. (empty($bg_image) && !bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
							. '>'
							. '<img src="'.esc_url($src).'"' . ($css_dim!='' ? ' style="'.esc_attr($css_dim).'"' : '') . ' data-zoom-image="'.esc_url($over).'" alt="" />'
					. '</div>'
					. (!empty($bg_image) 
						? '</div>' 
						: '')
				);
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_zoom', $atts, $content);
	}
	add_shortcode('trx_zoom', 'bestdeals_sc_zoom');
}
// ---------------------------------- [/trx_zoom] ---------------------------------------




// ---------------------------------- [trx_team] ---------------------------------------

/*
[trx_team id="unique_id" columns="3" style="team-1|team-2|..."]
	[trx_team_item user="user_login"]
	[trx_team_item member="member_id"]
	[trx_team_item name="team member name" photo="url" email="address" position="director"]
[/trx_team]
*/
if ( !function_exists( 'bestdeals_sc_team' ) ) {
	function bestdeals_sc_team($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "team-1",
			"slider" => "no",
			"controls" => "no",
			"slides_space" => 0,
			"interval" => "",
			"autoheight" => "no",
			"align" => "",
			"custom" => "no",
			"ids" => "",
			"cat" => "",
			"count" => 3,
			"columns" => 3,
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link" => '',
			"scheme" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($id)) $id = "sc_team_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
		$css .= ($ms) . ($hs) . ($ws);

		$count = max(1, (int) $count);
		$columns = max(1, min(12, (int) $columns));
		if (bestdeals_param_is_off($custom) && $count < $columns) $columns = $count;

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_team_id'] = $id;
		$BESTDEALS_GLOBALS['sc_team_style'] = $style;
		$BESTDEALS_GLOBALS['sc_team_columns'] = $columns;
		$BESTDEALS_GLOBALS['sc_team_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_team_slider'] = $slider;
		$BESTDEALS_GLOBALS['sc_team_css_wh'] = $ws . $hs;

		if (bestdeals_param_is_on($slider)) bestdeals_enqueue_slider('swiper');

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '')
			. ' class="sc_team_wrap'
			. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '')
			.'">'
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_team sc_team_style_'.esc_attr($style)
			. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
			. ' ' . esc_attr(bestdeals_get_slider_controls_classes($controls))
			. (bestdeals_param_is_on($slider)
				? ' sc_slider_swiper swiper-slider-container'
				. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				: '')
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
			.'"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
			. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
			. (!empty($subtitle) ? '<h6 class="sc_team_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
			. (!empty($title) ? '<h2 class="sc_team_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
			. (!empty($description) ? '<div class="sc_team_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
			. (bestdeals_param_is_on($slider)
				? '<div class="slides swiper-wrapper">'
				: ($columns > 1 // && bestdeals_get_template_property($style, 'need_columns')
					? '<div class="sc_columns columns_wrap">'
					: '')
			);

		$content = do_shortcode($content);

		if (bestdeals_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;

			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}

			$args = array(
				'post_type' => 'team',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
			);

			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}

			$args = bestdeals_query_add_sort_order($args, $orderby, $order);
			$args = bestdeals_query_add_posts_and_cats($args, $ids, 'team', $cat, 'team_group');
			$query = new WP_Query( $args );

			$post_number = 0;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					"columns_count" => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$post_data = bestdeals_get_post_data($args);
				$post_meta = get_post_meta($post_data['post_id'], 'team_data', true);
				$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $style));

				if (is_array($post_meta)) {
					if (isset($post_meta['team_member_position'])) { $args['position'] = $post_meta['team_member_position']; } else { $args['position'] = ''; }
					if (isset($post_meta['team_member_email'])) { $args['email'] = $post_meta['team_member_email']; } else { $args['email'] = ''; }
				} else {
					$args['position'] = '';
					$args['email'] = '';
				}

				$args['link'] = !empty($post_meta['team_member_link']) ? $post_meta['team_member_link'] : $post_data['post_link'];
				$args['photo'] = $post_data['post_thumb'];
				if (empty($args['photo']) && !empty($args['email'])) $args['photo'] = get_avatar($args['email'], $thumb_sizes['w']*min(2, max(1, bestdeals_get_theme_option("retina_ready"))));
				$args['socials'] = '';

				if ( (is_array($post_meta)) and (isset($post_meta['team_member_socials'])) ) {
					$soc_list = $post_meta['team_member_socials'];
				} else {
					$soc_list = '';
				}

				if (is_array($soc_list) && count($soc_list)>0) {
					$soc_str = '';
					foreach ($soc_list as $sn=>$sl) {
						if (!empty($sl))
							$soc_str .= (!empty($soc_str) ? '|' : '') . ($sn) . '=' . ($sl);
					}
					if (!empty($soc_str))
						$args['socials'] = bestdeals_do_shortcode('[trx_socials size="tiny" shape="round" socials="'.esc_attr($soc_str).'"][/trx_socials]');
				}

				$output .= bestdeals_show_post_layout($args, $post_data);
			}
			wp_reset_postdata();
		}

		if (bestdeals_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {// && bestdeals_get_template_property($style, 'need_columns')) {
			$output .= '</div>';
		}

		$output .= (!empty($link) ? '<div class="sc_team_button sc_item_button">'.bestdeals_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
			. '</div><!-- /.sc_team -->'
			. '</div><!-- /.sc_team_wrap -->';

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_team', $atts, $content);
	}
	add_shortcode('trx_team', 'bestdeals_sc_team');
}


if ( !function_exists( 'bestdeals_sc_team_item' ) ) {
	function bestdeals_sc_team_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"user" => "",
			"member" => "",
			"name" => "",
			"position" => "",
			"photo" => "",
			"email" => "",
			"link" => "",
			"socials" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => ""
		), $atts)));

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_team_counter']++;

		$id = $id ? $id : ($BESTDEALS_GLOBALS['sc_team_id'] ? $BESTDEALS_GLOBALS['sc_team_id'] . '_' . $BESTDEALS_GLOBALS['sc_team_counter'] : '');

		$descr = trim(chop(do_shortcode($content)));

		$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $BESTDEALS_GLOBALS['sc_team_style']));

		if (!empty($socials)) $socials = bestdeals_do_shortcode('[trx_socials size="tiny" shape="round" socials="'.esc_attr($socials).'"][/trx_socials]');

		if (!empty($user) && $user!='none' && ($user_obj = get_user_by('login', $user)) != false) {
			$meta = get_user_meta($user_obj->ID);
			if (empty($email))		$email = $user_obj->data->user_email;
			if (empty($name))		$name = $user_obj->data->display_name;
			if (empty($position))	$position = isset($meta['user_position'][0]) ? $meta['user_position'][0] : '';
			if (empty($descr))		$descr = isset($meta['description'][0]) ? $meta['description'][0] : '';
			if (empty($socials))	$socials = bestdeals_show_user_socials(array('author_id'=>$user_obj->ID, 'echo'=>false));
		}

		if (!empty($member) && $member!='none' && ($member_obj = (intval($member) > 0 ? get_post($member, OBJECT) : get_page_by_title($member, OBJECT, 'team'))) != null) {
			if (empty($name))		$name = $member_obj->post_title;
			if (empty($descr))		$descr = $member_obj->post_excerpt;
			$post_meta = get_post_meta($member_obj->ID, 'team_data', true);
			if (empty($position))	$position = $post_meta['team_member_position'];
			if (empty($link))		$link = !empty($post_meta['team_member_link']) ? $post_meta['team_member_link'] : get_permalink($member_obj->ID);
			if (empty($email))		$email = $post_meta['team_member_email'];
			if (empty($photo)) 		$photo = wp_get_attachment_url(get_post_thumbnail_id($member_obj->ID));
			if (empty($socials)) {
				$socials = '';
				$soc_list = $post_meta['team_member_socials'];
				if (is_array($soc_list) && count($soc_list)>0) {
					$soc_str = '';
					foreach ($soc_list as $sn=>$sl) {
						if (!empty($sl))
							$soc_str .= (!empty($soc_str) ? '|' : '') . ($sn) . '=' . ($sl);
					}
					if (!empty($soc_str))
						$socials = bestdeals_do_shortcode('[trx_socials size="tiny" shape="round" socials="'.esc_attr($soc_str).'"][/trx_socials]');
				}
			}
		}
		if (empty($photo)) {
			if (!empty($email)) $photo = get_avatar($email, $thumb_sizes['w']*min(2, max(1, bestdeals_get_theme_option("retina_ready"))));
		} else {
			if ($photo > 0) {
				$attach = wp_get_attachment_image_src( $photo, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$photo = $attach[0];
			}
			$photo = bestdeals_get_resized_image_tag($photo, $thumb_sizes['w'], $thumb_sizes['h']);
		}
		$post_data = array(
			'post_title' => $name,
			'post_excerpt' => $descr
		);
		$args = array(
			'layout' => $BESTDEALS_GLOBALS['sc_team_style'],
			'number' => $BESTDEALS_GLOBALS['sc_team_counter'],
			'columns_count' => $BESTDEALS_GLOBALS['sc_team_columns'],
			'slider' => $BESTDEALS_GLOBALS['sc_team_slider'],
			'show' => false,
			'descr'  => 0,
			'tag_id' => $id,
			'tag_class' => $class,
			'tag_animation' => $animation,
			'tag_css' => $css,
			'tag_css_wh' => $BESTDEALS_GLOBALS['sc_team_css_wh'],
			'position' => $position,
			'link' => $link,
			'email' => $email,
			'photo' => $photo,
			'socials' => $socials
		);
		$output = bestdeals_show_post_layout($args, $post_data);

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_team_item', $atts, $content);
	}
	add_shortcode('trx_team_item', 'bestdeals_sc_team_item');
}
// ---------------------------------- [/trx_team] ---------------------------------------



// Add [trx_team] and [trx_team_item] in the shortcodes list
if (!function_exists('bestdeals_team_reg_shortcodes')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list',	'bestdeals_team_reg_shortcodes');
	function bestdeals_team_reg_shortcodes() {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['shortcodes'])) {

			$users = bestdeals_get_list_users();
			$members = bestdeals_get_list_posts(false, array(
					'post_type'=>'team',
					'orderby'=>'title',
					'order'=>'asc',
					'return'=>'title'
				)
			);
			$team_groups = bestdeals_get_list_terms(false, 'team_group');
			$team_styles = bestdeals_get_list_templates('team');
			$controls	 = bestdeals_get_list_slider_controls();

			bestdeals_array_insert_after($BESTDEALS_GLOBALS['shortcodes'], 'trx_tabs', array(

				// Team
				"trx_team" => array(
					"title" => esc_html__("Team", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert team in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Team style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style to display team members", 'bestdeals-utils'),
							"value" => "1",
							"type" => "select",
							"options" => $team_styles
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns use to show team members", 'bestdeals-utils'),
							"value" => 3,
							"min" => 2,
							"max" => 5,
							"step" => 1,
							"type" => "spinner"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"slider" => array(
							"title" => esc_html__("Slider", 'bestdeals-utils'),
							"desc" => esc_attr__("Use slider to show team members", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Slider controls style and position", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $controls
						),
						"slides_space" => array(
							"title" => esc_html__("Space between slides", 'bestdeals-utils'),
							"desc" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Slides change interval", 'bestdeals-utils'),
							"desc" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'bestdeals-utils'),
							"desc" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Alignment of the team block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_attr__("Allow get team members from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_attr__("Select categories (groups) to show team members. If empty - select team members from any category (group) or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $team_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'bestdeals-utils'),
							"desc" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "title",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "asc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_team_item",
						"title" => esc_html__("Member", 'bestdeals-utils'),
						"desc" => esc_attr__("Team member", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"user" => array(
								"title" => esc_html__("Registerd user", 'bestdeals-utils'),
								"desc" => esc_attr__("Select one of registered users (if present) or put name, position, etc. in fields below", 'bestdeals-utils'),
								"value" => "",
								"type" => "select",
								"options" => $users
							),
							"member" => array(
								"title" => esc_html__("Team member", 'bestdeals-utils'),
								"desc" => esc_attr__("Select one of team members (if present) or put name, position, etc. in fields below", 'bestdeals-utils'),
								"value" => "",
								"type" => "select",
								"options" => $members
							),
							"link" => array(
								"title" => esc_html__("Link", 'bestdeals-utils'),
								"desc" => esc_attr__("Link on team member's personal page", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"name" => array(
								"title" => esc_html__("Name", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's name", 'bestdeals-utils'),
								"divider" => true,
								"dependency" => array(
									'user' => array('is_empty', 'none'),
									'member' => array('is_empty', 'none')
								),
								"value" => "",
								"type" => "text"
							),
							"position" => array(
								"title" => esc_html__("Position", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's position", 'bestdeals-utils'),
								"dependency" => array(
									'user' => array('is_empty', 'none'),
									'member' => array('is_empty', 'none')
								),
								"value" => "",
								"type" => "text"
							),
							"email" => array(
								"title" => esc_html__("E-mail", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's e-mail", 'bestdeals-utils'),
								"dependency" => array(
									'user' => array('is_empty', 'none'),
									'member' => array('is_empty', 'none')
								),
								"value" => "",
								"type" => "text"
							),
							"photo" => array(
								"title" => esc_html__("Photo", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's photo (avatar)", 'bestdeals-utils'),
								"dependency" => array(
									'user' => array('is_empty', 'none'),
									'member' => array('is_empty', 'none')
								),
								"value" => "",
								"readonly" => false,
								"type" => "media"
							),
							"socials" => array(
								"title" => esc_html__("Socials", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's socials icons: name=url|name=url... For example: facebook=http://facebook.com/myaccount|twitter=http://twitter.com/myaccount", 'bestdeals-utils'),
								"dependency" => array(
									'user' => array('is_empty', 'none'),
									'member' => array('is_empty', 'none')
								),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("Description", 'bestdeals-utils'),
								"desc" => esc_attr__("Team member's short description", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				)

			));
		}
	}
}


// Add [trx_team] and [trx_team_item] in the VC shortcodes list
if (!function_exists('bestdeals_team_reg_shortcodes_vc')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list_vc',	'bestdeals_team_reg_shortcodes_vc');
	function bestdeals_team_reg_shortcodes_vc() {
		global $BESTDEALS_GLOBALS;

		$users = bestdeals_get_list_users();
		$members = bestdeals_get_list_posts(false, array(
				'post_type'=>'team',
				'orderby'=>'title',
				'order'=>'asc',
				'return'=>'title'
			)
		);
		$team_groups = bestdeals_get_list_terms(false, 'team_group');
		$team_styles = bestdeals_get_list_templates('team');
		$controls	 = bestdeals_get_list_slider_controls();

		// Team
		vc_map( array(
			"base" => "trx_team",
			"name" => esc_html__("Team", 'bestdeals-utils'),
			"description" => esc_attr__("Insert team members", 'bestdeals-utils'),
			"category" => esc_html__('Content', 'bestdeals-utils'),
			'icon' => 'icon_trx_team',
			"class" => "trx_sc_columns trx_sc_team",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_team_item'),
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Team style", 'bestdeals-utils'),
					"description" => esc_attr__("Select style to display team members", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip($team_styles),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'bestdeals-utils'),
					"description" => esc_attr__("How many columns use to show team members", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "3",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
					"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slider",
					"heading" => esc_html__("Slider", 'bestdeals-utils'),
					"description" => esc_attr__("Use slider to show team members", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					"class" => "",
					"std" => "no",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['yes_no']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "controls",
					"heading" => esc_html__("Controls", 'bestdeals-utils'),
					"description" => esc_attr__("Slider controls style and position", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"std" => "no",
					"value" => array_flip($controls),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slides_space",
					"heading" => esc_html__("Space between slides", 'bestdeals-utils'),
					"description" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "interval",
					"heading" => esc_html__("Slides change interval", 'bestdeals-utils'),
					"description" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "7000",
					"type" => "textfield"
				),
				array(
					"param_name" => "autoheight",
					"heading" => esc_html__("Autoheight", 'bestdeals-utils'),
					"description" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => array("Autoheight" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'bestdeals-utils'),
					"description" => esc_attr__("Alignment of the team block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'bestdeals-utils'),
					"description" => esc_attr__("Allow get team members from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
					"class" => "",
					"value" => array("Custom members" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
					"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'bestdeals-utils'),
					"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "cat",
					"heading" => esc_html__("Categories", 'bestdeals-utils'),
					"description" => esc_attr__("Select category to show team members. If empty - select team members from any category (group) or from IDs list", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $team_groups)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "count",
					"heading" => esc_html__("Number of posts", 'bestdeals-utils'),
					"description" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "3",
					"type" => "textfield"
				),
				array(
					"param_name" => "offset",
					"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
					"description" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "orderby",
					"heading" => esc_html__("Post sorting", 'bestdeals-utils'),
					"description" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "order",
					"heading" => esc_html__("Post order", 'bestdeals-utils'),
					"description" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "ids",
					"heading" => esc_html__("Team member's IDs list", 'bestdeals-utils'),
					"description" => esc_attr__("Comma separated list of team members's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'bestdeals-utils'),
					"description" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'bestdeals-utils'),
					"description" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				bestdeals_vc_width(),
				bestdeals_vc_height(),
				$BESTDEALS_GLOBALS['vc_params']['margin_top'],
				$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
				$BESTDEALS_GLOBALS['vc_params']['margin_left'],
				$BESTDEALS_GLOBALS['vc_params']['margin_right'],
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			),
			'default_content' => '
					[trx_team_item user="' . esc_html__( 'Member 1', 'bestdeals-utils' ) . '"][/trx_team_item]
					[trx_team_item user="' . esc_html__( 'Member 2', 'bestdeals-utils' ) . '"][/trx_team_item]
					[trx_team_item user="' . esc_html__( 'Member 4', 'bestdeals-utils' ) . '"][/trx_team_item]
				',
			'js_view' => 'VcTrxColumnsView'
		) );


		vc_map( array(
			"base" => "trx_team_item",
			"name" => esc_html__("Team member", 'bestdeals-utils'),
			"description" => esc_attr__("Team member - all data pull out from it account on your site", 'bestdeals-utils'),
			"show_settings_on_create" => true,
			"class" => "trx_sc_item trx_sc_column_item trx_sc_team_item",
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_team_item',
			"as_child" => array('only' => 'trx_team'),
			"as_parent" => array('except' => 'trx_team'),
			"params" => array(
				array(
					"param_name" => "user",
					"heading" => esc_html__("Registered user", 'bestdeals-utils'),
					"description" => esc_attr__("Select one of registered users (if present) or put name, position, etc. in fields below", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip($users),
					"type" => "dropdown"
				),
				array(
					"param_name" => "member",
					"heading" => esc_html__("Team member", 'bestdeals-utils'),
					"description" => esc_attr__("Select one of team members (if present) or put name, position, etc. in fields below", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip($members),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link", 'bestdeals-utils'),
					"description" => esc_attr__("Link on team member's personal page", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "name",
					"heading" => esc_html__("Name", 'bestdeals-utils'),
					"description" => esc_attr__("Team member's name", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "position",
					"heading" => esc_html__("Position", 'bestdeals-utils'),
					"description" => esc_attr__("Team member's position", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "email",
					"heading" => esc_html__("E-mail", 'bestdeals-utils'),
					"description" => esc_attr__("Team member's e-mail", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "photo",
					"heading" => esc_html__("Member's Photo", 'bestdeals-utils'),
					"description" => esc_attr__("Team member's photo (avatar)", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "socials",
					"heading" => esc_html__("Socials", 'bestdeals-utils'),
					"description" => esc_attr__("Team member's socials icons: name=url|name=url... For example: facebook=http://facebook.com/myaccount|twitter=http://twitter.com/myaccount", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			)
		) );

		class WPBakeryShortCode_Trx_Team extends BESTDEALS_VC_ShortCodeColumns {}
		class WPBakeryShortCode_Trx_Team_Item extends BESTDEALS_VC_ShortCodeItem {}

	}
}





// ---------------------------------- [trx_clients] ---------------------------------------

/*
[trx_clients id="unique_id" columns="3" style="clients-1|clients-2|..."]
	[trx_clients_item name="client name" position="director" image="url"]Description text[/trx_clients_item]
	...
[/trx_clients]
*/
if ( !function_exists( 'bestdeals_sc_clients' ) ) {
	function bestdeals_sc_clients($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "clients-1",
			"columns" => 4,
			"slider" => "no",
			"slides_space" => 0,
			"controls" => "no",
			"interval" => "",
			"autoheight" => "no",
			"custom" => "no",
			"ids" => "",
			"cat" => "",
			"count" => 3,
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link" => '',
			"scheme" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($id)) $id = "sc_clients_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
		$css .= ($ms) . ($hs) . ($ws);

		if (bestdeals_param_is_on($slider)) bestdeals_enqueue_slider('swiper');

		$columns = max(1, min(12, $columns));
		$count = max(1, (int) $count);
		if (bestdeals_param_is_off($custom) && $count < $columns) $columns = $count;

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_clients_id'] = $id;
		$BESTDEALS_GLOBALS['sc_clients_style'] = $style;
		$BESTDEALS_GLOBALS['sc_clients_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_clients_columns'] = $columns;
		$BESTDEALS_GLOBALS['sc_clients_slider'] = $slider;
		$BESTDEALS_GLOBALS['sc_clients_css_wh'] = $ws . $hs;

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '')
			. ' class="sc_clients_wrap'
			. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '')
			.'">'
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_clients sc_clients_style_'.esc_attr($style)
			. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
			. ' ' . esc_attr(bestdeals_get_slider_controls_classes($controls))
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. (bestdeals_param_is_on($slider)
				? ' sc_slider_swiper swiper-slider-container'
				. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				: '')
			.'"'
			. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
			. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
			. (!empty($subtitle) ? '<h6 class="sc_clients_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
			. (!empty($title) ? '<h2 class="sc_clients_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
			. (!empty($description) ? '<div class="sc_clients_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
			. (bestdeals_param_is_on($slider)
				? '<div class="slides swiper-wrapper">'
				: ($columns > 1
					? '<div class="sc_columns columns_wrap">'
					: '')
			);

		$content = do_shortcode($content);

		if (bestdeals_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;

			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}

			$args = array(
				'post_type' => 'clients',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
			);

			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}

			$args = bestdeals_query_add_sort_order($args, $orderby, $order);
			$args = bestdeals_query_add_posts_and_cats($args, $ids, 'clients', $cat, 'clients_group');

			$query = new WP_Query( $args );

			$post_number = 0;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					'columns_count' => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$post_data = bestdeals_get_post_data($args);
				$post_meta = get_post_meta($post_data['post_id'], 'post_custom_options', true);
				$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $style));
				$args['client_name'] = $post_meta['client_name'];
				$args['client_position'] = $post_meta['client_position'];
				$args['client_image'] = $post_data['post_thumb'];
				$args['client_link'] = bestdeals_param_is_on('client_show_link')
					? (!empty($post_meta['client_link']) ? $post_meta['client_link'] : $post_data['post_link'])
					: '';
				$output .= bestdeals_show_post_layout($args, $post_data);
			}
			wp_reset_postdata();
		}

		if (bestdeals_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {
			$output .= '</div>';
		}

		$output .= (!empty($link) ? '<div class="sc_clients_button sc_item_button">'.bestdeals_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
			. '</div><!-- /.sc_clients -->'
			. '</div><!-- /.sc_clients_wrap -->';

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_clients', $atts, $content);
	}
	add_shortcode('trx_clients', 'bestdeals_sc_clients');
}


if ( !function_exists( 'bestdeals_sc_clients_item' ) ) {
	function bestdeals_sc_clients_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"name" => "",
			"position" => "",
			"image" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => ""
		), $atts)));

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_clients_counter']++;

		$id = isset($id) ? $id : ($BESTDEALS_GLOBALS['sc_clients_id'] ? $BESTDEALS_GLOBALS['sc_clients_id'] . '_' . $BESTDEALS_GLOBALS['sc_clients_counter'] : '');

		$descr = trim(chop(do_shortcode($content)));

		$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $BESTDEALS_GLOBALS['sc_clients_style']));

		if (!empty($image)) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		$image = bestdeals_get_resized_image_tag($image, $thumb_sizes['w'], $thumb_sizes['h']);

		$post_data = array(
			'post_title' => $name,
			'post_excerpt' => $descr
		);
		$args = array(
			'layout' => $BESTDEALS_GLOBALS['sc_clients_style'],
			'number' => $BESTDEALS_GLOBALS['sc_clients_counter'],
			'columns_count' => $BESTDEALS_GLOBALS['sc_clients_columns'],
			'slider' => $BESTDEALS_GLOBALS['sc_clients_slider'],
			'show' => false,
			'descr'  => 0,
			'tag_id' => $id,
			'tag_class' => $class,
			'tag_animation' => $animation,
			'tag_css' => $css,
			'tag_css_wh' => $BESTDEALS_GLOBALS['sc_clients_css_wh'],
			'client_position' => $position,
			'client_link' => $link,
			'client_image' => $image
		);
		$output = bestdeals_show_post_layout($args, $post_data);
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_clients_item', $atts, $content);
	}
	add_shortcode('trx_clients_item', 'bestdeals_sc_clients_item');
}
// ---------------------------------- [/trx_clients] ---------------------------------------



// Add [trx_clients] and [trx_clients_item] in the shortcodes list
if (!function_exists('bestdeals_clients_reg_shortcodes')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list',	'bestdeals_clients_reg_shortcodes');
	function bestdeals_clients_reg_shortcodes() {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['shortcodes'])) {

			$users = bestdeals_get_list_users();
			$members = bestdeals_get_list_posts(false, array(
					'post_type'=>'clients',
					'orderby'=>'title',
					'order'=>'asc',
					'return'=>'title'
				)
			);
			$clients_groups = bestdeals_get_list_terms(false, 'clients_group');
			$clients_styles = bestdeals_get_list_templates('clients');
			$controls 		= bestdeals_get_list_slider_controls();

			bestdeals_array_insert_after($BESTDEALS_GLOBALS['shortcodes'], 'trx_chat', array(

				// Clients
				"trx_clients" => array(
					"title" => esc_html__("Clients", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert clients list in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_html__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_html__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Clients style", 'bestdeals-utils'),
							"desc" => esc_html__("Select style to display clients list", 'bestdeals-utils'),
							"value" => "clients-1",
							"type" => "select",
							"options" => $clients_styles
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_html__("How many columns use to show clients", 'bestdeals-utils'),
							"value" => 3,
							"min" => 2,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_html__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"slider" => array(
							"title" => esc_html__("Slider", 'bestdeals-utils'),
							"desc" => esc_html__("Use slider to show clients", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Controls", 'bestdeals-utils'),
							"desc" => esc_html__("Slider controls style and position", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"divider" => true,
							"value" => "no",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $controls
						),
						"slides_space" => array(
							"title" => esc_html__("Space between slides", 'bestdeals-utils'),
							"desc" => esc_html__("Size of space (in px) between slides", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Slides change interval", 'bestdeals-utils'),
							"desc" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'bestdeals-utils'),
							"desc" => esc_html__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_html__("Allow get team members from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_html__("Select categories (groups) to show team members. If empty - select team members from any category (group) or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $clients_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'bestdeals-utils'),
							"desc" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_html__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'bestdeals-utils'),
							"desc" => esc_html__("Select desired posts sorting method", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "title",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", 'bestdeals-utils'),
							"desc" => esc_html__("Select desired posts order", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "asc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_html__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_clients_item",
						"title" => esc_html__("Client", 'bestdeals-utils'),
						"desc" => esc_html__("Single client (custom parameters)", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"name" => array(
								"title" => esc_html__("Name", 'bestdeals-utils'),
								"desc" => esc_html__("Client's name", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"position" => array(
								"title" => esc_html__("Position", 'bestdeals-utils'),
								"desc" => esc_html__("Client's position", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"link" => array(
								"title" => esc_html__("Link", 'bestdeals-utils'),
								"desc" => esc_html__("Link on client's personal page", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"image" => array(
								"title" => esc_html__("Image", 'bestdeals-utils'),
								"desc" => esc_html__("Client's image", 'bestdeals-utils'),
								"value" => "",
								"readonly" => false,
								"type" => "media"
							),
							"_content_" => array(
								"title" => esc_html__("Description", 'bestdeals-utils'),
								"desc" => esc_html__("Client's short description", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				)

			));
		}
	}
}


// Add [trx_clients] and [trx_clients_item] in the VC shortcodes list
if (!function_exists('bestdeals_clients_reg_shortcodes_vc')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list_vc',	'bestdeals_clients_reg_shortcodes_vc');
	function bestdeals_clients_reg_shortcodes_vc() {
		global $BESTDEALS_GLOBALS;

		$clients_groups = bestdeals_get_list_terms(false, 'clients_group');
		$clients_styles = bestdeals_get_list_templates('clients');
		$controls		= bestdeals_get_list_slider_controls();

		// Clients
		vc_map( array(
			"base" => "trx_clients",
			"name" => esc_html__("Clients", 'bestdeals-utils'),
			"description" => esc_html__("Insert clients list", 'bestdeals-utils'),
			"category" => esc_html__('Content', 'bestdeals-utils'),
			'icon' => 'icon_trx_clients',
			"class" => "trx_sc_columns trx_sc_clients",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_clients_item'),
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Clients style", 'bestdeals-utils'),
					"description" => esc_html__("Select style to display clients list", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip($clients_styles),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'bestdeals-utils'),
					"description" => esc_html__("How many columns use to show clients", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "4",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
					"description" => esc_html__("Select color scheme for this block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slider",
					"heading" => esc_html__("Slider", 'bestdeals-utils'),
					"description" => esc_html__("Use slider to show testimonials", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					"class" => "",
					"std" => "no",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['yes_no']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "controls",
					"heading" => esc_html__("Controls", 'bestdeals-utils'),
					"description" => esc_html__("Slider controls style and position", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"std" => "no",
					"value" => array_flip($controls),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slides_space",
					"heading" => esc_html__("Space between slides", 'bestdeals-utils'),
					"description" => esc_html__("Size of space (in px) between slides", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "interval",
					"heading" => esc_html__("Slides change interval", 'bestdeals-utils'),
					"description" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "7000",
					"type" => "textfield"
				),
				array(
					"param_name" => "autoheight",
					"heading" => esc_html__("Autoheight", 'bestdeals-utils'),
					"description" => esc_html__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => array("Autoheight" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'bestdeals-utils'),
					"description" => esc_html__("Allow get clients from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
					"class" => "",
					"value" => array("Custom clients" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_html__("Title for the block", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
					"description" => esc_html__("Subtitle for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'bestdeals-utils'),
					"description" => esc_html__("Description for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "cat",
					"heading" => esc_html__("Categories", 'bestdeals-utils'),
					"description" => esc_html__("Select category to show clients. If empty - select clients from any category (group) or from IDs list", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $clients_groups)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "count",
					"heading" => esc_html__("Number of posts", 'bestdeals-utils'),
					"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "3",
					"type" => "textfield"
				),
				array(
					"param_name" => "offset",
					"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
					"description" => esc_html__("Skip posts before select next part.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "orderby",
					"heading" => esc_html__("Post sorting", 'bestdeals-utils'),
					"description" => esc_html__("Select desired posts sorting method", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "order",
					"heading" => esc_html__("Post order", 'bestdeals-utils'),
					"description" => esc_html__("Select desired posts order", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "ids",
					"heading" => esc_html__("client's IDs list", 'bestdeals-utils'),
					"description" => esc_html__("Comma separated list of client's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'bestdeals-utils'),
					"description" => esc_html__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'bestdeals-utils'),
					"description" => esc_html__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				$BESTDEALS_GLOBALS['vc_params']['margin_top'],
				$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
				$BESTDEALS_GLOBALS['vc_params']['margin_left'],
				$BESTDEALS_GLOBALS['vc_params']['margin_right'],
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			),
			'js_view' => 'VcTrxColumnsView'
		) );


		vc_map( array(
			"base" => "trx_clients_item",
			"name" => esc_html__("Client", 'bestdeals-utils'),
			"description" => esc_html__("Client - all data pull out from it account on your site", 'bestdeals-utils'),
			"show_settings_on_create" => true,
			"class" => "trx_sc_item trx_sc_column_item trx_sc_clients_item",
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_clients_item',
			"as_child" => array('only' => 'trx_clients'),
			"as_parent" => array('except' => 'trx_clients'),
			"params" => array(
				array(
					"param_name" => "name",
					"heading" => esc_html__("Name", 'bestdeals-utils'),
					"description" => esc_html__("Client's name", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "position",
					"heading" => esc_html__("Position", 'bestdeals-utils'),
					"description" => esc_html__("Client's position", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link", 'bestdeals-utils'),
					"description" => esc_html__("Link on client's personal page", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("Client's image", 'bestdeals-utils'),
					"description" => esc_html__("Clients's image", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			)
		) );

		class WPBakeryShortCode_Trx_Clients extends BESTDEALS_VC_ShortCodeColumns {}
		class WPBakeryShortCode_Trx_Clients_Item extends BESTDEALS_VC_ShortCodeItem {}

	}
}


// ---------------------------------- [trx_property] ---------------------------------------

/*
[trx_property status="none|rent|sale" id="unique_id" columns="3" count="3" style="property-1|property-2|..."][/trx_property]
*/

global $BESTDEALS_GLOBALS;
$BESTDEALS_GLOBALS['sc_property_busy'] = false;

if ( !function_exists( 'bestdeals_sc_property' ) ) {
	function bestdeals_sc_property($atts, $content=null){

		global $BESTDEALS_GLOBALS;
		if (bestdeals_in_shortcode_property(true)) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"status" => "none",
			"style" => "property-1",
			"columns" => 3,
			"slider" => "no",
			"slides_space" => 0,
			"controls" => "no",
			"interval" => "",
			"autoheight" => "no",
			//"align" => "",
			"custom" => "no",
			"type" => "images",	// icons | images
			"ids" => "",
			"cat" => "",
			"count" => 6,
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			//"readmore" => esc_html__('Learn more', 'bestdeals-utils'),
			"title" => "",
			//"subtitle" => "",
			//"description" => "",
			//"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			//"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($id)) $id = "sc_property_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
		$css .= ($ms) . ($hs) . ($ws);

		$count = max(1, (int) $count);
		$columns = max(1, min(12, (int) $columns));
		if (bestdeals_param_is_off($custom) && $count < $columns) $columns = $count;

		$BESTDEALS_GLOBALS['sc_property_busy'] = true;
		$BESTDEALS_GLOBALS['sc_property_id'] = $id;
		$BESTDEALS_GLOBALS['sc_property_style'] = $style;
		$BESTDEALS_GLOBALS['sc_property_columns'] = $columns;
		$BESTDEALS_GLOBALS['sc_property_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_property_slider'] = $slider;
		$BESTDEALS_GLOBALS['sc_property_css_wh'] = $ws . $hs;
		//$BESTDEALS_GLOBALS['sc_property_readmore'] = $readmore;

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '')
			. ' class="sc_property_wrap'
			.'">'
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_property'
			. ' sc_property_'.esc_attr($style)
			. ' sc_property_type_'.esc_attr($type)
			. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
			. ' ' . esc_attr(bestdeals_get_slider_controls_classes($controls))
			. (bestdeals_param_is_on($slider)
				? ' sc_slider_swiper swiper-slider-container'
				. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				: '')
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
			. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
			. (!empty($title) ? '<div class="sc_property_title">' . trim(bestdeals_strmacros($title)) . '</div>' : '')
			. (bestdeals_param_is_on($slider)
				? '<div class="slides swiper-wrapper">'
				: ($columns > 1
					? '<div class="sc_columns columns_wrap">'
					: '')
			);


		$content = do_shortcode($content);

		if (bestdeals_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;

			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}

			$args = array(
				'post_type' => 'property',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
				//'readmore' => $readmore
			);

			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}

			$args = bestdeals_query_add_sort_order($args, $orderby, $order);
			$args = bestdeals_query_add_posts_and_cats($args, $ids, 'property', $cat, 'property_group');

			if ( $status != 'none' ) {
				$BESTDEALS_GLOBALS['blog_filters_property']['property-status'] = $status;
				$args = bestdeals_query_add_filters($args, $BESTDEALS_GLOBALS['blog_filters_property']);
			}

			$query = new WP_Query( $args );

			$post_number = 0;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					//'readmore' => $readmore,
					'tag_type' => $type,
					'columns_count' => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$output .= bestdeals_show_post_layout($args);
			}
			wp_reset_postdata();
		}


		if (bestdeals_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {
			$output .= '</div>';
		}

		$output .=  '</div><!-- /.sc_property -->'
			. '</div><!-- /.sc_property_wrap -->';

		$BESTDEALS_GLOBALS['sc_property_busy'] = false;
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_property', $atts, $content);
	}
	add_shortcode('trx_property', 'bestdeals_sc_property');
}


// Add [trx_property] and [trx_property_item] in the shortcodes list
if (!function_exists('bestdeals_property_reg_shortcodes')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list',	'bestdeals_property_reg_shortcodes');
	function bestdeals_property_reg_shortcodes() {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['shortcodes'])) {

			$property_groups = bestdeals_get_list_terms(false, 'property_group');
			$property_styles = bestdeals_get_list_templates('property');
			$controls 		 = bestdeals_get_list_slider_controls();

			bestdeals_array_insert_after($BESTDEALS_GLOBALS['shortcodes'], 'trx_section', array(

				// Property
				"trx_property" => array(
					"title" => esc_html__("Property", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert property list in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"status" => 	array(
							"title" => esc_html__("Property search status", 'bestdeals-utils'),
							"desc" => "",
							"value" => "none",
							"type" => "checklist",
							"options" => array(
								'none' => esc_html__('All property', 'bestdeals-utils'),
								'sale' => esc_html__('Sale', 'bestdeals-utils'),
								'rent' => esc_html__('Rent', 'bestdeals-utils')
							)
						),
						"style" => array(
							"title" => esc_html__("Property style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style to display property list", 'bestdeals-utils'),
							"value" => "property-1",
							"type" => "select",
							"options" => $property_styles
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns use to show property list", 'bestdeals-utils'),
							"value" => 3,
							"min" => 1,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_attr__("Allow get property items from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_attr__("Select categories (groups) to show property list. If empty - select property from any category (group) or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $property_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'bestdeals-utils'),
							"desc" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
				)

			));
		}
	}
}


// Add [trx_property] and [trx_property_item] in the VC shortcodes list
if (!function_exists('bestdeals_property_reg_shortcodes_vc')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list_vc',	'bestdeals_property_reg_shortcodes_vc');
	function bestdeals_property_reg_shortcodes_vc() {
		global $BESTDEALS_GLOBALS;

		$property_groups = bestdeals_get_list_terms(false, 'property_group');
		$property_styles = bestdeals_get_list_templates('property');
		$controls		 = bestdeals_get_list_slider_controls();

		// Property
		vc_map( array(
			"base" => "trx_property",
			"name" => esc_html__("Property", 'bestdeals-utils'),
			"description" => esc_attr__("Insert property list", 'bestdeals-utils'),
			"category" => esc_html__('Content', 'bestdeals-utils'),
			"icon" => 'icon_trx_property',
			"class" => "trx_sc_single trx_sc_property",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_property_item'),
			"params" => array(
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "status",
					"heading" => esc_html__("Property search status", 'bestdeals-utils'),
					"description" => "",
					"class" => "",
					"value" => "none",
					"value" => array(
						esc_html__('All property', 'bestdeals-utils') => 'none',
						esc_html__('Sale', 'bestdeals-utils') => 'sale',
						esc_html__('Rent', 'bestdeals-utils') => 'rent'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Property style", 'bestdeals-utils'),
					"description" => esc_attr__("Select style to display property list", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip($property_styles),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'bestdeals-utils'),
					"description" => esc_attr__("How many columns use to show property list", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'bestdeals-utils'),
					"description" => esc_attr__("Allow get property from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
					"class" => "",
					"value" => array("Custom property" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "cat",
					"heading" => esc_html__("Categories", 'bestdeals-utils'),
					"description" => esc_attr__("Select category to show property. If empty - select property from any category (group) or from IDs list", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $property_groups)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "count",
					"heading" => esc_html__("Number of posts", 'bestdeals-utils'),
					"description" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "offset",
					"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
					"description" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "ids",
					"heading" => esc_html__("Team member's IDs list", 'bestdeals-utils'),
					"description" => esc_attr__("Comma separated list of team members's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				bestdeals_vc_width(),
				bestdeals_vc_height(),
				$BESTDEALS_GLOBALS['vc_params']['margin_top'],
				$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
				$BESTDEALS_GLOBALS['vc_params']['margin_left'],
				$BESTDEALS_GLOBALS['vc_params']['margin_right'],
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			)
		) );

		class WPBakeryShortCode_Trx_Property extends BESTDEALS_VC_ShortCodeSingle {}

	}
}




// ---------------------------------- [trx_testimonials] ---------------------------------------

/*
[trx_testimonials id="unique_id" style="1|2|3"]
	[trx_testimonials_item user="user_login"]Testimonials text[/trx_testimonials_item]
	[trx_testimonials_item email="" name="" position="" photo="photo_url"]Testimonials text[/trx_testimonials]
[/trx_testimonials]
*/

if (!function_exists('bestdeals_sc_testimonials')) {
	function bestdeals_sc_testimonials($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "testimonials-1",
			"columns" => 1,
			"slider" => "yes",
			"slides_space" => 0,
			"controls" => "no",
			"interval" => "",
			"autoheight" => "no",
			"align" => "",
			"custom" => "no",
			"ids" => "",
			"cat" => "",
			"count" => "3",
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"scheme" => "",
			"bg_color" => "",
			"bg_image" => "",
			"bg_overlay" => "",
			"bg_texture" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($id)) $id = "sc_testimonials_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}

		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = bestdeals_get_scheme_color('bg');
			$rgb = bestdeals_hex2rgb($bg_color);
		}

		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
		$css .= ($ms) . ($hs) . ($ws);

		$count = max(1, (int) $count);
		$columns = max(1, min(12, (int) $columns));
		if (bestdeals_param_is_off($custom) && $count < $columns) $columns = $count;

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_testimonials_id'] = $id;
		$BESTDEALS_GLOBALS['sc_testimonials_style'] = $style;
		$BESTDEALS_GLOBALS['sc_testimonials_columns'] = $columns;
		$BESTDEALS_GLOBALS['sc_testimonials_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_testimonials_slider'] = $slider;
		$BESTDEALS_GLOBALS['sc_testimonials_css_wh'] = $ws . $hs;

		if (bestdeals_param_is_on($slider)) bestdeals_enqueue_slider('swiper');

		$output = ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2 || ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme))
				? '<div class="sc_testimonials_wrap sc_section'
				. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '')
				. '"'
				.' style="'
				. ($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
				. ($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');' : '')
				. '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
				. '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
				. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
				. (bestdeals_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
				. '"'
				. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
				. '>'
				: '')
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_testimonials sc_testimonials_style_'.esc_attr($style)
			. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
			. (bestdeals_param_is_on($slider)
				? ' sc_slider_swiper swiper-slider-container'
				. ' ' . esc_attr(bestdeals_get_slider_controls_classes($controls))
				. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				: '')
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
			. '"'
			. ($bg_color=='' && $bg_image=='' && $bg_overlay==0 && ($bg_texture=='' || $bg_texture=='0') && !bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
			. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
			. (!empty($subtitle) ? '<h6 class="sc_testimonials_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
			. (!empty($title) ? '<h2 class="sc_testimonials_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
			. (!empty($description) ? '<div class="sc_testimonials_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
			. (bestdeals_param_is_on($slider)
				? '<div class="slides swiper-wrapper">'
				: ($columns > 1
					? '<div class="sc_columns columns_wrap">'
					: '')
			);

		$content = do_shortcode($content);

		if (bestdeals_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;

			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}

			$args = array(
				'post_type' => 'testimonial',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
			);

			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}

			$args = bestdeals_query_add_sort_order($args, $orderby, $order);
			$args = bestdeals_query_add_posts_and_cats($args, $ids, 'testimonial', $cat, 'testimonial_group');

			$query = new WP_Query( $args );

			$post_number = 0;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					'columns_count' => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$post_data = bestdeals_get_post_data($args);
				$post_data['post_content'] = wpautop($post_data['post_content']);	// Add <p> around text and paragraphs. Need separate call because 'content'=>false (see above)
				$post_meta = get_post_meta($post_data['post_id'], 'testimonial_data', true);
				$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $style));
				$args['author'] = $post_meta['testimonial_author'];
				$args['position'] = $post_meta['testimonial_position'];
				$args['link'] = !empty($post_meta['testimonial_link']) ? $post_meta['testimonial_link'] : '';	//$post_data['post_link'];
				$args['email'] = $post_meta['testimonial_email'];
				$args['photo'] = $post_data['post_thumb'];
				if (empty($args['photo']) && !empty($args['email'])) $args['photo'] = get_avatar($args['email'], $thumb_sizes['w']*min(2, max(1, bestdeals_get_theme_option("retina_ready"))));
				$output .= bestdeals_show_post_layout($args, $post_data);
			}
			wp_reset_postdata();
		}

		if (bestdeals_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {
			$output .= '</div>';
		}

		$output .= '</div>'
			. ($bg_color!='' || $bg_image!='' || $bg_overlay>0 || $bg_texture>0 || bestdeals_strlen($bg_texture)>2
				?  '</div></div>'
				: '');

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_testimonials', $atts, $content);
	}
	add_shortcode('trx_testimonials', 'bestdeals_sc_testimonials');
}


if (!function_exists('bestdeals_sc_testimonials_item')) {
	function bestdeals_sc_testimonials_item($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"author" => "",
			"position" => "",
			"link" => "",
			"photo" => "",
			"email" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
		), $atts)));

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_testimonials_counter']++;

		$id = $id ? $id : ($BESTDEALS_GLOBALS['sc_testimonials_id'] ? $BESTDEALS_GLOBALS['sc_testimonials_id'] . '_' . $BESTDEALS_GLOBALS['sc_testimonials_counter'] : '');

		$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $BESTDEALS_GLOBALS['sc_testimonials_style']));

		if (empty($photo)) {
			if (!empty($email))
				$photo = get_avatar($email, $thumb_sizes['w']*min(2, max(1, bestdeals_get_theme_option("retina_ready"))));
		} else {
			if ($photo > 0) {
				$attach = wp_get_attachment_image_src( $photo, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$photo = $attach[0];
			}
			$photo = bestdeals_get_resized_image_tag($photo, $thumb_sizes['w'], $thumb_sizes['h']);
		}

		$post_data = array(
			'post_content' => do_shortcode($content)
		);
		$args = array(
			'layout' => $BESTDEALS_GLOBALS['sc_testimonials_style'],
			'number' => $BESTDEALS_GLOBALS['sc_testimonials_counter'],
			'columns_count' => $BESTDEALS_GLOBALS['sc_testimonials_columns'],
			'slider' => $BESTDEALS_GLOBALS['sc_testimonials_slider'],
			'show' => false,
			'descr'  => 0,
			'tag_id' => $id,
			'tag_class' => $class,
			'tag_animation' => '',
			'tag_css' => $css,
			'tag_css_wh' => $BESTDEALS_GLOBALS['sc_testimonials_css_wh'],
			'author' => $author,
			'position' => $position,
			'link' => $link,
			'email' => $email,
			'photo' => $photo
		);
		$output = bestdeals_show_post_layout($args, $post_data);

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_testimonials_item', $atts, $content);
	}
	add_shortcode('trx_testimonials_item', 'bestdeals_sc_testimonials_item');
}
// ---------------------------------- [/trx_testimonials] ---------------------------------------



// Add [trx_testimonials] and [trx_testimonials_item] in the shortcodes list
if (!function_exists('bestdeals_testimonials_reg_shortcodes')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list',	'bestdeals_testimonials_reg_shortcodes');
	function bestdeals_testimonials_reg_shortcodes() {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['shortcodes'])) {

			$testimonials_groups = bestdeals_get_list_terms(false, 'testimonial_group');
			$testimonials_styles = bestdeals_get_list_templates('testimonials');
			$controls = bestdeals_get_list_slider_controls();

			bestdeals_array_insert_before($BESTDEALS_GLOBALS['shortcodes'], 'trx_title', array(

				// Testimonials
				"trx_testimonials" => array(
					"title" => esc_html__("Testimonials", 'bestdeals-utils'),
					"desc" => esc_attr__("Insert testimonials into post (page)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_attr__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_attr__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Testimonials style", 'bestdeals-utils'),
							"desc" => esc_attr__("Select style to display testimonials", 'bestdeals-utils'),
							"value" => "testimonials-1",
							"type" => "select",
							"options" => $testimonials_styles
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_attr__("How many columns use to show testimonials", 'bestdeals-utils'),
							"value" => 1,
							"min" => 1,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"slider" => array(
							"title" => esc_html__("Slider", 'bestdeals-utils'),
							"desc" => esc_attr__("Use slider to show testimonials", 'bestdeals-utils'),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Controls", 'bestdeals-utils'),
							"desc" => esc_attr__("Slider controls style and position", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $controls
						),
						"slides_space" => array(
							"title" => esc_html__("Space between slides", 'bestdeals-utils'),
							"desc" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Slides change interval", 'bestdeals-utils'),
							"desc" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'bestdeals-utils'),
							"desc" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_attr__("Alignment of the testimonials block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_attr__("Allow get testimonials from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_attr__("Select categories (groups) to show testimonials. If empty - select testimonials from any category (group) or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $testimonials_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'bestdeals-utils'),
							"desc" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "date",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", 'bestdeals-utils'),
							"desc" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", 'bestdeals-utils'),
							"desc" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", 'bestdeals-utils'),
							"desc" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", 'bestdeals-utils'),
							"desc" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", 'bestdeals-utils'),
							"desc" => esc_attr__("Predefined texture style from 1 to 11. 0 - without texture.", 'bestdeals-utils'),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_testimonials_item",
						"title" => esc_html__("Item", 'bestdeals-utils'),
						"desc" => esc_attr__("Testimonials item (custom parameters)", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"author" => array(
								"title" => esc_html__("Author", 'bestdeals-utils'),
								"desc" => esc_attr__("Name of the testimonmials author", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"link" => array(
								"title" => esc_html__("Link", 'bestdeals-utils'),
								"desc" => esc_attr__("Link URL to the testimonmials author page", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"email" => array(
								"title" => esc_html__("E-mail", 'bestdeals-utils'),
								"desc" => esc_attr__("E-mail of the testimonmials author (to get gravatar)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"photo" => array(
								"title" => esc_html__("Photo", 'bestdeals-utils'),
								"desc" => esc_attr__("Select or upload photo of testimonmials author or write URL of photo from other site", 'bestdeals-utils'),
								"value" => "",
								"type" => "media"
							),
							"_content_" => array(
								"title" => esc_html__("Testimonials text", 'bestdeals-utils'),
								"desc" => esc_attr__("Current testimonials text", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				)

			));
		}
	}
}


// Add [trx_testimonials] and [trx_testimonials_item] in the VC shortcodes list
if (!function_exists('bestdeals_testimonials_reg_shortcodes_vc')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list_vc',	'bestdeals_testimonials_reg_shortcodes_vc');
	function bestdeals_testimonials_reg_shortcodes_vc() {
		global $BESTDEALS_GLOBALS;

		$testimonials_groups = bestdeals_get_list_terms(false, 'testimonial_group');
		$testimonials_styles = bestdeals_get_list_templates('testimonials');
		$controls			 = bestdeals_get_list_slider_controls();

		// Testimonials
		vc_map( array(
			"base" => "trx_testimonials",
			"name" => esc_html__("Testimonials", 'bestdeals-utils'),
			"description" => esc_attr__("Insert testimonials slider", 'bestdeals-utils'),
			"category" => esc_html__('Content', 'bestdeals-utils'),
			'icon' => 'icon_trx_testimonials',
			"class" => "trx_sc_collection trx_sc_testimonials",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_testimonials_item'),
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Testimonials style", 'bestdeals-utils'),
					"description" => esc_attr__("Select style to display testimonials", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip($testimonials_styles),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'bestdeals-utils'),
					"description" => esc_attr__("How many columns use to show testimonials", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "1",
					"type" => "textfield"
				),
				array(
					"param_name" => "slider",
					"heading" => esc_html__("Slider", 'bestdeals-utils'),
					"description" => esc_attr__("Use slider to show testimonials", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					"class" => "",
					"std" => "yes",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['yes_no']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "controls",
					"heading" => esc_html__("Controls", 'bestdeals-utils'),
					"description" => esc_attr__("Slider controls style and position", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"std" => "no",
					"value" => array_flip($controls),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slides_space",
					"heading" => esc_html__("Space between slides", 'bestdeals-utils'),
					"description" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "interval",
					"heading" => esc_html__("Slides change interval", 'bestdeals-utils'),
					"description" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "7000",
					"type" => "textfield"
				),
				array(
					"param_name" => "autoheight",
					"heading" => esc_html__("Autoheight", 'bestdeals-utils'),
					"description" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => array("Autoheight" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'bestdeals-utils'),
					"description" => esc_attr__("Alignment of the testimonials block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'bestdeals-utils'),
					"description" => esc_attr__("Allow get testimonials from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
					"class" => "",
					"value" => array("Custom slides" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
					"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'bestdeals-utils'),
					"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "cat",
					"heading" => esc_html__("Categories", 'bestdeals-utils'),
					"description" => esc_attr__("Select categories (groups) to show testimonials. If empty - select testimonials from any category (group) or from IDs list", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $testimonials_groups)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "count",
					"heading" => esc_html__("Number of posts", 'bestdeals-utils'),
					"description" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "3",
					"type" => "textfield"
				),
				array(
					"param_name" => "offset",
					"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
					"description" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "orderby",
					"heading" => esc_html__("Post sorting", 'bestdeals-utils'),
					"description" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "order",
					"heading" => esc_html__("Post order", 'bestdeals-utils'),
					"description" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "ids",
					"heading" => esc_html__("Post IDs list", 'bestdeals-utils'),
					"description" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
					"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
					"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'bestdeals-utils'),
					"description" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
					"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_image",
					"heading" => esc_html__("Background image URL", 'bestdeals-utils'),
					"description" => esc_attr__("Select background image from library for this section", 'bestdeals-utils'),
					"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "bg_overlay",
					"heading" => esc_html__("Overlay", 'bestdeals-utils'),
					"description" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
					"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "bg_texture",
					"heading" => esc_html__("Texture", 'bestdeals-utils'),
					"description" => esc_attr__("Texture style from 1 to 11. Empty or 0 - without texture.", 'bestdeals-utils'),
					"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				bestdeals_vc_width(),
				bestdeals_vc_height(),
				$BESTDEALS_GLOBALS['vc_params']['margin_top'],
				$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
				$BESTDEALS_GLOBALS['vc_params']['margin_left'],
				$BESTDEALS_GLOBALS['vc_params']['margin_right'],
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			)
		) );


		vc_map( array(
			"base" => "trx_testimonials_item",
			"name" => esc_html__("Testimonial", 'bestdeals-utils'),
			"description" => esc_attr__("Single testimonials item", 'bestdeals-utils'),
			"show_settings_on_create" => true,
			"class" => "trx_sc_single trx_sc_testimonials_item",
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_testimonials_item',
			"as_child" => array('only' => 'trx_testimonials'),
			"as_parent" => array('except' => 'trx_testimonials'),
			"params" => array(
				array(
					"param_name" => "author",
					"heading" => esc_html__("Author", 'bestdeals-utils'),
					"description" => esc_attr__("Name of the testimonmials author", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link", 'bestdeals-utils'),
					"description" => esc_attr__("Link URL to the testimonmials author page", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "email",
					"heading" => esc_html__("E-mail", 'bestdeals-utils'),
					"description" => esc_attr__("E-mail of the testimonmials author", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "photo",
					"heading" => esc_html__("Photo", 'bestdeals-utils'),
					"description" => esc_attr__("Select or upload photo of testimonmials author or write URL of photo from other site", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Testimonials text", 'bestdeals-utils'),
					"description" => esc_attr__("Current testimonials text", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			),
			'js_view' => 'VcTrxTextView'
		) );

		class WPBakeryShortCode_Trx_Testimonials extends BESTDEALS_VC_ShortCodeColumns {}
		class WPBakeryShortCode_Trx_Testimonials_Item extends BESTDEALS_VC_ShortCodeSingle {}

	}
}




// ---------------------------------- [trx_services] ---------------------------------------

/*
[trx_services id="unique_id" columns="4" count="4" style="services-1|services-2|..." title="Block title" subtitle="xxx" description="xxxxxx"]
	[trx_services_item icon="url" title="Item title" description="Item description" link="url" link_caption="Link text"]
	[trx_services_item icon="url" title="Item title" description="Item description" link="url" link_caption="Link text"]
[/trx_services]
*/
if ( !function_exists( 'bestdeals_sc_services' ) ) {
	function bestdeals_sc_services($atts, $content=null){
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "services-1",
			"columns" => 4,
			"slider" => "no",
			"slides_space" => 0,
			"controls" => "no",
			"interval" => "",
			"autoheight" => "no",
			"align" => "",
			"custom" => "no",
			"type" => "icons",	// icons | images
			"ids" => "",
			"cat" => "",
			"count" => 4,
			"offset" => "",
			"orderby" => "date",
			"order" => "desc",
			"readmore" => esc_html__('Learn more', 'bestdeals-utils'),
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link" => '',
			"scheme" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));

		if (empty($id)) $id = "sc_services_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
		if (!empty($height) && bestdeals_param_is_on($autoheight)) $autoheight = "no";
		if (empty($interval)) $interval = mt_rand(5000, 10000);

		$ms = bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$ws = bestdeals_get_css_position_from_values('', '', '', '', $width);
		$hs = bestdeals_get_css_position_from_values('', '', '', '', '', $height);
		$css .= ($ms) . ($hs) . ($ws);

		$count = max(1, (int) $count);
		$columns = max(1, min(12, (int) $columns));
		if (bestdeals_param_is_off($custom) && $count < $columns) $columns = $count;

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_services_id'] = $id;
		$BESTDEALS_GLOBALS['sc_services_style'] = $style;
		$BESTDEALS_GLOBALS['sc_services_columns'] = $columns;
		$BESTDEALS_GLOBALS['sc_services_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_services_slider'] = $slider;
		$BESTDEALS_GLOBALS['sc_services_css_wh'] = $ws . $hs;
		$BESTDEALS_GLOBALS['sc_services_readmore'] = $readmore;

		$output = '<div' . ($id ? ' id="'.esc_attr($id).'_wrap"' : '')
			. ' class="sc_services_wrap'
			. ($scheme && !bestdeals_param_is_off($scheme) && !bestdeals_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '')
			.'">'
			. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_services'
			. ' sc_services_style_'.esc_attr($style)
			. ' sc_services_type_'.esc_attr($type)
			. ' ' . esc_attr(bestdeals_get_template_property($style, 'container_classes'))
			. ' ' . esc_attr(bestdeals_get_slider_controls_classes($controls))
			. (bestdeals_param_is_on($slider)
				? ' sc_slider_swiper swiper-slider-container'
				. (bestdeals_param_is_on($autoheight) ? ' sc_slider_height_auto' : '')
				. ($hs ? ' sc_slider_height_fixed' : '')
				: '')
			. (!empty($class) ? ' '.esc_attr($class) : '')
			. ($align!='' && $align!='none' ? ' align'.esc_attr($align) : '')
			. '"'
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. (!empty($width) && bestdeals_strpos($width, '%')===false ? ' data-old-width="' . esc_attr($width) . '"' : '')
			. (!empty($height) && bestdeals_strpos($height, '%')===false ? ' data-old-height="' . esc_attr($height) . '"' : '')
			. ((int) $interval > 0 ? ' data-interval="'.esc_attr($interval).'"' : '')
			. ($columns > 1 ? ' data-slides-per-view="' . esc_attr($columns) . '"' : '')
			. ($slides_space > 0 ? ' data-slides-space="' . esc_attr($slides_space) . '"' : '')
			. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
			. '>'
			. (!empty($subtitle) ? '<h6 class="sc_services_subtitle sc_item_subtitle">' . trim(bestdeals_strmacros($subtitle)) . '</h6>' : '')
			. (!empty($title) ? '<h2 class="sc_services_title sc_item_title">' . trim(bestdeals_strmacros($title)) . '</h2>' : '')
			. (!empty($description) ? '<div class="sc_services_descr sc_item_descr">' . trim(bestdeals_strmacros($description)) . '</div>' : '')
			. (bestdeals_param_is_on($slider)
				? '<div class="slides swiper-wrapper">'
				: ($columns > 1
					? '<div class="sc_columns columns_wrap">'
					: '')
			);

		$content = do_shortcode($content);

		if (bestdeals_param_is_on($custom) && $content) {
			$output .= $content;
		} else {
			global $post;

			if (!empty($ids)) {
				$posts = explode(',', $ids);
				$count = count($posts);
			}

			$args = array(
				'post_type' => 'services',
				'post_status' => 'publish',
				'posts_per_page' => $count,
				'ignore_sticky_posts' => true,
				'order' => $order=='asc' ? 'asc' : 'desc',
				'readmore' => $readmore
			);

			if ($offset > 0 && empty($ids)) {
				$args['offset'] = $offset;
			}

			$args = bestdeals_query_add_sort_order($args, $orderby, $order);
			$args = bestdeals_query_add_posts_and_cats($args, $ids, 'services', $cat, 'services_group');
			$query = new WP_Query( $args );

			$post_number = 0;

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_number++;
				$args = array(
					'layout' => $style,
					'show' => false,
					'number' => $post_number,
					'posts_on_page' => ($count > 0 ? $count : $query->found_posts),
					"descr" => bestdeals_get_custom_option('post_excerpt_maxlength'.($columns > 1 ? '_masonry' : '')),
					"orderby" => $orderby,
					'content' => false,
					'terms_list' => false,
					'readmore' => $readmore,
					'tag_type' => $type,
					'columns_count' => $columns,
					'slider' => $slider,
					'tag_id' => $id ? $id . '_' . $post_number : '',
					'tag_class' => '',
					'tag_animation' => '',
					'tag_css' => '',
					'tag_css_wh' => $ws . $hs
				);
				$output .= bestdeals_show_post_layout($args);
			}
			wp_reset_postdata();
		}

		if (bestdeals_param_is_on($slider)) {
			$output .= '</div>'
				. '<div class="sc_slider_controls_wrap"><a class="sc_slider_prev" href="#"></a><a class="sc_slider_next" href="#"></a></div>'
				. '<div class="sc_slider_pagination_wrap"></div>';
		} else if ($columns > 1) {
			$output .= '</div>';
		}

		$output .=  (!empty($link) ? '<div class="sc_services_button sc_item_button">'.bestdeals_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
			. '</div><!-- /.sc_services -->'
			. '</div><!-- /.sc_services_wrap -->';

		return apply_filters('bestdeals_shortcode_output', $output, 'trx_services', $atts, $content);
	}
	add_shortcode('trx_services', 'bestdeals_sc_services');
}


if ( !function_exists( 'bestdeals_sc_services_item' ) ) {
	function bestdeals_sc_services_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"icon" => "",
			"image" => "",
			"title" => "",
			"link" => "",
			"readmore" => "(none)",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => ""
		), $atts)));

		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_services_counter']++;

		$id = $id ? $id : ($BESTDEALS_GLOBALS['sc_services_id'] ? $BESTDEALS_GLOBALS['sc_services_id'] . '_' . $BESTDEALS_GLOBALS['sc_services_counter'] : '');

		$descr = trim(chop(do_shortcode($content)));
		$readmore = $readmore=='(none)' ? $BESTDEALS_GLOBALS['sc_services_readmore'] : $readmore;

		if (!empty($icon)) {
			$type = 'icons';
		} else if (!empty($image)) {
			$type = 'images';
			if ($image > 0) {
				$attach = wp_get_attachment_image_src( $image, 'full' );
				if (isset($attach[0]) && $attach[0]!='')
					$image = $attach[0];
			}
			$thumb_sizes = bestdeals_get_thumb_sizes(array('layout' => $BESTDEALS_GLOBALS['sc_services_style']));
			$image = bestdeals_get_resized_image_tag($image, $thumb_sizes['w'], $thumb_sizes['h']);
		}

		$post_data = array(
			'post_title' => $title,
			'post_excerpt' => $descr,
			'post_thumb' => $image,
			'post_icon' => $icon,
			'post_link' => $link
		);
		$args = array(
			'layout' => $BESTDEALS_GLOBALS['sc_services_style'],
			'number' => $BESTDEALS_GLOBALS['sc_services_counter'],
			'columns_count' => $BESTDEALS_GLOBALS['sc_services_columns'],
			'slider' => $BESTDEALS_GLOBALS['sc_services_slider'],
			'show' => false,
			'descr'  => 0,
			'readmore' => $readmore,
			'tag_type' => $type,
			'tag_id' => $id,
			'tag_class' => $class,
			'tag_animation' => $animation,
			'tag_css' => $css,
			'tag_css_wh' => $BESTDEALS_GLOBALS['sc_services_css_wh']
		);
		$output = bestdeals_show_post_layout($args, $post_data);
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_services_item', $atts, $content);
	}
	add_shortcode('trx_services_item', 'bestdeals_sc_services_item');
}
// ---------------------------------- [/trx_services] ---------------------------------------



// Add [trx_services] and [trx_services_item] in the shortcodes list
if (!function_exists('bestdeals_services_reg_shortcodes')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list',	'bestdeals_services_reg_shortcodes');
	function bestdeals_services_reg_shortcodes() {
		global $BESTDEALS_GLOBALS;
		if (isset($BESTDEALS_GLOBALS['shortcodes'])) {

			$services_groups = bestdeals_get_list_terms(false, 'services_group');
			$services_styles = bestdeals_get_list_templates('services');
			$controls 		 = bestdeals_get_list_slider_controls();

			bestdeals_array_insert_after($BESTDEALS_GLOBALS['shortcodes'], 'trx_section', array(

				// Services
				"trx_services" => array(
					"title" => esc_html__("Services", 'bestdeals-utils'),
					"desc" => esc_html__("Insert services list in your page (post)", 'bestdeals-utils'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", 'bestdeals-utils'),
							"desc" => esc_html__("Title for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", 'bestdeals-utils'),
							"desc" => esc_html__("Subtitle for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", 'bestdeals-utils'),
							"desc" => esc_html__("Short description for the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Services style", 'bestdeals-utils'),
							"desc" => esc_html__("Select style to display services list", 'bestdeals-utils'),
							"value" => "services-1",
							"type" => "select",
							"options" => $services_styles
						),
						"type" => array(
							"title" => esc_html__("Icon's type", 'bestdeals-utils'),
							"desc" => esc_html__("Select type of icons: font icon or image", 'bestdeals-utils'),
							"value" => "icons",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'icons'  => esc_html__('Icons', 'bestdeals-utils'),
								'images' => esc_html__('Images', 'bestdeals-utils')
							)
						),
						"columns" => array(
							"title" => esc_html__("Columns", 'bestdeals-utils'),
							"desc" => esc_html__("How many columns use to show services list", 'bestdeals-utils'),
							"value" => 4,
							"min" => 2,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", 'bestdeals-utils'),
							"desc" => esc_html__("Select color scheme for this block", 'bestdeals-utils'),
							"value" => "",
							"type" => "checklist",
							"options" => $BESTDEALS_GLOBALS['sc_params']['schemes']
						),
						"slider" => array(
							"title" => esc_html__("Slider", 'bestdeals-utils'),
							"desc" => esc_html__("Use slider to show services", 'bestdeals-utils'),
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Controls", 'bestdeals-utils'),
							"desc" => esc_html__("Slider controls style and position", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $controls
						),
						"slides_space" => array(
							"title" => esc_html__("Space between slides", 'bestdeals-utils'),
							"desc" => esc_html__("Size of space (in px) between slides", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Slides change interval", 'bestdeals-utils'),
							"desc" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", 'bestdeals-utils'),
							"desc" => esc_html__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
							"dependency" => array(
								'slider' => array('yes')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Alignment", 'bestdeals-utils'),
							"desc" => esc_html__("Alignment of the services block", 'bestdeals-utils'),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $BESTDEALS_GLOBALS['sc_params']['align']
						),
						"custom" => array(
							"title" => esc_html__("Custom", 'bestdeals-utils'),
							"desc" => esc_html__("Allow get services items from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $BESTDEALS_GLOBALS['sc_params']['yes_no']
						),
						"cat" => array(
							"title" => esc_html__("Categories", 'bestdeals-utils'),
							"desc" => esc_html__("Select categories (groups) to show services list. If empty - select services from any category (group) or from IDs list", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $services_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of posts", 'bestdeals-utils'),
							"desc" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", 'bestdeals-utils'),
							"desc" => esc_html__("Skip posts before select next part.", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", 'bestdeals-utils'),
							"desc" => esc_html__("Select desired posts sorting method", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "title",
							"type" => "select",
							"options" => $BESTDEALS_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", 'bestdeals-utils'),
							"desc" => esc_html__("Select desired posts order", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "asc",
							"type" => "switch",
							"size" => "big",
							"options" => $BESTDEALS_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", 'bestdeals-utils'),
							"desc" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => "",
							"type" => "text"
						),
						"readmore" => array(
							"title" => esc_html__("Read more", 'bestdeals-utils'),
							"desc" => esc_html__("Caption for the Read more link (if empty - link not showed)", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Button URL", 'bestdeals-utils'),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", 'bestdeals-utils'),
							"desc" => esc_html__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
							"value" => "",
							"type" => "text"
						),
						"width" => bestdeals_shortcodes_width(),
						"height" => bestdeals_shortcodes_height(),
						"top" => $BESTDEALS_GLOBALS['sc_params']['top'],
						"bottom" => $BESTDEALS_GLOBALS['sc_params']['bottom'],
						"left" => $BESTDEALS_GLOBALS['sc_params']['left'],
						"right" => $BESTDEALS_GLOBALS['sc_params']['right'],
						"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
						"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
						"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
						"css" => $BESTDEALS_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_services_item",
						"title" => esc_html__("Service item", 'bestdeals-utils'),
						"desc" => esc_html__("Service item", 'bestdeals-utils'),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Title", 'bestdeals-utils'),
								"desc" => esc_html__("Item's title", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"icon" => array(
								"title" => esc_html__("Item's icon",  'bestdeals-utils'),
								"desc" => esc_html__('Select icon for the item from Fontello icons set',  'bestdeals-utils'),
								"value" => "",
								"type" => "icons",
								"options" => $BESTDEALS_GLOBALS['sc_params']['icons']
							),
							"image" => array(
								"title" => esc_html__("Item's image", 'bestdeals-utils'),
								"desc" => esc_html__("Item's image (if icon not selected)", 'bestdeals-utils'),
								"dependency" => array(
									'icon' => array('is_empty', 'none')
								),
								"value" => "",
								"readonly" => false,
								"type" => "media"
							),
							"link" => array(
								"title" => esc_html__("Link", 'bestdeals-utils'),
								"desc" => esc_html__("Link on service's item page", 'bestdeals-utils'),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"readmore" => array(
								"title" => esc_html__("Read more", 'bestdeals-utils'),
								"desc" => esc_html__("Caption for the Read more link (if empty - link not showed)", 'bestdeals-utils'),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("Description", 'bestdeals-utils'),
								"desc" => esc_html__("Item's short description", 'bestdeals-utils'),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $BESTDEALS_GLOBALS['sc_params']['id'],
							"class" => $BESTDEALS_GLOBALS['sc_params']['class'],
							"animation" => $BESTDEALS_GLOBALS['sc_params']['animation'],
							"css" => $BESTDEALS_GLOBALS['sc_params']['css']
						)
					)
				)

			));
		}
	}
}


// Add [trx_services] and [trx_services_item] in the VC shortcodes list
if (!function_exists('bestdeals_services_reg_shortcodes_vc')) {
	//Handler of add_filter('bestdeals_action_shortcodes_list_vc',	'bestdeals_services_reg_shortcodes_vc');
	function bestdeals_services_reg_shortcodes_vc() {
		global $BESTDEALS_GLOBALS;

		$services_groups = bestdeals_get_list_terms(false, 'services_group');
		$services_styles = bestdeals_get_list_templates('services');
		$controls		 = bestdeals_get_list_slider_controls();

		// Services
		vc_map( array(
			"base" => "trx_services",
			"name" => esc_html__("Services", 'bestdeals-utils'),
			"description" => esc_html__("Insert services list", 'bestdeals-utils'),
			"category" => esc_html__('Content', 'bestdeals-utils'),
			"icon" => 'icon_trx_services',
			"class" => "trx_sc_columns trx_sc_services",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_services_item'),
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Services style", 'bestdeals-utils'),
					"description" => esc_html__("Select style to display services list", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip($services_styles),
					"type" => "dropdown"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Icon's type", 'bestdeals-utils'),
					"description" => esc_html__("Select type of icons: font icon or image", 'bestdeals-utils'),
					"class" => "",
					"admin_label" => true,
					"value" => array(
						esc_html__('Icons', 'bestdeals-utils') => 'icons',
						esc_html__('Images', 'bestdeals-utils') => 'images'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns", 'bestdeals-utils'),
					"description" => esc_html__("How many columns use to show services list", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "4",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
					"description" => esc_html__("Select color scheme for this block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slider",
					"heading" => esc_html__("Slider", 'bestdeals-utils'),
					"description" => esc_html__("Use slider to show services", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					"class" => "",
					"std" => "no",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['yes_no']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "controls",
					"heading" => esc_html__("Controls", 'bestdeals-utils'),
					"description" => esc_html__("Slider controls style and position", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"std" => "no",
					"value" => array_flip($controls),
					"type" => "dropdown"
				),
				array(
					"param_name" => "slides_space",
					"heading" => esc_html__("Space between slides", 'bestdeals-utils'),
					"description" => esc_html__("Size of space (in px) between slides", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "interval",
					"heading" => esc_html__("Slides change interval", 'bestdeals-utils'),
					"description" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => "7000",
					"type" => "textfield"
				),
				array(
					"param_name" => "autoheight",
					"heading" => esc_html__("Autoheight", 'bestdeals-utils'),
					"description" => esc_html__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
					"group" => esc_html__('Slider', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'slider',
						'value' => 'yes'
					),
					"class" => "",
					"value" => array("Autoheight" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'bestdeals-utils'),
					"description" => esc_html__("Alignment of the services block", 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'bestdeals-utils'),
					"description" => esc_html__("Allow get services from inner shortcodes (custom) or get it from specified group (cat)", 'bestdeals-utils'),
					"class" => "",
					"value" => array("Custom services" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_html__("Title for the block", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
					"description" => esc_html__("Subtitle for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'bestdeals-utils'),
					"description" => esc_html__("Description for the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "cat",
					"heading" => esc_html__("Categories", 'bestdeals-utils'),
					"description" => esc_html__("Select category to show services. If empty - select services from any category (group) or from IDs list", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $services_groups)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "count",
					"heading" => esc_html__("Number of posts", 'bestdeals-utils'),
					"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "3",
					"type" => "textfield"
				),
				array(
					"param_name" => "offset",
					"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
					"description" => esc_html__("Skip posts before select next part.", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "0",
					"type" => "textfield"
				),
				array(
					"param_name" => "orderby",
					"heading" => esc_html__("Post sorting", 'bestdeals-utils'),
					"description" => esc_html__("Select desired posts sorting method", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "order",
					"heading" => esc_html__("Post order", 'bestdeals-utils'),
					"description" => esc_html__("Select desired posts order", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
					"type" => "dropdown"
				),
				array(
					"param_name" => "ids",
					"heading" => esc_html__("Team member's IDs list", 'bestdeals-utils'),
					"description" => esc_html__("Comma separated list of team members's ID. If set - parameters above (category, count, order, etc.)  are ignored!", 'bestdeals-utils'),
					"group" => esc_html__('Query', 'bestdeals-utils'),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "readmore",
					"heading" => esc_html__("Read more", 'bestdeals-utils'),
					"description" => esc_html__("Caption for the Read more link (if empty - link not showed)", 'bestdeals-utils'),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'bestdeals-utils'),
					"description" => esc_html__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'bestdeals-utils'),
					"description" => esc_html__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
					"group" => esc_html__('Captions', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				bestdeals_vc_width(),
				bestdeals_vc_height(),
				$BESTDEALS_GLOBALS['vc_params']['margin_top'],
				$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
				$BESTDEALS_GLOBALS['vc_params']['margin_left'],
				$BESTDEALS_GLOBALS['vc_params']['margin_right'],
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			),
			'default_content' => '
					[trx_services_item title="' . esc_html__( 'Service item 1', 'bestdeals-utils' ) . '"][/trx_services_item]
					[trx_services_item title="' . esc_html__( 'Service item 2', 'bestdeals-utils' ) . '"][/trx_services_item]
					[trx_services_item title="' . esc_html__( 'Service item 3', 'bestdeals-utils' ) . '"][/trx_services_item]
					[trx_services_item title="' . esc_html__( 'Service item 4', 'bestdeals-utils' ) . '"][/trx_services_item]
				',
			'js_view' => 'VcTrxColumnsView'
		) );


		vc_map( array(
			"base" => "trx_services_item",
			"name" => esc_html__("Services item", 'bestdeals-utils'),
			"description" => esc_html__("Custom services item - all data pull out from shortcode parameters", 'bestdeals-utils'),
			"show_settings_on_create" => true,
			"class" => "trx_sc_item trx_sc_column_item trx_sc_services_item",
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_services_item',
			"as_child" => array('only' => 'trx_services'),
			"as_parent" => array('except' => 'trx_services'),
			"params" => array(
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'bestdeals-utils'),
					"description" => esc_html__("Item's title", 'bestdeals-utils'),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Icon", 'bestdeals-utils'),
					"description" => esc_html__("Select icon for the item from Fontello icons set", 'bestdeals-utils'),
					"class" => "",
					"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
					"type" => "dropdown"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("Image", 'bestdeals-utils'),
					"description" => esc_html__("Item's image (if icon is empty)", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link", 'bestdeals-utils'),
					"description" => esc_html__("Link on item's page", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "readmore",
					"heading" => esc_html__("Read more", 'bestdeals-utils'),
					"description" => esc_html__("Caption for the Read more link (if empty - link not showed)", 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				$BESTDEALS_GLOBALS['vc_params']['id'],
				$BESTDEALS_GLOBALS['vc_params']['class'],
				$BESTDEALS_GLOBALS['vc_params']['animation'],
				$BESTDEALS_GLOBALS['vc_params']['css']
			)
		) );

		class WPBakeryShortCode_Trx_Services extends BESTDEALS_VC_ShortCodeColumns {}
		class WPBakeryShortCode_Trx_Services_Item extends BESTDEALS_VC_ShortCodeItem {}

	}
}
?>