<?php

if ((is_admin() && class_exists( 'WPBakeryShortCode' ))
	|| (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true' )
		|| (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline')
	) {
	require_once( trx_utils_get_file_dir('includes/core.shortcodes/shortcodes_vc_classes.php') );
}

// Width and height params
if ( !function_exists( 'bestdeals_vc_width' ) ) {
	function bestdeals_vc_width($w='') {
		return array(
			"param_name" => "width",
			"heading" => esc_html__("Width", 'bestdeals-utils'),
			"description" => esc_attr__("Width (in pixels or percent) of the current element", 'bestdeals-utils'),
			"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
			"value" => $w,
			"type" => "textfield"
		);
	}
}
if ( !function_exists( 'bestdeals_vc_height' ) ) {
	function bestdeals_vc_height($h='') {
		return array(
			"param_name" => "height",
			"heading" => esc_html__("Height", 'bestdeals-utils'),
			"description" => esc_attr__("Height (only in pixels) of the current element", 'bestdeals-utils'),
			"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
			"value" => $h,
			"type" => "textfield"
		);
	}
}

// Load scripts and styles for VC support
if ( !function_exists( 'bestdeals_shortcodes_vc_scripts_admin' ) ) {
	//add_action( 'admin_enqueue_scripts', 'bestdeals_shortcodes_vc_scripts_admin' );
	function bestdeals_shortcodes_vc_scripts_admin() {
		// Include CSS 
		wp_enqueue_style ( 'shortcodes_vc-style', trx_utils_get_file_url('includes/core.shortcodes/shortcodes_vc_admin.css'), array(), null );
		// Include JS
		wp_enqueue_script( 'shortcodes_vc-script', trx_utils_get_file_url('includes/core.shortcodes/shortcodes_vc_admin.js'), array(), null, true );
	}
}

// Load scripts and styles for VC support
if ( !function_exists( 'bestdeals_shortcodes_vc_scripts_front' ) ) {
	//add_action( 'wp_enqueue_scripts', 'bestdeals_shortcodes_vc_scripts_front' );
	function bestdeals_shortcodes_vc_scripts_front() {
		if (bestdeals_vc_is_frontend()) {
			// Include CSS 
			wp_enqueue_style ( 'shortcodes_vc-style', trx_utils_get_file_url('includes/core.shortcodes/shortcodes_vc_front.css'), array(), null );
			// Include JS
//			wp_enqueue_script( 'shortcodes_vc-script', trx_utils_get_file_url('includes/core.shortcodes/shortcodes_vc_front.js'), array(), null, true );
		}
	}
}

// Add init script into shortcodes output in VC frontend editor
if ( !function_exists( 'bestdeals_shortcodes_vc_add_init_script' ) ) {
	//add_filter('bestdeals_shortcode_output', 'bestdeals_shortcodes_vc_add_init_script', 10, 4);
	function bestdeals_shortcodes_vc_add_init_script($output, $tag='', $atts=array(), $content='') {
		if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')
				&& ( isset($_POST['shortcodes'][0]['tag']) && $_POST['shortcodes'][0]['tag']==$tag )
		) {
			if (bestdeals_strpos($output, 'bestdeals_vc_init_shortcodes')===false) {
				$id = "bestdeals_vc_init_shortcodes_".str_replace('.', '', mt_rand());
				$output .= '
					<script id="'.esc_attr($id).'">
						try {
							bestdeals_init_post_formats();
							bestdeals_init_shortcodes(jQuery("body").eq(0));
							bestdeals_scroll_actions();
						} catch (e) { };
					</script>
				';
			}
		}
		return $output;
	}
}


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'bestdeals_shortcodes_vc_theme_setup' ) ) {
	//if ( bestdeals_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'bestdeals_action_before_init_theme', 'bestdeals_shortcodes_vc_theme_setup', 20 );
	else
		add_action( 'bestdeals_action_after_init_theme', 'bestdeals_shortcodes_vc_theme_setup' );
	function bestdeals_shortcodes_vc_theme_setup() {
	
		// Set dir with theme specific VC shortcodes
		if ( function_exists( 'vc_set_shortcodes_templates_dir' ) ) {
			vc_set_shortcodes_templates_dir( trx_utils_get_folder_dir('core/core.shortcodes/vc_shortcodes' ) );
		}
		
		// Add/Remove params in the standard VC shortcodes
		vc_add_param("vc_row", array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
					"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
					"group" => esc_html__('Color scheme', 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip(bestdeals_get_list_color_schemes(true)),
					"type" => "dropdown"
		));

		if (bestdeals_shortcodes_is_used()) {

			// Set VC as main editor for the theme
			vc_set_as_theme( true );
			
			// Enable VC on follow post types
			vc_set_default_editor_post_types( array('page', 'team') );
			
			// Disable frontend editor
			//vc_disable_frontend();

			// Load scripts and styles for VC support
			add_action( 'wp_enqueue_scripts',		'bestdeals_shortcodes_vc_scripts_front');
			add_action( 'admin_enqueue_scripts',	'bestdeals_shortcodes_vc_scripts_admin' );

			// Add init script into shortcodes output in VC frontend editor
			add_filter('bestdeals_shortcode_output', 'bestdeals_shortcodes_vc_add_init_script', 10, 4);

			// Remove standard VC shortcodes
			vc_remove_element("vc_button");
			vc_remove_element("vc_posts_slider");
			vc_remove_element("vc_teaser_grid");
			vc_remove_element("vc_progress_bar");
			vc_remove_element("vc_facebook");
			vc_remove_element("vc_tweetmeme");
			vc_remove_element("vc_googleplus");
			vc_remove_element("vc_facebook");
			vc_remove_element("vc_pinterest");
			vc_remove_element("vc_message");
			vc_remove_element("vc_posts_grid");
			vc_remove_element("vc_carousel");
			vc_remove_element("vc_flickr");
			vc_remove_element("vc_tour");
			vc_remove_element("vc_separator");
			vc_remove_element("vc_single_image");
			vc_remove_element("vc_cta_button");
//			vc_remove_element("vc_accordion");
//			vc_remove_element("vc_accordion_tab");
			vc_remove_element("vc_toggle");
			vc_remove_element("vc_tabs");
			vc_remove_element("vc_tab");
			vc_remove_element("vc_images_carousel");
			
			// Remove standard WP widgets
			vc_remove_element("vc_wp_archives");
			vc_remove_element("vc_wp_calendar");
			vc_remove_element("vc_wp_categories");
			vc_remove_element("vc_wp_custommenu");
			vc_remove_element("vc_wp_links");
			vc_remove_element("vc_wp_meta");
			vc_remove_element("vc_wp_pages");
			vc_remove_element("vc_wp_posts");
			vc_remove_element("vc_wp_recentcomments");
			vc_remove_element("vc_wp_rss");
			vc_remove_element("vc_wp_search");
			vc_remove_element("vc_wp_tagcloud");
			vc_remove_element("vc_wp_text");
			
			global $BESTDEALS_GLOBALS;
			
			$BESTDEALS_GLOBALS['vc_params'] = array(
				
				// Common arrays and strings
				'category' => esc_html__("BestDEALS shortcodes", 'bestdeals-utils'),
			
				// Current element id
				'id' => array(
					"param_name" => "id",
					"heading" => esc_html__("Element ID", 'bestdeals-utils'),
					"description" => esc_attr__("ID for current element", 'bestdeals-utils'),
					"group" => esc_html__('ID &amp; Class', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				),
			
				// Current element class
				'class' => array(
					"param_name" => "class",
					"heading" => esc_html__("Element CSS class", 'bestdeals-utils'),
					"description" => esc_attr__("CSS class for current element", 'bestdeals-utils'),
					"group" => esc_html__('ID &amp; Class', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				),

				// Current element animation
				'animation' => array(
					"param_name" => "animation",
					"heading" => esc_html__("Animation", 'bestdeals-utils'),
					"description" => esc_attr__("Select animation while object enter in the visible area of page", 'bestdeals-utils'),
					"group" => esc_html__('ID &amp; Class', 'bestdeals-utils'),
					"class" => "",
					"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['animations']),
					"type" => "dropdown"
				),
			
				// Current element style
				'css' => array(
					"param_name" => "css",
					"heading" => esc_html__("CSS styles", 'bestdeals-utils'),
					"description" => esc_attr__("Any additional CSS rules (if need)", 'bestdeals-utils'),
					"group" => esc_html__('ID &amp; Class', 'bestdeals-utils'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
			
				// Margins params
				'margin_top' => array(
					"param_name" => "top",
					"heading" => esc_html__("Top margin", 'bestdeals-utils'),
					"description" => esc_attr__("Top margin (in pixels).", 'bestdeals-utils'),
					"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				),
			
				'margin_bottom' => array(
					"param_name" => "bottom",
					"heading" => esc_html__("Bottom margin", 'bestdeals-utils'),
					"description" => esc_attr__("Bottom margin (in pixels).", 'bestdeals-utils'),
					"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				),
			
				'margin_left' => array(
					"param_name" => "left",
					"heading" => esc_html__("Left margin", 'bestdeals-utils'),
					"description" => esc_attr__("Left margin (in pixels).", 'bestdeals-utils'),
					"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				),
				
				'margin_right' => array(
					"param_name" => "right",
					"heading" => esc_html__("Right margin", 'bestdeals-utils'),
					"description" => esc_attr__("Right margin (in pixels).", 'bestdeals-utils'),
					"group" => esc_html__('Size &amp; Margins', 'bestdeals-utils'),
					"value" => "",
					"type" => "textfield"
				)
			);
	
	
	
			// Accordion
			//-------------------------------------------------------------------------------------
			vc_map( array(
				"base" => "trx_accordion",
				"name" => esc_html__("Accordion", 'bestdeals-utils'),
				"description" => esc_attr__("Accordion items", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_accordion',
				"class" => "trx_sc_collection trx_sc_accordion",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_accordion_item'),	// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Accordion style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style for display accordion", 'bestdeals-utils'),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "counter",
						"heading" => esc_html__("Counter", 'bestdeals-utils'),
						"description" => esc_attr__("Display counter before each accordion title", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Add item numbers before each element" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "initial",
						"heading" => esc_html__("Initially opened item", 'bestdeals-utils'),
						"description" => esc_attr__("Number of initially opened item", 'bestdeals-utils'),
						"class" => "",
						"value" => 1,
						"type" => "textfield"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the closed accordion item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the opened accordion item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_accordion_item title="' . esc_html__( 'Item 1 title', 'bestdeals-utils') . '"][/trx_accordion_item]
					[trx_accordion_item title="' . esc_html__( 'Item 2 title', 'bestdeals-utils') . '"][/trx_accordion_item]
				',
				"custom_markup" => '
					<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
						%content%
					</div>
					<div class="tab_controls">
						<button class="add_tab" title="'.esc_html__("Add item", 'bestdeals-utils').'">'.esc_html__("Add item", 'bestdeals-utils').'</button>
					</div>
				',
				'js_view' => 'VcTrxAccordionView'
			) );
			
			
			vc_map( array(
				"base" => "trx_accordion_item",
				"name" => esc_html__("Accordion item", 'bestdeals-utils'),
				"description" => esc_attr__("Inner accordion item", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_accordion_item',
				"as_child" => array('only' => 'trx_accordion'), 	// Use only|except attributes to limit parent (separate multiple values with comma)
				"as_parent" => array('except' => 'trx_accordion'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for current accordion item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the closed accordion item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the opened accordion item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
			  'js_view' => 'VcTrxAccordionTabView'
			) );

			class WPBakeryShortCode_Trx_Accordion extends BESTDEALS_VC_ShortCodeAccordion {}
			class WPBakeryShortCode_Trx_Accordion_Item extends BESTDEALS_VC_ShortCodeAccordionItem {}
			
			
			
			
			
			
			// Anchor
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_anchor",
				"name" => esc_html__("Anchor", 'bestdeals-utils'),
				"description" => esc_attr__("Insert anchor for the TOC (table of content)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_anchor',
				"class" => "trx_sc_single trx_sc_anchor",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Anchor's icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the anchor from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Short title", 'bestdeals-utils'),
						"description" => esc_attr__("Short title of the anchor (for the table of content)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Long description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("External URL", 'bestdeals-utils'),
						"description" => esc_attr__("External URL for this TOC item", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "separator",
						"heading" => esc_html__("Add separator", 'bestdeals-utils'),
						"description" => esc_attr__("Add separator under item in the TOC", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Add separator" => "yes" ),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id']
				),
			) );
			
			class WPBakeryShortCode_Trx_Anchor extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Audio
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_audio",
				"name" => esc_html__("Audio", 'bestdeals-utils'),
				"description" => esc_attr__("Insert audio player", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_audio',
				"class" => "trx_sc_single trx_sc_audio",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("URL for audio file", 'bestdeals-utils'),
						"description" => esc_attr__("Put here URL for audio file", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Cover image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for audio cover", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title of the audio file", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "author",
						"heading" => esc_html__("Author", 'bestdeals-utils'),
						"description" => esc_attr__("Author of the audio file", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Controls", 'bestdeals-utils'),
						"description" => esc_attr__("Show/hide controls", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Hide controls" => "hide" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "autoplay",
						"heading" => esc_html__("Autoplay", 'bestdeals-utils'),
						"description" => esc_attr__("Autoplay audio on page load", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Autoplay" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Audio extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_block",
				"name" => esc_html__("Block container", 'bestdeals-utils'),
				"description" => esc_attr__("Container for any block ([section] analog - to enable nesting)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_block',
				"class" => "trx_sc_collection trx_sc_block",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "dedicated",
						"heading" => esc_html__("Dedicated", 'bestdeals-utils'),
						"description" => esc_attr__("Use this block as dedicated content - show it before post title on single page", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(__('Use as dedicated content', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns emulation", 'bestdeals-utils'),
						"description" => esc_attr__("Select width for columns emulation", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['columns']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "pan",
						"heading" => esc_html__("Use pan effect", 'bestdeals-utils'),
						"description" => esc_attr__("Use pan effect to show section content", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Content scroller', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", 'bestdeals-utils'),
						"description" => esc_attr__("Use scroller to show section content", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(__('Content scroller', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll_dir",
						"heading" => esc_html__("Scroll direction", 'bestdeals-utils'),
						"description" => esc_attr__("Scroll direction (if Use scroller = yes)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['dir']),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll_controls",
						"heading" => esc_html__("Scroll controls", 'bestdeals-utils'),
						"description" => esc_attr__("Show scroll controls (if Use scroller = yes)", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"value" => array(__('Show scroll controls', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for objects in this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", 'bestdeals-utils'),
						"description" => esc_attr__("Select background image from library for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", 'bestdeals-utils'),
						"description" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", 'bestdeals-utils'),
						"description" => esc_attr__("Texture style from 1 to 11. Empty or 0 - without texture.", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", 'bestdeals-utils'),
						"description" => esc_attr__("Font size of the text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", 'bestdeals-utils'),
						"description" => esc_attr__("Font weight of the text", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Default', 'bestdeals-utils') => 'inherit',
							__('Thin (100)', 'bestdeals-utils') => '100',
							__('Light (300)', 'bestdeals-utils') => '300',
							__('Normal (400)', 'bestdeals-utils') => '400',
							__('Bold (700)', 'bestdeals-utils') => '700'
						),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", 'bestdeals-utils'),
						"description" => esc_attr__("Content for section container", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Block extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			// Blogger
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_blogger",
				"name" => esc_html__("Blogger", 'bestdeals-utils'),
				"description" => esc_attr__("Insert posts (pages) in many styles from desired categories or directly from ids", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_blogger',
				"class" => "trx_sc_single trx_sc_blogger",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Output style", 'bestdeals-utils'),
						"description" => esc_attr__("Select desired style for posts output", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['blogger_styles']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "filters",
						"heading" => esc_html__("Show filters", 'bestdeals-utils'),
						"description" => esc_attr__("Use post's tags or categories as filter buttons", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['filters']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "hover",
						"heading" => esc_html__("Hover effect", 'bestdeals-utils'),
						"description" => esc_attr__("Select hover effect (only if style=Portfolio)", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['hovers']),
						'dependency' => array(
							'element' => 'style',
							'value' => array('portfolio_2','portfolio_3','portfolio_4','grid_2','grid_3','grid_4','square_2','square_3','square_4','short_2','short_3','short_4','colored_2','colored_3','colored_4')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "hover_dir",
						"heading" => esc_html__("Hover direction", 'bestdeals-utils'),
						"description" => esc_attr__("Select hover direction (only if style=Portfolio and hover=Circle|Square)", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['hovers_dir']),
						'dependency' => array(
							'element' => 'style',
							'value' => array('portfolio_2','portfolio_3','portfolio_4','grid_2','grid_3','grid_4','square_2','square_3','square_4','short_2','short_3','short_4','colored_2','colored_3','colored_4')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "location",
						"heading" => esc_html__("Dedicated content location", 'bestdeals-utils'),
						"description" => esc_attr__("Select position for dedicated content (only for style=excerpt)", 'bestdeals-utils'),
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('excerpt')
						),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['locations']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Posts direction", 'bestdeals-utils'),
						"description" => esc_attr__("Display posts in horizontal or vertical direction", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"std" => "horizontal",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "rating",
						"heading" => esc_html__("Show rating stars", 'bestdeals-utils'),
						"description" => esc_attr__("Show rating stars under post's header", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Show rating', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "info",
						"heading" => esc_html__("Show post info block", 'bestdeals-utils'),
						"description" => esc_attr__("Show post info block (author, date, tags, etc.)", 'bestdeals-utils'),
						"class" => "",
						"std" => 'yes',
						"value" => array(__('Show info', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "descr",
						"heading" => esc_html__("Description length", 'bestdeals-utils'),
						"description" => esc_attr__("How many characters are displayed from post excerpt? If 0 - don't show description", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						"class" => "",
						"value" => 0,
						"type" => "textfield"
					),
					array(
						"param_name" => "links",
						"heading" => esc_html__("Allow links to the post", 'bestdeals-utils'),
						"description" => esc_attr__("Allow links to the post from each blogger item", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						"class" => "",
						"std" => 'yes',
						"value" => array(__('Allow links', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "readmore",
						"heading" => esc_html__("More link text", 'bestdeals-utils'),
						"description" => esc_attr__("Read more link text. If empty - show 'More', else - used as link text", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
						"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", 'bestdeals-utils'),
						"description" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "post_type",
						"heading" => esc_html__("Post type", 'bestdeals-utils'),
						"description" => esc_attr__("Select post type to show", 'bestdeals-utils'),
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['posts_types']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("Post IDs list", 'bestdeals-utils'),
						"description" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories list", 'bestdeals-utils'),
						"description" => esc_attr__("Select category. If empty - show posts from any category or from IDs list", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $BESTDEALS_GLOBALS['sc_params']['categories'])),
						"type" => "dropdown"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Total posts to show", 'bestdeals-utils'),
						"description" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"admin_label" => true,
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => 3,
						"type" => "textfield"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns number", 'bestdeals-utils'),
						"description" => esc_attr__("How many columns used to display posts?", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'dir',
							'value' => 'horizontal'
						),
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Offset before select posts", 'bestdeals-utils'),
						"description" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"class" => "",
						"value" => 0,
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Post order by", 'bestdeals-utils'),
						"description" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Post order", 'bestdeals-utils'),
						"description" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "only",
						"heading" => esc_html__("Select posts only", 'bestdeals-utils'),
						"description" => esc_attr__("Select posts only with reviews, videos, audios, thumbs or galleries", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Query', 'bestdeals-utils'),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['formats']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", 'bestdeals-utils'),
						"description" => esc_attr__("Use scroller to show all posts", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Use scroller', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Show slider controls", 'bestdeals-utils'),
						"description" => esc_attr__("Show arrows to control scroll slider", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Show controls', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Blogger extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Br
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_br",
				"name" => esc_html__("Line break", 'bestdeals-utils'),
				"description" => esc_attr__("Line break or Clear Floating", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_br',
				"class" => "trx_sc_single trx_sc_br",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "clear",
						"heading" => esc_html__("Clear floating", 'bestdeals-utils'),
						"description" => esc_attr__("Select clear side (if need)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"value" => array(
							__('None', 'bestdeals-utils') => 'none',
							__('Left', 'bestdeals-utils') => 'left',
							__('Right', 'bestdeals-utils') => 'right',
							__('Both', 'bestdeals-utils') => 'both'
						),
						"type" => "dropdown"
					)
				)
			) );
			
			class WPBakeryShortCode_Trx_Br extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			// property info
			//-------------------------------------------------------------------------------------
			vc_map( array(
				"base" => "trx_property_info",
				"name" => esc_html__("Property info box", 'bestdeals-utils'),
				"description" => "",
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_wpb_row',
				"class" => "trx_sc_property_info trx_sc_property_info",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => false,
				"params" => array(
				)
			) );
			class WPBakeryShortCode_Trx_Property_Info extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			// property options
			//-------------------------------------------------------------------------------------
			vc_map( array(
				"base" => "trx_property_options",
				"name" => esc_html__("Property options box", 'bestdeals-utils'),
				"description" => "",
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_wpb_row',
				"class" => "trx_sc_property_options trx_sc_property_options",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns", 'bestdeals-utils'),
						"description" => esc_attr__("How many columns per row use for property list", 'bestdeals-utils'),
						"class" => "",
						"admin_label" => true,
						"value" => array(
							esc_html__('1 Column', 'bestdeals-utils') => '1',
							esc_html__('2 Columns', 'bestdeals-utils') => '2',
							esc_html__('3 Columns', 'bestdeals-utils') => '3',
						),
						"type" => "dropdown"
					)
				)
			) );
			class WPBakeryShortCode_Trx_Property_Options extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Property search
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_property_search",
				"name" => esc_html__("Property search", 'bestdeals-utils'),
				"description" => "",
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_wpb_row',
				"class" => "trx_property_search",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "status",
						"heading" => esc_html__("Property search status", 'bestdeals-utils'),
						"description" => "",
						"class" => "",
						"value" => "none",
						"value" => array(
							__('All property', 'bestdeals-utils') => 'none',
							__('Sale', 'bestdeals-utils') => 'sale',
							__('rent', 'bestdeals-utils') => 'rent'
						),
						"type" => "dropdown"
					)
				)
			) );
			
			class WPBakeryShortCode_Trx_Property_Search extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Button
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_button",
				"name" => esc_html__("Button", 'bestdeals-utils'),
				"description" => esc_attr__("Button with link", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_button',
				"class" => "trx_sc_single trx_sc_button",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "content",
						"heading" => esc_html__("Caption", 'bestdeals-utils'),
						"description" => esc_attr__("Button caption", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
//					array(
//						"param_name" => "type",
//						"heading" => esc_html__("Button's shape", 'bestdeals-utils'),
//						"description" => esc_attr__("Select button's shape", 'bestdeals-utils'),
//						"class" => "",
//						"value" => array(
//							__('Square', 'bestdeals-utils') => 'square',
//							__('Round', 'bestdeals-utils') => 'round'
//						),
//						"type" => "dropdown"
//					),
//					array(
//						"param_name" => "style",
//						"heading" => esc_html__("Button's style", 'bestdeals-utils'),
//						"description" => esc_attr__("Select button's style", 'bestdeals-utils'),
//						"class" => "",
//						"value" => array(
//							__('Filled', 'bestdeals-utils') => 'filled',
//							__('Border', 'bestdeals-utils') => 'border'
//						),
//						"type" => "dropdown"
//					),
					array(
						"param_name" => "size",
						"heading" => esc_html__("Button's size", 'bestdeals-utils'),
						"description" => esc_attr__("Select button's size", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('Small', 'bestdeals-utils') => 'small',
							__('Medium', 'bestdeals-utils') => 'medium',
							__('Large', 'bestdeals-utils') => 'large'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Button's icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the title from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Button's text color", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for button's caption", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Button's backcolor", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for button's background", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Button's alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align button to left, center or right", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", 'bestdeals-utils'),
						"description" => esc_attr__("URL for the link on button click", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "target",
						"heading" => esc_html__("Link target", 'bestdeals-utils'),
						"description" => esc_attr__("Target for the link on button click", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "popup",
						"heading" => esc_html__("Open link in popup", 'bestdeals-utils'),
						"description" => esc_attr__("Open link target in popup window", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"value" => array(__('Open in popup', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "rel",
						"heading" => esc_html__("Rel attribute", 'bestdeals-utils'),
						"description" => esc_attr__("Rel attribute for the button's link (if need", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Button extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Call to Action block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_call_to_action",
				"name" => esc_html__("Call to Action", 'bestdeals-utils'),
				"description" => esc_attr__("Insert call to action block in your page (post)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_call_to_action',
				"class" => "trx_sc_collection trx_sc_call_to_action",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Block's style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style to display this block", 'bestdeals-utils'),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "accent",
						"heading" => esc_html__("Accent", 'bestdeals-utils'),
						"description" => esc_attr__("Fill entire block with Accent1 color from current color scheme", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Fill with Accent1 color" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom", 'bestdeals-utils'),
						"description" => esc_attr__("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Custom content" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Image", 'bestdeals-utils'),
						"description" => esc_attr__("Image to display inside block", 'bestdeals-utils'),
				        'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "video",
						"heading" => esc_html__("URL for video file", 'bestdeals-utils'),
						"description" => esc_attr__("Paste URL for video file to display inside block", 'bestdeals-utils'),
				        'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
						"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", 'bestdeals-utils'),
						"description" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link2",
						"heading" => esc_html__("Button 2 URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL for the second button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link2_caption",
						"heading" => esc_html__("Button 2 caption", 'bestdeals-utils'),
						"description" => esc_attr__("Caption for the second button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Call_To_Action extends BESTDEALS_VC_ShortCodeCollection {}


			
			
			
			
			// Chat
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_chat",
				"name" => esc_html__("Chat", 'bestdeals-utils'),
				"description" => esc_attr__("Chat message", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_chat',
				"class" => "trx_sc_container trx_sc_chat",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Item title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for current chat item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "photo",
						"heading" => esc_html__("Item photo", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for the item photo (avatar)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", 'bestdeals-utils'),
						"description" => esc_attr__("URL for the link on chat title click", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Chat item content", 'bestdeals-utils'),
						"description" => esc_attr__("Current chat item content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			
			) );
			
			class WPBakeryShortCode_Trx_Chat extends BESTDEALS_VC_ShortCodeContainer {}
			
			
			
			
			
			
			// Columns
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_columns",
				"name" => esc_html__("Columns", 'bestdeals-utils'),
				"description" => esc_attr__("Insert columns with margins", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_columns',
				"class" => "trx_sc_columns",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_column_item'),
				"params" => array(
					array(
						"param_name" => "count",
						"heading" => esc_html__("Columns count", 'bestdeals-utils'),
						"description" => esc_attr__("Number of the columns in the container.", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "2",
						"type" => "textfield"
					),
					array(
						"param_name" => "fluid",
						"heading" => esc_html__("Fluid columns", 'bestdeals-utils'),
						"description" => esc_attr__("To squeeze the columns when reducing the size of the window (fluid=yes) or to rebuild them (fluid=no)", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Fluid columns', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_column_item][/trx_column_item]
					[trx_column_item][/trx_column_item]
				',
				'js_view' => 'VcTrxColumnsView'
			) );
			
			
			vc_map( array(
				"base" => "trx_column_item",
				"name" => esc_html__("Column", 'bestdeals-utils'),
				"description" => esc_attr__("Column item", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"class" => "trx_sc_collection trx_sc_column_item",
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_column_item',
				"as_child" => array('only' => 'trx_columns'),
				"as_parent" => array('except' => 'trx_columns'),
				"params" => array(
					array(
						"param_name" => "span",
						"heading" => esc_html__("Merge columns", 'bestdeals-utils'),
						"description" => esc_attr__("Count merged columns from current", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Alignment text in the column", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for objects in this column", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Any background color for this column", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("URL for background image file", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for the background", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Column's content", 'bestdeals-utils'),
						"description" => esc_attr__("Content of the current column", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxColumnItemView'
			) );
			
			class WPBakeryShortCode_Trx_Columns extends BESTDEALS_VC_ShortCodeColumns {}
			class WPBakeryShortCode_Trx_Column_Item extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Contact form
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_contact_form",
				"name" => esc_html__("Contact form", 'bestdeals-utils'),
				"description" => esc_attr__("Insert contact form", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_contact_form',
				"class" => "trx_sc_collection trx_sc_contact_form",
				"content_element" => true,
				"is_container" => true,
				"as_parent" => array('only' => 'trx_form_item'),
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom", 'bestdeals-utils'),
						"description" => esc_attr__("Use custom fields or create standard contact form (ignore info from 'Field' tabs)", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Create custom form', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style of the contact form", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "action",
						"heading" => esc_html__("Action", 'bestdeals-utils'),
						"description" => esc_attr__("Contact form action (URL to handle form data). If empty - use internal action", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select form alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
						"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			
			vc_map( array(
				"base" => "trx_form_item",
				"name" => esc_html__("Form item (custom field)", 'bestdeals-utils'),
				"description" => esc_attr__("Custom field for the contact form", 'bestdeals-utils'),
				"class" => "trx_sc_item trx_sc_form_item",
				'icon' => 'icon_trx_form_item',
				//"allowed_container_element" => 'vc_row',
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				"as_child" => array('only' => 'trx_contact_form'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Type", 'bestdeals-utils'),
						"description" => esc_attr__("Select type of the custom field", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['field_types']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "name",
						"heading" => esc_html__("Name", 'bestdeals-utils'),
						"description" => esc_attr__("Name of the custom field", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "value",
						"heading" => esc_html__("Default value", 'bestdeals-utils'),
						"description" => esc_attr__("Default value of the custom field", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "options",
						"heading" => esc_html__("Options", 'bestdeals-utils'),
						"description" => esc_attr__("Field options. For example: big=My daddy|middle=My brother|small=My little sister", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('radio','checkbox','select')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "label",
						"heading" => esc_html__("Label", 'bestdeals-utils'),
						"description" => esc_attr__("Label for the custom field", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "label_position",
						"heading" => esc_html__("Label position", 'bestdeals-utils'),
						"description" => esc_attr__("Label position relative to the field", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['label_positions']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Contact_Form extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Form_Item extends BESTDEALS_VC_ShortCodeItem {}
			
			
			
			
			
			
			
			// Content block on fullscreen page
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_content",
				"name" => esc_html__("Content block", 'bestdeals-utils'),
				"description" => esc_attr__("Container for main content block (use it only on fullscreen pages)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_content',
				"class" => "trx_sc_collection trx_sc_content",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", 'bestdeals-utils'),
						"description" => esc_attr__("Content for section container", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom']
				)
			) );
			
			class WPBakeryShortCode_Trx_Content extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Countdown
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_countdown",
				"name" => esc_html__("Countdown", 'bestdeals-utils'),
				"description" => esc_attr__("Insert countdown object", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_countdown',
				"class" => "trx_sc_single trx_sc_countdown",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "date",
						"heading" => esc_html__("Date", 'bestdeals-utils'),
						"description" => esc_attr__("Upcoming date (format: yyyy-mm-dd)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "time",
						"heading" => esc_html__("Time", 'bestdeals-utils'),
						"description" => esc_attr__("Upcoming time (format: HH:mm:ss)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Countdown style", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align counter to left, center or right", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Countdown extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Dropcaps
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_dropcaps",
				"name" => esc_html__("Dropcaps", 'bestdeals-utils'),
				"description" => esc_attr__("Make first letter of the text as dropcaps", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_dropcaps',
				"class" => "trx_sc_single trx_sc_dropcaps",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Dropcaps style", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 4)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Paragraph text", 'bestdeals-utils'),
						"description" => esc_attr__("Paragraph with dropcaps content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			
			) );
			
			class WPBakeryShortCode_Trx_Dropcaps extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Emailer
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_emailer",
				"name" => esc_html__("E-mail collector", 'bestdeals-utils'),
				"description" => esc_attr__("Collect e-mails into specified group", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_emailer',
				"class" => "trx_sc_single trx_sc_emailer",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" =>"group",
						"heading" => esc_html__("Group", 'bestdeals-utils'),
						"description" => esc_attr__("The name of group to collect e-mail address", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "open",
						"heading" => esc_html__("Opened", 'bestdeals-utils'),
						"description" => esc_attr__("Initially open the input field on show object", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Initially opened', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align field to left, center or right", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Emailer extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Gap
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_gap",
				"name" => esc_html__("Gap", 'bestdeals-utils'),
				"description" => esc_attr__("Insert gap (fullwidth area) in the post content", 'bestdeals-utils'),
				"category" => esc_html__('Structure', 'bestdeals-utils'),
				'icon' => 'icon_trx_gap',
				"class" => "trx_sc_collection trx_sc_gap",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"params" => array(
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Gap content", 'bestdeals-utils'),
						"description" => esc_attr__("Gap inner content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					)
					*/
				)
			) );
			
			class WPBakeryShortCode_Trx_Gap extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Googlemap
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_googlemap",
				"name" => esc_html__("Google map", 'bestdeals-utils'),
				"description" => esc_attr__("Insert Google map with desired address or coordinates", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_googlemap',
				"class" => "trx_sc_collection trx_sc_googlemap",
				"content_element" => true,
				"is_container" => true,
				"as_parent" => array('only' => 'trx_googlemap_marker'),
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "zoom",
						"heading" => esc_html__("Zoom", 'bestdeals-utils'),
						"description" => esc_attr__("Map zoom factor", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "16",
						"type" => "textfield"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Map custom style", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['googlemap_styles']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width('100%'),
					bestdeals_vc_height(240),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			vc_map( array(
				"base" => "trx_googlemap_marker",
				"name" => esc_html__("Googlemap marker", 'bestdeals-utils'),
				"description" => esc_attr__("Insert new marker into Google map", 'bestdeals-utils'),
				"class" => "trx_sc_collection trx_sc_googlemap_marker",
				'icon' => 'icon_trx_googlemap_marker',
				//"allowed_container_element" => 'vc_row',
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				"as_child" => array('only' => 'trx_googlemap'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "address",
						"heading" => esc_html__("Address", 'bestdeals-utils'),
						"description" => esc_attr__("Address of this marker", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "latlng",
						"heading" => esc_html__("Latitude and Longitude", 'bestdeals-utils'),
						"description" => esc_attr__("Comma separated marker's coorditanes (instead Address)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for this marker", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "point",
						"heading" => esc_html__("URL for marker image file", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for this marker. If empty - use default marker", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					$BESTDEALS_GLOBALS['vc_params']['id']
				)
			) );
			
			class WPBakeryShortCode_Trx_Googlemap extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Googlemap_Marker extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			
			
			// Highlight
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_highlight",
				"name" => esc_html__("Highlight text", 'bestdeals-utils'),
				"description" => esc_attr__("Highlight text with selected color, background color and other styles", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_highlight',
				"class" => "trx_sc_single trx_sc_highlight",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Type", 'bestdeals-utils'),
						"description" => esc_attr__("Highlight type", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								__('Custom', 'bestdeals-utils') => 0,
								__('Type 1', 'bestdeals-utils') => 1,
								__('Type 2', 'bestdeals-utils') => 2,
								__('Type 3', 'bestdeals-utils') => 3
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", 'bestdeals-utils'),
						"description" => esc_attr__("Color for the highlighted text", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Background color for the highlighted text", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", 'bestdeals-utils'),
						"description" => esc_attr__("Font size for the highlighted text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Highlight text", 'bestdeals-utils'),
						"description" => esc_attr__("Content for highlight", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Highlight extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Icon
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_icon",
				"name" => esc_html__("Icon", 'bestdeals-utils'),
				"description" => esc_attr__("Insert the icon", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_icon',
				"class" => "trx_sc_single trx_sc_icon",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon class from Fontello icons set", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", 'bestdeals-utils'),
						"description" => esc_attr__("Icon's color", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Background color for the icon", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_shape",
						"heading" => esc_html__("Background shape", 'bestdeals-utils'),
						"description" => esc_attr__("Shape of the icon background", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('None', 'bestdeals-utils') => 'none',
							__('Round', 'bestdeals-utils') => 'round',
							__('Square', 'bestdeals-utils') => 'square'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", 'bestdeals-utils'),
						"description" => esc_attr__("Icon's font size", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", 'bestdeals-utils'),
						"description" => esc_attr__("Icon's font weight", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Default', 'bestdeals-utils') => 'inherit',
							__('Thin (100)', 'bestdeals-utils') => '100',
							__('Light (300)', 'bestdeals-utils') => '300',
							__('Normal (400)', 'bestdeals-utils') => '400',
							__('Bold (700)', 'bestdeals-utils') => '700'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Icon's alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align icon to left, center or right", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL from this icon (if not empty)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Icon extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Image
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_image",
				"name" => esc_html__("Image", 'bestdeals-utils'),
				"description" => esc_attr__("Insert image", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_image',
				"class" => "trx_sc_single trx_sc_image",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("Select image", 'bestdeals-utils'),
						"description" => esc_attr__("Select image from library", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Image alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align image to left or right side", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "shape",
						"heading" => esc_html__("Image shape", 'bestdeals-utils'),
						"description" => esc_attr__("Shape of the image: square or round", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('Square', 'bestdeals-utils') => 'square',
							__('Round', 'bestdeals-utils') => 'round'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Image's title", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Title's icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the title from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link", 'bestdeals-utils'),
						"description" => esc_attr__("The link URL from the image", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Image extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Infobox
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_infobox",
				"name" => esc_html__("Infobox", 'bestdeals-utils'),
				"description" => esc_attr__("Box with info or error message", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_infobox',
				"class" => "trx_sc_container trx_sc_infobox",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Infobox style", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								__('Regular', 'bestdeals-utils') => 'regular',
								__('Info', 'bestdeals-utils') => 'info',
								__('Success', 'bestdeals-utils') => 'success',
								__('Error', 'bestdeals-utils') => 'error',
								__('Result', 'bestdeals-utils') => 'result'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "closeable",
						"heading" => esc_html__("Closeable", 'bestdeals-utils'),
						"description" => esc_attr__("Create closeable box (with close button)", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Close button', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Custom icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the infobox from Fontello icons set. If empty - use default icon", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for the text and headers", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Any background color for this infobox", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Message text", 'bestdeals-utils'),
						"description" => esc_attr__("Message for the infobox", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			) );
			
			class WPBakeryShortCode_Trx_Infobox extends BESTDEALS_VC_ShortCodeContainer {}
			
			
			
			
			
			
			
			// Line
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_line",
				"name" => esc_html__("Line", 'bestdeals-utils'),
				"description" => esc_attr__("Insert line (delimiter)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				"class" => "trx_sc_single trx_sc_line",
				'icon' => 'icon_trx_line',
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Line style", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								__('Solid', 'bestdeals-utils') => 'solid',
								__('Dashed', 'bestdeals-utils') => 'dashed',
								__('Dotted', 'bestdeals-utils') => 'dotted',
								__('Double', 'bestdeals-utils') => 'double',
								__('Shadow', 'bestdeals-utils') => 'shadow'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Line color", 'bestdeals-utils'),
						"description" => esc_attr__("Line color", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Line extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// List
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_list",
				"name" => esc_html__("List", 'bestdeals-utils'),
				"description" => esc_attr__("List items with specific bullets", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				"class" => "trx_sc_collection trx_sc_list",
				'icon' => 'icon_trx_list',
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_list_item'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Bullet's style", 'bestdeals-utils'),
						"description" => esc_attr__("Bullet's style for each list item", 'bestdeals-utils'),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['list_styles']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", 'bestdeals-utils'),
						"description" => esc_attr__("List items color", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("List icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select list icon from Fontello icons set (only for style=Iconed)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_color",
						"heading" => esc_html__("Icon color", 'bestdeals-utils'),
						"description" => esc_attr__("List icons color", 'bestdeals-utils'),
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => "",
						"type" => "colorpicker"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_list_item]' . esc_html__( 'Item 1', 'bestdeals-utils') . '[/trx_list_item]
					[trx_list_item]' . esc_html__( 'Item 2', 'bestdeals-utils') . '[/trx_list_item]
				'
			) );
			
			
			vc_map( array(
				"base" => "trx_list_item",
				"name" => esc_html__("List item", 'bestdeals-utils'),
				"description" => esc_attr__("List item with specific bullet", 'bestdeals-utils'),
				"class" => "trx_sc_single trx_sc_list_item",
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_list_item',
				"as_child" => array('only' => 'trx_list'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"as_parent" => array('except' => 'trx_list'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("List item title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the current list item (show it as tooltip)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL for the current list item", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "target",
						"heading" => esc_html__("Link target", 'bestdeals-utils'),
						"description" => esc_attr__("Link target for the current list item", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Link', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", 'bestdeals-utils'),
						"description" => esc_attr__("Text color for this item", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("List item icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select list item icon from Fontello icons set (only for style=Iconed)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_color",
						"heading" => esc_html__("Icon color", 'bestdeals-utils'),
						"description" => esc_attr__("Icon color for this item", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("List item text", 'bestdeals-utils'),
						"description" => esc_attr__("Current list item content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			
			) );
			
			class WPBakeryShortCode_Trx_List extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_List_Item extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			// List education
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_list_educ",
				"name" => esc_html__("List education", 'bestdeals-utils'),
				"description" => esc_attr__("List education", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				"class" => "trx_sc_collection trx_sc_list_educ",
				'icon' => 'icon_trx_list',
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_list_educ_item'),
				"params" => array(
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
				),
				'default_content' => '
					[trx_list_educ_item]' . esc_html__( 'Item 1', 'bestdeals-utils') . '[/trx_list_educ_item]
					[trx_list_educ_item]' . esc_html__( 'Item 2', 'bestdeals-utils') . '[/trx_list_educ_item]
				'
			) );
			
			
			vc_map( array(
				"base" => "trx_list_educ_item",
				"name" => esc_html__("List item", 'bestdeals-utils'),
				"description" => esc_attr__("List educ", 'bestdeals-utils'),
				"class" => "trx_sc_single trx_sc_list_educ_item",
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_list_item',
				"as_child" => array('only' => 'trx_list'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"as_parent" => array('except' => 'trx_list'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("List item title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the current list item (show it as tooltip)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("List item text", 'bestdeals-utils'),
						"description" => esc_attr__("Current list item content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			
			) );
			
			class WPBakeryShortCode_Trx_List_Educ extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_List_Educ_Item extends BESTDEALS_VC_ShortCodeSingle {}
			
			/* *********************************************** */
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// Number
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_number",
				"name" => esc_html__("Number", 'bestdeals-utils'),
				"description" => esc_attr__("Insert number or any word as set of separated characters", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				"class" => "trx_sc_single trx_sc_number",
				'icon' => 'icon_trx_number',
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "value",
						"heading" => esc_html__("Value", 'bestdeals-utils'),
						"description" => esc_attr__("Number or any word to separate", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Number extends BESTDEALS_VC_ShortCodeSingle {}


			
			
			
			
			
			// Parallax
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_parallax",
				"name" => esc_html__("Parallax", 'bestdeals-utils'),
				"description" => esc_attr__("Create the parallax container (with asinc background image)", 'bestdeals-utils'),
				"category" => esc_html__('Structure', 'bestdeals-utils'),
				'icon' => 'icon_trx_parallax',
				"class" => "trx_sc_collection trx_sc_parallax",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "gap",
						"heading" => esc_html__("Create gap", 'bestdeals-utils'),
						"description" => esc_attr__("Create gap around parallax container (not need in fullscreen pages)", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Create gap', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Direction", 'bestdeals-utils'),
						"description" => esc_attr__("Scroll direction for the parallax background", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								__('Up', 'bestdeals-utils') => 'up',
								__('Down', 'bestdeals-utils') => 'down'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "speed",
						"heading" => esc_html__("Speed", 'bestdeals-utils'),
						"description" => esc_attr__("Parallax background motion speed (from 0.0 to 1.0)", 'bestdeals-utils'),
						"class" => "",
						"value" => "0.3",
						"type" => "textfield"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", 'bestdeals-utils'),
						"description" => esc_attr__("Select color for text object inside parallax block", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Backgroud color", 'bestdeals-utils'),
						"description" => esc_attr__("Select color for parallax background", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for the parallax background", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_image_x",
						"heading" => esc_html__("Image X position", 'bestdeals-utils'),
						"description" => esc_attr__("Parallax background X position (in percents)", 'bestdeals-utils'),
						"class" => "",
						"value" => "50%",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_video",
						"heading" => esc_html__("Video background", 'bestdeals-utils'),
						"description" => esc_attr__("Paste URL for video file to show it as parallax background", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_video_ratio",
						"heading" => esc_html__("Video ratio", 'bestdeals-utils'),
						"description" => esc_attr__("Specify ratio of the video background. For example: 16:9 (default), 4:3, etc.", 'bestdeals-utils'),
						"class" => "",
						"value" => "16:9",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", 'bestdeals-utils'),
						"description" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", 'bestdeals-utils'),
						"description" => esc_attr__("Texture style from 1 to 11. Empty or 0 - without texture.", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Content", 'bestdeals-utils'),
						"description" => esc_attr__("Content for the parallax container", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Parallax extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			// Popup
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_popup",
				"name" => esc_html__("Popup window", 'bestdeals-utils'),
				"description" => esc_attr__("Container for any html-block with desired class and style for popup window", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_popup',
				"class" => "trx_sc_collection trx_sc_popup",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", 'bestdeals-utils'),
						"description" => esc_attr__("Content for popup container", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Popup extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Price
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_price",
				"name" => esc_html__("Price", 'bestdeals-utils'),
				"description" => esc_attr__("Insert price with decoration", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_price',
				"class" => "trx_sc_single trx_sc_price",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "money",
						"heading" => esc_html__("Money", 'bestdeals-utils'),
						"description" => esc_attr__("Money value (dot or comma separated)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency symbol", 'bestdeals-utils'),
						"description" => esc_attr__("Currency character", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "$",
						"type" => "textfield"
					),
					array(
						"param_name" => "period",
						"heading" => esc_html__("Period", 'bestdeals-utils'),
						"description" => esc_attr__("Period text (if need). For example: monthly, daily, etc.", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align price to left or right side", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Price extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Price block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_price_block",
				"name" => esc_html__("Price block", 'bestdeals-utils'),
				"description" => esc_attr__("Insert price block with title, price and description", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_price_block',
				"class" => "trx_sc_single trx_sc_price_block",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Block title", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", 'bestdeals-utils'),
						"description" => esc_attr__("URL for link from button (at bottom of the block)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_text",
						"heading" => esc_html__("Link text", 'bestdeals-utils'),
						"description" => esc_attr__("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon from Fontello icons set (placed before/instead price)", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "money",
						"heading" => esc_html__("Money", 'bestdeals-utils'),
						"description" => esc_attr__("Money value (dot or comma separated)", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Money', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency symbol", 'bestdeals-utils'),
						"description" => esc_attr__("Currency character", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Money', 'bestdeals-utils'),
						"class" => "",
						"value" => "$",
						"type" => "textfield"
					),
					array(
						"param_name" => "period",
						"heading" => esc_html__("Period", 'bestdeals-utils'),
						"description" => esc_attr__("Period text (if need). For example: monthly, daily, etc.", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Money', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align price to left or right side", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for this price block", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_PriceBlock extends BESTDEALS_VC_ShortCodeSingle {}

			
			
			
			
			// Quote
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_quote",
				"name" => esc_html__("Quote", 'bestdeals-utils'),
				"description" => esc_attr__("Quote text", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_quote',
				"class" => "trx_sc_single trx_sc_quote",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "cite",
						"heading" => esc_html__("Quote cite", 'bestdeals-utils'),
						"description" => esc_attr__("URL for the quote cite link", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title (author)", 'bestdeals-utils'),
						"description" => esc_attr__("Quote title (author name)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Quote content", 'bestdeals-utils'),
						"description" => esc_attr__("Quote content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Quote extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Reviews
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_reviews",
				"name" => esc_html__("Reviews", 'bestdeals-utils'),
				"description" => esc_attr__("Insert reviews block in the single post", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_reviews',
				"class" => "trx_sc_single trx_sc_reviews",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align counter to left, center or right", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Reviews extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Search
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_search",
				"name" => esc_html__("Search form", 'bestdeals-utils'),
				"description" => esc_attr__("Insert search form", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_search',
				"class" => "trx_sc_single trx_sc_search",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style to display search field", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Regular', 'bestdeals-utils') => "regular",
							__('Flat', 'bestdeals-utils') => "flat"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "state",
						"heading" => esc_html__("State", 'bestdeals-utils'),
						"description" => esc_attr__("Select search field initial state", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Fixed', 'bestdeals-utils')  => "fixed",
							__('Opened', 'bestdeals-utils') => "opened",
							__('Closed', 'bestdeals-utils') => "closed"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title (placeholder) for the search field", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => esc_html__("Search &hellip;", 'bestdeals-utils'),
						"type" => "textfield"
					),
					array(
						"param_name" => "ajax",
						"heading" => esc_html__("AJAX", 'bestdeals-utils'),
						"description" => esc_attr__("Search via AJAX or reload page", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Use AJAX search', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Search extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Section
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_section",
				"name" => esc_html__("Section container", 'bestdeals-utils'),
				"description" => esc_attr__("Container for any block ([block] analog - to enable nesting)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				"class" => "trx_sc_collection trx_sc_section",
				'icon' => 'icon_trx_block',
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "dedicated",
						"heading" => esc_html__("Dedicated", 'bestdeals-utils'),
						"description" => esc_attr__("Use this block as dedicated content - show it before post title on single page", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(__('Use as dedicated content', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns emulation", 'bestdeals-utils'),
						"description" => esc_attr__("Select width for columns emulation", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['columns']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "pan",
						"heading" => esc_html__("Use pan effect", 'bestdeals-utils'),
						"description" => esc_attr__("Use pan effect to show section content", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Content scroller', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", 'bestdeals-utils'),
						"description" => esc_attr__("Use scroller to show section content", 'bestdeals-utils'),
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(__('Content scroller', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll_dir",
						"heading" => esc_html__("Scroll and Pan direction", 'bestdeals-utils'),
						"description" => esc_attr__("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll_controls",
						"heading" => esc_html__("Scroll controls", 'bestdeals-utils'),
						"description" => esc_attr__("Show scroll controls (if Use scroller = yes)", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Scroll', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"value" => array(__('Show scroll controls', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", 'bestdeals-utils'),
						"description" => esc_attr__("Any color for objects in this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", 'bestdeals-utils'),
						"description" => esc_attr__("Select background image from library for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", 'bestdeals-utils'),
						"description" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", 'bestdeals-utils'),
						"description" => esc_attr__("Texture style from 1 to 11. Empty or 0 - without texture.", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", 'bestdeals-utils'),
						"description" => esc_attr__("Font size of the text (default - in pixels, allows any CSS units of measure)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", 'bestdeals-utils'),
						"description" => esc_attr__("Font weight of the text", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Default', 'bestdeals-utils') => 'inherit',
							__('Thin (100)', 'bestdeals-utils') => '100',
							__('Light (300)', 'bestdeals-utils') => '300',
							__('Normal (400)', 'bestdeals-utils') => '400',
							__('Bold (700)', 'bestdeals-utils') => '700'
						),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", 'bestdeals-utils'),
						"description" => esc_attr__("Content for section container", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Section extends BESTDEALS_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Skills
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_skills",
				"name" => esc_html__("Skills", 'bestdeals-utils'),
				"description" => esc_attr__("Insert skills diagramm", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_skills',
				"class" => "trx_sc_collection trx_sc_skills",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_skills_item'),
				"params" => array(
					array(
						"param_name" => "max_value",
						"heading" => esc_html__("Max value", 'bestdeals-utils'),
						"description" => esc_attr__("Max value for skills items", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "100",
						"type" => "textfield"
					),
					array(
						"param_name" => "type",
						"heading" => esc_html__("Skills type", 'bestdeals-utils'),
						"description" => esc_attr__("Select type of skills block", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('Bar', 'bestdeals-utils') => 'bar',
							__('Pie chart', 'bestdeals-utils') => 'pie',
							__('Counter', 'bestdeals-utils') => 'counter',
							__('Arc', 'bestdeals-utils') => 'arc'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "layout",
						"heading" => esc_html__("Skills layout", 'bestdeals-utils'),
						"description" => esc_attr__("Select layout of skills block", 'bestdeals-utils'),
						"admin_label" => true,
						'dependency' => array(
							'element' => 'type',
							'value' => array('counter','bar','pie')
						),
						"class" => "",
						"value" => array(
							__('Rows', 'bestdeals-utils') => 'rows',
							__('Columns', 'bestdeals-utils') => 'columns'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Direction", 'bestdeals-utils'),
						"description" => esc_attr__("Select direction of skills block", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Counters style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style of skills items (only for type=counter)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 4)),
						'dependency' => array(
							'element' => 'type',
							'value' => array('counter')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns count", 'bestdeals-utils'),
						"description" => esc_attr__("Skills columns count (required)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", 'bestdeals-utils'),
						"description" => esc_attr__("Color for all skills items", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Background color for all skills items (only for type=pie)", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "border_color",
						"heading" => esc_html__("Border color", 'bestdeals-utils'),
						"description" => esc_attr__("Border color for all skills items (only for type=pie)", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Align skills block to left or right side", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "arc_caption",
						"heading" => esc_html__("Arc caption", 'bestdeals-utils'),
						"description" => esc_attr__("Arc caption - text in the center of the diagram", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('arc')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "pie_compact",
						"heading" => esc_html__("Pie compact", 'bestdeals-utils'),
						"description" => esc_attr__("Show all skills in one diagram or as separate diagrams", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => array(__('Show all skills in one diagram', 'bestdeals-utils') => 'on'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "pie_cutout",
						"heading" => esc_html__("Pie cutout", 'bestdeals-utils'),
						"description" => esc_attr__("Pie cutout (0-99). 0 - without cutout, 99 - max cutout", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the block", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", 'bestdeals-utils'),
						"description" => esc_attr__("Subtitle for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", 'bestdeals-utils'),
						"description" => esc_attr__("Description for the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", 'bestdeals-utils'),
						"description" => esc_attr__("Link URL for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", 'bestdeals-utils'),
						"description" => esc_attr__("Caption for the button at the bottom of the block", 'bestdeals-utils'),
						"group" => esc_html__('Captions', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			
			vc_map( array(
				"base" => "trx_skills_item",
				"name" => esc_html__("Skill", 'bestdeals-utils'),
				"description" => esc_attr__("Skills item", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"class" => "trx_sc_single trx_sc_skills_item",
				"content_element" => true,
				"is_container" => false,
				"as_child" => array('only' => 'trx_skills'),
				"as_parent" => array('except' => 'trx_skills'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for the current skills item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "value",
						"heading" => esc_html__("Value", 'bestdeals-utils'),
						"description" => esc_attr__("Value for the current skills item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", 'bestdeals-utils'),
						"description" => esc_attr__("Color for current skills item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Background color for current skills item (only for type=pie)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "border_color",
						"heading" => esc_html__("Border color", 'bestdeals-utils'),
						"description" => esc_attr__("Border color for current skills item (only for type=pie)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Counter style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style for the current skills item (only for type=counter)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 4)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Counter icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon from Fontello icons set, placed before counter (only for type=counter)", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				)
			) );
			
			class WPBakeryShortCode_Trx_Skills extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Skills_Item extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Slider
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_slider",
				"name" => esc_html__("Slider", 'bestdeals-utils'),
				"description" => esc_attr__("Insert slider", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_slider',
				"class" => "trx_sc_collection trx_sc_slider",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_slider_item'),
				"params" => array_merge(array(
					array(
						"param_name" => "engine",
						"heading" => esc_html__("Engine", 'bestdeals-utils'),
						"description" => esc_attr__("Select engine for slider. Attention! Swiper is built-in engine, all other engines appears only if corresponding plugings are installed", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sliders']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Float slider", 'bestdeals-utils'),
						"description" => esc_attr__("Float slider to left or right side", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom slides", 'bestdeals-utils'),
						"description" => esc_attr__("Make custom slides from inner shortcodes (prepare it on tabs) or prepare slides from posts thumbnails", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Custom slides', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					)
					),
					bestdeals_exists_revslider() ? array(
					array(
						"param_name" => "alias",
						"heading" => esc_html__("Revolution slider alias", 'bestdeals-utils'),
						"description" => esc_attr__("Select Revolution slider to display", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						'dependency' => array(
							'element' => 'engine',
							'value' => array('revo')
						),
						"value" => array_flip(bestdeals_array_merge(array('none' => esc_html__('- Select slider -', 'bestdeals-utils')), $BESTDEALS_GLOBALS['sc_params']['revo_sliders'])),
						"type" => "dropdown"
					)) : array(), array(
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories list", 'bestdeals-utils'),
						"description" => esc_attr__("Select category. If empty - show posts from any category or from IDs list", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip(bestdeals_array_merge(array(0 => esc_html__('- Select category -', 'bestdeals-utils')), $BESTDEALS_GLOBALS['sc_params']['categories'])),
						"type" => "dropdown"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Swiper: Number of posts", 'bestdeals-utils'),
						"description" => esc_attr__("How many posts will be displayed? If used IDs - this parameter ignored.", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Swiper: Offset before select posts", 'bestdeals-utils'),
						"description" => esc_attr__("Skip posts before select next part.", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Swiper: Post sorting", 'bestdeals-utils'),
						"description" => esc_attr__("Select desired posts sorting method", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sorting']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Swiper: Post order", 'bestdeals-utils'),
						"description" => esc_attr__("Select desired posts order", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("Swiper: Post IDs list", 'bestdeals-utils'),
						"description" => esc_attr__("Comma separated list of posts ID. If set - parameters above are ignored!", 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Swiper: Show slider controls", 'bestdeals-utils'),
						"description" => esc_attr__("Show arrows inside slider", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(__('Show controls', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "pagination",
						"heading" => esc_html__("Swiper: Show slider pagination", 'bestdeals-utils'),
						"description" => esc_attr__("Show bullets or titles to switch slides", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"std" => "no",
						"value" => array(
								__('None', 'bestdeals-utils') => 'no',
								__('Dots', 'bestdeals-utils') => 'yes',
								__('Side Titles', 'bestdeals-utils') => 'full',
								__('Over Titles', 'bestdeals-utils') => 'over'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "titles",
						"heading" => esc_html__("Swiper: Show titles section", 'bestdeals-utils'),
						"description" => esc_attr__("Show section with post's title and short post's description", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(
								__('Not show', 'bestdeals-utils') => "no",
								__('Show/Hide info', 'bestdeals-utils') => "slide",
								__('Fixed info', 'bestdeals-utils') => "fixed"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "descriptions",
						"heading" => esc_html__("Swiper: Post descriptions", 'bestdeals-utils'),
						"description" => esc_attr__("Show post's excerpt max length (characters)", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "links",
						"heading" => esc_html__("Swiper: Post's title as link", 'bestdeals-utils'),
						"description" => esc_attr__("Make links from post's titles", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(__('Titles as a links', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "crop",
						"heading" => esc_html__("Swiper: Crop images", 'bestdeals-utils'),
						"description" => esc_attr__("Crop images in each slide or live it unchanged", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(__('Crop images', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "autoheight",
						"heading" => esc_html__("Swiper: Autoheight", 'bestdeals-utils'),
						"description" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(__('Autoheight', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "slides_per_view",
						"heading" => esc_html__("Swiper: Slides per view", 'bestdeals-utils'),
						"description" => esc_attr__("Slides per view showed in this slider", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "1",
						"type" => "textfield"
					),
					array(
						"param_name" => "slides_space",
						"heading" => esc_html__("Swiper: Space between slides", 'bestdeals-utils'),
						"description" => esc_attr__("Size of space (in px) between slides", 'bestdeals-utils'),
						"admin_label" => true,
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "interval",
						"heading" => esc_html__("Swiper: Slides change interval", 'bestdeals-utils'),
						"description" => esc_attr__("Slides change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
						"group" => esc_html__('Details', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "5000",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				))
			) );
			
			
			vc_map( array(
				"base" => "trx_slider_item",
				"name" => esc_html__("Slide", 'bestdeals-utils'),
				"description" => esc_attr__("Slider item - single slide", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_slider_item',
				"as_child" => array('only' => 'trx_slider'),
				"as_parent" => array('except' => 'trx_slider'),
				"params" => array(
					array(
						"param_name" => "src",
						"heading" => esc_html__("URL (source) for image file", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for the current slide", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				)
			) );
			
			class WPBakeryShortCode_Trx_Slider extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Slider_Item extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Socials
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_socials",
				"name" => esc_html__("Social icons", 'bestdeals-utils'),
				"description" => esc_attr__("Custom social icons", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_socials',
				"class" => "trx_sc_collection trx_sc_socials",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_social_item'),
				"params" => array_merge(array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Icon's type", 'bestdeals-utils'),
						"description" => esc_attr__("Type of the icons - images or font icons", 'bestdeals-utils'),
						"class" => "",
						"std" => bestdeals_get_theme_setting('socials_type'),
						"value" => array(
							__('Icons', 'bestdeals-utils') => 'icons',
							__('Images', 'bestdeals-utils') => 'images'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "size",
						"heading" => esc_html__("Icon's size", 'bestdeals-utils'),
						"description" => esc_attr__("Size of the icons", 'bestdeals-utils'),
						"class" => "",
						"std" => "small",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['sizes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "shape",
						"heading" => esc_html__("Icon's shape", 'bestdeals-utils'),
						"description" => esc_attr__("Shape of the icons", 'bestdeals-utils'),
						"class" => "",
						"std" => "square",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['shapes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "socials",
						"heading" => esc_html__("Manual socials list", 'bestdeals-utils'),
						"description" => esc_attr__("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebooc.com/my_profile. If empty - use socials from Theme options.", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom socials", 'bestdeals-utils'),
						"description" => esc_attr__("Make custom icons from inner shortcodes (prepare it on tabs)", 'bestdeals-utils'),
						"class" => "",
						"value" => array(__('Custom socials', 'bestdeals-utils') => 'yes'),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				))
			) );
			
			
			vc_map( array(
				"base" => "trx_social_item",
				"name" => esc_html__("Custom social item", 'bestdeals-utils'),
				"description" => esc_attr__("Custom social item: name, profile url and icon url", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_social_item',
				"as_child" => array('only' => 'trx_socials'),
				"as_parent" => array('except' => 'trx_socials'),
				"params" => array(
					array(
						"param_name" => "name",
						"heading" => esc_html__("Social name", 'bestdeals-utils'),
						"description" => esc_attr__("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("Your profile URL", 'bestdeals-utils'),
						"description" => esc_attr__("URL of your profile in specified social network", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("URL (source) for icon file", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for the current social icon", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					)
				)
			) );
			
			class WPBakeryShortCode_Trx_Socials extends BESTDEALS_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Social_Item extends BESTDEALS_VC_ShortCodeSingle {}
			

			
			
			
			
			
			// Table
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_table",
				"name" => esc_html__("Table", 'bestdeals-utils'),
				"description" => esc_attr__("Insert a table", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_table',
				"class" => "trx_sc_container trx_sc_table",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "align",
						"heading" => esc_html__("Cells content alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select alignment for each table cell", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Table content", 'bestdeals-utils'),
						"description" => esc_attr__("Content, created with any table-generator", 'bestdeals-utils'),
						"class" => "",
						"value" => "Paste here table content, generated on one of many public internet resources, for example: http://www.impressivewebs.com/html-table-code-generator/ or http://html-tables.com/",
						"type" => "textarea_html"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			) );
			
			class WPBakeryShortCode_Trx_Table extends BESTDEALS_VC_ShortCodeContainer {}
			
			
			
			
			
			
			
			// Tabs
			//-------------------------------------------------------------------------------------
			
			$tab_id_1 = 'sc_tab_'.time() . '_1_' . rand( 0, 100 );
			$tab_id_2 = 'sc_tab_'.time() . '_2_' . rand( 0, 100 );
			vc_map( array(
				"base" => "trx_tabs",
				"name" => esc_html__("Tabs", 'bestdeals-utils'),
				"description" => esc_attr__("Tabs", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_tabs',
				"class" => "trx_sc_collection trx_sc_tabs",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_tab'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Tabs style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style of tabs items", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "initial",
						"heading" => esc_html__("Initially opened tab", 'bestdeals-utils'),
						"description" => esc_attr__("Number of initially opened tab", 'bestdeals-utils'),
						"class" => "",
						"value" => 1,
						"type" => "textfield"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Scroller", 'bestdeals-utils'),
						"description" => esc_attr__("Use scroller to show tab content (height parameter required)", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Use scroller" => "yes" ),
						"type" => "checkbox"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_tab title="' . esc_html__( 'Tab 1', 'bestdeals-utils') . '" tab_id="'.esc_attr($tab_id_1).'"][/trx_tab]
					[trx_tab title="' . esc_html__( 'Tab 2', 'bestdeals-utils') . '" tab_id="'.esc_attr($tab_id_2).'"][/trx_tab]
				',
				"custom_markup" => '
					<div class="wpb_tabs_holder wpb_holder vc_container_for_children">
						<ul class="tabs_controls">
						</ul>
						%content%
					</div>
				',
				'js_view' => 'VcTrxTabsView'
			) );
			
			
			vc_map( array(
				"base" => "trx_tab",
				"name" => esc_html__("Tab item", 'bestdeals-utils'),
				"description" => esc_attr__("Single tab item", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"class" => "trx_sc_collection trx_sc_tab",
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_tab',
				"as_child" => array('only' => 'trx_tabs'),
				"as_parent" => array('except' => 'trx_tabs'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Tab title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for current tab", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "tab_id",
						"heading" => esc_html__("Tab ID", 'bestdeals-utils'),
						"description" => esc_attr__("ID for current tab (required). Please, start it from letter.", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
			  'js_view' => 'VcTrxTabView'
			) );
			class WPBakeryShortCode_Trx_Tabs extends BESTDEALS_VC_ShortCodeTabs {}
			class WPBakeryShortCode_Trx_Tab extends BESTDEALS_VC_ShortCodeTab {}
			
			
			
			
			
			
			
			// Title
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_title",
				"name" => esc_html__("Title", 'bestdeals-utils'),
				"description" => esc_attr__("Create header tag (1-6 level) with many styles", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_title',
				"class" => "trx_sc_single trx_sc_title",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "content",
						"heading" => esc_html__("Title content", 'bestdeals-utils'),
						"description" => esc_attr__("Title content", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					array(
						"param_name" => "type",
						"heading" => esc_html__("Title type", 'bestdeals-utils'),
						"description" => esc_attr__("Title type (header level)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('Header 1', 'bestdeals-utils') => '1',
							__('Header 2', 'bestdeals-utils') => '2',
							__('Header 3', 'bestdeals-utils') => '3',
							__('Header 4', 'bestdeals-utils') => '4',
							__('Header 5', 'bestdeals-utils') => '5',
							__('Header 6', 'bestdeals-utils') => '6'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Title style", 'bestdeals-utils'),
						"description" => esc_attr__("Title style: only text (regular) or with icon/image (iconed)", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							__('Regular', 'bestdeals-utils') => 'regular',
							__('Underline', 'bestdeals-utils') => 'underline',
							__('Divider', 'bestdeals-utils') => 'divider',
							__('With icon (image)', 'bestdeals-utils') => 'iconed'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Title text alignment", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", 'bestdeals-utils'),
						"description" => esc_attr__("Custom font size. If empty - use theme default", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", 'bestdeals-utils'),
						"description" => esc_attr__("Custom font weight. If empty or inherit - use theme default", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Default', 'bestdeals-utils') => 'inherit',
							__('Thin (100)', 'bestdeals-utils') => '100',
							__('Light (300)', 'bestdeals-utils') => '300',
							__('Normal (400)', 'bestdeals-utils') => '400',
							__('Semibold (600)', 'bestdeals-utils') => '600',
							__('Bold (700)', 'bestdeals-utils') => '700',
							__('Black (900)', 'bestdeals-utils') => '900'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Title color", 'bestdeals-utils'),
						"description" => esc_attr__("Select color for the title", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Title font icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select font icon for the title from Fontello icons set (if style=iconed)", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Icon &amp; Image', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("or image icon", 'bestdeals-utils'),
						"description" => esc_attr__("Select image icon for the title instead icon above (if style=iconed)", 'bestdeals-utils'),
						"class" => "",
						"group" => esc_html__('Icon &amp; Image', 'bestdeals-utils'),
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $BESTDEALS_GLOBALS['sc_params']['images'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "picture",
						"heading" => esc_html__("or select uploaded image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site (if style=iconed)", 'bestdeals-utils'),
						"group" => esc_html__('Icon &amp; Image', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "image_size",
						"heading" => esc_html__("Image (picture) size", 'bestdeals-utils'),
						"description" => esc_attr__("Select image (picture) size (if style=iconed)", 'bestdeals-utils'),
						"group" => esc_html__('Icon &amp; Image', 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Small', 'bestdeals-utils') => 'small',
							__('Medium', 'bestdeals-utils') => 'medium',
							__('Large', 'bestdeals-utils') => 'large'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "position",
						"heading" => esc_html__("Icon (image) position", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon (image) position (if style=iconed)", 'bestdeals-utils'),
						"group" => esc_html__('Icon &amp; Image', 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('Top', 'bestdeals-utils') => 'top',
							__('Left', 'bestdeals-utils') => 'left'
						),
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Title extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Toggles
			//-------------------------------------------------------------------------------------
				
			vc_map( array(
				"base" => "trx_toggles",
				"name" => esc_html__("Toggles", 'bestdeals-utils'),
				"description" => esc_attr__("Toggles items", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_toggles',
				"class" => "trx_sc_collection trx_sc_toggles",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_toggles_item'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Toggles style", 'bestdeals-utils'),
						"description" => esc_attr__("Select style for display toggles", 'bestdeals-utils'),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(bestdeals_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "counter",
						"heading" => esc_html__("Counter", 'bestdeals-utils'),
						"description" => esc_attr__("Display counter before each toggles title", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Add item numbers before each element" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the closed toggles item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the opened toggles item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_toggles_item title="' . esc_html__( 'Item 1 title', 'bestdeals-utils') . '"][/trx_toggles_item]
					[trx_toggles_item title="' . esc_html__( 'Item 2 title', 'bestdeals-utils') . '"][/trx_toggles_item]
				',
				"custom_markup" => '
					<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
						%content%
					</div>
					<div class="tab_controls">
						<button class="add_tab" title="'.esc_html__("Add item", 'bestdeals-utils').'">'.esc_html__("Add item", 'bestdeals-utils').'</button>
					</div>
				',
				'js_view' => 'VcTrxTogglesView'
			) );
			
			
			vc_map( array(
				"base" => "trx_toggles_item",
				"name" => esc_html__("Toggles item", 'bestdeals-utils'),
				"description" => esc_attr__("Single toggles item", 'bestdeals-utils'),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_toggles_item',
				"as_child" => array('only' => 'trx_toggles'),
				"as_parent" => array('except' => 'trx_toggles'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", 'bestdeals-utils'),
						"description" => esc_attr__("Title for current toggles item", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "open",
						"heading" => esc_html__("Open on show", 'bestdeals-utils'),
						"description" => esc_attr__("Open current toggle item on show", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Opened" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the closed toggles item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", 'bestdeals-utils'),
						"description" => esc_attr__("Select icon for the opened toggles item from Fontello icons set", 'bestdeals-utils'),
						"class" => "",
						"value" => $BESTDEALS_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTogglesTabView'
			) );
			class WPBakeryShortCode_Trx_Toggles extends BESTDEALS_VC_ShortCodeToggles {}
			class WPBakeryShortCode_Trx_Toggles_Item extends BESTDEALS_VC_ShortCodeTogglesItem {}
			
			
			
			
			
			
			// Twitter
			//-------------------------------------------------------------------------------------

			vc_map( array(
				"base" => "trx_twitter",
				"name" => esc_html__("Twitter", 'bestdeals-utils'),
				"description" => esc_attr__("Insert twitter feed into post (page)", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_twitter',
				"class" => "trx_sc_single trx_sc_twitter",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "user",
						"heading" => esc_html__("Twitter Username", 'bestdeals-utils'),
						"description" => esc_attr__("Your username in the twitter account. If empty - get it from Theme Options.", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "consumer_key",
						"heading" => esc_html__("Consumer Key", 'bestdeals-utils'),
						"description" => esc_attr__("Consumer Key from the twitter account", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "consumer_secret",
						"heading" => esc_html__("Consumer Secret", 'bestdeals-utils'),
						"description" => esc_attr__("Consumer Secret from the twitter account", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "token_key",
						"heading" => esc_html__("Token Key", 'bestdeals-utils'),
						"description" => esc_attr__("Token Key from the twitter account", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "token_secret",
						"heading" => esc_html__("Token Secret", 'bestdeals-utils'),
						"description" => esc_attr__("Token Secret from the twitter account", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Tweets number", 'bestdeals-utils'),
						"description" => esc_attr__("Number tweets to show", 'bestdeals-utils'),
						"class" => "",
						"divider" => true,
						"value" => 3,
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Show arrows", 'bestdeals-utils'),
						"description" => esc_attr__("Show control buttons", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['yes_no']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "interval",
						"heading" => esc_html__("Tweets change interval", 'bestdeals-utils'),
						"description" => esc_attr__("Tweets change interval (in milliseconds: 1000ms = 1s)", 'bestdeals-utils'),
						"class" => "",
						"value" => "7000",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Alignment of the tweets block", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "autoheight",
						"heading" => esc_html__("Autoheight", 'bestdeals-utils'),
						"description" => esc_attr__("Change whole slider's height (make it equal current slide's height)", 'bestdeals-utils'),
						"class" => "",
						"value" => array("Autoheight" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", 'bestdeals-utils'),
						"description" => esc_attr__("Select color scheme for this block", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", 'bestdeals-utils'),
						"description" => esc_attr__("Any background color for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", 'bestdeals-utils'),
						"description" => esc_attr__("Select background image from library for this section", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", 'bestdeals-utils'),
						"description" => esc_attr__("Overlay color opacity (from 0.0 to 1.0)", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", 'bestdeals-utils'),
						"description" => esc_attr__("Texture style from 1 to 11. Empty or 0 - without texture.", 'bestdeals-utils'),
						"group" => esc_html__('Colors and Images', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Twitter extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Video
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_video",
				"name" => esc_html__("Video", 'bestdeals-utils'),
				"description" => esc_attr__("Insert video player", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_video',
				"class" => "trx_sc_single trx_sc_video",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("URL for video file", 'bestdeals-utils'),
						"description" => esc_attr__("Paste URL for video file", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "ratio",
						"heading" => esc_html__("Ratio", 'bestdeals-utils'),
						"description" => esc_attr__("Select ratio for display video", 'bestdeals-utils'),
						"class" => "",
						"value" => array(
							__('16:9', 'bestdeals-utils') => "16:9",
							__('4:3', 'bestdeals-utils') => "4:3"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "autoplay",
						"heading" => esc_html__("Autoplay video", 'bestdeals-utils'),
						"description" => esc_attr__("Autoplay video on page load", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array("Autoplay" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Select block alignment", 'bestdeals-utils'),
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Cover image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for video preview", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for video background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_top",
						"heading" => esc_html__("Top offset", 'bestdeals-utils'),
						"description" => esc_attr__("Top offset (padding) from background image to video block (in percent). For example: 3%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_bottom",
						"heading" => esc_html__("Bottom offset", 'bestdeals-utils'),
						"description" => esc_attr__("Bottom offset (padding) from background image to video block (in percent). For example: 3%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_left",
						"heading" => esc_html__("Left offset", 'bestdeals-utils'),
						"description" => esc_attr__("Left offset (padding) from background image to video block (in percent). For example: 20%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_right",
						"heading" => esc_html__("Right offset", 'bestdeals-utils'),
						"description" => esc_attr__("Right offset (padding) from background image to video block (in percent). For example: 12%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Video extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Zoom
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_zoom",
				"name" => esc_html__("Zoom", 'bestdeals-utils'),
				"description" => esc_attr__("Insert the image with zoom/lens effect", 'bestdeals-utils'),
				"category" => esc_html__('Content', 'bestdeals-utils'),
				'icon' => 'icon_trx_zoom',
				"class" => "trx_sc_single trx_sc_zoom",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "effect",
						"heading" => esc_html__("Effect", 'bestdeals-utils'),
						"description" => esc_attr__("Select effect to display overlapping image", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"std" => "zoom",
						"value" => array(
							__('Lens', 'bestdeals-utils') => 'lens',
							__('Zoom', 'bestdeals-utils') => 'zoom'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("Main image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload main image", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "over",
						"heading" => esc_html__("Overlaping image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload overlaping image", 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", 'bestdeals-utils'),
						"description" => esc_attr__("Float zoom to left or right side", 'bestdeals-utils'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", 'bestdeals-utils'),
						"description" => esc_attr__("Select or upload image or write URL from other site for zoom background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_top",
						"heading" => esc_html__("Top offset", 'bestdeals-utils'),
						"description" => esc_attr__("Top offset (padding) from background image to zoom block (in percent). For example: 3%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_bottom",
						"heading" => esc_html__("Bottom offset", 'bestdeals-utils'),
						"description" => esc_attr__("Bottom offset (padding) from background image to zoom block (in percent). For example: 3%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_left",
						"heading" => esc_html__("Left offset", 'bestdeals-utils'),
						"description" => esc_attr__("Left offset (padding) from background image to zoom block (in percent). For example: 20%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_right",
						"heading" => esc_html__("Right offset", 'bestdeals-utils'),
						"description" => esc_attr__("Right offset (padding) from background image to zoom block (in percent). For example: 12%", 'bestdeals-utils'),
						"group" => esc_html__('Background', 'bestdeals-utils'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$BESTDEALS_GLOBALS['vc_params']['id'],
					$BESTDEALS_GLOBALS['vc_params']['class'],
					$BESTDEALS_GLOBALS['vc_params']['animation'],
					$BESTDEALS_GLOBALS['vc_params']['css'],
					bestdeals_vc_width(),
					bestdeals_vc_height(),
					$BESTDEALS_GLOBALS['vc_params']['margin_top'],
					$BESTDEALS_GLOBALS['vc_params']['margin_bottom'],
					$BESTDEALS_GLOBALS['vc_params']['margin_left'],
					$BESTDEALS_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Zoom extends BESTDEALS_VC_ShortCodeSingle {}
			

			do_action('bestdeals_action_shortcodes_list_vc');
			
			
			if (false && bestdeals_exists_woocommerce()) {
			
				// WooCommerce - Cart
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_cart",
					"name" => esc_html__("Cart", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show cart page", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_wooc_cart',
					"class" => "trx_sc_alone trx_sc_woocommerce_cart",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", 'bestdeals-utils'),
							"description" => esc_attr__("Dummy data - not used in shortcodes", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Cart extends BESTDEALS_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Checkout
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_checkout",
					"name" => esc_html__("Checkout", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show checkout page", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_wooc_checkout',
					"class" => "trx_sc_alone trx_sc_woocommerce_checkout",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", 'bestdeals-utils'),
							"description" => esc_attr__("Dummy data - not used in shortcodes", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Checkout extends BESTDEALS_VC_ShortCodeAlone {}
			
			
				// WooCommerce - My Account
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_my_account",
					"name" => esc_html__("My Account", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show my account page", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_wooc_my_account',
					"class" => "trx_sc_alone trx_sc_woocommerce_my_account",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", 'bestdeals-utils'),
							"description" => esc_attr__("Dummy data - not used in shortcodes", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_My_Account extends BESTDEALS_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Order Tracking
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_order_tracking",
					"name" => esc_html__("Order Tracking", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show order tracking page", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_wooc_order_tracking',
					"class" => "trx_sc_alone trx_sc_woocommerce_order_tracking",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", 'bestdeals-utils'),
							"description" => esc_attr__("Dummy data - not used in shortcodes", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Order_Tracking extends BESTDEALS_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Shop Messages
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "shop_messages",
					"name" => esc_html__("Shop Messages", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show shop messages", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_wooc_shop_messages',
					"class" => "trx_sc_alone trx_sc_shop_messages",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", 'bestdeals-utils'),
							"description" => esc_attr__("Dummy data - not used in shortcodes", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Shop_Messages extends BESTDEALS_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Product Page
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_page",
					"name" => esc_html__("Product Page", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: display single product page", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_product_page',
					"class" => "trx_sc_single trx_sc_product_page",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", 'bestdeals-utils'),
							"description" => esc_attr__("SKU code of displayed product", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", 'bestdeals-utils'),
							"description" => esc_attr__("ID of displayed product", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "posts_per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "post_type",
							"heading" => esc_html__("Post type", 'bestdeals-utils'),
							"description" => esc_attr__("Post type for the WP query (leave 'product')", 'bestdeals-utils'),
							"class" => "",
							"value" => "product",
							"type" => "textfield"
						),
						array(
							"param_name" => "post_status",
							"heading" => esc_html__("Post status", 'bestdeals-utils'),
							"description" => esc_attr__("Display posts only with this status", 'bestdeals-utils'),
							"class" => "",
							"value" => array(
								__('Publish', 'bestdeals-utils') => 'publish',
								__('Protected', 'bestdeals-utils') => 'protected',
								__('Private', 'bestdeals-utils') => 'private',
								__('Pending', 'bestdeals-utils') => 'pending',
								__('Draft', 'bestdeals-utils') => 'draft'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Page extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Product
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product",
					"name" => esc_html__("Product", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: display one product", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_product',
					"class" => "trx_sc_single trx_sc_product",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", 'bestdeals-utils'),
							"description" => esc_attr__("Product's SKU code", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", 'bestdeals-utils'),
							"description" => esc_attr__("Product's ID", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Product extends BESTDEALS_VC_ShortCodeSingle {}
			
			
				// WooCommerce - Best Selling Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "best_selling_products",
					"name" => esc_html__("Best Selling Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show best selling products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_best_selling_products',
					"class" => "trx_sc_single trx_sc_best_selling_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Best_Selling_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Recent Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "recent_products",
					"name" => esc_html__("Recent Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show recent products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_recent_products',
					"class" => "trx_sc_single trx_sc_recent_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Recent_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Related Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "related_products",
					"name" => esc_html__("Related Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show related products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_related_products',
					"class" => "trx_sc_single trx_sc_related_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "posts_per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Related_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Featured Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "featured_products",
					"name" => esc_html__("Featured Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show featured products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_featured_products',
					"class" => "trx_sc_single trx_sc_featured_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Featured_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Top Rated Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "top_rated_products",
					"name" => esc_html__("Top Rated Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show top rated products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_top_rated_products',
					"class" => "trx_sc_single trx_sc_top_rated_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Top_Rated_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Sale Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "sale_products",
					"name" => esc_html__("Sale Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: list products on sale", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_sale_products',
					"class" => "trx_sc_single trx_sc_sale_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Sale_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Product Category
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_category",
					"name" => esc_html__("Products from category", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: list products in specified category(-ies)", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_product_category',
					"class" => "trx_sc_single trx_sc_product_category",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "category",
							"heading" => esc_html__("Categories", 'bestdeals-utils'),
							"description" => esc_attr__("Comma separated category slugs", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "operator",
							"heading" => esc_html__("Operator", 'bestdeals-utils'),
							"description" => esc_attr__("Categories operator", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('IN', 'bestdeals-utils') => 'IN',
								__('NOT IN', 'bestdeals-utils') => 'NOT IN',
								__('AND', 'bestdeals-utils') => 'AND'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Category extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "products",
					"name" => esc_html__("Products", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: list all products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_products',
					"class" => "trx_sc_single trx_sc_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "skus",
							"heading" => esc_html__("SKUs", 'bestdeals-utils'),
							"description" => esc_attr__("Comma separated SKU codes of products", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "ids",
							"heading" => esc_html__("IDs", 'bestdeals-utils'),
							"description" => esc_attr__("Comma separated ID of products", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Products extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
			
				// WooCommerce - Product Attribute
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_attribute",
					"name" => esc_html__("Products by Attribute", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show products with specified attribute", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_product_attribute',
					"class" => "trx_sc_single trx_sc_product_attribute",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many products showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "attribute",
							"heading" => esc_html__("Attribute", 'bestdeals-utils'),
							"description" => esc_attr__("Attribute name", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "filter",
							"heading" => esc_html__("Filter", 'bestdeals-utils'),
							"description" => esc_attr__("Attribute value", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Attribute extends BESTDEALS_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Products Categories
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_categories",
					"name" => esc_html__("Product Categories", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: show categories with products", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_product_categories',
					"class" => "trx_sc_single trx_sc_product_categories",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "number",
							"heading" => esc_html__("Number", 'bestdeals-utils'),
							"description" => esc_attr__("How many categories showed", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", 'bestdeals-utils'),
							"description" => esc_attr__("How many columns per row use for categories output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								__('Date', 'bestdeals-utils') => 'date',
								__('Title', 'bestdeals-utils') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", 'bestdeals-utils'),
							"description" => esc_attr__("Sorting order for products output", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($BESTDEALS_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "parent",
							"heading" => esc_html__("Parent", 'bestdeals-utils'),
							"description" => esc_attr__("Parent category slug", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "date",
							"type" => "textfield"
						),
						array(
							"param_name" => "ids",
							"heading" => esc_html__("IDs", 'bestdeals-utils'),
							"description" => esc_attr__("Comma separated ID of products", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "hide_empty",
							"heading" => esc_html__("Hide empty", 'bestdeals-utils'),
							"description" => esc_attr__("Hide empty categories", 'bestdeals-utils'),
							"class" => "",
							"value" => array("Hide empty" => "1" ),
							"type" => "checkbox"
						)
					)
				) );
				
				class WPBakeryShortCode_Products_Categories extends BESTDEALS_VC_ShortCodeSingle {}
			
				/*
			
				// WooCommerce - Add to cart
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "add_to_cart",
					"name" => esc_html__("Add to cart", 'bestdeals-utils'),
					"description" => esc_attr__("WooCommerce shortcode: Display a single product price + cart button", 'bestdeals-utils'),
					"category" => esc_html__('WooCommerce', 'bestdeals-utils'),
					'icon' => 'icon_trx_add_to_cart',
					"class" => "trx_sc_single trx_sc_add_to_cart",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", 'bestdeals-utils'),
							"description" => esc_attr__("Product's ID", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", 'bestdeals-utils'),
							"description" => esc_attr__("Product's SKU code", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "quantity",
							"heading" => esc_html__("Quantity", 'bestdeals-utils'),
							"description" => esc_attr__("How many item add", 'bestdeals-utils'),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "show_price",
							"heading" => esc_html__("Show price", 'bestdeals-utils'),
							"description" => esc_attr__("Show price near button", 'bestdeals-utils'),
							"class" => "",
							"value" => array("Show price" => "true" ),
							"type" => "checkbox"
						),
						array(
							"param_name" => "class",
							"heading" => esc_html__("Class", 'bestdeals-utils'),
							"description" => esc_attr__("CSS class", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "style",
							"heading" => esc_html__("CSS style", 'bestdeals-utils'),
							"description" => esc_attr__("CSS style for additional decoration", 'bestdeals-utils'),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Add_To_Cart extends BESTDEALS_VC_ShortCodeSingle {}
				*/
			}

		}
	}
}
?>