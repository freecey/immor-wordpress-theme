<?php

// 
// Customization shorcodes
// 

// ---------------------------------- [trx_accordion] ---------------------------------------


//  Enqueue Charts and Diagrams scripts and styles
if ( !function_exists( 'bestdeals_enqueue_diagram' ) ) {
	function bestdeals_enqueue_diagram($type='all') {
		if ($type=='all' || $type=='pie') wp_enqueue_script( 'bestdeals-diagram-chart-script',	bestdeals_get_file_url('js/diagram/chart.min.js'), array(), null, true );
		if ($type=='all' || $type=='arc') wp_enqueue_script( 'bestdeals-diagram-raphael-script',	bestdeals_get_file_url('js/diagram/diagram.raphael.min.js'), array(), null, true );
	}
}

/*
[trx_accordion style="1" counter="off" initial="1"]
	[trx_accordion_item title="Accordion Title 1"]Lorem ipsum dolor sit amet, consectetur adipisicing elit[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 2"]Proin dignissim commodo magna at luctus. Nam molestie justo augue, nec eleifend urna laoreet non.[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 3 with custom icons" icon_closed="icon-check" icon_opened="icon-delete"]Curabitur tristique tempus arcu a placerat.[/trx_accordion_item]
[/trx_accordion]
*/
if (!function_exists('bestdeals_sc_accordion')) {	
	function bestdeals_sc_accordion($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"initial" => "1",
			"counter" => "off",
			"icon_closed" => "icon-plus",
			"icon_opened" => "icon-minus",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
		$style = max(1, min(2, $style));
		$initial = max(0, (int) $initial);
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_accordion_style_best'] = $style;
		$BESTDEALS_GLOBALS['sc_accordion_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_accordion_show_counter'] = bestdeals_param_is_on($counter);
		$BESTDEALS_GLOBALS['sc_accordion_icon_closed'] = empty($icon_closed) || bestdeals_param_is_inherit($icon_closed) ? "icon-plus" : $icon_closed;
		$BESTDEALS_GLOBALS['sc_accordion_icon_opened'] = empty($icon_opened) || bestdeals_param_is_inherit($icon_opened) ? "icon-minus" : $icon_opened;
		wp_enqueue_script('jquery-ui-accordion', false, array('jquery','jquery-ui-core'), null, true);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_accordion sc_accordion_style_'.esc_attr($style)
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. (bestdeals_param_is_on($counter) ? ' sc_show_counter' : '') 
				. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. ' data-active="' . ($initial-1) . '"'
				. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
				. '>'
				. do_shortcode($content)
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_accordion', $atts, $content);
	}
	add_shortcode('trx_accordion', 'bestdeals_sc_accordion');
}


if (!function_exists('bestdeals_sc_accordion_item')) {	
	function bestdeals_sc_accordion_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"icon_closed" => "",
			"icon_opened" => "",
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_accordion_counter']++;
		if (empty($icon_closed) || bestdeals_param_is_inherit($icon_closed)) $icon_closed = $BESTDEALS_GLOBALS['sc_accordion_icon_closed'] ? $BESTDEALS_GLOBALS['sc_accordion_icon_closed'] : "icon-plus";
		if (empty($icon_opened) || bestdeals_param_is_inherit($icon_opened)) $icon_opened = $BESTDEALS_GLOBALS['sc_accordion_icon_opened'] ? $BESTDEALS_GLOBALS['sc_accordion_icon_opened'] : "icon-minus";
		
		if ( $BESTDEALS_GLOBALS['sc_accordion_style_best'] == 1 ) {
		
			$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_accordion_item' 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($BESTDEALS_GLOBALS['sc_accordion_counter'] % 2 == 1 ? ' odd' : ' even') 
					. ($BESTDEALS_GLOBALS['sc_accordion_counter'] == 1 ? ' first' : '') 
					. '">'
					. '<h4 class="sc_accordion_title">'
					. (!bestdeals_param_is_off($icon_closed) ? '<span class="sc_accordion_icon sc_accordion_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
					. (!bestdeals_param_is_off($icon_opened) ? '<span class="sc_accordion_icon sc_accordion_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
					. ($BESTDEALS_GLOBALS['sc_accordion_show_counter'] ? '<span class="sc_items_counter">'.($BESTDEALS_GLOBALS['sc_accordion_counter']).'</span>' : '')
					. ($title)
					. '<span class="sc_accordion_icon_a">A</span>'
					. '</h4>'
					. '<div class="sc_accordion_content"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. '>'
						. do_shortcode($content) 
					. '</div>'
					. '</div>';
		} else {
			
			$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_accordion_item' 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($BESTDEALS_GLOBALS['sc_accordion_counter'] % 2 == 1 ? ' odd' : ' even') 
					. ($BESTDEALS_GLOBALS['sc_accordion_counter'] == 1 ? ' first' : '') 
					. '">'
					. '<h5 class="sc_accordion_title">'
					. (!bestdeals_param_is_off($icon_closed) ? '<span class="sc_accordion_icon sc_accordion_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
					. (!bestdeals_param_is_off($icon_opened) ? '<span class="sc_accordion_icon sc_accordion_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
					. ($BESTDEALS_GLOBALS['sc_accordion_show_counter'] ? '<span class="sc_items_counter">'.($BESTDEALS_GLOBALS['sc_accordion_counter']).'</span>' : '')
					. ($title)
					. '</h5>'
					. '<div class="sc_accordion_content"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. '>'
						. do_shortcode($content) 
					. '</div>'
					. '</div>';
			
		}
		
		
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_accordion_item', $atts, $content);
	}
	add_shortcode('trx_accordion_item', 'bestdeals_sc_accordion_item');
}
// ---------------------------------- [/trx_accordion] ---------------------------------------



// ---------------------------------- [trx_tabs] ---------------------------------------

/*
[trx_tabs id="unique_id" tab_names="Planning|Development|Support" style="1|2" initial="1 - num_tabs"]
	[trx_tab]Randomised words which don't look even slightly believable. If you are going to use a passage. You need to be sure there isn't anything embarrassing hidden in the middle of text established fact that a reader will be istracted by the readable content of a page when looking at its layout.[/trx_tab]
	[trx_tab]Fact reader will be distracted by the <a href="#" class="main_link">readable content</a> of a page when. Looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here, content here, making it look like readable English will uncover many web sites still in their infancy. Various versions have evolved over. There are many variations of passages of Lorem Ipsum available, but the majority.[/trx_tab]
	[trx_tab]Distracted by the  readable content  of a page when. Looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here, content here, making it look like readable English will uncover many web sites still in their infancy. Various versions have  evolved over.  There are many variations of passages of Lorem Ipsum available.[/trx_tab]
[/trx_tabs]
*/

if (!function_exists('bestdeals_sc_tabs')) {	
	function bestdeals_sc_tabs($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"initial" => "1",
			"scroll" => "no",
			"style" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width);
	
		if (!bestdeals_param_is_off($scroll)) bestdeals_enqueue_slider();
		if (empty($id)) $id = 'sc_tabs_'.str_replace('.', '', mt_rand());
	
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_tab_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_tab_scroll'] = $scroll;
		$BESTDEALS_GLOBALS['sc_tab_height'] = bestdeals_prepare_css_value($height);
		$BESTDEALS_GLOBALS['sc_tab_id']     = $id;
		$BESTDEALS_GLOBALS['sc_tab_titles'] = array();
	
		$content = do_shortcode($content);
	
		$sc_tab_titles = $BESTDEALS_GLOBALS['sc_tab_titles'];
	
		$initial = max(1, min(count($sc_tab_titles), (int) $initial));
	
		$tabs_output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
							. ' class="sc_tabs sc_tabs_style_'.esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
							. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
							. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
							. ' data-active="' . ($initial-1) . '"'
							. '>'
						.'<ul class="sc_tabs_titles">';
		$titles_output = '';
		for ($i = 0; $i < count($sc_tab_titles); $i++) {
			$classes = array('sc_tabs_title');
			if ($i == 0) $classes[] = 'first';
			else if ($i == count($sc_tab_titles) - 1) $classes[] = 'last';
			$titles_output .= '<li class="'.join(' ', $classes).'">'
								. '<a href="#'.esc_attr($sc_tab_titles[$i]['id']).'" class="theme_button" id="'.esc_attr($sc_tab_titles[$i]['id']).'_tab"><span>' . ($sc_tab_titles[$i]['title']) . '</span></a>'
								. '</li>';
		}
	
		wp_enqueue_script('jquery-ui-tabs');
		wp_enqueue_script('jquery-effects-fade');
	
		$tabs_output .= $titles_output
			. '</ul><div class="sc_tabs_title_box">' 
			. ($content)
			.'</div></div>';
		return apply_filters('bestdeals_shortcode_output', $tabs_output, 'trx_tabs', $atts, $content);
	}
	add_shortcode("trx_tabs", "bestdeals_sc_tabs");
}


if (!function_exists('bestdeals_sc_tab')) {	
	function bestdeals_sc_tab($atts, $content = null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"tab_id" => "",		// get it from VC
			"title" => "",		// get it from VC
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_tab_counter']++;
		if (empty($id))
			$id = !empty($tab_id) ? $tab_id : ($BESTDEALS_GLOBALS['sc_tab_id']).'_'.($BESTDEALS_GLOBALS['sc_tab_counter']);
		$sc_tab_titles = $BESTDEALS_GLOBALS['sc_tab_titles'];
		if (isset($sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1])) {
			$sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1]['id'] = $id;
			if (!empty($title))
				$sc_tab_titles[$BESTDEALS_GLOBALS['sc_tab_counter']-1]['title'] = $title;
		} else {
			$sc_tab_titles[] = array(
				'id' => $id,
				'title' => $title
			);
		}
		$BESTDEALS_GLOBALS['sc_tab_titles'] = $sc_tab_titles;
		$output = '<div id="'.esc_attr($id).'"'
					.' class="sc_tabs_content' 
						. ($BESTDEALS_GLOBALS['sc_tab_counter'] % 2 == 1 ? ' odd' : ' even') 
						. ($BESTDEALS_GLOBALS['sc_tab_counter'] == 1 ? ' first' : '') 
						. (!empty($class) ? ' '.esc_attr($class) : '') 
						. '"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
						. '>' 
				. (bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_tab_scroll']) 
					? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_vertical" style="height:'.($BESTDEALS_GLOBALS['sc_tab_height'] != '' ? $BESTDEALS_GLOBALS['sc_tab_height'] : '200px').';"><div class="sc_scroll_wrapper swiper-wrapper"><div class="sc_scroll_slide swiper-slide">' 
					: '')
				. do_shortcode($content) 
				. (bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_tab_scroll']) 
					? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_vertical '.esc_attr($id).'_scroll_bar"></div></div>' 
					: '')
			. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_tab', $atts, $content);
	}
	add_shortcode("trx_tab", "bestdeals_sc_tab");
}
// ---------------------------------- [/trx_tabs] ---------------------------------------



// ---------------------------------- [trx_price] ---------------------------------------

/*
[trx_price id="unique_id" currency="$" money="29.99" period="monthly"]
*/

if (!function_exists('bestdeals_sc_price')) {	
	function bestdeals_sc_price($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		if (!empty($money)) {
			$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left);
			$m = explode('.', str_replace(',', '.', $money));
			if (!empty($m[1])) {$m[0]=$m[0].'.';}
			$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price'
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. '>'
				. '<span class="sc_price_currency">'.($currency).'</span>'
				. '<span class="sc_price_money">'.($m[0]).'</span>'
				. (!empty($m[1]) ? '<span class="sc_price_info">' : '')
				. (!empty($m[1]) ? '<span class="sc_price_penny">'.($m[1]).'</span>' : '')
				. (!empty($period) ? '<span class="sc_price_period">'.($period).'</span>' : (!empty($m[1]) ? '<span class="sc_price_period_empty"></span>' : ''))
				. (!empty($m[1]) ? '</span>' : '')
				. '</div>';
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_price', $atts, $content);
	}
	add_shortcode('trx_price', 'bestdeals_sc_price');
}
// ---------------------------------- [/trx_price] ---------------------------------------



// ---------------------------------- [trx_skills] ---------------------------------------

/*
[trx_skills id="unique_id" type="bar|pie|arc|counter" dir="horizontal|vertical" layout="rows|columns" count="" max_value="100" align="left|right"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
	[trx_skills_item title="Scelerisque pid" value="50%"]
[/trx_skills]
*/

if (!function_exists('bestdeals_sc_skills')) {	
	function bestdeals_sc_skills($atts, $content=null){	
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts(array(
			// Individual params
			"max_value" => "100",
			"type" => "bar",
			"layout" => "",
			"dir" => "",
			"style" => "1",
			"columns" => "",
			"align" => "",
			"color" => "",
			"bg_color" => "",
			"border_color" => "",
			"arc_caption" => esc_html__("Skills", 'bestdeals-utils'),
			"pie_compact" => "off",
			"pie_cutout" => 0,
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'bestdeals-utils'),
			"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_skills_counter'] = 0;
		$BESTDEALS_GLOBALS['sc_skills_columns'] = 0;
		$BESTDEALS_GLOBALS['sc_skills_height']  = 0;
		$BESTDEALS_GLOBALS['sc_skills_type']    = $type;
		$BESTDEALS_GLOBALS['sc_skills_pie_compact'] = $pie_compact;
		$BESTDEALS_GLOBALS['sc_skills_pie_cutout']  = max(0, min(99, $pie_cutout));
		$BESTDEALS_GLOBALS['sc_skills_color']   = $color;
		$BESTDEALS_GLOBALS['sc_skills_bg_color']= $bg_color;
		$BESTDEALS_GLOBALS['sc_skills_border_color']= $border_color;
		$BESTDEALS_GLOBALS['sc_skills_legend']  = '';
		$BESTDEALS_GLOBALS['sc_skills_data']    = '';
		bestdeals_enqueue_diagram($type);
		if ($type!='arc') {
			if ($layout=='' || ($layout=='columns' && $columns<1)) $layout = 'rows';
			if ($layout=='columns') $BESTDEALS_GLOBALS['sc_skills_columns'] = $columns;
			if ($type=='bar') {
				if ($dir == '') $dir = 'horizontal';
				if ($dir == 'vertical' && $height < 1) $height = 300;
			}
		}
		if (empty($id)) $id = 'sc_skills_diagram_'.str_replace('.','',mt_rand());
		if ($max_value < 1) $max_value = 100;
		if ($style) {
			$style = max(1, min(4, $style));
			$BESTDEALS_GLOBALS['sc_skills_style'] = $style;
		}
		$BESTDEALS_GLOBALS['sc_skills_max'] = $max_value;
		$BESTDEALS_GLOBALS['sc_skills_dir'] = $dir;
		$BESTDEALS_GLOBALS['sc_skills_height'] = bestdeals_prepare_css_value($height);
		$css .= bestdeals_get_css_position_from_values($top, $right, $bottom, $left, $width, $height);
		$content = do_shortcode($content);
		$output = '<div id="'.esc_attr($id).'"' 
					. ' class="sc_skills sc_skills_' . esc_attr($type) 
						. ($type=='bar' ? ' sc_skills_'.esc_attr($dir) : '') 
						. ($type=='pie' ? ' sc_skills_compact_'.esc_attr($pie_compact) : '') 
						. (!empty($class) ? ' '.esc_attr($class) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!bestdeals_param_is_off($animation) ? ' data-animation="'.esc_attr(bestdeals_get_animation_classes($animation)).'"' : '')
					. ' data-type="'.esc_attr($type).'"'
					. ' data-caption="'.esc_attr($arc_caption).'"'
					. ($type=='bar' ? ' data-dir="'.esc_attr($dir).'"' : '')
				. '>'
					. (!empty($subtitle) ? '<h6 class="sc_skills_subtitle sc_item_subtitle">' . esc_html($subtitle) . '</h6>' : '')
					. (!empty($title) ? '<h2 class="sc_skills_title sc_item_title">' . esc_html($title) . '</h2>' : '')
					. (!empty($description) ? '<div class="sc_skills_descr sc_item_descr">' . trim($description) . '</div>' : '')
					. ($layout == 'columns' ? '<div class="columns_wrap sc_skills_'.esc_attr($layout).' sc_skills_columns_'.esc_attr($columns).'">' : '')
					. ($type=='arc' 
						? ('<div class="sc_skills_legend">'.($BESTDEALS_GLOBALS['sc_skills_legend']).'</div>'
							. '<div id="'.esc_attr($id).'_diagram" class="sc_skills_arc_canvas"></div>'
							. '<div class="sc_skills_data" style="display:none;">' . ($BESTDEALS_GLOBALS['sc_skills_data']) . '</div>'
						  )
						: '')
					. ($type=='pie' && bestdeals_param_is_on($pie_compact)
						? ('<div class="sc_skills_legend">'.($BESTDEALS_GLOBALS['sc_skills_legend']).'</div>'
							. '<div id="'.esc_attr($id).'_pie" class="sc_skills_item">'
								. '<canvas id="'.esc_attr($id).'_pie" class="sc_skills_pie_canvas"></canvas>'
								. '<div class="sc_skills_data" style="display:none;">' . ($BESTDEALS_GLOBALS['sc_skills_data']) . '</div>'
							. '</div>'
						  )
						: '')
					. ($content)
					. ($layout == 'columns' ? '</div>' : '')
					. (!empty($link) ? '<div class="sc_skills_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
				. '</div>';
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_skills', $atts, $content);
	}
	add_shortcode('trx_skills', 'bestdeals_sc_skills');
}


if (!function_exists('bestdeals_sc_skills_item')) {	
	function bestdeals_sc_skills_item($atts, $content=null) {
		if (bestdeals_in_shortcode_blogger()) return '';
		extract(bestdeals_html_decode(shortcode_atts( array(
			// Individual params
			"title" => "",
			"value" => "",
			"color" => "",
			"bg_color" => "",
			"border_color" => "",
			"style" => "",
			"icon" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		global $BESTDEALS_GLOBALS;
		$BESTDEALS_GLOBALS['sc_skills_counter']++;
		$ed = bestdeals_substr($value, -1)=='%' ? '%' : '';
		$value = str_replace('%', '', $value);
		if ($BESTDEALS_GLOBALS['sc_skills_max'] < $value) $BESTDEALS_GLOBALS['sc_skills_max'] = $value;
		$percent = round($value / $BESTDEALS_GLOBALS['sc_skills_max'] * 100);
		$start = 0;
		$stop = $value;
		$steps = 100;
		$step = max(1, round($BESTDEALS_GLOBALS['sc_skills_max']/$steps));
		$speed = mt_rand(10,40);
		$animation = round(($stop - $start) / $step * $speed);
		$title_block = '<div class="sc_skills_info"><div class="sc_skills_label">' . ($title) . '</div></div>';
		$old_color = $color;
		if (empty($color)) $color = $BESTDEALS_GLOBALS['sc_skills_color'];
		if (empty($color)) $color = bestdeals_get_scheme_color('accent1', $color);
		if (empty($bg_color)) $bg_color = $BESTDEALS_GLOBALS['sc_skills_bg_color'];
		if (empty($bg_color)) $bg_color = bestdeals_get_scheme_color('bg_color', $bg_color);
		if (empty($border_color)) $border_color = $BESTDEALS_GLOBALS['sc_skills_border_color'];
		if (empty($border_color)) $border_color = bestdeals_get_scheme_color('bd_color', $border_color);;
		if (empty($style)) $style = $BESTDEALS_GLOBALS['sc_skills_style'];
		$style = max(1, min(4, $style));
		$output = '';
		if ($BESTDEALS_GLOBALS['sc_skills_type'] == 'arc' || ($BESTDEALS_GLOBALS['sc_skills_type'] == 'pie' && bestdeals_param_is_on($BESTDEALS_GLOBALS['sc_skills_pie_compact']))) {
			if ($BESTDEALS_GLOBALS['sc_skills_type'] == 'arc' && empty($old_color)) {
				$rgb = bestdeals_hex2rgb($color);
				$color = 'rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.(1 - 0.1*($BESTDEALS_GLOBALS['sc_skills_counter']-1)).')';
			}
			$BESTDEALS_GLOBALS['sc_skills_legend'] .= '<div class="sc_skills_legend_item"><span class="sc_skills_legend_marker" style="background-color:'.esc_attr($color).'"></span><span class="sc_skills_legend_title">' . ($title) . '</span><span class="sc_skills_legend_value">' . ($value) . '</span></div>';
			$BESTDEALS_GLOBALS['sc_skills_data'] .= '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_type']).'"'
				. ($BESTDEALS_GLOBALS['sc_skills_type']=='pie'
					? ( ' data-start="'.esc_attr($start).'"'
						. ' data-stop="'.esc_attr($stop).'"'
						. ' data-step="'.esc_attr($step).'"'
						. ' data-steps="'.esc_attr($steps).'"'
						. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
						. ' data-speed="'.esc_attr($speed).'"'
						. ' data-duration="'.esc_attr($animation).'"'
						. ' data-color="'.esc_attr($color).'"'
						. ' data-bg_color="'.esc_attr($bg_color).'"'
						. ' data-border_color="'.esc_attr($border_color).'"'
						. ' data-cutout="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_pie_cutout']).'"'
						. ' data-easing="easeOutCirc"'
						. ' data-ed="'.esc_attr($ed).'"'
						)
					: '')
				. '><input type="hidden" class="text" value="'.esc_attr($title).'" /><input type="hidden" class="percent" value="'.esc_attr($percent).'" /><input type="hidden" class="color" value="'.esc_attr($color).'" /></div>';
		} else {
			$output .= ($BESTDEALS_GLOBALS['sc_skills_columns'] > 0 ? '<div class="sc_skills_column column-1_'.esc_attr($BESTDEALS_GLOBALS['sc_skills_columns']).'">' : '')
					. ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $BESTDEALS_GLOBALS['sc_skills_dir']=='horizontal' ? $title_block : '')
					. '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_skills_item' . ($style ? ' sc_skills_style_'.esc_attr($style) : '') 
							. (!empty($class) ? ' '.esc_attr($class) : '')
							. ($BESTDEALS_GLOBALS['sc_skills_counter'] % 2 == 1 ? ' odd' : ' even') 
							. ($BESTDEALS_GLOBALS['sc_skills_counter'] == 1 ? ' first' : '') 
							. '"'
						. ($BESTDEALS_GLOBALS['sc_skills_height'] !='' || $css ? ' style="height: '.esc_attr($BESTDEALS_GLOBALS['sc_skills_height']).';'.($css).'"' : '')
					. '>'
					. (!empty($icon) ? '<div class="sc_skills_icon '.esc_attr($icon).'"></div>' : '');
			if (in_array($BESTDEALS_GLOBALS['sc_skills_type'], array('bar', 'counter'))) {
				
				$output .= '<div class="sc_skills_total"'
								. ' data-start="'.esc_attr($start).'"'
								. ' data-stop="'.esc_attr($stop).'"'
								. ' data-step="'.esc_attr($step).'"'
								. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
								. ' data-speed="'.esc_attr($speed).'"'
								. ' data-duration="'.esc_attr($animation).'"'
								. ' data-ed="'.esc_attr($ed).'">'
								. ($start) . ($ed)
							.'</div>';
				
				$output .= '<div class="sc_skills_count"' . ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $color ? ' style="background-color:' . esc_attr($color) . '; border-color:' . esc_attr($color) . '"' : '') . '>'
						. '</div>';
				
			} else if ($BESTDEALS_GLOBALS['sc_skills_type']=='pie') {
				if (empty($id)) $id = 'sc_skills_canvas_'.str_replace('.','',mt_rand());
				$output .= '<canvas id="'.esc_attr($id).'"></canvas>'
					. '<div class="sc_skills_total"'
						. ' data-start="'.esc_attr($start).'"'
						. ' data-stop="'.esc_attr($stop).'"'
						. ' data-step="'.esc_attr($step).'"'
						. ' data-steps="'.esc_attr($steps).'"'
						. ' data-max="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_max']).'"'
						. ' data-speed="'.esc_attr($speed).'"'
						. ' data-duration="'.esc_attr($animation).'"'
						. ' data-color="'.esc_attr($color).'"'
						. ' data-bg_color="'.esc_attr($bg_color).'"'
						. ' data-border_color="'.esc_attr($border_color).'"'
						. ' data-cutout="'.esc_attr($BESTDEALS_GLOBALS['sc_skills_pie_cutout']).'"'
						. ' data-easing="easeOutCirc"'
						. ' data-ed="'.esc_attr($ed).'">'
						. ($start) . ($ed)
					.'</div>';
			}
			$output .= 
					  ($BESTDEALS_GLOBALS['sc_skills_type']=='counter' ? $title_block : '')
					. '</div>'
					. ($BESTDEALS_GLOBALS['sc_skills_type']=='bar' && $BESTDEALS_GLOBALS['sc_skills_dir']=='vertical' || $BESTDEALS_GLOBALS['sc_skills_type'] == 'pie' ? $title_block : '')
					. ($BESTDEALS_GLOBALS['sc_skills_columns'] > 0 ? '</div>' : '');
		}
		return apply_filters('bestdeals_shortcode_output', $output, 'trx_skills_item', $atts, $content);
	}
	add_shortcode('trx_skills_item', 'bestdeals_sc_skills_item');
}
// ---------------------------------- [/trx_skills] ---------------------------------------



// Replace GAP wrapper in the content
if (!function_exists('bestdeals_gap_wrapper')) {
    function bestdeals_gap_wrapper($str) {
   	 // Move VC row and column and wrapper inside gap
   	 $str_new = preg_replace('/(<div\s+class="[^"]*vc_row[^>]*>)[\r\n\s]*(<div\s+class="[^"]*vc_col[^>]*>)[\r\n\s]*(<div\s+class="[^"]*wpb_wrapper[^>]*>)[\r\n\s]*('.bestdeals_gap_start().')/i', '\\4\\1\\2\\3', $str);
   	 if ($str_new != $str) $str_new = preg_replace('/('.bestdeals_gap_end().')[\r\n\s]*(<\/div>)[\r\n\s]*(<\/div>)[\r\n\s]*(<\/div>)/i', '\\2\\3\\4\\1', $str_new);
   	 // Gap layout
   	 return str_replace(
   			 array(
   				 bestdeals_gap_start(),
   				 bestdeals_gap_end()
   			 ),
   			 array(
   				 bestdeals_close_all_wrappers(false) . '<div class="sc_gap">',
   				 '</div>' . bestdeals_open_all_wrappers(false)
   			 ),
   			 $str_new
   		 );
    }
}